<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %> 
<%@taglib uri="http://www.sun.com/web/ui" prefix="ui" %>
<f:view>
  <ui:page>
    <ui:head title="Tips on Searching" />      
    <ui:body styleClass="HlpBdy">
      <ui:form id="tips">              
       <h2><ui:staticText text="#{JavaHelpBean.tipsTitle}" /></h2>

<p><ui:staticText text="#{JavaHelpBean.tipsImprove}" /></p>
<ul>
 <li><ui:staticText text="#{JavaHelpBean.tipsImprove1}" /></li>

 <li><ui:staticText text="#{JavaHelpBean.tipsImprove2}" /></li>
   
 <li><ui:staticText text="#{JavaHelpBean.tipsImprove3}" /></li>
   
 <li><ui:staticText text="#{JavaHelpBean.tipsImprove4}" /></li>
</ul>
<p>
<b><ui:staticText text="#{JavaHelpBean.tipsNote}" /></b> <ui:staticText text="#{JavaHelpBean.tipsNoteDetails}" />
</p>  
<p><ui:staticText text="#{JavaHelpBean.tipsSearch}" /></p>

<ul>
  <li><ui:staticText text="#{JavaHelpBean.tipsSearch1}" /></li>
  <li><ui:staticText text="#{JavaHelpBean.tipsSearch2}" /></li>
  <li><ui:staticText text="#{JavaHelpBean.tipsSearch3}" /></li>
  <li><ui:staticText text="#{JavaHelpBean.tipsSearch4}" /></li>
</ul>
      </ui:form>      
    </ui:body> 
  </ui:page>
</f:view>