/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ExtChartComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jsf;

import org.openesb.tools.extchart.jfchart.JFChartRenderer;
import org.openesb.tools.extchart.jfchart.data.DataAccess;
import org.openesb.tools.extpropertysheet.IExtPropertyGroupsBean;
import java.io.IOException;
import java.util.logging.Logger;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.el.ValueBinding;

/**
 *
 * @author rdwivedi
 */
public class ExtChartComponent extends UIComponentBase {
    
    private static Logger mLogger = Logger.getLogger(ExtChartComponent.class.getName());
    public static final String COMPONENT_TYPE = "org.openesb.tools.extchart.jsf.ExtChartComponent";
    
    public static final String COMPONENT_FAMILY = "org.openesb.tools.extchart.jsf";
    private IExtPropertyGroupsBean mPropGroups = null;
    private DataAccess mDA = null;
    
    
    /** Creates a new instance of ExtChartComponent */
    public ExtChartComponent() {
    }
    
    public String getFamily() {
        return COMPONENT_FAMILY;
    }
    JFChartRenderer rend = null;
    public void encodeBegin(FacesContext context) throws IOException {
         rend = new JFChartRenderer();
        rend.encodeBegin(context,this);
        
    }
    public void encodeEnd(FacesContext context) throws IOException {
        rend.encodeEnd(context);
    }
    
    
    
    public Object getPropertyGroups() {
        if(mPropGroups != null)
            return mPropGroups;
        
        ValueBinding vb = getValueBinding("propertyGroups");
        Object v = vb != null ? vb.getValue(getFacesContext()) : null;
        mLogger.info("obje" + v);
        return v != null ? v: null;
    }
    
    public void setPropertyGroups(Object pGs) {
        mLogger.info("Se>>>>>" + pGs.getClass().getName());
        this.mPropGroups = (IExtPropertyGroupsBean)pGs;
    }
    
    public Object getDataAccess() {
        if(mDA != null)
            return mDA;
        
        ValueBinding vb = getValueBinding("dataAccess");
        Object v = vb != null ? vb.getValue(getFacesContext()) : null;
        mLogger.info("obje" + v);
        return v != null ? v: null;
    }
    
    public void setDataAccess(Object da) {
        mLogger.info("Se>>>>>" + da.getClass().getName());
        this.mDA = (DataAccess)da;
    }
    
    
    
    public void decode(FacesContext context) {
        mLogger.info("Decoding begins");
        super.decode(context);
        mLogger.info("Decoding ends");
    }
    
    
    public void restoreState(FacesContext context, Object state) {
        super.restoreState(context,state);
        mLogger.info("restoreState begins state for some state" + state.getClass().getName());
        
    }
    
    
    
    public void processDecodes(FacesContext context) {
        super.processDecodes(context);
        mLogger.info("process Decoding begins");
    }
    
    
}
