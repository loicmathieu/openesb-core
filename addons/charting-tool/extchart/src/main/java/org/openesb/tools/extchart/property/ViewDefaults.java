/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ViewDefaults.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property;

import org.openesb.tools.extpropertysheet.IExtProperty;
import org.openesb.tools.extpropertysheet.IExtPropertyOption;
import org.openesb.tools.extpropertysheet.IExtPropertyOptions;
import org.openesb.tools.extpropertysheet.impl.ExtProperty;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyGroup;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyOptions;
import java.awt.Color;
import java.awt.Font;





/**
 * ViewDefaults.java
 *
 * ViewDefaults defines all methods to access defaults common to all view
 * objects.
 *
 * @author Chris Johnston
 * @modified by Rahul
 * @version :$Revision: 1.4 $
 */
public class ViewDefaults extends ExtPropertyGroup implements Constants  {

    /** the RCS id */
    private static final String RCS_ID =
        "$Id: ViewDefaults.java,v 1.4 2010/07/18 16:18:08 ksorokin Exp $";

    /** DEFAULT_DISPLAY_TITLE is used as a hard default. */
    public static final boolean DEFAULT_DISPLAY_TITLE = true;

    /** DEFAULT_TITLE is used as a hard default. */
    public static final String DEFAULT_TITLE = "Chart";

    /** DEFAULT_BACKGROUND_COLOR is used as a hard default. */
    public static final Color DEFAULT_BACKGROUND_COLOR = Color.WHITE;

    /** DEFAULT_TEXT_COLOR is used as a hard default. */
    public static final Color DEFAULT_TEXT_COLOR = Color.BLACK;

    /** DEFAULT_TITLE_FONT_SIZE is used as a hard default. */
    public static final int DEFAULT_TITLE_FONT_SIZE = 20;

    /** Default value for BorderVisible property */
    public static final boolean DEFAULT_BORDER_VISIBLE = false;

    /** DEFAULT_ALL_FIELDS_FONT_SIZE is used as a hard default. */
    public static final int DEFAULT_ALL_FIELDS_FONT_SIZE = 9;

    /** DEFAULT_TITLE_STYLE is used as a hard default. */
    public static final int DEFAULT_ALL_FIELDS_FONT_STYLE  = Font.PLAIN ;

    /** DEFAULT_TITLE_ALIGNMENT is used as a hard default. */
    public static final int DEFAULT_TITLE_ALIGNMENT = CENTER;

    /** DEFAULT_TITLE_STYLE is used as a hard default. */
    public static final int DEFAULT_TITLE_STYLE  = Font.BOLD + Font.ITALIC;

    /** DEFAULT_TITLE_FONT is used as a hard default. */
    public static final Font DEFAULT_TITLE_FONT =
        new Font("SansSerif", DEFAULT_TITLE_STYLE, DEFAULT_TITLE_FONT_SIZE);

    /** DEFAULT_NULL_REAL is used as a hard default. */
    //public static final String  DEFAULT_NULL_REAL              = "0.0";

    /** DEFAULT_NULL_INTEGRAL is used as a hard default. */
    //public static final String  DEFAULT_NULL_INTEGRAL          = "0";

    /** DEFAULT_NULL_STRING is used as a hard default. */
    //public static final String  DEFAULT_NULL_STRING            = "";

    /** DEFAULT_INTEGRAL_NUMBER_FORMAT is used as a hard default. */
    public static final int  DEFAULT_INTEGRAL_NUMBER_FORMAT = 0; //"##########0";

    /** DEFAULT_REAL_NUMBER_FORMAT is used as a hard default. */
    public static final int  DEFAULT_REAL_NUMBER_FORMAT     = 0; //"##########0.0";

    /** DEFAULT_FONT_SIZES is used for an initial list of font sizes. */
    public static final int[] DEFAULT_FONT_SIZES =
        {7, 8, 9, 10, 11, 12, 13, 14, 16, 18, 20, 22, 24, 28, 36, 48};

    /** Default refreshing frequency in seconds */
    public static final int DEFAULT_FREQUENCY = 60;

    /** SUGGESTED_INTEGRAL_NUMBER_FORMATS contains a list of suggested integral number display formats. */
    public static final String[] SUGGESTED_INTEGRAL_NUMBER_FORMATS = {
        "0",
        "#0",
        "##0",
        "#,##0",
        "#,###,##0",
        "#,###,###,##0"
    };

    private static final ExtPropertyOptions INTEGRAL_NUMBAR_FORMATS_OPTIONS = new ExtPropertyOptions(SUGGESTED_INTEGRAL_NUMBER_FORMATS,true);
    
    
    
    public static final String[] SUG_HORIZONTAL = {
        ""+LEFT,
        ""+RIGHT,
        ""+CENTER
    };
    public static final String[] SUG_HORIZONTAL_DISPLAY = {
        "LEFT",
        "RIGHT",
        "CENTER"     
                
    };
    
    public static final ExtPropertyOptions TITLE_ALIGN_OPTIONS = new ExtPropertyOptions(SUG_HORIZONTAL,SUG_HORIZONTAL_DISPLAY);
    /** SUGGESTED_REAL_NUMBER_FORMATS contains a list of suggested real number display formats. */
    public static final String[] SUGGESTED_REAL_NUMBER_FORMATS = {
        "$0",
        "0",
        "#,###,###,##0",
        "$0.00",
        "$#,##0.00",
        "$#,###,##0.00",
        "0.000",
        "#,##0.000",
        "#,###,##0.000"
    };
    
    
    private static final ExtPropertyOptions REAL_NUMBER_FORMATS_OPTIONS = new ExtPropertyOptions(SUGGESTED_REAL_NUMBER_FORMATS,true);
    

    /** MY_DEFAULT_KEYS contains the default keys for this class. */
    protected static final String[] MY_DEFAULT_KEYS = {
        DISPLAY_TITLE_KEY,
        TITLE_KEY,
        TITLE_FONT_SIZE_KEY,
        TITLE_FONT_KEY,
        TITLE_ALIGNMENT_KEY,
        TITLE_COLOR_KEY,
        TITLE_BACKGROUND_KEY,
        BACKGROUND_COLOR_KEY,
        FREQUENCY,
        TEXT_COLOR_KEY,
        //NULL_REAL_KEY,
        //NULL_INTEGRAL_KEY,
        //NULL_STRING_KEY,
       // INTEGRAL_NUMBER_FORMAT_KEY,
        REAL_NUMBER_FORMAT_KEY
    };
    
    
    
    
    
    
    
    /**
     */
    public ViewDefaults() {
        super();
        initDefault();
    }
    
    private void initDefault() {

        setDisplayTitle(DEFAULT_DISPLAY_TITLE);
        setTitle(DEFAULT_TITLE);
        setTitleFont(DEFAULT_TITLE_FONT);
        setTitleAlignment(DEFAULT_TITLE_ALIGNMENT);
        setBackgroundColor(DEFAULT_BACKGROUND_COLOR);
        setFrequency(DEFAULT_FREQUENCY);
        setTextColor(DEFAULT_TEXT_COLOR);
        //setNullReal(DEFAULT_NULL_REAL);
        //setNullIntegral(DEFAULT_NULL_INTEGRAL);
        //setNullString(DEFAULT_NULL_STRING);
        //setIntegralNumberFormat(DEFAULT_INTEGRAL_NUMBER_FORMAT);
        setRealNumberFormat(DEFAULT_REAL_NUMBER_FORMAT);

    }
    
    
    
    

    /**
     * @see com.stc.ebam.server.chart.engine.view.ViewDefaults.getKeys()
     */
    public String[] getKeys() {
        return MY_DEFAULT_KEYS;
    }

    
    /**
     * Sets if display title
     * @param displayTitle - true/false
     */
    public void setDisplayTitle(boolean displayTitle) {
        this.setProperty(ViewDefaults.DISPLAY_TITLE_KEY,  Boolean.valueOf(displayTitle));
    }
    /**
     * Returns if display title
     * @return - true/false
     */
    public boolean isDisplayTitle() {
        Boolean val = (Boolean)getPropertyValue(ViewDefaults.DISPLAY_TITLE_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        //return default
        return DEFAULT_DISPLAY_TITLE;
    }
    /**
     * Sets title
     * @param title - title
     */
    public void setTitle(String title) {
        this.setProperty(ViewDefaults.TITLE_KEY, title);
    }
    /**
     * Return title
     * @return - title
     */
    public String getTitle() {
        String val = (String) this.getPropertyValue(ViewDefaults.TITLE_KEY);
        if (val != null) {
            return val;
        }

        //return default
        return DEFAULT_TITLE;
    }
    /**
     * Sets title font
     * @param font - title font
     */
    public void setTitleFont(Font font) {
        this.setFont(font, TITLE_FONT_KEY);
    }
    /**
     * Returns title font
     * @return title font
     */
    public Font getTitleFont() {
        return this.getFont(TITLE_FONT_KEY);
    }
    /**
     * Sets title text color
     * @param color - color
     */
    public void setTitleColor(Color color) {
        this.setProperty(TITLE_COLOR_KEY, color);
    }
    /**
     * Returns title text color
     * @return  title text color
     */
    public Color getTitleColor() {
        return (Color) this.getPropertyValue(TITLE_COLOR_KEY);
    }
    /**
     * Sets title background color
     * @param color - color
     */
    public void setTitleBackground(Color color) {
        this.setProperty(TITLE_BACKGROUND_KEY, color);
    }
    /**
     * Returns title background color
     * @return title background color
     */
    public Color getTitleBackground() {
        return (Color) this.getPropertyValue(TITLE_BACKGROUND_KEY);
    }
    /**
     * Sets title alignment
     * @param alignment - title alignment
     */
    public void setTitleAlignment(int alignment) {
        this.setProperty(TITLE_ALIGNMENT_KEY, new Integer(alignment),TITLE_ALIGN_OPTIONS);
    }
    /**
     * Set refreshing frequency
     * @param frequency - frequency in seconds
     */
    public void setFrequency(int frequency) {
        this.setProperty(FREQUENCY, new Integer(frequency));
    }
    /**
     * Return refreshing frequency
     * @return frequency in seconds
     */
     public int getFrequency() {
        Integer val = (Integer) this.getPropertyValue(FREQUENCY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_FREQUENCY;
    }


    /**
     * Returns title alignment
     * @return - title alignment
     */
    public int getTitleAlignment() {
        Integer val = (Integer) this.getPropertyValue(TITLE_ALIGNMENT_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_TITLE_ALIGNMENT;
    }
    /**
     * Sets data limit
     * @param limit - limit
     */
    public void setDataLimit(int limit) {
        this.setProperty(DATA_LIMIT_KEY, new Integer(limit));
    }
    /**
     * Returns data limit
     * @return limit
     */
    public int getDataLimit() {
        Integer val = (Integer) this.getPropertyValue(DATA_LIMIT_KEY);
        if (val != null) {
            return val.intValue();
        }
        return Integer.MAX_VALUE;
    }
    /**
     * Returns if border is visible
     * @return true/false
     */
    public boolean isBorderVisible() {
        Boolean val = (Boolean) this.getPropertyValue(BORDER_VISIBLE_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_BORDER_VISIBLE;
    }
    /**
     * Sets border visible
     * @param visible - visible
     */
    public void setBorderVisible(boolean visible) {
        this.setProperty(BORDER_VISIBLE_KEY, new Boolean(visible));
    }
    /**
     * Returns border paint
     * @return border paint
     */
    public Color getBorderPaint() {
        Color val = (Color) this.getPropertyValue(BORDER_PAINT_KEY);
        return val;
    }
    /**
     * Sets border paint
     * @param color - border paint
     */
    public void setBorderPaint(Color color) {
        this.setProperty(BORDER_PAINT_KEY, color);
    }
    /**
     * Sets background color
     * @param color - background color
     */
    public void setBackgroundColor(Color color) {
        this.setProperty(BACKGROUND_COLOR_KEY, color);
    }
    /**
     * Returns background color
     * @return - background color
     */
    public Color getBackgroundColor() {
        Color val = (Color) this.getPropertyValue(BACKGROUND_COLOR_KEY);
        if (val != null) {
            return val;
        }
        return DEFAULT_BACKGROUND_COLOR;
    }
    /**
     * Sets text color
     * @param color - text color
     */
    public void setTextColor(Color color) {
        this.setProperty(TEXT_COLOR_KEY, color);
    }
    /**
     * Returns text color
     * @return - text color
     */
    public Color getTextColor() {
        Color val = (Color) this.getPropertyValue(TEXT_COLOR_KEY);
        if (val != null) {
            return val;
        }
        //return default
        return DEFAULT_TEXT_COLOR;
    }
    /**
     * Sets null real
     * @param nullReal - null real
     */
    /*
    public void setNullReal(String nullReal) {
        this.setProperty(NULL_REAL_KEY, nullReal);
    }
     **/
    /**
     * Returns null real
     * @return - null real
     */
    /*
    public String getNullReal() {
        String val = (String) this.getPropertyValue(NULL_REAL_KEY);
        if (val != null) {
            return val;
        }

        //return default
        return DEFAULT_NULL_REAL;
    }
     **/
    /**
     * Sets null integral
     * @param nullIntg - null integral
     */
    /*
    public void setNullIntegral(String nullIntg) {
        this.setProperty(NULL_INTEGRAL_KEY, nullIntg);
    }
     **/
    /**
     * Returns null integral
     * @return - null integral
     */
    /*
    public String getNullIntegral() {
        String val = (String) this.getPropertyValue(NULL_INTEGRAL_KEY);
        if (val != null) {
            return val;
        }

        //return default
        return DEFAULT_NULL_INTEGRAL;
    }
    /**
     * Sets null String
     * @param nullStr - null string
     */
    /*
    public void setNullString(String nullStr) {
        this.setProperty(ViewDefaults.NULL_STRING_KEY, nullStr);
    }
    /**
     * Returns null string
     * @return - null string
     */
    /*
    public String getNullString() {
        String val = (String) this.getPropertyValue(ViewDefaults.NULL_STRING_KEY);
        if (val != null) {
            return val;
        }

        //return default
        return DEFAULT_NULL_STRING;
    }
    /**
     * Sets integral number format
     * @param intNumFormat - integral number format
     */
   /* public void setIntegralNumberFormat(int intNumFormat) {
        setProperty(ViewDefaults.INTEGRAL_NUMBER_FORMAT_KEY, new Integer(intNumFormat),INTEGRAL_NUMBAR_FORMATS_OPTIONS);
    }
    */
    /**
     * Return integral number format
     * @return - integral number format
     */
    /*
    public String getIntegralNumberFormat() {
        Integer val = ((Integer) this.getPropertyValue(ViewDefaults.INTEGRAL_NUMBER_FORMAT_KEY));
        if (val != null && val.intValue() < SUGGESTED_INTEGRAL_NUMBER_FORMATS.length) {
            return SUGGESTED_INTEGRAL_NUMBER_FORMATS[val.intValue()];
        }
        
        //return default
        return SUGGESTED_INTEGRAL_NUMBER_FORMATS[0];
    }
     */
    /**
     * Sets real number format
     * @param rFormat - real number format
     */
    public void setRealNumberFormat(int rFormat) {
        setProperty(ViewDefaults.REAL_NUMBER_FORMAT_KEY, new Integer(rFormat),REAL_NUMBER_FORMATS_OPTIONS);
    }
    /**
     * Returns real number format
     * @return - real number format
     */
    public String getRealNumberFormat() {
        Integer val = ((Integer) this.getPropertyValue(ViewDefaults.REAL_NUMBER_FORMAT_KEY));
        if (val != null && val.intValue() < SUGGESTED_REAL_NUMBER_FORMATS.length) {
            return SUGGESTED_REAL_NUMBER_FORMATS[val.intValue()];
        }
        
        //return default
        return SUGGESTED_REAL_NUMBER_FORMATS[0];
        
        
        
    }
   

    
    /**
     * Return property template name
     * @return - template name
     */
    public String getPropertyTemplateName() {
        return null;
    }
    
    
    /**
     * Returns a font property
     * @param property - property name
     * @return font
     */
    protected Font getFont(String property) {
        Font val = (Font) this.getPropertyValue(property);
        if(val == null) {
            val = this.DEFAULT_TITLE_FONT;
        }
        return val;
    }
    /**
     * Set font property
     * @param font - font
     * @param property - property name
     */
    public void setFont(Font font, String property) {
        if (font != null) {
            setProperty(property + FONT_NAME_SUFFIX, font);
        }
    }
        
   
   
}
