/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XYProperties.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property.bar;

import org.openesb.tools.extchart.property.JFChartConstants;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyGroup;
import org.openesb.tools.extpropertysheet.impl.ExtPropertyOptions;





/**
 * XYProperties.java
 *
 * This class implements default management for xy-style charts.
 *
 * @author Chris Johnston
 * @version :$Revision: 1.4 $
 */

public class XYProperties extends ExtPropertyGroup {

    /** the RCS id */
    static final String RCS_ID =
    "$Id: XYProperties.java,v 1.4 2010/07/18 16:18:09 ksorokin Exp $";

/** HORIZONTAL is a logical constant for calls. */
public static final int HORIZONTAL                = 1;
/** VERTICAL is a logical constant for calls. */
public static final int VERTICAL                  = 0;

/** ORIENTATION_KEY is the key for the default orientation default. */
public static final String ORIENTATION_KEY       = "xd_orientation";

/** DOMAIN_LABEL_KEY is the key for the domain label default. */
public static final String DOMAIN_LABEL_KEY      = "xd_domainLabel";

/** SHOW_DOMAIN_LABEL_KEY is the key for the show domain label default. */
public static final String SHOW_DOMAIN_LABEL_KEY = "xd_showDomainLabel";

/** SHOW_RANGE_LABEL_KEY is the key for the range label default. */
public static final String RANGE_LABEL_KEY       = "xd_rangeLabel";

/** SHOW_RANGE_LABEL_KEY is the key for the show range label default. */
public static final String SHOW_RANGE_LABEL_KEY  = "xd_showRangeLabel";

/** DOMAIN_AXIS_KEY is the key for the domain axis label default. */
public static final String DOMAIN_AXIS_KEY       = "xd_domainAxis";

/** SHOW_DOMAIN_AXIS_KEY is the key for the show domain axis label default. */
public static final String SHOW_DOMAIN_AXIS_KEY  = "xd_showDomainAxis";

/** RANGE_AXIS_KEY is the key for the range axis label default. */
public static final String RANGE_AXIS_KEY        = "xd_rangeAxis";

/** SHOW_RANGE_AXIS_KEY is the key for the show range axis label default. */
public static final String SHOW_RANGE_AXIS_KEY   = "xd_showRangeAxis";

    /** DEFAULT_XY_CHART_TITLE is used as a hard default. */
    public static final String  DEFAULT_XY_CHART_TITLE    = "XY Chart";

    /** DEFAULT_ORIENTATION is used as a hard default. */
    public static final int DEFAULT_ORIENTATION       = VERTICAL;

    /** DEFAULT_SHOW_DOMAIN_LABEL is used as a hard default. */
    public static final boolean DEFAULT_SHOW_DOMAIN_LABEL = true;

    /** DEFAULT_DOMAIN_LABEL is used as a hard default. */
    public static final String  DEFAULT_DOMAIN_LABEL      = "domain";

    /** DEFAULT_SHOW_DOMAIN_AXIS is used as a hard default. */
    public static final boolean DEFAULT_SHOW_DOMAIN_AXIS  = true;

    /** DEFAULT_DOMAIN_AXIS is used as a hard default. */
    public static final String  DEFAULT_DOMAIN_AXIS       = "domainAxis";

    /** DEFAULT_SHOW_RANGE_LABEL is used as a hard default. */
    public static final boolean DEFAULT_SHOW_RANGE_LABEL  = true;

    /** DEFAULT_RANGE_LABEL is used as a hard default. */
    public static final String  DEFAULT_RANGE_LABEL       = "range";

    /** DEFAULT_SHOW_RANGE_AXIS is used as a hard default. */
    public static final boolean DEFAULT_SHOW_RANGE_AXIS   = true;

    /** DEFAULT_RANGE_AXIS is used as a hard default. */
    public static final String  DEFAULT_RANGE_AXIS        = "rangeAxis";
    
    
    
    public static final String[] OR_HORIZONTAL = {
        ""+VERTICAL,
        ""+HORIZONTAL
       
    };
    public static final String[] OR_HORIZONTAL_DISPLAY = {
        "VERTICAL",
        "HORIZONTAL"
         
                
    };
 private static final ExtPropertyOptions ORIENTATION_OPTIONS = new ExtPropertyOptions(OR_HORIZONTAL,OR_HORIZONTAL_DISPLAY);
    

    Object[] defaultObjects = {
        new Integer(DEFAULT_ORIENTATION),
        DEFAULT_DOMAIN_LABEL,
        Boolean.valueOf(DEFAULT_SHOW_DOMAIN_LABEL),
        DEFAULT_RANGE_LABEL,
        Boolean.valueOf(SHOW_RANGE_LABEL_KEY),
        DEFAULT_DOMAIN_AXIS,
        Boolean.valueOf(SHOW_DOMAIN_AXIS_KEY),
        DEFAULT_RANGE_AXIS,
        Boolean.valueOf(SHOW_RANGE_AXIS_KEY)
    };

    /** MY_DEFAULT_KEYS are the keys for this object in array form. */
    protected static final String[] MY_DEFAULT_KEYS = {
        ORIENTATION_KEY,
        DOMAIN_LABEL_KEY,
        SHOW_DOMAIN_LABEL_KEY,
        RANGE_LABEL_KEY,
        SHOW_RANGE_LABEL_KEY,
        DOMAIN_AXIS_KEY,
        SHOW_DOMAIN_AXIS_KEY,
        RANGE_AXIS_KEY,
        SHOW_RANGE_AXIS_KEY };

        static final String[] XY_CHART_KEYS;
        static {
            //String[] superKeys = ChartDefaults.MY_DEFAULT_KEYS;
            //String[] myKeys    = new String[superKeys.length + MY_DEFAULT_KEYS.length];
            String[] myKeys    = new String[ MY_DEFAULT_KEYS.length];
            for (int n = 0; n < myKeys.length; n++) {
               // if (n < superKeys.length) {
               //     myKeys[n] = superKeys[n];
              //  } else {
                    myKeys[n] = MY_DEFAULT_KEYS[n ];
               // }
            }
            XY_CHART_KEYS = myKeys;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.ViewDefaults.getKeys()
         */
        public String[] getKeys() {
            return XY_CHART_KEYS;
        }


        

        /**
         * The XYChartDefaults constructor is used to create an instance of this
         * class.
         */
        public XYProperties( ) {
            super( );
            initDefault();
            this.setGroupName(JFChartConstants.CHART_SPECIFIC_PROPERTIES);
        }
        
       
        /**
         * Init
         */
        public void initDefault() {
            setOrientation(DEFAULT_ORIENTATION);
            setShowDomainLabel(DEFAULT_SHOW_DOMAIN_LABEL);
            setDomainLabel(DEFAULT_DOMAIN_LABEL);
            setShowDomainAxis(DEFAULT_SHOW_DOMAIN_AXIS);
            setDomainAxis(DEFAULT_DOMAIN_AXIS);
            setShowRangeLabel(DEFAULT_SHOW_RANGE_LABEL);
            setRangeLabel(DEFAULT_RANGE_LABEL);
            setShowRangeAxis(DEFAULT_SHOW_RANGE_AXIS);
            setRangeAxis(DEFAULT_RANGE_AXIS);
        }
        /**
         * Sets orientation
         * @param orientation - orientation
         */
        public void setOrientation(int orientation) {
            setProperty(ORIENTATION_KEY, new Integer(orientation),ORIENTATION_OPTIONS);
        }
        /**
         * Returns orientation
         * @return - orientation
         */
        public int getOrientation() {
            Integer val = DEFAULT_ORIENTATION;
            if( this.getPropertyValue(ORIENTATION_KEY) instanceof String) {
                val = new Integer((String)this.getPropertyValue(ORIENTATION_KEY));
            } else {
                val = (Integer) this.getPropertyValue(ORIENTATION_KEY);
            }
            if (val != null) {
                return val.intValue();
            }

            //return default??
            return DEFAULT_ORIENTATION;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.isShowDomainLabel()
         */
        public boolean isShowDomainLabel() {
            Boolean val = (Boolean) this.getPropertyValue(SHOW_DOMAIN_LABEL_KEY);
            if (val != null) {
                return val.booleanValue();
            }

            //return default
            return DEFAULT_SHOW_DOMAIN_LABEL;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setShowDomainLabel()
         */
        public void setShowDomainLabel(boolean bShow) {
            setProperty(SHOW_DOMAIN_LABEL_KEY, Boolean.valueOf(bShow));
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.getDomainAxis()
         */
        public String getDomainLabel() {
            String val = (String) this.getPropertyValue(DOMAIN_LABEL_KEY);
            if (val != null) {
                return val;
            }

            //return default
            return DEFAULT_DOMAIN_LABEL;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setDomainLabel()
         */
        public void setDomainLabel(String newDomainLabel) {
            setProperty(DOMAIN_LABEL_KEY, newDomainLabel);
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.getDomainAxis()
         */
        public String getDomainAxis() {
            String val = (String) this.getPropertyValue(DOMAIN_AXIS_KEY);
            if (val != null) {
                return val;
            }

            //return default
            return DEFAULT_DOMAIN_AXIS;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setDomainAxis()
         */
        public void setDomainAxis(String newDomainAxis) {
            setProperty(DOMAIN_AXIS_KEY, newDomainAxis);
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.isShowDomainAxis()
         */
        public boolean isShowDomainAxis() {
            Boolean val = (Boolean) this.getPropertyValue(SHOW_DOMAIN_AXIS_KEY);
            if (val != null) {
                return val.booleanValue();
            }

            //return default
            return DEFAULT_SHOW_DOMAIN_AXIS;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setShowDomainAxis()
         */
        public void setShowDomainAxis(boolean bShow) {
            setProperty(SHOW_DOMAIN_AXIS_KEY, Boolean.valueOf(bShow));
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.isShowRangeLabel()
         */
        public boolean isShowRangeLabel() {
            Boolean val = (Boolean) this.getPropertyValue(SHOW_RANGE_LABEL_KEY);
            if (val != null) {
                return val.booleanValue();
            }

            //return default
            return DEFAULT_SHOW_RANGE_LABEL;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setShowRangeLabel()
         */
        public void setShowRangeLabel(boolean bShow) {
            setProperty(SHOW_RANGE_LABEL_KEY, Boolean.valueOf(bShow));
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.getRangeLabel()
         */
        public String getRangeLabel() {
            String val = (String) this.getPropertyValue(RANGE_LABEL_KEY);
            if (val != null) {
                return val;
            }

            //return default
            return this.DEFAULT_RANGE_LABEL;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setRangeLabel()
         */
        public void setRangeLabel(String newRangeLabel) {
            setProperty(RANGE_LABEL_KEY, newRangeLabel);
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.isShowRangeAxis()
         */
        public boolean isShowRangeAxis() {
            Boolean val = (Boolean) this.getPropertyValue(SHOW_RANGE_AXIS_KEY);
            if (val != null) {
                return val.booleanValue();
            }

            //return default
            return DEFAULT_SHOW_RANGE_AXIS;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setShowRangeAxis()
         */
        public void setShowRangeAxis(boolean bShow) {
            setProperty(SHOW_RANGE_AXIS_KEY, Boolean.valueOf(bShow));
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.getRangeAxis()
         */
        public String getRangeAxis() {
            String val = (String) this.getPropertyValue(RANGE_AXIS_KEY);
            if (val != null) {
                return val;
            }

            //return default
            return DEFAULT_RANGE_AXIS;
        }

        /**
         * @see com.stc.ebam.server.chart.engine.view.chart.xy.XYChartDefaults.setRangeAxis()
         */
        public void setRangeAxis(String newRangeAxis) {
            setProperty(RANGE_AXIS_KEY, newRangeAxis);
        }

        
        /**
         * toString
         * @return - string
         */
        public String toString() {
            return "XY Chart";
        }
        
      public String getPropertyTemplateName() {
        return "XYChart";
    }


}
