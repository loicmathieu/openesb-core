/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MeterProperties.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.property.meter;


import org.openesb.tools.extpropertysheet.impl.ExtPropertyGroup;
import java.awt.Color;
import java.awt.Font;





/**
 *
 * @author Wei Han
 * @version :$Revision: 1.4 $
 */
public class MeterProperties extends ExtPropertyGroup {
    /** Key for maximum value */
    public static final String MAXINUM_VALUE_KEY = "max-value";

    /** Key for minimum value */
    public static final String MINIMUM_VALUE_KEY = "min-value";

    /** Key for maximum critical value */
    public static final String MAXINUM_CRITICAL_VALUE_KEY = "max-critical-value";

    /** Key for minimum critical value */
    public static final String MINIMUM_CRITICAL_VALUE_KEY = "min-critical-value";

    /** Key for maximum normal value */
    public static final String MAXINUM_NORMAL_VALUE_KEY = "max-normal-value";

    /** Key for minimum normal value */
    public static final String MINIMUM_NORMAL_VALUE_KEY = "min-normal-value";

    /** Key for maximum warning value */
    public static final String MAXINUM_WARNING_VALUE_KEY = "max-warning-value";

    /** Key for minimum warning value */
    public static final String MINIMUM_WARNING_VALUE_KEY = "min-warning-value";

    /** Key for units */
    public static final String UNITS_KEY = "units";

    /** Key for border type */
    public static final String BORDER_TYPE_KEY = "border-type";
    /** Key for dial type */
    public static final String DIAL_TYPE_KEY = "dial-type";
    /** Key for draw border flag */
    public static final String DRAW_BORDER_KEY = "draw-border";
    /** Key for draw border flag */
    public static final String TICK_LABEL_TYPE_KEY = "tick-label-type";
    /** Key for critical paint */
    public static final String CRITICAL_PAINT_KEY = "critical-paint";
    /** Key for normal paint */
    public static final String NORMAL_PAINT_KEY = "normal-paint";
    /** Key for warning paint */
    public static final String WARNING_PAINT_KEY = "warning-paint";
    /** Key for dial border color */
    public static final String DIAL_BORDER_COLOR_KEY = "dial-border-color";
    /** Key for dial background paint */
    public static final String DIAL_BACKGROUND_PAINT_KEY = "dial-background-paint";
    /** Key for needle paint */
    public static final String NEEDLE_PAINT_KEY = "needle-paint";
    /** Key for value paint */
    public static final String VALUE_PAINT_KEY = "value-paint";
    /** Key for ValueFont property */
    public static final String VALUE_FONT_KEY = "value-font";
    /** Key for legend text */
    public static final String LEGEND_TEXT_KEY = "legend-text";
    /** Key for tick label font name */
    public static final String TICK_LABEL_FONT_KEY = "tick-label-font";
    /** Key for meter angle property */
    public static final String METER_ANGLE_KEY = "meter-angle";

    /** Circle dial type */
    public static final int DIALTYPE_CIRCLE = 0;
    /** Pie dial type */
    public static final int DIALTYPE_PIE = 1;
    /** Chord dial type */
    public static final int DIALTYPE_CHORD = 2;
    /** No tick label */
    public static final int TICK_NO_LABELS = 0;
    /** Value tick label */
    public static final int TICK_VALUE_LABELS = 1;
    /** Border type - normal */
    public static final int BORDER_TYPE_NORMAL = 0;
    /** Border type - warning */
    public static final int BORDER_TYPE_WARNING = 1;
    /** Border type - critical */
    public static final int BORDER_TYPE_CRITICAL = 2;
    /** Border type - full */
    public static final int BORDER_TYPE_FULL = 3;

    /** XML tag */
    public static final String METER_TAG = "meter-defaults";

    /** Default max value */
    public static final double DEFAULT_MAX_VALUE = 3;

    /** Default min value */
    public static final double DEFAULT_MIN_VALUE = 0;

    /** Default max critical value */
    public static final double DEFAULT_MAX_CRITICAL_VALUE = 3;

    /** Default min critical value */
    public static final double DEFAULT_MIN_CRITICAL_VALUE = 2;

    /** Default max normal value */
    public static final double DEFAULT_MAX_NORMAL_VALUE = 1;

    /** Default min normal value */
    public static final double DEFAULT_MIN_NORMAL_VALUE = 0;

    /** Default max warning value */
    public static final double DEFAULT_MAX_WARNING_VALUE = 2;

    /** Default min warning value */
    public static final double DEFAULT_MIN_WARNING_VALUE = 1;

    /** Default units value */
    public static final String DEFAULT_UNITS = "units";

    /** Default border type */
    public static final int DEFAULT_BORDER_TYPE = BORDER_TYPE_NORMAL;

    /** Default meter angle */
  //  public static final int DEFAULT_METER_ANGLE = MeterPlot.DEFAULT_METER_ANGLE;

    /** Default dial type */
    public static final int DEFAULT_DIAL_TYPE = DIALTYPE_CIRCLE;

    /** Default tick label type */
    public static final int DEFAULT_TICK_LABEL_TYPE = TICK_VALUE_LABELS;

    /** Default draw border flag */
    public static final boolean DEFAULT_DRAW_BORDER = false;

    /** Default critical paint color */
    public static final Color DEFAULT_CRITICAL_PAINT = Color.RED;
    
    public static final Color DEFAULT_NORMAL_PAINT = Color.GREEN;
    
    
    public static final Color DEFAULT_WARNING_PAINT = Color.YELLOW;
    
    public static final Color DEFAULT_DIAL_BORDER_COLOR = Color.BLACK;
    public static final Color DEFAULT_DIAL_BACKGROUND_COLOR = Color.WHITE;
    public static final Color DEFAULT_NEEDLE_COLOR = Color.BLACK;
    public static final Color DEFAULT_VALUE_COLOR = Color.BLACK;
    private static final int DEFAULT_ALL_FIELDS_FONT_STYLE  = Font.PLAIN ;
    private static final int DEFAULT_ALL_FIELDS_FONT_SIZE = 9;
    
    


    /** Default legend text */
    public static final String DEFAULT_LEGEND_TEXT = "Meter";
    
    
    public static final Font DEFAULT_VALUE_FONT =
        new Font("SansSerif", DEFAULT_ALL_FIELDS_FONT_STYLE, DEFAULT_ALL_FIELDS_FONT_SIZE);

    /**
     * New instance
     */
    public MeterProperties( ) {
        super();
        
            initDefaults();
        
    }
     
    /**
     * Init
     */
    public void initDefaults() {
        setValueFont(DEFAULT_VALUE_FONT);
        setTickLabelFont(DEFAULT_VALUE_FONT);
        setCriticalPaint(DEFAULT_CRITICAL_PAINT);
        setMaximumCriticalValue(DEFAULT_MAX_CRITICAL_VALUE);
        setMinimumCriticalValue(DEFAULT_MIN_CRITICAL_VALUE);
        setMaximumNormalValue(DEFAULT_MAX_NORMAL_VALUE);
        setMinimumNormalValue(DEFAULT_MIN_NORMAL_VALUE);
        setMaximumWarningValue(DEFAULT_MAX_WARNING_VALUE);
        setMinimumWarningValue(DEFAULT_MIN_WARNING_VALUE);
        setUnits(DEFAULT_UNITS);
        setValueFont(DEFAULT_VALUE_FONT);
        setMaximumValue(DEFAULT_MAX_VALUE);
        setMinimumValue(DEFAULT_MIN_VALUE);
        // setPieChartDefaults
    }

   
    /**
     * Returns the maximum value.
     * @return - the maximum value
     */
    public double getMaximumValue() {
        Double val = (Double) this.getPropertyValue(MAXINUM_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MAX_VALUE;
    }

    /**
     * Sets the maximum value.
     * @param value - the maximum value.
     */
    public void setMaximumValue(double value) {
    	
    		setProperty(MAXINUM_VALUE_KEY, new Double(value));
    	
    }

    /**
     * Returns the minimum value.
     * @return - the minimum value
     */
    public double getMinimumValue() {
        Double val = (Double) this.getPropertyValue(MINIMUM_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MIN_VALUE;
    }

    /**
     * Sets the minimum value.
     * @param value - the minimum value.
     */
    public void setMinimumValue(double value){
    	
        setProperty(MINIMUM_VALUE_KEY, new Double(value));
    	
    }

    /**
     * Returns the maximum critical value.
     * @return - the maximum critical value
     */
    public double getMaximumCriticalValue() {
        Double val = (Double) this.getPropertyValue(MAXINUM_CRITICAL_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MAX_CRITICAL_VALUE;
    }

    /**
     * Sets the maximum critical value.
     * @param value - the maximum critical value.
     */
    public void setMaximumCriticalValue(double value) {
    	
    		setProperty(MAXINUM_CRITICAL_VALUE_KEY, new Double(value));
    	
    }

    /**
     * Returns the minimum critical value.
     * @return - the minimum critical value
     */
    public double getMinimumCriticalValue() {
        Double val = (Double) this.getPropertyValue(MINIMUM_CRITICAL_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MIN_CRITICAL_VALUE;
    }

    /**
     * Sets the minimum critical value.
     * @param value - the minimum critical value.
     */
    public void setMinimumCriticalValue(double value){
    	
        setProperty(MINIMUM_CRITICAL_VALUE_KEY, new Double(value));
    }

    /**
     * Returns the maximum normal value.
     * @return - the maximum normal value
     */
    public double getMaximumNormalValue() {
        Double val = (Double) this.getPropertyValue(MAXINUM_NORMAL_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MAX_NORMAL_VALUE;
    }

    /**
     * Sets the maximum normal value.
     * @param value - the maximum normal value.
     */
    public void setMaximumNormalValue(double value){
    	
        setProperty(MAXINUM_NORMAL_VALUE_KEY, new Double(value));
    }

    /**
     * Returns the minimum normal value.
     * @return - the minimum normal value
     */
    public double getMinimumNormalValue() {
        Double val = (Double) this.getPropertyValue(MINIMUM_NORMAL_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MIN_NORMAL_VALUE;
    }

    /**
     * Sets the minimum normal value.
     * @param value - the minimum normal value.
     */
    public void setMinimumNormalValue(double value){
    	
    		setProperty(MINIMUM_NORMAL_VALUE_KEY, new Double(value));
    	
    }

    /**
     * Returns the maximum warning value.
     * @return - the maximum warning value
     */
    public double getMaximumWarningValue() {
        Double val = (Double) this.getPropertyValue(MAXINUM_WARNING_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MAX_WARNING_VALUE;
    }

    /**
     * Sets the maximum warning value.
     * @param value - the maximum warning value.
     */
    public void setMaximumWarningValue(double value){
    	
        setProperty(MAXINUM_WARNING_VALUE_KEY, new Double(value));
    }

    /**
     * Returns the minimum warning value.
     * @return - the minimum warning value
     */
    public double getMinimumWarningValue() {
        Double val = (Double) this.getPropertyValue(MINIMUM_WARNING_VALUE_KEY);
        if (val != null) {
            return val.doubleValue();
        }
        return DEFAULT_MIN_WARNING_VALUE;
    }

    /**
     * Sets the minimum warning value.
     * @param value - the minimum warning value.
     */
    public void setMinimumWarningValue(double value){
    	
        setProperty(MINIMUM_WARNING_VALUE_KEY, new Double(value));
    }

    /**
     * Returns the units
     * @return - the units
     */
    public String getUnits() {
        String val = (String) this.getPropertyValue(UNITS_KEY);
        if (val != null) {
            return val;
        }
        return DEFAULT_UNITS;
    }

    /**
     * Sets the units
     * @param units - the units
     */
    public void setUnits(String units) {
        setProperty(UNITS_KEY, units);
    }

    /**
     * Returns the paint used to display the 'critical' range
     * @return - The paint
     */
    public Color getCriticalPaint() {
        Color val = (Color) this.getPropertyValue(CRITICAL_PAINT_KEY);
        if (val == null) {
            val = DEFAULT_CRITICAL_PAINT;
        }
        return val;
    }
    /**
     * Sets the paint used to display the 'critical' range.
     * @param paint - the paint
     */
    public void setCriticalPaint(Color paint) {
        if (paint != null) {
            setProperty(CRITICAL_PAINT_KEY, paint);
        }
    }
    /**
     * Returns the paint used to display the 'normal' range
     * @return - The paint
     */
    public Color getNormalPaint() {
        Color val = (Color) this.getPropertyValue(NORMAL_PAINT_KEY);
        if (val == null) {
            val = DEFAULT_NORMAL_PAINT;
        }
        return val;
    }
    /**
     * Sets the paint used to display the 'normal' range.
     * @param paint - the paint
     */
    public void setNormalPaint(Color paint) {
        if (paint != null) {
            setProperty(NORMAL_PAINT_KEY, paint);
        }
    }
    /**
     * Returns the paint used to display the 'warning' range
     * @return - The paint
     */
    public Color getWarningPaint() {
        Color val = (Color) this.getPropertyValue(WARNING_PAINT_KEY);
        if (val == null) {
            val = DEFAULT_WARNING_PAINT;
        }
        return val;
    }
    /**
     * Sets the paint used to display the 'warning' range.
     * @param paint - the paint
     */
    public void setWarningPaint(Color paint) {
        if (paint != null) {
            setProperty(WARNING_PAINT_KEY, paint);
        } 
    }
    /**
     * Returns the paint for the dial background.
     * @return the paint
     */
    public Color getDialBackgroundPaint() {
        Color val = (Color) this.getPropertyValue(DIAL_BACKGROUND_PAINT_KEY);
        if (val == null) {
            val = DEFAULT_DIAL_BACKGROUND_COLOR;
        }
        
        return val;
    }
    /**
     * Sets the paint used to fill the dial background
     * @param paint - the paint
     */
    public void setDialBackgroundPaint(Color paint) {
        if (paint != null) {
            setProperty(DIAL_BACKGROUND_PAINT_KEY, paint);
        }
    }
    /**
     * Returns the color of the border for the dial.
     * @return the color of the border for the dial.
     */
    public Color getDialBorderColor() {
        Color val = (Color) this.getPropertyValue(DIAL_BORDER_COLOR_KEY);
        if (val == null) {
            val = DEFAULT_DIAL_BORDER_COLOR;
        }
        return val;
    }
    /**
     * Sets the color for the border of the dial.
     * @param color - the color
     */
    public void setDialBorderColor(Color color) {
        if (color != null) {
            setProperty(DIAL_BORDER_COLOR_KEY, color);
        }
    }
    /**
     * Returns meter angle
     * @return meter angle
     */
 /**   public int getMeterAngle() {
        Integer val = (Integer) this.getPropertyValue(METER_ANGLE_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_METER_ANGLE;
    }
  */
    /**
     * Sets meter angle
     * @param angle - meter angle
     */
    public void setMeterAngle(int angle) {
        
        setProperty(METER_ANGLE_KEY, new Integer(angle));
    }
    /**
     * Returns the paint for the needle.
     * @return the paint
     */
    public Color getNeedlePaint() {
        Color val = (Color) this.getPropertyValue(NEEDLE_PAINT_KEY);
        if (val == null) {
            val = DEFAULT_NEEDLE_COLOR;
        }
        return val;
    }
    /**
     * Sets the paint used by the needle
     * @param paint - the paint
     */
    public void setNeedlePaint(Color paint) {
        if (paint != null) {
            setProperty(NEEDLE_PAINT_KEY, paint);
        }
    }
    /**
     * Returns value font
     * @return value font
     */
    public Font getValueFont() {
        Font f = (Font)this.getPropertyValue(VALUE_FONT_KEY);
        if( f== null){
            f = DEFAULT_VALUE_FONT;
        }
        return f;
         
    }
    /**
     * Sets value font
     * @param font - value font
     */
    public void setValueFont(Font font) {
        setProperty( VALUE_FONT_KEY,font);
    }
    /**
     * Returns the paint for the value.
     * @return the paint
     */
    public Color getValuePaint() {
        Color val = (Color) this.getPropertyValue(VALUE_PAINT_KEY);
        if (val == null) {
            val = DEFAULT_VALUE_COLOR;
        }
        return val;
    }
    /**
     * Sets the paint used by the value
     * @param paint - the paint
     */
    public void setValuePaint(Color paint) {
        if (paint != null) {
            setProperty(VALUE_PAINT_KEY, paint);
        }
    }
    /**
     * Returns the border type
     * @return - the border type
     */
    public int getBorderType() {
        Integer val = (Integer) this.getPropertyValue(BORDER_TYPE_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_BORDER_TYPE;
    }

    /**
     * Sets the border type
     * @param borderType - the border type
     */
    public void setBorderType(int borderType) {
        setProperty(BORDER_TYPE_KEY, new Integer(borderType));
    }

    /**
     * Returns the type of dial (DIALTYPE_PIE, DIALTYPE_CIRCLE, DIALTYPE_CHORD).
     * @return - the dial type
     */
    public int getDialType() {
        Integer val = (Integer) this.getPropertyValue(DIAL_TYPE_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_DIAL_TYPE;
    }
    /**
     * Sets the dial type (background shape)
     * This controls the shape of the dial background. Use one of the constants:
     * DIALTYPE_PIE, DIALTYPE_CIRCLE, or DIALTYPE_CHORD.
     * @param type - dial type
     */
    public void setDialType(int type) {
        setProperty(DIAL_TYPE_KEY, new Integer(type));
    }
    /**
     * Returns the tick label type. Defined by the constants: NO_LABELS, VALUE_LABELS
     * @return - The tick label type.
     */
    public int getTickLabelType() {
        Integer val = (Integer) this.getPropertyValue(TICK_LABEL_TYPE_KEY);
        if (val != null) {
            return val.intValue();
        }
        return DEFAULT_TICK_LABEL_TYPE;
    }
    /**
     * Sets the tick label type
     * @param type - the type of tick labels - either NO_LABELS or VALUE_LABELS
     */
    public void setTickLabelType(int type) {
        setProperty(TICK_LABEL_TYPE_KEY, new Integer(type));
    }
    /**
     * Returns tick label font
     * @return tick label font
     */
    public Font getTickLabelFont() {
        Font f =(Font) this.getPropertyValue(TICK_LABEL_FONT_KEY);
        if( f== null){
            f = DEFAULT_VALUE_FONT;
        }
        return f;
    }
    /**
     * Sets tick label font
     * @param font - tick label font
     */
    public void setTickLabelFont(Font font) {
        setProperty(TICK_LABEL_FONT_KEY,font);
    }
    /**
     * Returns a flag that controls whether or not a rectangular border is
     * drawn around the plot area.
     * @return - a flag
     */
    public boolean isDrawBorder() {
        Boolean val = (Boolean) this.getPropertyValue(DRAW_BORDER_KEY);
        if (val != null) {
            return val.booleanValue();
        }
        return DEFAULT_DRAW_BORDER;
    }
    /**
     * Sets the flag that controls whether or not a rectangular border is drawn around the plot area.
     * Note: it looks like the true setting needs some work to provide some insets
     * @param draw - The flag
     */
    public void setDrawBorder(boolean draw) {
        setProperty(DRAW_BORDER_KEY, new Boolean(draw));
    }
    /**
     * Returns legend text
     * @return legend text
     */
    public String getLegendText() {
        String val = (String) this.getPropertyValue(LEGEND_TEXT_KEY);
        if (val != null) {
            return val;
        }
        return DEFAULT_LEGEND_TEXT;
    }
    /**
     * Sets legend text
     * @param text - legend text
     */
    public void setLegendText(String text) {
        setProperty(LEGEND_TEXT_KEY, text);
    }
    /**
     * toString
     * @return - string
     */
    public String toString() {
        return "Meter Chart";
    }

    /**
     * Returns property template name
     * @return - property template name
     */
    public String getPropertyTemplateName() {
        return "MeterChart";
    }

   
    
}
