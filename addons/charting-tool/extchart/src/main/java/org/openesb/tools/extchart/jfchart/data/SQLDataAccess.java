/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SQLDataAccess.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package org.openesb.tools.extchart.jfchart.data;

import org.openesb.tools.extchart.exception.ChartException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.jfree.data.general.Dataset;

/**
 *
 * @author rdwivedi
 */
public class SQLDataAccess extends DataAccess {
    
    String mQuery = null;
    String mJNDIName = null;
    private static Logger mLogger = Logger.getLogger("org.openesb.tools.extchart.jfchart.data.SQLDataAccess");
    /** Creates a new instance of SQLDataAccess */
    public SQLDataAccess(String jndiName, String dataSetType , String query) {
        super(dataSetType);
        mQuery = query;
        mJNDIName = jndiName;
        
    }
    
     public  Dataset getDataSet() throws ChartException{
         Dataset set = null;
         DataSetCreator creator = DataSetCreator.create(getDatasetType());
         Connection con = null;
         Statement stmt = null;
        ResultSet rset = null;
         try {
            con = getConnectionJNDI();
            stmt = con.createStatement ();
            rset = stmt.executeQuery(mQuery);
            creator.processResultSet(rset);
            set = creator.getDefaultDataSet();
            rset.close();
            stmt.close();
        
            } catch (SQLException e) {
                throw new ChartException(e);
        
            } finally {
            if (con != null) {
                try {
                    con.close();
                } catch (SQLException e) {
                    // do nothing.
                }
            }

        }
         
         return set;
     }
     
     private Connection getConnectionJNDI() throws SQLException {
       try {
        InitialContext ctx = new InitialContext();
        DataSource ds = (DataSource)
        ctx.lookup(mJNDIName);
        Connection con = ds.getConnection();
        return con;
       } catch(NamingException e){
           mLogger.log(Level.SEVERE, "Naming Exception:" + e.toString());
       }
       return null;
   }

    
}
