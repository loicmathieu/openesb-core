<jsp:root version="1.2" xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html" xmlns:jsp="http://java.sun.com/JSP/Page" xmlns:webuijsf="http://www.sun.com/web/ui">
<jsp:directive.page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"/>

<f:view>

  <webuijsf:page id="page1">
    <webuijsf:html id="html1" >
      
     
        <webuijsf:head id="head1" title="{msgs.masthead_title}">
             <jsp:directive.include file="informationPage.jsp"/>
        </webuijsf:head>
       
        <webuijsf:form id="treeForm">
        <webuijsf:tree id="tree" url="#">
        
            <f:facet name="content">
                <h:panelGroup>
                    <webuijsf:imageHyperlink   id="add0" imageURL="/images/chart.png"  hspace="3" toolTip="Add a new data access"   action="#{ChartTreeBean.addNewDataAccessNode}"/>
                    <webuijsf:imageHyperlink   id="add" imageURL="/images/chart.png"  hspace="3" toolTip="Add a new chart"   action="#{ChartTreeBean.addNewChartNode}"/>
                    <webuijsf:imageHyperlink id="add1" imageURL="/images/Charts_group.png"  hspace="3" toolTip="Add a chart group" action="#{ChartTreeBean.addNewChartParentNode}"/>
                    <webuijsf:imageHyperlink id="add2" imageURL="/images/remove.png"  hspace="3" toolTip="Remove Node" action="#{ChartTreeBean.removeSelectedNode}"/>
                    <webuijsf:imageHyperlink id="add3" imageURL="/images/add.png"  hspace="3" toolTip="Rename node" action="#{ChartTreeBean.renameSelectedNode}"/>
                    
                </h:panelGroup>
            </f:facet>
    
            <webuijsf:treeNode  id="rootNode"  binding="#{ChartTreeBean.rootNode}"   />  
            
       </webuijsf:tree>   
            
       </webuijsf:form>     
  </webuijsf:html>
  </webuijsf:page>

</f:view>
</jsp:root>
