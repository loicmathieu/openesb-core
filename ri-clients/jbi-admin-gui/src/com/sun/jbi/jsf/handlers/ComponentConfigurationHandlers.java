/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
package com.sun.jbi.jsf.handlers;

import com.sun.data.provider.FieldKey;
import com.sun.data.provider.RowKey;
import com.sun.data.provider.impl.ObjectListDataProvider;
import com.sun.enterprise.tools.admingui.handlers.ConfigurationHandlers;
import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.jbi.jsf.bean.AlertBean;
import com.sun.jbi.jsf.bean.CompConfigBean;
import com.sun.jbi.jsf.bean.ConfigProperty;
import com.sun.jbi.jsf.bean.ConfigPropertyGroup;
import com.sun.jbi.jsf.bean.ShowBean;
import com.sun.jbi.jsf.util.AlertUtilities;
import com.sun.jbi.jsf.util.AppVarUtils;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.ClusterUtilities;
import com.sun.jbi.jsf.util.CompConfigUtils;
import com.sun.jbi.jsf.util.ESBConstants;
import com.sun.jbi.jsf.util.I18nUtilities;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JBIUtils;
import com.sun.jbi.jsf.util.JSFUtils;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jsftemplating.annotation.Handler;
import com.sun.jsftemplating.annotation.HandlerInput;
import com.sun.jsftemplating.annotation.HandlerOutput;
import com.sun.jsftemplating.layout.descriptors.handler.HandlerContext;
import com.sun.webui.jsf.component.PropertySheet;
import com.sun.webui.jsf.component.TableRowGroup;
import com.sun.webui.jsf.model.Option;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides jsftemplating handlers for Component Configuration setting
 *
 * @author   Sun Microsystems Inc.
 */
public final class ComponentConfigurationHandlers
{

    /**
     *  <p> This handler commits the changes to a <code>TableRowGroup</code>'s
     *      DataProvider.</p>
     *
     *  @param handlerCtx The HandlerContext.
     */
    @Handler(id="jbiCommitTableRowGroup",
             input={
                 @HandlerInput(name="tableRowGroup", type=TableRowGroup.class, required=true)})
    public static void jbiCommitTableRowGroup(HandlerContext handlerCtx) {
        TableRowGroup trg =
            (TableRowGroup) handlerCtx.getInputValue("tableRowGroup");
        
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiCommitTableRowGroup() trg=" + 
                  trg);
        }
        ObjectListDataProvider dp =
            (ObjectListDataProvider) trg.getSourceData();
        dp.commitChanges();
    }
    

    /**
     *  <p> This handler deletes the given <code>RowKey</code>s.</p>
     *
     *  @param handlerCtx The HandlerContext.
     */
    @Handler(id="jbiDeleteTableRows",
             input={
                 @HandlerInput(name="tableRowGroup", type=TableRowGroup.class, required=true),
                 @HandlerInput(name="rowKeys", type=RowKey[].class, required=true)})
    public static void jbiDeleteTableRows(HandlerContext handlerCtx) {
        TableRowGroup trg =
            (TableRowGroup) handlerCtx.getInputValue("tableRowGroup");
        RowKey[] keys = (RowKey []) handlerCtx.getInputValue("rowKeys");
        
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiDeleteTableRows() = " + keys);
        }
        
        ObjectListDataProvider dp =
            (ObjectListDataProvider) trg.getSourceData();
        for (RowKey key : keys) {
            dp.removeRow(key);
            
            if (sLog.isLoggable(Level.FINER)) {
               sLog.finer("ComponentConfigurationHandlers.jbiDeleteTableRows Row with id = " + 
                      key.getRowId()); 
           }
        }
    }

    /**
     *  <p> This handler returns the selected row keys.</p>
     *
     *  @param handlerCtx The HandlerContext.
     */
    @Handler(id="jbiGetSelectedTableRowKeys",
             input={
                 @HandlerInput(name="tableRowGroup", type=TableRowGroup.class, required=true)},
             output={
                 @HandlerOutput(name="rowKeys", type=RowKey[].class)})
    public static void jbiGetSelectedTableRowKeys(HandlerContext handlerCtx) {
        TableRowGroup trg =
            (TableRowGroup) handlerCtx.getInputValue("tableRowGroup");
        RowKey[] keys = trg.getSelectedRowKeys();
        
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiGetSelectedTableRowKeys() keys=" + 
                  keys);
        }
        
        handlerCtx.setOutputValue("rowKeys", keys);
    }


    /**
     * For a given component and instance aves changes to the 
     * <code>CompConfigBean</code> configuration property sheet of the 
     * requested configuration type
     *
     * <p>Input value: "compName" -- Type: <code> java.lang.String</code> the
     * requested component name</p> 
     * <p>Input value: "compType" -- Type: <code> java.lang.String</code> the
     * requested component type</p> 
     * <p>Input value: "configType" -- Type: <code> java.lang.String</code> the
     * requested configuration data type, one of: <code>config-type-comp-app</code>,
     * or <code>config-type-comp-runtime</code></p> 
     * <p>Input value: "currSelectedInstance" -- Type: <code> java.lang.String</code>
     * the currently selected instance name</p>
     * <p>Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * true if alert should be displayed</p> 
     * <p>Output value: "alertSummary" -- Type: <code>String</code> alert
     * summary, if neeed</p> 
     * <p>Output value: "alertDetails" -- Type: <code>String</code> alert
     * details, if needed</p>
     *
     * @param aHandlerCtx  Description of Parameter
     */
    @Handler(id="jbiSaveComponentConfiguration",
             input={
                 @HandlerInput(name="compName", type=String.class, required=true),
                 @HandlerInput(name="compType", type=String.class, required=true),
                 @HandlerInput(name="appConfigName", type=String.class, required=false),
                 @HandlerInput(name="configType", type=String.class, required=true),
                 @HandlerInput(name="currSelectedInstance", type=String.class, 
                               required=true),
                 @HandlerInput(name="redirectOnFailure", type=String.class,
                               required=false),
                 @HandlerInput(name="redirectOnSuccess", type=String.class,
                               required=false)
             },
             output={
                 @HandlerOutput(name="alertDetails", type=String.class),
                 @HandlerOutput(name="alertSummary", type=String.class),
                 @HandlerOutput(name="isAlertNeeded", type=Boolean.class),
                 @HandlerOutput(name="redirectTo", type=String.class) 
             })

        public static void jbiSaveComponentConfiguration(HandlerContext aHandlerCtx)
    {
        String configType = (String) aHandlerCtx.getInputValue("configType");

        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aHandlerCtx,
                        configType,
                    };
                JBILogger.entering(CN, MN_JBI_SAVE_COMPONENT_CONFIGURATION, 
                                   args);
            }


        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiSaveCompConfigData(" +
                  aHandlerCtx + ")" +
                  ", configType=" + configType);
        }

        if (ESBConstants.CONFIG_TYPE_COMP_APP.equals(configType))
            {
                saveCompAppConfig(aHandlerCtx);
            }
        else if (ESBConstants.CONFIG_TYPE_COMP_RUNTIME.equals(configType))
            {
                saveCompRuntimeConfig(aHandlerCtx);
            }
        else
            {
                // configType="installation" handled in InstallationHandlers
            }

        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_JBI_SAVE_COMPONENT_CONFIGURATION);
            }
    }


    /**
     * <p>
     *
     * For a given (started) component type and name and a given instance,
     * updates the ShowBean with the list of application configuration names.
     * </p> <p>
     *
     * If the component has no targets, or the selected instance is not
     * started, or the component is not started, or the component has not
     * provided an application configuration MBean, returns an alert summary
     * and details. </p> <p>
     *
     * Input value: "requestedCompName" -- Type: <code> java.lang.String</code>
     * the requested component name</p> <p>
     *
     * Input value: "compType" -- Type: <code> java.lang.String</code> the
     * requested component type</p> <p>
     *
     * Input value: "requestedInstanceName" -- Type: <code> java.lang.String</code>
     * the requested instance name, or null</p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code> alert
     * details, if needed</p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code> alert
     * summary, if neeed</p> <p>
     *
     * Output value: "instanceList" -- Type: <code>Option[]</code>list of
     * instances to populate dropDown, if any</p> <p>
     *
     * Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * true if alert should be displayed</p> <p>
     *
     * Output value: "selectedInstance" -- Type: <code>String</code>the
     * currently selected instance, or a default</p>
     *
     * <p>Output value: "showStartButton" -- Type: <code>java.lang.Boolean</code> show start 
     * button indicator</p>
     * 
     * <p>Output value: "installedOnZeroTargets" -- Type: <code>java.lang.Boolean</code> render instance 
     * list indicator</p>
     * 
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiSetCompAppConfigNamesData",
        input={
            @HandlerInput(
                name="compName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compType",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="instance",
                type=String.class,
                required=true)
        },
        output={
            @HandlerOutput(name="alertDetails", type=String.class) ,
            @HandlerOutput(name="alertSummary", type=String.class) ,
            @HandlerOutput(name="instanceList", type=Option[].class) ,
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="selectedInstance", type=String.class),
            @HandlerOutput(name="showStartButton", type=Boolean.class),
            @HandlerOutput(name="installedOnZeroTargets", type=Boolean.class)            
        })

        public static void jbiSetCompAppConfigNamesData(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String compType = (String) handlerCtx.getInputValue("compType");
        String instance = (String) handlerCtx.getInputValue("instance");

        if (sLog.isLoggable(Level.FINER)) {        
           sLog.finer("ComponentConfigurationHandlers.jbiSetCompAppConfigNamesData(" +
                  handlerCtx + "), compName=" + compName +
                  ", compType=" + compType +
                  ", instance=" + instance);
        }

        String alertDetails =
            AlertBean.ALERT_DETAIL_NONE;
        String alertSummary =
            AlertBean.ALERT_SUMMARY_NONE;
        boolean isAlertNeeded = false;
        boolean isConfigSupportedByMetadata = false;
        boolean showStartButton = false;
        boolean installedOnZeroTargets = true;
        
        List installedTargetsList = new ArrayList();
        List installedList = ClusterUtilities.findTargetsForNameByType(compName, compType);
        // Construct the installedTargetsList
        Iterator targetsIterator = installedList.iterator();
        while (targetsIterator.hasNext()){
            Properties targetProp = (Properties) targetsIterator.next();
            String target = targetProp.getProperty(SharedConstants.KEY_NAME);
            installedTargetsList.add(target);
        }

        List installedInstancesList =
            ClusterUtilities.getAllInstances(installedTargetsList);
        Collections.sort(installedInstancesList);        
        
        // Return the options list
        String[] instances = new String[0];
        instances = (String[]) installedInstancesList.toArray(instances);
        Option[] instanceList = (Option[]) ConfigurationHandlers.getOptions(instances);        
        if (instanceList != null && instanceList.length > 0) {
        	installedOnZeroTargets = false;
        } else {
            //no target
            isAlertNeeded = true;
            AlertBean alertBean = BeanUtilities.getAlertBean();
            alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
            alertSummary =
                I18nUtilities
                .getResourceString("jbi.comp.app.config.unable.to.list");
            alertDetails =
                I18nUtilities
                .getResourceString("jbi.comp.app.config.has.no.targets");        	
        }
                
        //in case current instance is in the list, select first one in the list
        List<String> instanceNamesList = 
                 JBIUtils.getInstancesAsStringList(compName, compType);
        String selectedInstance =         
            JBIUtils.selectCurrentInstance(instanceNamesList, instance);

        // If no alert so far,
        // determines if the component metadata supports 
        // component application configuration
        ShowBean showBean = BeanUtilities.getShowBean();
        if (!isAlertNeeded)
        {
             List<ConfigPropertyGroup> configGroups =
                    CompConfigUtils.getMetadataConfigExtensions(compName,
                                                                ESBConstants
                                                                .ELT_APP_CFG);
             if ((null != configGroups)
                    &&(0 < configGroups.size()))
                    {
                        isConfigSupportedByMetadata = true; 
                    }

             showBean
                    .setCompAppConfigSupported(isConfigSupportedByMetadata);



            // If configuration is supported,
            // checks preconditions
            if (isConfigSupportedByMetadata)
            {
                Properties statusProps = new Properties();
                
                boolean isCompStartedOnInstance =
                    JBIUtils.isCompStartedOnInstance(compName,
                                                     compType,
                                                     instance,
                                                     statusProps);
                // If the component is started on the instance,
                // reloads the table
                if (isCompStartedOnInstance)
                    {
                        showBean.setCompAppConfigNamesTableData(compName,
                                                                compType,
                                                                instance);
                    }
                // If the component is not started on the instance
                // disables the table to avoid doomed-to-fail attempts
                // to edit or create an application cofiguration
                // and issue an alert
                else
                    {
                        showBean
                            .setCompAppConfigSupported(false); 
                        isAlertNeeded = true;

                        // If the instance is not running
                        // issues an "instance not running" alert
                        if (!ClusterUtilities.isInstanceRunning(instance))
                            {
                                String alertSummaryKey =
                                    "jbi.instance.info.not.available";
                                alertSummary =
                                    I18nUtilities
                                    .getResourceString(alertSummaryKey);

                                String alertDetailsKey =
                                    "jbi.instance.not.running";
                                alertDetails =
                                    I18nUtilities
                                    .getResourceString(alertDetailsKey);
                            }
                        // If the instance is running
                        // issues a "component not started on instance" alert
                        else
                            {
                        	    showStartButton = true;
                                alertSummary = (String)
                                    statusProps.get(SharedConstants.ALERT_SUMMARY_KEY);
                                if (null == alertSummary)
                                    {
                                        alertSummary =
                                            I18nUtilities.getResourceString("jbi.configuration.alert");
                                    }
                                alertDetails = (String)
                                    statusProps.get(SharedConstants.ALERT_DETAIL_KEY);
                                if (null == alertDetails)
                                    {
                                        alertDetails =
                                            I18nUtilities.getResourceString("jbi.configuration.msgComponentDown");
                                    }
                            }
                        AlertBean alertBean = BeanUtilities.getAlertBean();
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                    }
            }
            else
            {
                isAlertNeeded = true;
                alertSummary =
                    I18nUtilities.getResourceString("jbi.comp.app.config.unable.to.list");
                alertDetails = 
                    I18nUtilities.getResourceString("jbi.comp.app.config.not.supported");
                AlertBean alertBean = BeanUtilities.getAlertBean();
                alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
            }
        }
        
        // If there is no new alert,
        // shows any old alert
        if (!isAlertNeeded)
            {
                // If  there is a pending alert,
                // shows the pending alert summary and details, if any 
                // (If there is an alert pending but missing a summary or details,
                // provides internal error defaults)
                if (AlertBean.isSessionAlertNeeded())
                    {
                        isAlertNeeded = true;

                        String defaultSummaryKey =
                            "jbi.comp.config.internal.error";
                        String defaultSummary =
                            I18nUtilities
                            .getResourceString(defaultSummaryKey);

                        alertSummary = 
                            AlertBean
                            .getSessionAlertSummary(defaultSummary);

                        String defaultDetailsKey =
                            "jbi.comp.config.internal.error.details.unknown";
                        String defaultDetails =
                            I18nUtilities
                            .getResourceString(defaultDetailsKey);
                        
                        alertDetails = 
                            AlertBean
                            .getSessionAlertDetail(defaultDetails);
                    }
            }

        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("instanceList", instanceList);
        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("selectedInstance", selectedInstance);
        handlerCtx.setOutputValue("showStartButton", showStartButton);
        handlerCtx.setOutputValue("installedOnZeroTargets", installedOnZeroTargets);
        
        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("ComponentConfigurationHandlers.jbiSetCompAppConfigNamesData(...)" +
                  ", alertDetails=" + alertDetails +
                  ", alertSummary=" + alertSummary +
                  ", instanceList=" + instanceList +
                  ", isAlertNeeded=" + isAlertNeeded +
                  ", selectedInstance=" + selectedInstance +
                  ", showStartButton=" + showStartButton +
                  ", installedOnZeroTargets=" + installedOnZeroTargets +
                  "");
        }
    }

    /**
     * For a given component and instance updates the <code>CompConfigBean</code>
     * with a configuration property sheet of the requested configuration type
     * (if the component supports the use-case)
     * <p>If the component has no targets, or the (possibly default) selected
     * instance is not running, or the component is not started, or the
     * component does not support the requested type of configuration, returns
     * an alert summary and details.</p>
     * <p>Input value: "compName" -- Type: <code> java.lang.String</code> the
     * requested component name</p> 
     * <p>Input value: "compType" -- Type: <code> java.lang.String</code> the
     * requested component type</p>
     * <p>Input value: "configType" -- Type: <code> java.lang.String</code> the
     * requested configuration data type, one of: <code>config-type-comp-app</code>,
     * <code>config-type-comp-install</code>, or <code>config-type-comp-runtime</code></p> 
     * <p>Input value: "prevSelectedInstance" -- Type: <code> java.lang.String</code>
     * the previous selected instance name, or <code>null</code> if this page
     * is being loaded for the first time.</p>
     * <p>Output value: "instancesList" -- Type: <code>Option[]</code>
     * instances, if any, to populate dropDown</p>
     * <p>Output value: "currSelectedInstance" -- Type: <code>String</code>the
     * currently selected instance, or a default initial selection, or null if
     * there are no targets for this component</p>
     * <p>Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * true if alert should be displayed</p>
     * <p>Output value: "alertSummary" -- Type: <code>String</code> alert
     * summary, if neeed</p>
     * <p>Output value: "alertDetails" -- Type: <code>String</code> alert
     * details, if needed</p>
     * <p>Output value: "showStartButton" -- Type: <code>java.lang.Boolean</code> show start 
     * button indicator</p>
     * <p>Output value: "installedOnZeroTargets" -- Type: <code>java.lang.Boolean</code> render instance 
     * list indicator</p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiSetCompConfigData",
        input=
        {
            @HandlerInput(
                name="compName",
                type=String.class,
                required=true)
            ,
            @HandlerInput(
                name="compType",
                type=String.class,
                required=true)
            ,
            @HandlerInput(
                name="configType",
                type=String.class,
                required=true)
            ,
            @HandlerInput(
                name="prevSelectedInstance",
                type=String.class,
                required=true)
        },
        output=
        {
            @HandlerOutput(name="instancesList", type=Option[].class)
            ,
            @HandlerOutput(name="currSelectedInstance", type=String.class)
            ,
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class)
            ,
            @HandlerOutput(name="alertSummary", type=String.class)
            ,
            @HandlerOutput(name="alertDetails", type=String.class)
            ,
            @HandlerOutput(name="showStartButton", type=Boolean.class)
            ,
            @HandlerOutput(name="installedOnZeroTargets", type=Boolean.class)
        })

        public static void jbiSetCompConfigData(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String compType = (String) handlerCtx.getInputValue("compType");
        String configType = (String) handlerCtx.getInputValue("configType");
        String prevSelectedInstance = (String) handlerCtx.getInputValue("prevSelectedInstance");

        if (sLog.isLoggable(Level.FINER)) {        
           sLog.finer("ComponentConfigurationHandlers.jbiSetCompConfigData(" +
                  handlerCtx + "), compName=" + compName +
                  ", compType=" + compType +
                  ", configType=" + configType +
                  ", prevSelectedInstance=" + prevSelectedInstance);
        }

        AlertBean alertBean = 
            BeanUtilities.getAlertBean();
        String alertDetails =
            AlertBean.ALERT_DETAIL_NONE;
        String alertSummary =
            AlertBean.ALERT_SUMMARY_NONE;

        String currSelectedInstance = null;
        List<String> instanceNamesList = null;
        String instanceState = null;
        boolean isAlertNeeded = false;
        boolean isAlertSeeLogEpilogSkipped = false;
        boolean isCompStartedOnInstance = false;
        boolean isConfigDataAvailable = false;
        boolean isConfigSupportedByMetadata = false;
        boolean isConfigSupportedByRuntime = false;
        boolean isInstalledOnZeroTargets = false;
        boolean isInstanceRunning = false;
        boolean isInternalError = false;
        boolean showStartButton = false;
        
        Option[] instancesList =
            JBIUtils.getInstancesAsOptionArray(compName,
                                               compType);        

        // If there is no alert so far,
        // determines if the component metadata supports component configuration
        if (!isAlertNeeded)
            {
                // If this is a component application configuration use-case,
                // checks the metadata for support of component application configuration
                if (ESBConstants
                    .CONFIG_TYPE_COMP_APP
                    .equals(configType))
                    {
                        List<ConfigPropertyGroup> configGroups =
                            CompConfigUtils.getMetadataConfigExtensions(compName,
                                                                        ESBConstants
                                                                        .ELT_APP_CFG);
                        // If there are configuration groups,
                        // indicates that the use-case is supported
                        if ((null != configGroups)
                            &&(0 < configGroups.size()))
                            {
                                isConfigSupportedByMetadata = true; 
                            }
                    }
                // If this is a component runtime configuration use-case,
                // checks the metadata for support of component runtime configuration
                else if (ESBConstants
                         .CONFIG_TYPE_COMP_RUNTIME
                         .equals(configType))
                    {
                        List<ConfigPropertyGroup> configGroups =
                            CompConfigUtils.getMetadataConfigExtensions(compName,
                                                                        ESBConstants
                                                                        .ELT_CFG);
                
                        // If there are configuration groups,
                        // indicates that the use-case is supported
                        if ((null != configGroups)
                            &&(0 < configGroups.size()))
                            {
                                isConfigSupportedByMetadata = true; 
                            }
                    }

                // If component metatdata indicates lack of support for component configuration,
                // shows a "Component Configuration Not Supported" info alert
                if (!isConfigSupportedByMetadata)
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.cannot.show.configuration");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.not.supported.info1");
                        alertDetails += 
                            SharedConstants
                            .HTML_BREAK +
                            I18nUtilities
                            .getResourceString("jbi.comp.config.not.supported.info2");
                        isAlertSeeLogEpilogSkipped = true;
                    }

                instanceNamesList = 
                    JBIUtils.getInstancesAsStringList(compName,
                                                      compType);
                
                // If there are no instances to choose from
                // indicates that the component has no targets
                if (0 == instanceNamesList.size())
                    {
                        // If running in a cluster-profile domain
                        // indicates taht there are no targets
                        if (IS_CLUSTER_PROFILE)
                            {
                                isInstalledOnZeroTargets = true;
                            }
                        // If running in a developer-profile domain
                        // there must be a target ("server")
                    }
            
                // If the component has targets
                // determines the currently selected instance and its state
                if (!isInstalledOnZeroTargets)
                    {
                        // 1. (uses the prevSelectedInstance if valid, regardless of component 
                        // state; if prevSelectedInstance is null or invalid, selects "server" 
                        // (if valid) or the "first" instance or (if none), the previous
                        // (even if invalid)
                        currSelectedInstance =
                            JBIUtils.selectCurrentInstance(instanceNamesList,

                                                           prevSelectedInstance);

                        // 2. determines if the instance is running
                        // Note: since this code only runs on "server"
                        // if that is the current instance it must be running
                        instanceState =
                            ClusterUtilities
                            .getInstanceState(currSelectedInstance);
                        if ((JBIAdminCommands
                             .SERVER_TARGET_KEY
                             .equals(currSelectedInstance))
                            ||(SharedConstants
                               .STATE_RUNNING
                               .equals(instanceState)))
                            {
                                isInstanceRunning = true;
                            }
                    }
                // If the component has no targets
                // shows a "Component Has No Targets" info alert
                else
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.cannot.show.configuration");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.comp.has.no.targets");
                        isAlertSeeLogEpilogSkipped = true;
                    }
            }
        

        // If no alert needed so far and...
        if (!isAlertNeeded)
            {
                // If the currently selected instance is running,
                // gets the state of the component on the currently selected instance
                if (isInstanceRunning)
                    {
                        Properties statusProps = 
                            new Properties();
                        
                        isCompStartedOnInstance =
                            JBIUtils.isCompStartedOnInstance(compName,
                                                             compType,
                                                             currSelectedInstance,
                                                             statusProps);                                                
                    }
                // If the instance is not running,
                // shows an "Instance Not Running" info alert
                else
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.cannot.show.configuration");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.instance.not.running");
                        isAlertSeeLogEpilogSkipped = true;
                    }
            }
        
        // If no alert needed so far and...
        if (!isAlertNeeded)
            {
                // the component is started on the instance,
                // determines if the configuration type is supported
                if (isCompStartedOnInstance)
                    {
                        isConfigSupportedByRuntime =
                            CompConfigUtils.isConfigTypeSupported(configType,
                                                                  compName,
                                                                  compType,
                                                                  currSelectedInstance);
                    }
                // If the component is not started,
                // shows a "Component Not Started" info alert
                else
                    {
                	    showStartButton = true;
                	    isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.cannot.show.configuration");
                        String compNotStartedKey;
                        if (ClusterUtilities.isClusterProfile())
                            {
                                compNotStartedKey =
                                    "jbi.comp.config.comp.not.started";
                            }
                        else
                            {
                                compNotStartedKey =
                                    "jbi.comp.config.comp.not.started.pe";
                            }

                        alertDetails =
                            I18nUtilities
                            .getResourceString(compNotStartedKey);
                        isAlertSeeLogEpilogSkipped = true;
                    }                

            }
        
        // If no alert needed so far and...
        if (!isAlertNeeded)
            {
                // If configuration is supported by the component runtime
                // attempts to display the editable configuration
                if (isConfigSupportedByRuntime)
                    {
                        isConfigDataAvailable =
                            CompConfigUtils.setConfigPS(configType,
                                                        compName,
                                                        compType,
                                                        currSelectedInstance);
                    }
                // If configuration is not supported by the component runtime,
                // shows a "Missing Component Runtime Support for Configuration" error alert
                else
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                        isInternalError = true;
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.cannot.show.configuration");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.missing.comp.rt.support1");
                        alertDetails += 
                            SharedConstants
                            .HTML_BREAK +
                            I18nUtilities
                            .getResourceString("jbi.comp.config.missing.comp.rt.support2");
                    }
                
            }
        
        // If no alert needed so far and...
        if (!isAlertNeeded)
            {
                // If configuration data should be available but is not available,
                // shows an "Internal Error" alert
                if ((isConfigSupportedByRuntime)
                    &&(!isConfigDataAvailable))
                    {
                        isAlertNeeded = true;
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                        isInternalError = true;
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.cannot.show.configuration");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.internal.error");
                    }
            }

        if (isAlertNeeded)
            {
                PropertySheet placeholderConfigPS =
                    JSFUtils
                    .getConfigUnavailablePS(ESBConstants
                                            .CONFIG_TYPE_COMP_RUNTIME,
                                            compName,
                                            compType);

                CompConfigBean compConfigBean =
                    BeanUtilities.getCompConfigBean();

                compConfigBean
                    .setCompRuntimeCompConfigPS(placeholderConfigPS);
                
                if (!isAlertSeeLogEpilogSkipped)
                    {
                        alertDetails = 
                            BeanUtilities
                            .addAlertFooterMessage(alertDetails);
                    }
                

            }

        // If no new alert,
        // shows pending alert, if any. (e.g. from prior update to this view)
        // For example, "User Action Required" 
        // "one or more changed properties require an application
        // (or component or server) restart" message(s) after clicking Save
        if (!isAlertNeeded)
            {
                if (AlertBean.isSessionAlertNeeded())
                    {
                        isAlertNeeded = true;
                        // keep pending alert type

                        String defaultSummaryKey =
                            "jbi.comp.config.internal.error";
                        String defaultSummary =
                            I18nUtilities
                            .getResourceString(defaultSummaryKey);

                        alertSummary = 
                            AlertBean
                            .getSessionAlertSummary(defaultSummary);

                        String defaultDetailsKey =
                            "jbi.comp.config.internal.error.details.unknown";
                        String defaultDetails =
                            I18nUtilities
                            .getResourceString(defaultDetailsKey);

                        alertDetails = 
                            AlertBean
                            .getSessionAlertDetail(defaultDetails);
                        isAlertSeeLogEpilogSkipped = true;
                    }
            }
        

        if (sLog.isLoggable(Level.FINER)) {        
           sLog.finer("ComponentConfigurationHandlers.jbiSetCompConfigData(...)" +
                   ", instanceState=" + instanceState +
                   ", isCompStartedOnInstance=" + isCompStartedOnInstance +
                   ", isConfigDataAvailable=" + isConfigDataAvailable +
                   ", isConfigSupportedByMetadata=" + isConfigSupportedByMetadata +
                   ", isConfigSupportedByRuntime=" + isConfigSupportedByRuntime +
                   ", isInstalledOnZeroTargets=" + isInstalledOnZeroTargets +
                   ", isInternalError=" + isInternalError);
        }

        handlerCtx.setOutputValue("instancesList", instancesList);
        handlerCtx.setOutputValue("currSelectedInstance", currSelectedInstance);
        handlerCtx.setOutputValue("isAlertNeeded", isAlertNeeded);
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("showStartButton", showStartButton);        
        handlerCtx.setOutputValue("installedOnZeroTargets", isInstalledOnZeroTargets);
                
        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("ComponentConfigurationHandlers.jbiSetCompConfigData(...)" +
                  ", instancesList=" + instancesList +
                  ", currSelectedInstance=" + currSelectedInstance +
                  ", isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails +
                  ", showStartButton=" + showStartButton +
                  "");
        }
    }

    /**
     * <p>
     *
     * For a given component name saves the edited variables <p>
     *
     * Input value: "compName" -- Type: <code> java.lang.String</code> the
     * component name</p> <p>
     *
     * Input value: "instanceName" -- Type: <code> java.lang.String</code> the
     * instance name</p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code>details, if
     * failure</p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code>summary, if
     * failure</p> <p>
     *
     * Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * show alert or not</p> Sets the component application variables
     * properties into the ShowBean</p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiSaveCompAppVarsChanges",

        input={
            @HandlerInput(
                name="compName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="instanceName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="tableRowGroup",
                type=TableRowGroup.class,
                required=true)
        },
        output={
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="alertDetails", type=String.class) ,
            @HandlerOutput(name="alertSummary", type=String.class)
        })

        public static void jbiSaveCompAppVarsChanges(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String instanceName = (String) handlerCtx.getInputValue("instanceName");
        TableRowGroup trg = (TableRowGroup) handlerCtx.getInputValue("tableRowGroup");

        AlertBean alertBean = BeanUtilities.getAlertBean();
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE;
        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        boolean isAlertNeeded = false;

        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiSaveCompAppVarsChanges(" + handlerCtx + ")" +
                  ", compName=" + compName +
                  ", instanceName=" + instanceName +
                  ", trg=" + trg);
        }

        Properties editedProps =
            new Properties();

        ObjectListDataProvider dp =
            (ObjectListDataProvider) trg.getSourceData();

        if (null != dp)
            {
                try
                    {
                        dp.commitChanges();

                        FieldKey fkName = dp.getFieldKey("name");
                        FieldKey fkType = dp.getFieldKey("type");
                        FieldKey fkValue = dp.getFieldKey("value");

                        RowKey[] rowKeys = trg.getRowKeys();

                        if (sLog.isLoggable(Level.FINER)) {
                           sLog.finer("ComponentConfigurationHandlers.jbiSaveCompAppVarsChanges(...)" +
                                  ", fkName=" + fkName +
                                  ", fkValue=" + fkValue +
                                  ", rowKeys=" + rowKeys);
                        }

                        for (int cnt = 0; cnt < rowKeys.length; cnt++)
                            {
                                String propName =
                                    (String) dp.getValue(fkName, rowKeys[cnt]);

                                String propType =
                                    (String) dp.getValue(fkType, rowKeys[cnt]);

                                String propValue =
                                    (String) dp.getValue(fkValue, rowKeys[cnt]);

                                if (sLog.isLoggable(Level.FINER)) {
                                    sLog.finer("ComponentConfigurationHandlers.jbiSaveCompAppVarsChanges(...)" +
                                          ", propName=" + propName +
                                          ", propType=" + propType +
                                          ", propValue=" + propValue);
                                }

                                editedProps.put(propName,
                                                (propType +
                                                 propValue));
                            }
                    }
                catch (Exception ex)
                    {
                	    if (sLog.isLoggable(Level.FINE)) {
                            sLog.log(Level.FINE,
                                 ("ComponentConfigurationHandlers.jbiSaveCompAppVarsChanges(), caught ex="
                                  + ex),
                                 ex);
                	    }
                    }
            }
        else
            {
                isAlertNeeded = true;
                alertSummary = 
                    I18nUtilities
                    .getResourceString("jbi.comp.config.internal.error");
                alertDetails = 
                    I18nUtilities
                    .getResourceString("jbi.comp.config.internal.error.details.unknown");
                alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
            }
        
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiSaveCompAppVarsChanges(...)" +
                  ", editedProps=" + editedProps);
        }
        

        if (0 < editedProps.size())
            {
                Properties updateResult =
                    AppVarUtils.update(compName,
                                       instanceName,
                                       editedProps);

                Object successResult =
                    updateResult.get(SharedConstants
                                     .SUCCESS_RESULT);
                if (null != successResult)
                    {
                        isAlertNeeded = true;
                        alertSummary = 
                            I18nUtilities
                            .getResourceString("jbi.app.vars.save.succeeded");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.app.vars.changes.saved");
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_SUCCESS);
                    }
                else
                    {
                        isAlertNeeded = true;
                        alertSummary = 
                            I18nUtilities
                            .getResourceString("jbi.app.vars.save.failed");
                        alertDetails =
                            (String) updateResult
                            .get(SharedConstants
                                 .FAILURE_RESULT);
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                    }
            }

        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));

        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("ComponentConfigPropsTableHandler.jbiSaveCompAppVarsChanges(" + handlerCtx + ")" +
                  ", alertDetails=" + alertDetails +
                  ", alertSummary=" + alertSummary +
                  ", isAlertNeeded=" + isAlertNeeded +
                  "");
        }
    }

    /**
     * <p>
     *
     * For a given component name and an application configuration name,
     * returns the properties. <p>
     *
     * Input value: "appConfigName" -- Type: <code> java.lang.String</code>
     * the application configuration name</p> <p>
     *
     * Input value: "compName" -- Type: <code> java.lang.String</code> the
     * component name</p> <p>
     *
     * Input value: "instanceName" -- Type: <code> java.lang.String</code> the
     * instance name</p> Sets the application configuration properties into
     * the ShowBean</p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(id="jbiSetAppConfig",
             input={
                 @HandlerInput(
                     name="appConfigName",
                     type=String.class,
                     required=true) ,
                 @HandlerInput(
                     name="compName",
                     type=String.class,
                     required=true) ,
                 @HandlerInput(
                     name="instanceName",
                     type=String.class,
                     required=true)
             })

        public static void jbiSetAppConfig(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String instanceName = (String) handlerCtx.getInputValue("instanceName");
        String appConfigName = (String) handlerCtx.getInputValue("appConfigName");

        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigPropsTableHandler.jbiGetAppConfig(" +
                  handlerCtx + "), compName=" + compName +
                  ", instanceName=" + instanceName +
                  ", appConfigName=" + appConfigName);
        }

        ShowBean showBean = BeanUtilities.getShowBean();
        showBean.setCompAppConfigProps(compName,
                                       instanceName,
                                       appConfigName);

    }


    /**
     * <p>
     *
     * For a given component name creates an empty set of properties <p>
     *
     * Input value: "compName" -- Type: <code> java.lang.String</code> the
     * component name</p> <p>
     *
     * Input value: "instanceName" -- Type: <code> java.lang.String</code> the
     * instance name</p> Sets the new application configuration properties
     * into the ShowBean</p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(id="jbiSetNewAppConfig",
             input={
                 @HandlerInput(
                     name="compName",
                     type=String.class,
                     required=true) ,
                 @HandlerInput(
                     name="instanceName",
                     type=String.class,
                     required=true) })

        public static void jbiSetNewAppConfig(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String instanceName = (String) handlerCtx.getInputValue("instanceName");

        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigPropsTableHandler.jbiSetNewAppConfig(" +
                  handlerCtx + "), compName=" + compName +
                  ", instanceName=" + instanceName);
        }

        ShowBean showBean = BeanUtilities.getShowBean();
        showBean.setNewCompAppConfigProps(compName,
                                          instanceName);
    }


    /**
     * <p>
     *
     * Returns Component Variables data to populate table rows in a List of
     * HashMaps, each HashMap containing the name, type, and value.</p> <p>
     *
     * Input value: "compName" -- Type: <code> java.lang.String</code> the
     * component name</p> <p>
     * Input value: "compType" -- Type: <code> java.lang.String</code> the
     * component type, on of: <code>binding-component</code> or
     * <code>service-engine</code> <p>
     *
     * Input value: "instanceName" -- Type: <code> java.lang.String</code> the
     * instance name</p> <p>
     *
     * Output value: "TableList" -- Type: <code>java.util.List</code>/</p>
     * <p>Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * true if alert should be displayed</p> 
     * <p>Output value: "alertSummary" -- Type: <code>String</code> alert
     * summary, if neeed</p> 
     * <p>Output value: "alertDetails" -- Type: <code>String</code> alert
     * details, if needed</p>
     * <p>Output value: "showStartButton" -- Type: <code>java.lang.Boolean</code> show start 
     * button indicator</p>
     * <p>Output value: "showSaveButton" -- Type: <code>java.lang.Boolean</code> show stave
     * button indicator</p>
     * <p>Output value: "installedOnZeroTargets" -- Type: <code>java.lang.Boolean</code> render instance 
     * list indicator</p>
     * 
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiGetCompVarsTableList",

        input={
            @HandlerInput(
                name="compName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compType",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="instanceName",
                type=String.class,
                required=true)
        },
        output={
            @HandlerOutput(name="alertDetails", type=String.class),
            @HandlerOutput(name="alertSummary", type=String.class),
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class),
            @HandlerOutput(name="isCreateOrEditAllowed", type=Boolean.class),
            @HandlerOutput(name="instanceList", type=Option[].class),
            @HandlerOutput(name="selectedInstance", type=String.class),
            @HandlerOutput(name="TableList", type=List.class),
            @HandlerOutput(name="showStartButton", type=Boolean.class),
            @HandlerOutput(name="showSaveButton", type=Boolean.class),
            @HandlerOutput(name="installedOnZeroTargets", type=Boolean.class)
        })

        public static void jbiGetCompVarsTableList(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String compType = (String) handlerCtx.getInputValue("compType");
        String instanceName = (String) handlerCtx.getInputValue("instanceName");
        
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiGetCompVarsTableList(" +
                  handlerCtx + "), compName=" + compName +
                  ", instanceName=" + instanceName);
        }
        
        String alertDetails =
            AlertBean.ALERT_DETAIL_NONE;
        String alertSummary =
            AlertBean.ALERT_SUMMARY_NONE;
        boolean isAlertNeeded = false;
        boolean isConfigSupportedByMetadata = false;
        boolean isCreateOrEditAllowed = false;
        boolean showStartButton = false;
        boolean showSaveButton = true;
        boolean installedOnZeroTargets = true;
        List tableListData = new ArrayList();
                                       
        List installedTargetsList = new ArrayList();
        List installedList = ClusterUtilities.findTargetsForNameByType(compName, compType);
        // Construct the installedTargetsList
        Iterator targetsIterator = installedList.iterator();
        while (targetsIterator.hasNext()){
            Properties targetProp = (Properties) targetsIterator.next();
            String target = targetProp.getProperty(SharedConstants.KEY_NAME);
            installedTargetsList.add(target);
        }

        List installedInstancesList =
            ClusterUtilities.getAllInstances(installedTargetsList);
        Collections.sort(installedInstancesList);        
        
        // Return the options list
        String[] instances = new String[0];
        instances = (String[]) installedInstancesList.toArray(instances);
        Option[] instanceList = (Option[]) ConfigurationHandlers.getOptions(instances);
        if (instanceList != null && instanceList.length > 0) {
        	installedOnZeroTargets = false;
        } else {
            //no target
            isAlertNeeded = true;
            AlertBean alertBean = BeanUtilities.getAlertBean();
            alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
            alertSummary =
                I18nUtilities
                .getResourceString("jbi.app.vars.cannot.show");
            alertDetails =
                I18nUtilities
                .getResourceString("jbi.app.var.has.no.targets");        	
        }

        // If no alert so far,
        // determines if the component metadata supports application variables
        if (!isAlertNeeded)
            {

                List<ConfigPropertyGroup> configGroups =
                    CompConfigUtils.getMetadataConfigExtensions(compName,
                                                                ESBConstants
                                                                .ELT_APP_VAR);
                if ((null != configGroups)
                    &&(0 < configGroups.size()))
                    {
                        isConfigSupportedByMetadata = true; 
                    }

                if (isConfigSupportedByMetadata)
                    {
                        Properties statusProps =
                            new Properties();
                        boolean isCompStartedOnInstance =
                            JBIUtils.isCompStartedOnInstance(compName,
                                                             compType,
                                                             instanceName,
                                                             statusProps);
                        // If (the use-case is supported but)
                        // the component is not started,
                        // shows an info-not-available, comp-not-started alert
                        if (!isCompStartedOnInstance)
                            {
                                isAlertNeeded = true;
                                // If the instance is not running
                                // issues an "instance not running" alert
                                if (!ClusterUtilities
                                    .isInstanceRunning(instanceName))
                                    {
                                        String alertSummaryKey =
                                            "jbi.instance.info.not.available";
                                        alertSummary =
                                            I18nUtilities
                                            .getResourceString(alertSummaryKey);
                                        
                                        String alertDetailsKey =
                                            "jbi.instance.not.running";
                                        alertDetails =
                                            I18nUtilities
                                            .getResourceString(alertDetailsKey);
                                    }
                                // If the instance is running
                                // issues a "component not started on instance" alert
                                else
                                    {
                                	    showStartButton = true;
                                        alertSummary = 
                                            I18nUtilities
                                            .getResourceString("jbi.app.vars.info.not.available");
                                        String compNotStartedKey;
                                        if (ClusterUtilities.isClusterProfile())
                                            {
                                                compNotStartedKey =
                                                    "jbi.comp.config.comp.not.started";
                                            }
                                        else
                                            {
                                                compNotStartedKey =
                                                    "jbi.comp.config.comp.not.started.pe";
                                            }
                                
                                        alertDetails =
                                            I18nUtilities
                                            .getResourceString(compNotStartedKey);
                                    }
                                AlertBean alertBean = BeanUtilities.getAlertBean();
                                alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                            }
                    }
                // If component metatdata indicates 
                // lack of support for application variables,
                // shows a "Application Variables Not Supported" info alert
                else
                    {
                        isCreateOrEditAllowed = false;
                        isAlertNeeded = true;
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.app.vars.cannot.show");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.app.vars.not.supported");
                
                        AlertBean alertBean = BeanUtilities.getAlertBean();
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                    }

                // If (use-case supported and component is started and)
                // no alert needed so far,
                // gets the component's application variables
                if (!isAlertNeeded)
                    {
                        isCreateOrEditAllowed = true;
                        Properties status =
                            new Properties();
                        // maps name to [type]value (or just to value)
                        Properties appVarsProps =
                            AppVarUtils.getCompAppVars(compName,
                                                       instanceName,
                                                       status);
                
                        Object failureResult =
                            status.get(SharedConstants
                                       .FAILURE_RESULT);
                
                        // If an error was detected,
                        // shows an alert.
                        if (null != failureResult)
                            {
                                isCreateOrEditAllowed = false;
                                isAlertNeeded = true;
                                alertSummary = 
                                    I18nUtilities
                                    .getResourceString("jbi.list.comp.app.var.failed.alert.summary.text");
                        
                                if (failureResult instanceof Exception)
                                    {
                                        Exception ex =
                                            (Exception) failureResult;
                                        JBIManagementMessage
                                            mgmtMessage =
                                            BeanUtilities
                                            .extractJBIManagementMessage(ex);
                                        alertDetails =
                                            mgmtMessage.getMessage();
                                    }
                                if (AlertBean
                                    .ALERT_DETAIL_NONE
                                    .equals(alertDetails))
                                    {
                                        alertDetails = 
                                            failureResult.toString();
                                    }
                        
                                alertDetails = 
                                    BeanUtilities
                                    .addAlertFooterMessage(alertDetails);
                            }
                
                        Iterator varsIt = appVarsProps.keySet().iterator();
                        while (varsIt.hasNext())
                            {
                                String appVarName =
                                    (String) varsIt.next();
                                String appVarOptTypeAndValue =
                                    (String) appVarsProps.get(appVarName);
                                
                                if (sLog.isLoggable(Level.FINER)) {
                                    sLog.finer("ComponentConfigurationHandlers.jbiGetCompVarsTableList(), appVarName=" +
                                          appVarName +
                                          ", appVarOptTypeAndValue=" +
                                          appVarOptTypeAndValue);
                                }
                        
                                String displayType = null;
                                String appVarType = null;
                                String appVarValue = null;
                        
                                if (0 == appVarOptTypeAndValue.indexOf(SharedConstants.APP_VAR_TYPE_BOOLEAN))
                                    {
                                        displayType =
                                            I18nUtilities.getResourceString("jbi.show.comp.app.vars.typeIsBoolean");
                                        appVarType =
                                            SharedConstants.APP_VAR_TYPE_BOOLEAN;
                                        appVarValue =
                                            appVarOptTypeAndValue
                                            .substring(SharedConstants
                                                       .APP_VAR_TYPE_BOOLEAN
                                                       .length());
                                        // ensure TRUE or FALSE when needed
                                        if (SharedConstants
                                            .APP_VAR_TYPE_BOOLEAN
                                            .equals(appVarType))
                                            {
                                                if ("true"
                                                    .equalsIgnoreCase(appVarValue))
                                                    {
                                                        appVarValue = "TRUE";
                                                    }
                                                else
                                                    {
                                                        appVarValue = "FALSE";
                                                    }
                                            }
                                    }
                                else if (0 == appVarOptTypeAndValue.indexOf(SharedConstants.APP_VAR_TYPE_NUMBER))
                                    {
                                        displayType =
                                            I18nUtilities.getResourceString("jbi.show.comp.app.vars.typeIsNumber");
                                        appVarType =
                                            SharedConstants.APP_VAR_TYPE_NUMBER;
                                        appVarValue =
                                            appVarOptTypeAndValue
                                            .substring(SharedConstants
                                                       .APP_VAR_TYPE_NUMBER
                                                       .length());
                                    }
                                else if (0 == appVarOptTypeAndValue
                                         .indexOf(SharedConstants
                                                  .APP_VAR_TYPE_PASSWORD))
                                    {
                                        String pwTypeKey =
                                            "jbi.show.comp.app.vars.typeIsPassword";
                                        displayType =
                                            I18nUtilities
                                            .getResourceString(pwTypeKey);
                                        appVarType =
                                            SharedConstants
                                            .APP_VAR_TYPE_PASSWORD;
                                        appVarValue =
                                            appVarOptTypeAndValue
                                            .substring(SharedConstants
                                                       .APP_VAR_TYPE_PASSWORD
                                                       .length());
                                    }
                                else
                                    {
                                        String strTypeKey =
                                            "jbi.show.comp.app.vars.typeIsString";
                                        displayType =
                                            I18nUtilities
                                            .getResourceString(strTypeKey);
                                        appVarType =
                                            SharedConstants.APP_VAR_TYPE_STRING;
                                
                                        // if prefixed by [STRING] remove that
                                        if (0 == appVarOptTypeAndValue
                                            .indexOf(SharedConstants
                                                     .APP_VAR_TYPE_STRING))
                                            {
                                                appVarValue =
                                                    appVarOptTypeAndValue
                                                    .substring(SharedConstants
                                                               .APP_VAR_TYPE_STRING.length());
                                            }
                                        // implicitly a String 
                                        // (not explicit type, 
                                        // or unrecognized type
                                        else
                                            {
                                                appVarValue =
                                                    appVarOptTypeAndValue;
                                            }
                                    }
                        
                                if (sLog.isLoggable(Level.FINER)) {
                                   sLog.finer("ComponentConfigurationHandlers.jbiGetCompVarsTableList()" +
                                          ", displayType=" + displayType +
                                          ", appVarValue=" + appVarValue);
                                }

                                HashMap compVarsRow = new HashMap();
                                compVarsRow.put("selected", false);
                                compVarsRow.put("name", appVarName);
                                compVarsRow.put("type", appVarType);
                                compVarsRow.put("displayType", displayType);
                                compVarsRow.put("value", appVarValue);
                                compVarsRow.put("disabled", true);
                                tableListData.add(compVarsRow);
                                
                                if (sLog.isLoggable(Level.FINEST)) {
                                    sLog.finest("ComponentConfigurationHandlers.jbiGetCompVarsTableList()" +
                                           ", selected=" + compVarsRow.get("selected") +
                                           ", name=" + compVarsRow.get("name") +
                                           ", type=" + compVarsRow.get("type") +
                                           ", displayType=" + compVarsRow.get("displayType") +
                                           ", value=" + compVarsRow.get("value") +
                                           ", disabled=" + compVarsRow.get("disabled"));
                                }
                            }

                    }

                // If no new alert,
                // checks for any old, pending alerts
                if (!isAlertNeeded)
                    {
                        // If  there is a pending alert,
                        // shows the pending alert summary and details, if any 
                        // (If there is an alert pending 
                        // but missing a summary or details,
                        // provides internal error defaults)
                        if (AlertBean.isSessionAlertNeeded())
                            {
                                isAlertNeeded = true;

                                String defaultSummaryKey =
                                    "jbi.comp.config.internal.error";
                                String defaultSummary =
                                    I18nUtilities
                                    .getResourceString(defaultSummaryKey);

                                alertSummary = 
                                    AlertBean
                                    .getSessionAlertSummary(defaultSummary);

                                String defaultDetailsKey =
                                    "jbi.comp.config.internal.error.details.unknown";
                                String defaultDetails =
                                    I18nUtilities
                                    .getResourceString(defaultDetailsKey);
                                alertDetails = 
                                    AlertBean
                                    .getSessionAlertDetail(defaultDetails);
                            }
                    }

            }

        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("ComponentConfigurationHandlers.jbiGetCompVarsTableList()" +
                  ", isCreateOrEditAllowed=" + isCreateOrEditAllowed +
                  ", instanceList=" + instanceList +
                  ", tableListData=" + tableListData + 
                  ", showStartButton=" + showStartButton +
                  ", installedOnZeroTargets=" + installedOnZeroTargets);
        }

        // issue-1160,1186
        if ( tableListData.isEmpty() ) {
            showSaveButton = false;
        }

        handlerCtx.setOutputValue("isCreateOrEditAllowed", Boolean.toString(isCreateOrEditAllowed));
        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);

        handlerCtx.setOutputValue("instanceList", instanceList);
        handlerCtx.setOutputValue("TableList", tableListData);
        handlerCtx.setOutputValue("showStartButton", showStartButton);
        handlerCtx.setOutputValue("showSaveButton", showSaveButton);
        handlerCtx.setOutputValue("installedOnZeroTargets", installedOnZeroTargets);
    }


    /**
     * <p>
     *
     * Validates and adds a new component Application Variable name and type
     * on the specified instance for the specified component. <p>
     *
     * Input value: "instance" -- Type: <code> java.lang.String</code> target
     * instance <p>
     *
     * Input value: "component" -- Type: <code> java.lang.String</code> target
     * component <p>
     *
     * Input value: "compAppVarName" -- Type: <code> java.lang.String</code>
     * new app var name <p>
     *
     * Input value: "compAppVarType" -- Type: <code> java.lang.String</code>
     * new app var type <p>
     *
     * Input value: "compAppVarValue" -- Type: <code> java.lang.String</code>
     * new app var value <p>
     *
     * Input value: "redirectOnFailure" -- Type: <code> java.lang.String</code>
     * what to show if it fails <p>
     *
     * Input value: "redirectOnSuccess" -- Type: <code> java.lang.String</code>
     * what to show if it works <p>
     *
     * Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * show alert or not</p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code>summary, if
     * failure</p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code>details, if
     * failure</p> <p>
     *
     * Output value: "redirectTo" -- Type: <code>String</code> Where to go
     * next, based on success/failure</p>
     *
     * @param handlerCtx  <code>HandlerContext</code> provides inputs and
     *      outputs.
     */
    @Handler(
        id="jbiAddCompAppVar",
        input={
            @HandlerInput(
                name="instanceName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="componentName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compAppVarName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compAppVarType",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compAppVarValue",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="redirectOnFailure",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="redirectOnSuccess",
                type=String.class,
                required=true) },
        output={
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="alertSummary", type=String.class) ,
            @HandlerOutput(name="alertDetails", type=String.class) ,
            @HandlerOutput(name="redirectTo", type=String.class) })

        public static void jbiAddCompAppVar(HandlerContext handlerCtx)
    {
        String instanceName = (String) handlerCtx.getInputValue("instanceName");
        String componentName = (String) handlerCtx.getInputValue("componentName");
        String compAppVarName = (String) handlerCtx.getInputValue("compAppVarName");
        String compAppVarType = (String) handlerCtx.getInputValue("compAppVarType");
        String compAppVarValue = (String) handlerCtx.getInputValue("compAppVarValue");
        String redirectOnFailure = (String) handlerCtx.getInputValue("redirectOnFailure");
        String redirectOnSuccess = (String) handlerCtx.getInputValue("redirectOnSuccess");
        String redirectTo = redirectOnSuccess;

        String compAppVarValueMasked;
        if (SharedConstants.APP_VAR_TYPE_PASSWORD.equals(compAppVarType))
            {
                compAppVarValueMasked = "*masked*"; // not I18n
            }
        else
            {
                compAppVarValueMasked = compAppVarValue;
            }

        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiAddCompAppVar(), instanceName=" + instanceName +
                  ", componentName=" + componentName + ", compAppVarName=" + compAppVarName +
                  ", compAppVarType=" + compAppVarType +
                  ", compAppVarValue=" + compAppVarValueMasked +
                  ", redirectOnFailure=" + redirectOnFailure +
                  ", redirectOnSuccess=" + redirectOnSuccess);
        }

        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE;
        boolean isAlertNeeded = false;

        Properties saveResult =
            AppVarUtils.saveNewCompAppVar(componentName,
                                          instanceName,
                                          compAppVarName,
                                          compAppVarType,
                                          compAppVarValue);

        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("ComponentConfigurationHandlers.jbiAddCompAppVar(), saveResult=" +
                  saveResult);
        }

        String failureResult =
            (String) saveResult.get(SharedConstants
                                    .FAILURE_RESULT);
        if (null != failureResult)
            {
                isAlertNeeded = true;
                alertSummary = I18nUtilities.getResourceString("jbi.add.comp.app.var.failed.alert.summary.text");
                alertDetails = failureResult;
                AlertBean alertBean = BeanUtilities.getAlertBean();
                alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                redirectTo = redirectOnFailure;
            }

        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("redirectTo", redirectTo);

        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("ComponentConfigurationHandlers.jbiAddCompAppVar(...), " +
                  " isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails +
                  ", redirectTo=" + redirectTo);
        }
    }


    /**
     * <p>
     *
     * Delegates JBI deletion requests for each selected component Application
     * Configuration row. <p>
     *
     * Input value: "tableRowGroup" -- Type: <code> com.sun.webui.jsf.component.TableRowGroup</code>
     * </p> <p>
     *
     * Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * </p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code>/</p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code>/</p>
     *
     * @param handlerCtx  <code>HandlerContext</code> provides inputs and
     *      outputs.
     */
    @Handler(
        id="jbiDeleteSelectedCompAppConfigRows",
        input={
            @HandlerInput(
                name="compName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="instanceName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="tableRowGroup",
                type=TableRowGroup.class,
                required=true) },
        output={
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="alertSummary", type=String.class) ,
            @HandlerOutput(name="alertDetails", type=String.class) })

        public static void jbiDeleteSelectedCompAppConfigRows(HandlerContext handlerCtx)
    {

        String compName = (String) handlerCtx.getInputValue("compName");
        String instanceName = (String) handlerCtx.getInputValue("instanceName");
        TableRowGroup trg = (TableRowGroup) handlerCtx.getInputValue("tableRowGroup");

        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(" +
                  compName + ", " + instanceName + ", " + trg + ")");
        }

        int countFailedDeletions = 0;
        int countSuccessfulDeletions = 0;
        int countWarningDeletions = 0;

        boolean isAlertNeeded = false;

        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        String alertType = AlertBean.ALERT_TYPE_ERROR;

        ObjectListDataProvider dp = (ObjectListDataProvider)
            trg.getSourceData();
        FieldKey fkName = dp.getFieldKey("name");
        RowKey[] rowKeys = trg.getSelectedRowKeys();

        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...)" +
                  ", dp=" + dp);

           sLog.finer("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...)" +
                  ", fkName=" + fkName);

           sLog.finer("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...)" +
                  ", rowKeys=" + rowKeys);
        }

        for (int cnt = 0; cnt < rowKeys.length; cnt++)
            {
                String compAppConfigName = (String)
                    dp.getValue(fkName, rowKeys[cnt]);
                
                if (sLog.isLoggable(Level.FINEST)) {
                    sLog.finest("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...)" +
                          ", rowKeys[" + cnt + " ].getValue(" + fkName + ").getValue=" +
                          compAppConfigName);

                    sLog.finest("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...)" +
                          ", delete compName/instanceName/compAppConfigName=" + compAppConfigName);
                }

                String result = null;

                try
                    {
                        JBIAdminCommands jac =
                            BeanUtilities.getClient();

                        result =
                            jac.deleteApplicationConfiguration(compName,
                                                               instanceName,
                                                               compAppConfigName);

                    }
                catch (com.sun.jbi.ui.common.JBIRemoteException jrEx)
                    {
                        sLog.log(Level.FINE,
                                 ("ComponentConfigurationHandlers.jbiDelteSelectedCompAppConfigRows(...) caught jrEx=" +
                                  jrEx),
                                 jrEx);
                    }
                
                if (sLog.isLoggable(Level.FINEST)) {
                    sLog.finest("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...), result=" + result);
                }

                String successResult = result;

                if (null != successResult)
                    {
                        ++countSuccessfulDeletions;
                        
                        if (sLog.isLoggable(Level.FINEST)) {
                            sLog.finest("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...), " +
                                  " success for " + compAppConfigName +
                                  ", countFailedDeletions=" + countFailedDeletions +
                                  ", countSuccessfulDeletions=" + countSuccessfulDeletions);
                        }
                    }
                else
                    {
                        String failureResult = null;

                        if ((null != failureResult)
                            && (failureResult.trim().startsWith("WARNING")))
                            {
                                ++countWarningDeletions;
                            }
                        else
                            {
                                ++countFailedDeletions;
                            }

                        if (sLog.isLoggable(Level.FINEST)) {
                           sLog.finest("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...), " +
                                  " failure for " + compAppConfigName +
                                  ", countFailedDeletions=" + countFailedDeletions +
                                  ", countSuccessfulDeletions=" + countSuccessfulDeletions);
                        }

                        String details = AlertBean.ALERT_DETAIL_NONE;
                        String exceptionMessage = AlertUtilities.getMessage(failureResult);
                        if ("".equals(exceptionMessage))
                            {
                                details = "" + failureResult;
                            }
                        else
                            {
                                details = "" + exceptionMessage;
                            }

                        Object[] args = {compAppConfigName, details};
                        String i18nMessage = 
                            I18nUtilities.getResourceString("jbi.show.comp.app.config.deletion.failed.for.row");
                        
                        if (sLog.isLoggable(Level.FINEST)) {
                           sLog.finest("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...)" +
                                  ", i18nMessage=" + i18nMessage +
                                  ", compAppConfigName=" + compAppConfigName +
                                  ", details=" + details);
                        }
                        if (null != i18nMessage)
                            {
                                alertDetails += GuiUtil.getMessage(i18nMessage, args) + 
                                    SharedConstants
                                    .HTML_BREAK;
                            }
                    }
            }

        String key = "";
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE;
        if (0 < countFailedDeletions)
            {
                if ((0 < countSuccessfulDeletions) || (0 < countWarningDeletions))
                    {
                        if (1 == countFailedDeletions)
                            {
                                key =
                                    "jbi.show.comp.app.config.delete.one.failed.alert.summary.text";
                                alertSummary =
                                    I18nUtilities
                                    .getResourceString(key);
                            }
                        else
                            {
                                key = 
                                    "jbi.show.comp.app.config.delete.some.failed.alert.summary.text";
                                alertSummary =
                                    I18nUtilities
                                    .getResourceString(key);
                            }
                    }
                else
                    {
                        key =
                            "jbi.show.comp.app.config.delete.all.failed.alert.summary.text";
                        alertSummary =
                            I18nUtilities
                            .getResourceString(key);
                    }
                alertDetails = BeanUtilities.addAlertFooterMessage(alertDetails);
            }
        else if (0 < countWarningDeletions)
            {
                alertSummary = 
                    I18nUtilities
                    .getResourceString("jbi.show.comp.app.config.deletion.warning.summary");

                AlertBean alertBean = BeanUtilities.getAlertBean();
                alertBean.setAlertType(AlertBean.ALERT_TYPE_WARNING);

            }

        if ((countFailedDeletions > 0) || (countWarningDeletions > 0))
            {
                isAlertNeeded = true;
            }
        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);

        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("ComponentConfigurationHandlers.jbiDeleteSelectedCompAppConfigRows(...), " +
                  " isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails);
        }
    }

    /**
     * <p>
     *
     * extracts the value of an option to a returned string <p>
     *
     * Input value: "option" -- Type: <code> com.sun.webui.jsf.model.Option</code>
     * </p> <p>
     *
     * Output value: "string" -- Type: <code>java.lang.String</code></p>
     *
     * @param handlerCtx  <code>HandlerContext</code> provides inputs and
     *      outputs.
     */
    @Handler(
        id="jbiOptionToString",
        input={
            @HandlerInput(
                name="option",
                type=Object.class,
                required=true) },
        output={
            @HandlerOutput(name="string", type=String.class) })

        public static void jbiOptionToString(HandlerContext handlerCtx)
    {
        String result = "";

        Object option = handlerCtx.getInputValue("option");

        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("ComponentConfigurationHandlers.jbiOptionToString()");
        }

        Object value = null;
        // option.getValue();

        if (value instanceof String)
            {
                result =
                    (String) value;
            }

        handlerCtx.setOutputValue("string", result);

        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("ComponentConfigurationHandlers.jbiOptionToString(), result=" +
                  result);
        }
    }

    /**
     * <p>
     *
     * This handler deletes the given <code>RowKey</code>s and named
     * Application Variables</p> <p>
     *
     * Input value: "instance" -- Type: <code> java.lang.String</code> target
     * instance <p>
     *
     * Input value: "component" -- Type: <code> java.lang.String</code> target
     * component <p>
     *
     * Input value: "tableRowGroup" -- Type: <code>com.sun.webui.jsf.component.TableRowGroup</code>
     * table group to find rows <p>
     *
     * Input value: "rowKeys" -- Type: <code>com.sun.data.provider.RowKey[].class</code>
     * selected row keys
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(id="jbiDeleteCompAppVarsTableRows",
             input={
                 @HandlerInput(
                     name="compName",
                     type=String.class,
                     required=true) ,
                 @HandlerInput(
                     name="instanceName",
                     type=String.class,
                     required=true) ,
                 @HandlerInput(
                     name="tableRowGroup",
                     type=TableRowGroup.class,
                     required=true) ,
                 @HandlerInput(
                     name="rowKeys",
                     type=RowKey[].class,
                     required=true) })
    public static void jbiDeleteCompAppVarsTableRows(HandlerContext handlerCtx)
    {
        String compName = (String) handlerCtx.getInputValue("compName");
        String instanceName = (String) handlerCtx.getInputValue("instanceName");
        TableRowGroup trg =
            (TableRowGroup) handlerCtx.getInputValue("tableRowGroup");
        RowKey[] keys = (RowKey[]) handlerCtx.getInputValue("rowKeys");
        
        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("ComponentConfigurationHandlers.jbiDeleteCompAppVarsTableRows()" +
                  ", keys=" + keys);
        }

        ObjectListDataProvider dp =
            (ObjectListDataProvider) trg.getSourceData();
        ArrayList toBeDeleted =
            new ArrayList();
        for (RowKey key : keys)
            {
                FieldKey fkName = dp.getFieldKey("name");
                String compAppVarName = (String)
                    dp.getValue(fkName, key);
                
                if (sLog.isLoggable(Level.FINEST)) {
                    sLog.finest("ComponentConfigurationHandlers.jbiDeleteCompAppVarsTableRows()" +
                          ", delete Row with id = " + key.getRowId() +
                          " and compAppVarName=" + compAppVarName);
                }
                
                toBeDeleted.add(compAppVarName);
                dp.removeRow(key);
            }

        String[] appVarNamesArray =
            (String[]) toBeDeleted.toArray(new String[]{});

        Properties result =
            AppVarUtils.deleteCompAppVars(compName,
                                          instanceName,
                                          appVarNamesArray);

        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("ComponentConfigurationHandlers.jbiDeleteCompAppVarsTableRows(), result=" +
                  result);
        }
    }

    /**
     * <p>
     *
     * This handler saves the updated component runtime configuration </p>
     *
     * @param aHandlerCtx  Description of Parameter
     */
    private static void saveCompAppConfig(HandlerContext aHandlerCtx)
    {
        String compName = (String) aHandlerCtx.getInputValue("compName");
        String compType = (String) aHandlerCtx.getInputValue("compType");
        String appConfigName = (String) aHandlerCtx.getInputValue("appConfigName");
        String currSelectedInstance = 
            (String) aHandlerCtx.getInputValue("currSelectedInstance");
        String redirectOnFailure = 
            (String) aHandlerCtx.getInputValue("redirectOnFailure");
        String redirectOnSuccess = 
            (String) aHandlerCtx.getInputValue("redirectOnSuccess");
        String redirectTo = redirectOnFailure;

        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aHandlerCtx,
                        compName,
                        compType,
                        appConfigName,
                        currSelectedInstance,
                        redirectOnFailure,
                        redirectOnSuccess,
                    };
                JBILogger.entering(CN, MN_SAVE_COMP_APP_CONFIG, 
                                   args);
            }

        AlertBean alertBean = 
            BeanUtilities.getAlertBean();
        String alertDetails =
            AlertBean.ALERT_DETAIL_NONE;
        String alertSummary =
            AlertBean.ALERT_SUMMARY_NONE;

        boolean isAlertNeeded = false;

        String updateOrCreateAction =
            SharedConstants
            .APP_CONFIG_ACTION_UPDATE;

        CompConfigBean compConfigBean =
            BeanUtilities.getCompConfigBean();

        // remove unchanged properties 
        Properties uneditedAppConfigProps =
            JBIUtils.getCompAppConfigProps(compName,
                                           currSelectedInstance,
                                           appConfigName);
        Properties editedAppConfigProps =
            compConfigBean.getCompAppConfigProperties();

        Properties changedAppConfigProps;

        if (null != uneditedAppConfigProps)
            {
                changedAppConfigProps =
                    new Properties();
                
                Enumeration uneditedPropertyNames =
                    uneditedAppConfigProps.propertyNames();
                
                String aryPropRootName =
                    null;
                while (uneditedPropertyNames.hasMoreElements())
                    {
                        String key =
                            (String) uneditedPropertyNames.nextElement();
                        String uneditedValue =
                            (String) uneditedAppConfigProps.get(key);
                        String editedValue = 
                            (String) editedAppConfigProps.get(key);
                        
                        if ((null != editedValue)
                            &&(!editedValue.equals(uneditedValue)))
                            {
                                changedAppConfigProps.put(key,
                                                          editedValue);
                                int offsetToDot =
                                    key.indexOf(".");
                                if ((0 < offsetToDot)
                                    &&(0 < editedValue.indexOf("=")))
                                    {
                                        aryPropRootName =
                                            key.substring(0, 
                                                          (offsetToDot +1));
                                            
                                    }
                            }
                    }

                if (sLog.isLoggable(Level.FINER)) {
                    sLog.finer("ComponentConfigurationHandlers.saveCompAppConfig()" +
                           ", aryPropRootName=" + aryPropRootName +
                           ", changedAppConfigProps.size()=" +
                           changedAppConfigProps.size());
                }

                // (workaround for bug where passing in only changed
                // rows of an array appears to succeed but actually no-ops)
                // If any row in an array changed,
                // passes in all rows
                if ((0 < changedAppConfigProps.size())
                    &&(null != aryPropRootName))
                    {
                        Enumeration editedAppConfigPropNames =
                            editedAppConfigProps.propertyNames();
                        while (editedAppConfigPropNames.hasMoreElements())
                            {
                                String key =
                                    (String) editedAppConfigPropNames
                                    .nextElement();
                                if (key.startsWith(aryPropRootName))
                                    {
                                        String editedValue =
                                            (String) editedAppConfigProps
                                            .get(key);
                                        changedAppConfigProps
                                            .put(key,
                                                 editedValue);
                                        
                                        if (sLog.isLoggable(Level.FINEST)) {
                                           sLog.finest("ComponentConfigurationHandlers.saveCompAppConfig()" +
                                                   ", adding all array elements " +
                                                   ", key=" + key +
                                                   ", editedValue=" + editedValue);
                                        }
                                    }
                            }
                    }
            }
        else
            {
                appConfigName =
                    (String) editedAppConfigProps
                    .get(ESBConstants
                         .APP_CONFIG_NAME_PROP);

                updateOrCreateAction =
                    SharedConstants
                    .APP_CONFIG_ACTION_CREATE;

                changedAppConfigProps =
                    editedAppConfigProps;
            }
                  
        // If any property values were changed,
        // saves the updated properties
        if (0 < changedAppConfigProps.size())
            {
                Properties result =
                    JBIUtils.updateCompAppCompConfig(updateOrCreateAction,
                                                     currSelectedInstance,
                                                     compName,
                                                     appConfigName,
                                                     changedAppConfigProps);

                Object successResult =
                    result.get(SharedConstants
                               .SUCCESS_RESULT);
                // If the update was successful,
                // sets up to redirect to the view for a success (a list view)
                // and issues a success alert
                if (null != successResult)
                    {
                        redirectTo = redirectOnSuccess;
                        isAlertNeeded = true; // for Success
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.app.config.save.succeeded");
                        alertDetails =
                            I18nUtilities
                            .getResourceString("jbi.app.config.changes.saved");
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_SUCCESS);
                    }
                // If the update failed,
                // uses existing redirect to the view for a failure (the edit view)
                // and issues an error alert
                else
                    {
                        Object failureResult =
                            result.get(SharedConstants
                                       .FAILURE_RESULT);
                        JBIManagementMessage mgmtMsg = null;
                        if (failureResult instanceof JBIRemoteException)
                            {
                                JBIRemoteException jrEx =
                                    (JBIRemoteException) failureResult;
                                mgmtMsg =
                                    BeanUtilities
                                    .extractJBIManagementMessage(jrEx);
                            }
                        isAlertNeeded = true;
                        alertSummary = 
                            I18nUtilities
                            .getResourceString("jbi.app.config.save.failed");
                        if (null != mgmtMsg)
                            {
                                alertDetails = mgmtMsg.getMessage();
                            }
                        else
                            {
                                alertDetails =
                                I18nUtilities
                                .getResourceString("jbi.app.config.internal.error");

                            }
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                    }

                if (isAlertNeeded)
                    {
                        aHandlerCtx
                            .setOutputValue("alertDetails", 
                                            alertDetails);
                        aHandlerCtx
                            .setOutputValue("alertSummary", 
                                            alertSummary);
                        aHandlerCtx
                            .setOutputValue("isAlertNeeded", 
                                            Boolean.toString(isAlertNeeded));

                    }

            }
            
        aHandlerCtx
            .setOutputValue("redirectTo", 
                            redirectTo);

        if (JBILogger.isLoggableFiner())
        {
            JBILogger.logFiner(CN, MN_SAVE_COMP_APP_CONFIG,
                               ", isAlertNeeded=" + isAlertNeeded +
                               ", alertSummary=" + alertSummary +
                               ", alertDetails=" + alertDetails +
                               ", redirectTo=" + redirectTo);
        }
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_SAVE_COMP_APP_CONFIG);
            }
    }

    /**
     * Saves the updated component runtime configuration
     *
     * @param aHandlerCtx  Description of Parameter
     */
    private static void saveCompRuntimeConfig(HandlerContext aHandlerCtx)
    {
        String compName = (String) aHandlerCtx.getInputValue("compName");
        String compType = (String) aHandlerCtx.getInputValue("compType");
        String currSelectedInstance = (String) aHandlerCtx.getInputValue("currSelectedInstance");

        if (JBILogger.isLoggableFiner())
            {
                Object[] args =
                    {
                        aHandlerCtx,
                        compName,
                        compType,
                        currSelectedInstance,
                    };
                JBILogger.entering(CN, MN_SAVE_COMP_RUNTIME_CONFIG, 
                                   args);
            }

        String alertDetails =
            AlertBean.ALERT_DETAIL_NONE;
        String alertSummary =
            AlertBean.ALERT_SUMMARY_NONE;

        boolean isAlertFooterNeeded = false;
        boolean isAlertNeeded = false;

        CompConfigBean compConfigBean =
            BeanUtilities.getCompConfigBean();

        // remove unchanged properties
        Properties uneditedConfigProps =
            JBIUtils.getCompConfigProps(compName,
                                        currSelectedInstance);
        
        Properties editedConfigProps =
            compConfigBean.getCompRuntimeConfigProperties();
        
        Properties changedConfigProps =
            new Properties();
        
        Enumeration uneditedPropertyNames =
            uneditedConfigProps.propertyNames();

        while (uneditedPropertyNames.hasMoreElements())
            {
                String key =
                    (String) uneditedPropertyNames.nextElement();
                String uneditedValue =
                    (String) uneditedConfigProps.get(key);
                String editedValue = 
                    (String) editedConfigProps.get(key);

                if ((null != editedValue)
                    &&(!editedValue.equals(uneditedValue)))
                    {
                        changedConfigProps.put(key,
                                               editedValue);
                    }
            }
        
        
        
        // save updated properties 
        if (0 < changedConfigProps.size())
            {
                Properties status =
                    JBIUtils.updateCompRuntimeCompConfig(currSelectedInstance,
                                                         compName,
                                                         changedConfigProps);
                
                // check result->alert
                Object failureResult =
                    status.get(SharedConstants
                               .FAILURE_RESULT);
                
                // If an error was detected,
                // shows an alert.
                if (null != failureResult)
                    {
                        isAlertNeeded = true;
                        alertSummary = 
                            I18nUtilities
                            .getResourceString("jbi.save.comp.rt.config.failed.alert.summary.text");
                        
                        if (failureResult instanceof Exception)
                            {
                                Exception ex =
                                    (Exception) failureResult;
                                JBIManagementMessage
                                    mgmtMessage =
                                    BeanUtilities
                                    .extractJBIManagementMessage(ex);
                                alertDetails =
                                    mgmtMessage.getMessage();
                            }
                        if (AlertBean
                            .ALERT_DETAIL_NONE
                            .equals(alertDetails))
                            {
                                alertDetails = 
                                    failureResult.toString();
                            }
                
                        isAlertFooterNeeded = true;
                    }
                

                // Processes on-change-messages and restart-required-messages

                List<ConfigPropertyGroup> configGroups =
                    CompConfigUtils.getMetadataConfigExtensions(compName,
                                                                ESBConstants
                                                                .ELT_CFG);
        
                List<ConfigProperty> changedConfigProperties =
                    new ArrayList<ConfigProperty>();                

                // Processes all potentially-changed groups
                for (int i = 0; i < configGroups.size(); ++i)
                    {
                        ConfigPropertyGroup group =
                            configGroups.get(i);

                        // Processes all potentially-changed properties in a group
                        List<ConfigProperty> configProps =
                            group.getConfigProperties();
                        for (int j = 0; j < configProps.size(); ++j)
                            {
                                ConfigProperty prop =
                                    configProps.get(j);
                                String candidateName =
                                    prop.getName();
                                
                                // Scan actually-changed property names 
                                Enumeration propNamesEnum =
                                    changedConfigProps.propertyNames();
                                while (propNamesEnum.hasMoreElements())
                                    {
                                        // If a changed name is found, add its
                                        // ConfigProperty to the list of changed properties
                                        String changedConfigPropName = 
                                            (String) propNamesEnum.nextElement();
                                        if (candidateName
                                            .equals(changedConfigPropName))
                                            {
                                                changedConfigProperties.add(prop);
                                                break;
                                            }
                                    }
                            }

                    }

                // Check the actually-changed property descriptions for required messages
                String allOnChangeMessages = "";
                boolean isAppRestartRequired = false;
                boolean isCompRestartRequired = false;
                boolean isServerRestartRequired = false;
                if (0 < changedConfigProps.size())
                    {
                        for (int k = 0; k < changedConfigProperties.size(); ++k)
                            {
                                ConfigProperty changedConfigProp =
                                    changedConfigProperties.get(k);
                                String onChangeMessage =
                                    changedConfigProp.getOnChangeMessage();
                                if (null != onChangeMessage)
                                    {
                                        allOnChangeMessages +=
                                            SharedConstants
                                            .HTML_BREAK +
                                            onChangeMessage;
                                    }
                                if (changedConfigProp
                                    .getIsAppRestartRequired())
                                    {
                                        isAppRestartRequired = true;
                                    }
                                if (changedConfigProp
                                    .getIsCompRestartRequired())
                                    {
                                        isCompRestartRequired = true;
                                    }
                                if (changedConfigProp
                                    .getIsServerRestartRequired())
                                    {
                                        isServerRestartRequired = true;
                                    }
                            }
                    }
                
                // If a server restart is required,
                // adds a "One or more changed properties require a server restart" 
                // message to the alert details
                if (isServerRestartRequired)
                    {
                        alertDetails +=
                            SharedConstants
                            .HTML_BREAK +
                            I18nUtilities
                            .getResourceString("jbi.comp.config.restart.required.for.server");
                    }

                // If a component restart is required,
                // adds a "One or more changed properties require a component restart" 
                // message to the alert details
                if (isCompRestartRequired)
                    {
                        alertDetails +=
                            SharedConstants
                            .HTML_BREAK +
                            I18nUtilities
                            .getResourceString("jbi.comp.config.restart.required.for.comp");
                    }

                // If an application restart is required,
                // adds a "One or more changed properties require an application restart" 
                // message to the alert details
                if (isAppRestartRequired)
                    {
                        alertDetails +=
                            SharedConstants
                            .HTML_BREAK +
                            I18nUtilities
                            .getResourceString("jbi.comp.config.restart.required.for.app");
                    }

                // If a one or more properties added on-change messages,
                // adds all of the on-change messages to the alert details
                if (!"".equals(allOnChangeMessages))
                    {
                        // alertDetails already starts with HTML BREAK (if nonempty)
                        alertDetails +=
                            allOnChangeMessages; 
                    }

                // If there are any alert details to be displayed,
                // shows a "User Action Required" summary message
                if (!AlertBean
                    .ALERT_DETAIL_NONE.equals(alertDetails))
                    {
                        isAlertNeeded = true;
                        alertSummary =
                            I18nUtilities
                            .getResourceString("jbi.comp.config.restart.required.user.action");

                        if (isAlertFooterNeeded)
                            {
                                alertDetails = 
                                    BeanUtilities
                                    .addAlertFooterMessage(alertDetails);
                            }
                        AlertBean alertBean = BeanUtilities.getAlertBean();
                        alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);

                        aHandlerCtx.setOutputValue("alertDetails", alertDetails);
                        aHandlerCtx.setOutputValue("alertSummary", alertSummary);
                        aHandlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
                    }

                if (sLog.isLoggable(Level.FINE)) {
                    sLog.fine(CN + "." + MN_SAVE_COMP_RUNTIME_CONFIG +
                          ", allOnChangeMessages=" + allOnChangeMessages +
                          ", changedConfigProps=" + changedConfigProps +
                          ", isAppRestartRequired=" + isAppRestartRequired +
                          ", isCompRestartRequired=" + isCompRestartRequired +
                          ", isServerRestartRequired=" + isServerRestartRequired);
                }
            }

            
        if (JBILogger.isLoggableFiner())
            {
                JBILogger.exiting(CN,
                                  MN_SAVE_COMP_RUNTIME_CONFIG);
            }
    }

    /**
     * prevents instantiation and subclasssing
     */
    private ComponentConfigurationHandlers()
    {
    }

    /**
     * Set up a flag for cluster profile support.
     */
    private static final boolean IS_CLUSTER_PROFILE =
        ClusterUtilities.isClusterProfile();

    private static final String CN = 
        ComponentConfigurationHandlers.class.getName();
    private static final String MN_JBI_SAVE_COMPONENT_CONFIGURATION =
        "jbiSaveComponentConfiguration";
    private static final String MN_SAVE_COMP_RUNTIME_CONFIG = 
        "saveCompRuntimeConfig";
    private static final String MN_SAVE_COMP_APP_CONFIG = 
        "saveCompAppConfig";

    private static Logger sLog = JBILogger.getInstance();

}
