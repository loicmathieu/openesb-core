/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
package com.sun.jbi.jsf.handlers;

import com.sun.enterprise.tools.admingui.handlers.ConfigurationHandlers;
import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.jbi.jsf.bean.AlertBean;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.ClusterUtilities;
import com.sun.jbi.jsf.util.I18nUtilities;
import com.sun.jbi.jsf.util.JBIConstants;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jsftemplating.annotation.Handler;
import com.sun.jsftemplating.annotation.HandlerInput;
import com.sun.jsftemplating.annotation.HandlerOutput;
import com.sun.jsftemplating.layout.descriptors.handler.HandlerContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import javax.faces.model.SelectItem;
import java.util.logging.Level;

/**
 * Provides jsftemplating handlers for instance selection.
 *
 * @author   Sun Microsystems Inc.
 */
public final class InstanceSelectionHandlers
{
    /**
     * <p>
     *
     * Returns the values for all(started and stopped) instances list <p>
     *
     * Output value: "instancesList" -- Type: <code>SelectItem[]</code></p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiGetAllInstances",
        input={
            @HandlerInput(
                name="addDomainFlag",
                type=Boolean.class,
                required=false) },
        output={
            @HandlerOutput(name="instancesList", type=SelectItem[].class) })

        public static void jbiGetAllInstances(HandlerContext handlerCtx)
    {
        List targetsList = ClusterUtilities.findAllNonDomainTargets();
        List allInstancesList = ClusterUtilities.getAllInstances(targetsList);
        Collections.sort(allInstancesList);
        
        String[] instancesArray = new String[0];
        instancesArray = (String[]) allInstancesList.toArray(instancesArray);
        SelectItem[] options = ConfigurationHandlers.getOptions(instancesArray);
        handlerCtx.setOutputValue("instancesList", options);
        
        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("InstanceSelectionHandlers.jbiGetAllInstances(), options.length=" +
                  options.length);
        }
    }

    /**
     * <p>
     *
     * Returns the values for runtime instances list <p>
     *
     * Output value: "InstancesList" -- Type: <code>SelectItem[]</code></p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiGetRuntimeInstances",
        input={
            @HandlerInput(
                name="addDomainFlag",
                type=Boolean.class,
                required=false) },
        output={
            @HandlerOutput(name="instancesList", type=SelectItem[].class) })

        public static void jbiGetRuntimeInstances(HandlerContext handlerCtx)
    {
        List targetsList = ClusterUtilities.findAllNonDomainTargets();

        List allInstancesList =
            ClusterUtilities
            .getAllStartedInstances(targetsList);

        Collections.sort(allInstancesList);

        String[] instancesArray =
            new String[0];
        instancesArray =
            (String[]) allInstancesList.toArray(instancesArray);
        SelectItem[] options =
            ConfigurationHandlers.getOptions(instancesArray);

        handlerCtx.setOutputValue("instancesList", options);
        
        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("InstanceSelectionHandlers.jbiGetRuntimeInstances(), options.length=" +
                  options.length);
        }
    }

    /**
     * <p>
     *
     * Returns the values for runtime configuration targets list <p>
     *
     * Input value: "addDomainFlag" <code>Boolean</code> true if "domain"
     * should appear in the returned list of targets <p>
     *
     * Output value: "TargetsList" -- Type: <code>SelectItem[]</code></p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiGetRuntimeTargets",
        input={
            @HandlerInput(
                name="addDomainFlag",
                type=Boolean.class,
                required=false) },
        output={
            @HandlerOutput(name="targetsList", type=SelectItem[].class) })

        public static void jbiGetRuntimeTargets(HandlerContext handlerCtx)
    {
        boolean addDomain = true;
        Boolean addDomainFlag = (Boolean) handlerCtx.getInputValue("addDomainFlag");
        if (addDomainFlag != null)
            {
                addDomain = addDomainFlag;
            }

        List targetsList = ClusterUtilities.findAllNonDomainTargets();
        Collections.sort(targetsList);
        if (addDomain)
            {
                targetsList.add(0, "domain");
            }

        String[] targetsArray =
            new String[0];
        targetsArray =
            (String[]) targetsList.toArray(targetsArray);
        SelectItem[] options = ConfigurationHandlers.getOptions(targetsArray);

        handlerCtx.setOutputValue("targetsList", options);
        
        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("InstanceSelectionHandlers.jbiGetRuntimeTargets(), options.length=" +
                  options.length);
        }
    }


    /**
     * <p>
     *
     * Returns an alert if the target is not running
     *
     * Input value: "targetName" -- Type: <code>String</code></p> 
     * The name of the target to be checked
     *
     * Output value: "isAlertNeeded" -- Type: <code>Boolean</code></p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code></p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code></p>
     *
     * @param handlerCtx  the context used for inputs and outputs
     */
    @Handler(
        id="jbiAlertIfTargetNotRunning",
        input={
            @HandlerInput(
                name="targetName",
                type=String.class,
                required=true) },
        output={
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="alertSummary", type=String.class) ,
            @HandlerOutput(name="alertDetails", type=String.class) })

        public static void jbiAlertIfTargetNotRunning(HandlerContext handlerCtx)
    {
        String targetName = (String) handlerCtx.getInputValue("targetName");
        
        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("InstanceSelectionHandlers.jbiAlertIfTargetNotRunning()" +
                  ", targetName=" + targetName);
        }

        AlertBean alertBean = BeanUtilities.getAlertBean();
        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE;
        boolean isAlertNeeded = false;

        if (!ClusterUtilities.isInstanceRunning(targetName))
            {
                isAlertNeeded = true;

                alertBean
                    .setAlertType(AlertBean
                                  .ALERT_TYPE_INFO);

                alertSummary =
                    I18nUtilities
                    .getResourceString("jbi.instance.info.not.available");
                alertDetails =
                    I18nUtilities
                    .getResourceString("jbi.instance.not.running");
            }
        
        // If there is no new alert,
        // shows any old alert
        if (!isAlertNeeded)
            {
                // If there is a pending alert,
                // shows the pending alert summary and details, if any 
                // (If there is an alert pending but missing a summary or details,
                // provides internal error defaults)
                if (AlertBean.isSessionAlertNeeded())
                    {
                        isAlertNeeded = true;

                        String defaultSummaryKey =
                            "jbi.instance.internal.error";
                        String defaultSummary =
                            I18nUtilities
                            .getResourceString(defaultSummaryKey);
                        alertSummary = 
                            AlertBean
                            .getSessionAlertSummary(defaultSummary);

                        // If the pending alert is an error or a warning
                        // but has no details, add details:
                        // "internal error details unknown"
                        // (else leave details blank for info or success)
                        if ((AlertBean
                             .ALERT_TYPE_ERROR
                             .equals(alertBean.getAlertType()))
                            ||(AlertBean
                               .ALERT_TYPE_WARNING
                               .equals(alertBean.getAlertType())))
                            {
                                String defaultDetailsKey =
                                    "jbi.instance.internal.error.details.unknown";
                                String defaultDetails =
                                    I18nUtilities
                                    .getResourceString(defaultDetailsKey);
                                alertDetails = 
                                    AlertBean
                                    .getSessionAlertDetail(defaultDetails);
                            }
                    }
            }

        handlerCtx.setOutputValue("isAlertNeeded", isAlertNeeded);
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        
        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("InstanceSelectionHandlers.jbiAlertIfTargetsNotRunning()" +
                  ", isAlertNeeded=" + isAlertNeeded +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails);
        }
    }

    /**
     * <p>
     *
     * Returns the values for instances in configureComponent page <p>
     *
     * Input value: "type" -- Type: <code>String</code></p> one of: <code>binding-component</code>
     * , <code>service-engine</code>, or <code>service-assembly</code> <p>
     *
     * Input value: "compOrSaName" -- Type: <code>String</code></p> <p>
     *
     * Input value: "prevSelectedInstanceIfAny" -- Type: <code>String</code></p> <p>
     *
     * Output value: "InstanceList" -- Type: <code>SelectItem[]</code></p> <p>
     *
     * Output value: "FirstInstance" -- Type: <code>String</code></p> <p>
     *
     * Output value: "isAlertNeeded" -- Type: <code>Boolean</code></p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code></p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code></p> <p>
     *
     * Output value: "installedOnZeroTargets" -- Type: <code>java.lang.Boolean</code> render instance 
     * list indicator</p>
     * 
     * @param handlerCtx  the context used for inputs and outputs
     */
    @Handler(
        id="jbiGetInstances",
        input={
            @HandlerInput(
                name="type",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compOrSaName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="prevSelectedInstanceIfAny",
                type=String.class,
                required=false) },
        output={
            @HandlerOutput(name="instancesList", type=SelectItem[].class) ,
            @HandlerOutput(name="firstInstance", type=String.class) ,
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="alertSummary", type=String.class) ,
            @HandlerOutput(name="alertDetails", type=String.class),
            @HandlerOutput(name="installedOnZeroTargets", type=Boolean.class)})

        public static void jbiGetInstances(HandlerContext handlerCtx)
    {
        String type = (String) handlerCtx.getInputValue("type");
        String compOrSaName = (String) handlerCtx.getInputValue("compOrSaName");
        String prevSelectedInstanceIfAny = 
            (String) handlerCtx.getInputValue("prevSelectedInstanceIfAny");

        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("InstanceSelectionHandlers.jbiGetInstances(), type=" +
                  type + ", compOrSaName=" + compOrSaName +
                  ", prevSelectedInstanceIfAny=" + prevSelectedInstanceIfAny);
        }

        List installedTargetsList = new ArrayList();
        List installedList = ClusterUtilities.findTargetsForNameByType(compOrSaName,
                                                                       type);
        // Construct the installedTargetsList
        Iterator targetsIterator = installedList.iterator();
        while (targetsIterator.hasNext())
            {
                Properties targetProp = (Properties) targetsIterator.next();
                String target = targetProp.getProperty(SharedConstants.KEY_NAME);
                installedTargetsList.add(target);
            }

        List installedInstancesList =
            ClusterUtilities
            .getAllInstances(installedTargetsList);

        Collections.sort(installedInstancesList);

        // Construct the warning message if component is not installed on any target
        AlertBean alertBean = BeanUtilities.getAlertBean();
        String alertDetails = AlertBean.ALERT_DETAIL_NONE;
        String alertSummary = AlertBean.ALERT_SUMMARY_NONE;
        boolean isAlertNeeded = false;
        boolean installedOnZeroTargets = true;
        
        if (installedInstancesList.size() == 0)
            {
                isAlertNeeded = true;
                alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                if (JBIConstants
                    .JBI_SERVICE_ASSEMBLY_TYPE
                    .equals(type))
                    {
                        alertSummary =
                            I18nUtilities.getResourceString("jbi.sa.not.installed.to.target.summary");
                    }
                else
                    {
                        alertSummary =
                            I18nUtilities.getResourceString("jbi.component.not.installed.to.target.summary");
                    }
                if (null == compOrSaName)
                    {
                        compOrSaName = "";
                        // prevent NPE in GuiUtil
                    }
                Object[] args = {compOrSaName};
                alertDetails = 
                    GuiUtil
                    .getMessage(I18nUtilities
                                .getResourceString("jbi.component.not.installed.to.target.details"), 
                                args);
            }

        // Return the options list
        String[] instances =
            new String[0];
        instances = (String[]) installedInstancesList.toArray(instances);
        SelectItem[] options = ConfigurationHandlers.getOptions(instances);
        if ( options != null && options.length > 0) {
        	installedOnZeroTargets = false;
        }
        
        // If the instance list has entries,
        // Returns the first instance in the list
        String firstInstance = 
            JBIAdminCommands
            .SERVER_TARGET_KEY;
        if (0 < installedInstancesList.size())
            {
                firstInstance =
                    (String) installedInstancesList
                    .get(0);
            }

        // If the previously selected instance was specified
        // and is valid, 
        // uses it instead of the first
        if ((null != prevSelectedInstanceIfAny)
            &&(installedInstancesList.contains(prevSelectedInstanceIfAny)))
            {
                firstInstance =
                    prevSelectedInstanceIfAny;
            }

        handlerCtx.setOutputValue("instancesList", options);
        handlerCtx.setOutputValue("firstInstance", firstInstance);
        handlerCtx.setOutputValue("isAlertNeeded", isAlertNeeded);
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("installedOnZeroTargets", installedOnZeroTargets);
        
        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("InstanceSelectionHandlers.jbiGetInstances(), options.length=" + options.length);
        }        
    }

    /**
     * <p>
     *
     * Returns the values for started instances for a given component type
     * and name
     *
     * Input value: "type" -- Type: <code>String</code></p> one of: 
     * <code>binding-component</code>, <code>service-engine</code>, or
     * <code>service-assembly</code> <p>
     *
     * Input value: "compName" -- Type: <code>String</code></p> <p>
     *
     * Output value: "InstanceList" -- Type: <code>SelectItem[]</code></p> <p>
     *
     * @param handlerCtx  the context used for inputs and outputs
     */
    @Handler(
        id="jbiGetStartedInstances",
        input={
            @HandlerInput(
                name="type",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="compName",
                type=String.class,
                required=true) },
        output={
            @HandlerOutput(name="instancesList", type=SelectItem[].class) ,
            @HandlerOutput(name="firstInstance", type=String.class) })

        public static void jbiGetStartedInstances(HandlerContext handlerCtx)
    {
        String type = (String) handlerCtx.getInputValue("type");
        String compName = (String) handlerCtx.getInputValue("compName");
        
        if (sLog.isLoggable(Level.FINER)) {
            sLog.finer("InstanceSelectionHandlers.jbiGetStartedInstances()" +
                  ", type=" + type + 
                  ", compName=" + compName);
        }

        List installedTargetsList = new ArrayList();
        List installedList = 
            ClusterUtilities
            .findTargetsForNameByType(compName,
                                      type);

        // Construct the installedTargetsList
        Iterator targetsIterator = installedList.iterator();
        while (targetsIterator.hasNext())
            {
                Properties targetProp = (Properties) targetsIterator.next();
                String target = 
                    targetProp.getProperty(SharedConstants.KEY_NAME);
                installedTargetsList.add(target);
            }

        List installedStartedInstancesList =
            ClusterUtilities
            .getAllStartedInstances(installedTargetsList);

        Collections.sort(installedStartedInstancesList);

        // Return the options list
        String[] startedInstances =
            new String[0];
        startedInstances = 
            (String[]) installedStartedInstancesList
            .toArray(startedInstances);
        SelectItem[] options = 
            ConfigurationHandlers.getOptions(startedInstances);

        handlerCtx.setOutputValue("instancesList", options);

        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("InstanceSelectionHandlers.jbiGetStartedInstances()" +
                  ", options.length=" + options.length);
        }
    }


    /**
     * <p>
     *
     * This handler sets the values for instances to be updated in
     * configureComponent page <p>
     *
     * Input value: "fromInstance" -- Type: <code>java.lang.String</code></p>
     * <p>
     *
     * Output value: "toInstances" -- Type: <code>java.lang.String[]</code>
     * </p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiSetDefaultSelectedInstances",
        input={
            @HandlerInput(
                name="fromInstance",
                type=String.class,
                required=true) },
        output={
            @HandlerOutput(name="toInstances", type=String[].class) })

        public static void jbiSetDefaultSelectedInstances(HandlerContext handlerCtx)
    {
        String[] result =
            new String[0];
        String fromInstance = (String) handlerCtx.getInputValue("fromInstance");
        result[0] = fromInstance;
        
        if (sLog.isLoggable(Level.FINE)) {
           sLog.fine("InstanceSelectionHandlers.jbiSetDefaultSelectedInstances(" + 
                  fromInstance + "), result.length=" + result.length);
        }
        
        handlerCtx.setOutputValue("toInstances", result);
    }


    /**
     * <p>
     *
     * This converts an object array (containing strings) to a string array
     * <p>
     *
     * Input value: "fromObjectArray" -- Type: <code>java.lang.Object[]</code>
     * </p> <p>
     *
     * Output value: "toStringArray" -- Type: <code>java.lang.String[]</code>
     * </p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiObjectArrayToStringArray",
        input={
            @HandlerInput(
                name="fromObjectArray",
                type=Object[].class,
                required=true) },
        output={
            @HandlerOutput(name="toStringArray", type=String[].class) })

        public static void jbiObjectArrayToStringArray(HandlerContext handlerCtx)
    {
        String[] result;
        Object[] fromObjectArray = (Object[]) handlerCtx.getInputValue("fromObjectArray");
        if (null != fromObjectArray)
            {
                result = new String[fromObjectArray.length];
                for (int i = 0; i < result.length; ++i)
                    {
                        result[i] = (String) fromObjectArray[i];
                    }
            }
        else
            {
                result = new String[0];
            }
        
        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("InstanceSelectionHandlers.jbiObjectArrayToStringArray(" +
                  fromObjectArray + "), result.length=" + result.length);
        }
        handlerCtx.setOutputValue("toStringArray", result);
    }


    /**
     * <p>
     *
     * This handler converts an array of strings to a printable comma
     * separated list in one string <p>
     *
     * Input value: "fromStringArray" -- Type: <code>java.lang.String[]</code>
     * </p> <p>
     *
     * Output value: "toStringWithCommas" -- Type: <code>java.lang.String</code>
     * </p>
     *
     * @param handlerCtx  Description of Parameter
     */
    @Handler(
        id="jbiListArrayStrings",
        input={
            @HandlerInput(
                name="fromStringArray",
                type=Object[].class,
                required=true) },
        output={
            @HandlerOutput(name="toStringWithCommas", type=String.class) })

        public static void jbiListArrayStrings(HandlerContext handlerCtx)
    {
        String result = "list: [";
        Object[] fromStringArray = (Object[]) handlerCtx.getInputValue("fromStringArray");
        for (int i = 0; i < fromStringArray.length; ++i)
            {
                result += (String) fromStringArray[i];
                result += ", ";
            }
        result += " ]";

        if (sLog.isLoggable(Level.FINE)) {
            sLog.fine("InstanceSelectionHandlers.jbiListArrayStrings(" + fromStringArray + "), result=" + result);
        }
        
        handlerCtx.setOutputValue("toStringWithCommas", result);
    }

    /**
     * prevents instantiation
     */
    private InstanceSelectionHandlers()
    {
    }

    private static Logger sLog = JBILogger.getInstance();
}
