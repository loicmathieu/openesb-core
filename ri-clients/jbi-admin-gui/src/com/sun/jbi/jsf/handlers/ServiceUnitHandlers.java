/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package com.sun.jbi.jsf.handlers;

import com.sun.jbi.jsf.bean.ListBean;
import com.sun.jbi.jsf.bean.ServiceUnitBean;
import com.sun.jbi.jsf.bean.SelectableServiceUnitInfo;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jsftemplating.annotation.Handler;
import com.sun.jsftemplating.annotation.HandlerInput;
import com.sun.jsftemplating.layout.descriptors.handler.HandlerContext;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.ServiceUnitInfo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Provides jsftemplating handlers for List/Show tables
 */
public final class ServiceUnitHandlers
{
    public static final String TBL_PAGE_TYPE_BINDINGS_ENGINES  = "bindingsEngines";
    public static final String TBL_PAGE_TYPE_DEPLOYMENTS       = "deployments";
    public static final String TBL_PAGE_TYPE_LIBRARIES         = "libraries";

    private static Logger sLog = JBILogger.getInstance();

    /**
     * creates a selectable wrapper representation of a Service Unit
     * @param aServiceUnitInfo to be wrapped
     * @param aSaName the name of the containing Service Assembly
     * @param aTarget the name of a glassfish instance
     * @return a selectable wrapper for the specified Service Unit
     */
    private static SelectableServiceUnitInfo createSelectableServiceUnitInfo (
        ServiceUnitInfo aServiceUnitInfo,
        String aSaName,
        String aTarget)
    {
        String suName   = aServiceUnitInfo.getName();
        String suDesc   = aServiceUnitInfo.getDescription();
        String suTgt    = aServiceUnitInfo.getTargetName();
        String suState  = aServiceUnitInfo.getState();

        SelectableServiceUnitInfo selectableServiceUnit =
            new SelectableServiceUnitInfo(suName, suDesc, suTgt, suState, aSaName);

        String compType = BeanUtilities.getComponentType(suTgt,aTarget);
        selectableServiceUnit.setComponentType (compType);

        return selectableServiceUnit;
    }


    /**
     *  <p> This method sets the information for the service unit in the show bean.
     *  <p> Input value: "serviceUnitName" -- Type: <code>java.lang.String</code></p>
     *  @param  handlerCtx The HandlerContext.
     */
    @Handler (id="jbiSetServiceUnitInfo",
              input={
                  @HandlerInput (name="serviceUnitName", type=String.class, required=true),
                  @HandlerInput (name="serviceAssemblyName", type=String.class, required=true),
                  @HandlerInput (name="componentType", type=String.class, required=true),
                  @HandlerInput (name="targetComponent", type=String.class, required=true)})

    public static void  jbiSetServiceUnitInfo(HandlerContext handlerCtx)
    {
        String serviceUnitName     = (String)handlerCtx.getInputValue ("serviceUnitName");
        String serviceAssemblyName = (String)handlerCtx.getInputValue ("serviceAssemblyName");
        String componentType       = (String)handlerCtx.getInputValue ("componentType");
        String targetComponent     = (String)handlerCtx.getInputValue ("targetComponent");

        if (sLog.isLoggable(Level.FINER)) {
           sLog.finer("QueryHandlers.jbiGetServiceUnitInfo(...), serviceUnitName=" + serviceUnitName);
        }
        
        ServiceUnitBean serviceUnitBean = BeanUtilities.getServiceUnitBean();
        serviceUnitBean.setName(serviceUnitName);
        serviceUnitBean.setServiceAssemblyName(serviceAssemblyName);
        serviceUnitBean.setComponentType(componentType);
        serviceUnitBean.setTargetComponent(targetComponent);
                
        try {
            JBIAdminCommands mJac = BeanUtilities.getClient();
            String xml = mJac.listServiceAssemblies(targetComponent,
                                                    JBIAdminCommands.DOMAIN_TARGET_KEY);
            List saInfoList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xml);
            
            if ((null != saInfoList)&&(0 == saInfoList.size()))
            {
                xml = mJac.listServiceAssemblies(targetComponent,
                                                    JBIAdminCommands.SERVER_TARGET_KEY);
                saInfoList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xml);
            }
            
            if (saInfoList != null) {
                Iterator<Object> it = saInfoList.iterator();
                boolean done = false;
                while (it.hasNext() && !done) {
                    Object saInfoObject = it.next();
                    String saName = ((ServiceAssemblyInfo)saInfoObject).getName();
                    if (serviceAssemblyName.equals(saName))
                    {
                    	List infoList = ((ServiceAssemblyInfo)saInfoObject).getServiceUnitInfoList();
                    	Iterator<Object> it2 = infoList.iterator();
                    	while (it2.hasNext()) {
                    		Object suInfoObject = it2.next();
                    		String suName = ((ServiceUnitInfo)suInfoObject).getName();
                    		if (suName.equals(serviceUnitName)){
                            	serviceUnitBean.setDescription(((ServiceUnitInfo)suInfoObject).getDescription());
                            	serviceUnitBean.setState(((ServiceUnitInfo)suInfoObject).getState());
                            	done = true;
                            	break;
                    		}
                    			
                    	}
                        
                    }
                }
            }
        }
        catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
        {
            if (sLog.isLoggable(Level.FINE)) {
        	sLog.log(Level.FINE,
                     ("ServiceUnitHandlers.jbiSetServiceUnitInfo()" +
                      ", caught jbiRemoteEx=" + jbiRemoteEx),
                     jbiRemoteEx);
            }
        }

    }


    /**
     *  <p> This method initializes the Service Unit Bean for listing
     * the service units.
     *  <p> Input target: "String" -- Type: <code>String.class</code></p>
     *  <p> Input pageType: "String" -- Type: <code>String.class</code></p>
     *  <p> Input name: "String" -- Type: <code>String.class</code></p>
     *  @param handlerCtx The HandlerContext.
     */
    @Handler (id="jbiInitServieAssemblyServiceUnitsList",
              input={
                  @HandlerInput (name="target",  type=String.class, required=false),
                  @HandlerInput (name="pageType",  type=String.class, required=true),
                  @HandlerInput (name="name",  type=String.class, required=true)})

    public static void  jbiInitServieAssemblyServiceUnitsList (HandlerContext handlerCtx)
    {
        String pageType = (String)handlerCtx.getInputValue ("pageType");
        String name = (String)handlerCtx.getInputValue ("name");
        String target = (String)handlerCtx.getInputValue ("target");

        ListBean  listBean = BeanUtilities.getListBean();
        ServiceUnitBean  serviceUnitBean = BeanUtilities.getServiceUnitBean();
        List filteredServiceUnitList = new ArrayList();
        String saName = "";

        String xmlQueryResults = listBean.getListServiceAssemblies();

        if (pageType.equals(TBL_PAGE_TYPE_DEPLOYMENTS))
        {
            List saInfoList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xmlQueryResults);
            if (saInfoList != null) {
                Iterator<Object> it = saInfoList.iterator();
                while (it.hasNext()) {
                    Object saInfoObject = it.next();
                    saName = ((ServiceAssemblyInfo)saInfoObject).getName();
                    if (name.equals(saName))
                    {
                        List infoList = ((ServiceAssemblyInfo)saInfoObject).getServiceUnitInfoList();
                        Iterator<Object> it2 = infoList.iterator();
                        while (it2.hasNext()) {
                            Object suInfoObject = it2.next();

                            SelectableServiceUnitInfo selectableServiceUnit =
                                createSelectableServiceUnitInfo ((ServiceUnitInfo)suInfoObject,
                                                             saName,
                                                             target);
                            selectableServiceUnit.setQueryString("sa");
                            filteredServiceUnitList.add(selectableServiceUnit);
                        }
                        break;  // There can only be one match, so no use continuing
                    }
                }
            }
        }

        else if (pageType.equals(TBL_PAGE_TYPE_BINDINGS_ENGINES))
        {
            try {
                JBIAdminCommands mJac = BeanUtilities.getClient();
                String xml = mJac.listServiceAssemblies(name,target);
                List saInfoList = ServiceAssemblyInfo.readFromXmlTextWithProlog(xml);
                if (saInfoList != null) {
                    Iterator<Object> it = saInfoList.iterator();
                    while (it.hasNext()) {
                        Object saInfoObject = it.next();
                        List infoList = ((ServiceAssemblyInfo)saInfoObject).getServiceUnitInfoList();
                        saName = ((ServiceAssemblyInfo)saInfoObject).getName();
                        Iterator<Object> it2 = infoList.iterator();
                        while (it2.hasNext()) {
                            Object suInfoObject = it2.next();
                            String tgtName = ((ServiceUnitInfo)suInfoObject).getTargetName();
                            if (tgtName.equals(name))
                            {
                                SelectableServiceUnitInfo selectableServiceUnit =
                                    createSelectableServiceUnitInfo ((ServiceUnitInfo)suInfoObject,
                                                                 saName,
                                                                 target);
                                selectableServiceUnit.setQueryString("comp");
                                filteredServiceUnitList.add(selectableServiceUnit);
                            }
                        }
                    }
                }
            }
            catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
            {
                if (sLog.isLoggable(Level.FINE)) {
                sLog.log(Level.FINE,
                         ("ServiceUnitHandlers.jbiInitServieAssemblyServiceUnitsList()" +
                          ", caught jbiRemoteEx=" + jbiRemoteEx),
                         jbiRemoteEx);
                }
            }
        }
        serviceUnitBean.setServiceUnitsList(filteredServiceUnitList);
    }

    /**
     * prevents instantiation and subclassing
     */
    private ServiceUnitHandlers()
    {
    }

}
