/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
package com.sun.jbi.jsf.handlers;

import com.sun.enterprise.tools.admingui.util.FileUtil;
import com.sun.enterprise.tools.admingui.util.GuiUtil;
import com.sun.jbi.jsf.bean.AlertBean;
import com.sun.jbi.jsf.bean.OperationBean;
import com.sun.jbi.jsf.bean.UploadCopyRadioBean;
import com.sun.jbi.jsf.util.BeanUtilities;
import com.sun.jbi.jsf.util.ClusterUtilities;
import com.sun.jbi.jsf.util.I18nUtilities;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.SharedConstants;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jsftemplating.annotation.Handler;
import com.sun.jsftemplating.annotation.HandlerInput;
import com.sun.jsftemplating.annotation.HandlerOutput;
import com.sun.jsftemplating.layout.descriptors.handler.HandlerContext;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides jsftemplating handlers for Upgrade Component and Update Component
 * use-cases
 *
 * @author   Sun Microsystems Inc.
 */
public final class UpgradeComponentHandlers
{

    /**
     * <p>
     *
     * Upgrades, or updates, the target component for an uploaded, or copied,
     * component archive. <p>
     *
     * Input value: "archivePath" -- Type: <code> java.lang.String</code> path
     * to validated archive <p>
     *
     * Input value: "jbiName" -- Type: <code> java.lang.String</code> JBI name
     * from validated archive <p>
     *
     * Input value: "redirectOnFailure" -- Type: <code> java.lang.String</code>
     * what to show if it fails <p>
     *
     * Input value: "redirectOnSuccess" -- Type: <code> java.lang.String</code>
     * what to show if it works <p>
     *
     * Output value: "isAlertNeeded" -- Type: <code>java.lang.Boolean</code>
     * show alert or not</p> <p>
     *
     * Output value: "alertSummary" -- Type: <code>String</code>summary, if
     * failure</p> <p>
     *
     * Output value: "alertDetails" -- Type: <code>String</code>details, if
     * failure</p> <p>
     *
     * Output value: "redirectTo" -- Type: <code>String</code> Where to go
     * next, based on success/failure</p>
     *
     * @param handlerCtx  <code>HandlerContext</code> provides inputs and
     *      outputs.
     */
    @Handler(
        id="jbiUpgradeComponent",
        input={
            @HandlerInput(
                name="archivePath",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="jbiName",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="jbiType",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="uploadSelected",
                type=Boolean.class,
                required=true) ,
            @HandlerInput(
                name="redirectOnFailure",
                type=String.class,
                required=true) ,
            @HandlerInput(
                name="redirectOnSuccess",
                type=String.class,
                required=true) },
        output={
            @HandlerOutput(name="isAlertNeeded", type=Boolean.class) ,
            @HandlerOutput(name="alertSummary", type=String.class) ,
            @HandlerOutput(name="alertDetails", type=String.class) ,
            @HandlerOutput(name="redirectTo", type=String.class) })

        public static void jbiUpgradeComponent(HandlerContext handlerCtx)
    {
        String archivePath = (String) handlerCtx.getInputValue("archivePath");
        String jbiName = (String) handlerCtx.getInputValue("jbiName");
        String jbiType = (String) handlerCtx.getInputValue("jbiType");
        String redirectOnFailure = (String) handlerCtx.getInputValue("redirectOnFailure");
        String redirectOnSuccess = (String) handlerCtx.getInputValue("redirectOnSuccess");

        String redirectTo = redirectOnSuccess;

        if (sLog.isLoggable(Level.FINER)) {        
           sLog.finer("UpgradeComponentHandlers.jbiUpgradeComponent(...)" +
                  ", archivePath=" + archivePath +
                  ", jbiName=" + jbiName +
                  ", redirectOnFailure=" + redirectOnFailure +
                  ", redirectOnSuccess=" + redirectOnSuccess);
        }

        String alertDetails = "";
        String alertSummary = "";
        String internalError = "";
        String successResult = "";
        String failureResult = "";
        String warningResult = "";

        boolean isAlertNeeded = true;
        boolean isRedirectSuccess = true;

        AlertBean alertBean = BeanUtilities.getAlertBean();
        UploadCopyRadioBean uploadCpBean = BeanUtilities.getUploadCopyRadioBean();

        uploadCpBean.setNavDestValid(redirectOnSuccess);
        uploadCpBean.setNavDestInvalid(redirectOnFailure);


        // if we get here, the shutdown/upgrade confirmation dialog was answered yes

        // 1. find all targets
        List<Properties> targetsAsPropertiesList = 
            ClusterUtilities
            .findTargetsForNameByType(jbiName,
                                      jbiType);

        // 2. shutdown
        Properties compProps =
            new Properties();
        compProps.setProperty(SharedConstants.KEY_TYPE, 
                              jbiType);
        compProps.setProperty(SharedConstants.KEY_NAME, 
                              jbiName);
        OperationBean operationBean =
            BeanUtilities.getOperationBean();
        compProps =
            operationBean.shutDown(compProps,
                                   targetsAsPropertiesList);
        // 3. attempt upgrade
        try
            {
                String invalidAlertDetail =
                    JbiArchiveValidationHandler.checkValidArchive(archivePath, 
                                                                  JbiArchiveValidationHandler.JBI_TYPE_COMP);
                //If the Archive is valid, no invalidAlertDetail is returned.
                //Check that and then attempt update
                if ("".equals(invalidAlertDetail))
                    {
                        JBIAdminCommands jac = BeanUtilities.getClient();
                        successResult = jac.updateComponent(jbiName, archivePath);
                    }
                else
                    {
                        failureResult = invalidAlertDetail;
                    }
            }
        catch (com.sun.jbi.ui.common.JBIRemoteException jbiRemoteEx)
            {
                JBIManagementMessage mgmtMsg =
                    BeanUtilities.extractJBIManagementMessage(jbiRemoteEx);
                
                if (sLog.isLoggable(Level.FINE)) {                
                    sLog.fine("UpgradeComponentHandlers.jbiUpgradeComponent(...), caught jbiRemoteEx=" + jbiRemoteEx +
                          ", mgmtMsg=" + mgmtMsg);
                }
                
                if (null == mgmtMsg)
                    {
                        failureResult = jbiRemoteEx.getMessage();
                        if (null == failureResult)
                            {
                                failureResult = jbiRemoteEx.toString();
                            }
                    }
                else
                    {
                        failureResult = mgmtMsg.getMessage();
                    }
            }
        
        if (sLog.isLoggable(Level.FINER)) {        
            sLog.finer("UpgradeComponentHandlers.jbiUpgradeComponent(...), jbiName=" +
                  jbiName + ", archiivePath=" + archivePath + ", successResult=" +
                  successResult + ", failureResult=" + failureResult +
                  ", internalError=" + internalError);
        }

        if (!"".equals(internalError))
            {
                isRedirectSuccess = false;
                alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                alertSummary = I18nUtilities.getResourceString("jbi.internal.error.summary");
                alertDetails += internalError + SharedConstants.HTML_BREAK;
            }

        if (!"".equals(successResult))
            {
                alertBean.setAlertType(AlertBean.ALERT_TYPE_INFO);
                alertSummary = I18nUtilities.getResourceString("jbi.upgrade.success.alert.summary.text");
            }
        else
            {
                isRedirectSuccess = false;
                alertBean.setAlertType(AlertBean.ALERT_TYPE_ERROR);
                alertSummary = I18nUtilities.getResourceString("jbi.upgrade.failed.alert.summary.text");
                alertDetails += failureResult + SharedConstants.HTML_BREAK;
            }

        if (isRedirectSuccess)
            {
                Object[] args = {jbiName};
                alertDetails = 
                    GuiUtil
                    .getMessage(I18nUtilities
                                .getResourceString("jbi.upgrade.component.success.details"), 
                                args);
                redirectTo = redirectOnSuccess;
                // check for enabled checkbox 
                String jbiIsEnabledAfterUpgrade =
                    BeanUtilities
                    .getStringPropertyUsingExpression("#{jbiIsEnabledAfterUpgrade}");
                
                // if enabled checked, attempt start (on all targets)
                if ("true".equalsIgnoreCase(jbiIsEnabledAfterUpgrade))
                {
                    compProps = operationBean.start(compProps,targetsAsPropertiesList);

                    warningResult = (String) compProps.getProperty(SharedConstants.WARNING_RESULT);
                    failureResult = (String) compProps.getProperty(SharedConstants.FAILURE_RESULT);

                    // If the start had any failures or warnings, we should display them
                    // but not change the alert type, since the update was successful.
                    if ((warningResult != null) || (failureResult != null))
                    {
                        alertDetails += SharedConstants.HTML_BREAK;
                        if (failureResult != null)
                        {
                            alertDetails += SharedConstants.HTML_BREAK;
                            alertDetails += failureResult;
                        }
                        if (warningResult != null)
                        {
                            alertDetails += SharedConstants.HTML_BREAK;
                            alertDetails += warningResult;
                        }
                        alertDetails += SharedConstants.HTML_BREAK;
                        alertDetails = BeanUtilities.addAlertFooterMessage(alertDetails);
                    }
                }

            }
        else
            {
                alertDetails = BeanUtilities.addAlertFooterMessage(alertDetails);
                redirectTo = redirectOnFailure;
            }

        //Files gets uploaded to temporary location on hard disk before succesful upgrade, failed upgrade
        //failed validation and needs to be cleaned up
        if ((Boolean) handlerCtx.getInputValue("uploadSelected"))
            {
            if (sLog.isLoggable(Level.FINER)) {        	
                sLog.finer("UpgradeComponentHandlers.jbiUpgradeComponent(...),to delete uploaded tmp file="
                          + archivePath);
            }
                FileUtil.delete(archivePath);
                //Delete an invalid uploaded archive
            }

        handlerCtx.setOutputValue("isAlertNeeded", Boolean.toString(isAlertNeeded));
        handlerCtx.setOutputValue("alertSummary", alertSummary);
        handlerCtx.setOutputValue("alertDetails", alertDetails);
        handlerCtx.setOutputValue("redirectTo", redirectTo);

        if (sLog.isLoggable(Level.FINE)) {        
        sLog.fine("UpgradeComponentHandlers.jbiUpgradeValidatedArchive(...), " +
                  " isAlertNeeded=" + isAlertNeeded +
                  " isRedirectSuccess=" + isRedirectSuccess +
                  ", alertSummary=" + alertSummary +
                  ", alertDetails=" + alertDetails +
                  ", redirectTo=" + redirectTo);
        }
    }

    private static Logger sLog = JBILogger.getInstance();

    /**
     * prevents instantiation
     */
    private UpgradeComponentHandlers()
    {
    }
}
