/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  RuntimeStatsBean.java
 */

package com.sun.jbi.jsf.bean;

import java.util.logging.Logger;
import java.util.Properties;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JSFUtils;
import com.sun.jbi.jsf.util.RuntimeMonitoringUtils;
import com.sun.webui.jsf.component.TextField;
import com.sun.webui.jsf.component.Checkbox;
import com.sun.webui.jsf.component.PropertySheet;
import javax.management.openmbean.TabularData;
import java.util.logging.Level;

/**
 * Stores the framework and NMR statistics for an instance.
 */
public class RuntimeStatsBean
{

    /**
     * No argument constructor
     */
    public RuntimeStatsBean()
    {
    }


    /**
     * gets the NMR runtime statistics property sheet
     * @return the NMR statistics
     */
    public PropertySheet getNMRPS()
    {
        return mNMRPS;
    }


    /**
     * sets the NMR runtime statistics property sheet
     * @param the NMR statistics
     */
    public void setInstanceName(String anInstanceName)
    {
        //mNMRPS = aPropertySheet;
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("RuntimeStatsBean.setInstanceName(" + anInstanceName + ")");
    	}
    	
        mInstanceName = anInstanceName;
    }


    /**
     * Required but not used setter for read only property sheet.
     *
     * @param anIgnoredArg  not used
     */
    public void setNmrPropertySheet(PropertySheet anIgnoredArg)
    {
    	if (sLog.isLoggable(Level.FINEST)){
          sLog.finest("RuntimeStatsBean.setNMRPropertySheet(" +
                    anIgnoredArg + ")");
    	}
    }


    /**
     * Required but not used setter for read only property sheet.
     *
     * @param anIgnoredArg  not used
     */
    public void setFrameworkPropertySheet(PropertySheet anIgnoredArg)
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("RuntimeStatsBean.setFrameworkPropertySheet(" +
                    anIgnoredArg + ")");
    	}
    }


    /**
     * Gets the FrameworkPropertySheet attribute of the RuntimeStatsBean object
     *
     * @return   The FrameworkPropertySheet value
     */
    public PropertySheet getNmrPropertySheet()
    {
    	if (sLog.isLoggable(Level.FINEST)){
           sLog.finest("RuntimeStatusBean.getNMRPropertySheet()");
    	}

        Properties status = new Properties();
        TabularData tabularData = RuntimeMonitoringUtils.getNMRStats(mInstanceName, status);
        PropertySheet result = JSFUtils.getNMRPS(mInstanceName, tabularData);
        return result;
    }


    /**
     * Gets the FrameworkPropertySheet attribute of the RuntimeStatsBean object
     *
     * @return   The FrameworkPropertySheet value
     */
    public PropertySheet getFrameworkPropertySheet()
    {
    	if (sLog.isLoggable(Level.FINEST)){
            sLog.finest("RuntimeStatusBean.getFrameworkPropertySheet()");
    	}

        Properties status = new Properties();
        TabularData tabularData = RuntimeMonitoringUtils.getFrameworkStats(mInstanceName, status);
        PropertySheet result = JSFUtils.getFrameworkPS(mInstanceName, tabularData);
        return result;
    }


    //Get logger to log fine, info level messages in server.log file
    private Logger sLog = JBILogger.getInstance();

    /**
     * the framework statistics
     */
    private PropertySheet mFrameworkPS;

    /**
     * the NMR statistics
     */
    private PropertySheet mNMRPS;

    /**
     * the instance name
     */
    private String mInstanceName;

}
