/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2007 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License. You can obtain
 * a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 * or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 * 
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 * Sun designates this particular file as subject to the "Classpath" exception
 * as provided by Sun in the GPL Version 2 section of the License file that
 * accompanied this code.  If applicable, add the following below the License
 * Header, with the fields enclosed by brackets [] replaced by your own
 * identifying information: "Portions Copyrighted [year]
 * [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

/*
 *  SelectableTargetInfo.java
 */

package com.sun.jbi.jsf.bean;

import com.sun.jbi.ui.common.ServiceUnitInfo;
import com.sun.jbi.jsf.util.JBILogger;
import com.sun.jbi.jsf.util.JBIConstants;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SelectableServiceUnitInfo extends ServiceUnitInfo
{
   
    private static Logger sLog = JBILogger.getInstance();
    private String mServiceAssemblyName = "";

    public SelectableServiceUnitInfo (String name, 
                                      String description,
                                      String deployedOn, 
                                      String state)
    {
        super (name, description, deployedOn, state);
    }

    public SelectableServiceUnitInfo (String name, 
                                      String description,
                                      String deployedOn, 
                                      String state,
                                      String serviceAssemblyName)
    {
        super (name, description, deployedOn, state);
        setServiceAssemblyName(serviceAssemblyName);
    }

    public SelectableServiceUnitInfo(String aName, String aDescription)
    {
    	if (sLog.isLoggable(Level.FINEST)) {        
            sLog.finest("SelectableServiceUnitInfo.<init>(" + aName + ", " + aDescription + ")");
    	}
    	
        setName(aName);
        setDescription(aDescription);
    }

    public String getServiceAssemblyName()
    {
        return mServiceAssemblyName;
    }

    public void setServiceAssemblyName (String aServiceAssemblyName)
    {
        mServiceAssemblyName = aServiceAssemblyName;
    }

    public boolean getSelected()
    {
        return mSelected;
    }

    public void setSelected (boolean aSelection)
    {
        mSelected = aSelection;
    }

    public String getComponentType()
    {
        return mComponentType;
    }

    public void setComponentType (String aComponentType)
    {
        mComponentType = aComponentType;
    }

    public void setQueryString(String aQueryString)
    {
        mQueryString = aQueryString;
    }

    public String getQueryString()
    {
        return mQueryString;
    }

    public String getFormattedQueryString()
    {
        String returnString = "";
        if (mQueryString.length() > 0)
        {
            returnString = "&qs=" + mQueryString;
        }
        return returnString;
    }    

    private boolean mSelected = false;
    private String mQueryString = "";
    private String mComponentType = "";
}

