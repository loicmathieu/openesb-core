/*
 *  DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 *  Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 *
 *  The contents of this file are subject to the terms of either the GNU
 *  General Public License Version 2 only ("GPL") or the Common Development
 *  and Distribution License("CDDL") (collectively, the "License").  You
 *  may not use this file except in compliance with the License. You can obtain
 *  a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 *  or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 *  language governing permissions and limitations under the License.
 *
 *  When distributing the software, include this License Header Notice in each
 *  file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 *  Sun designates this particular file as subject to the "Classpath" exception
 *  as provided by Sun in the GPL Version 2 section of the License file that
 *  accompanied this code.  If applicable, add the following below the License
 *  Header, with the fields enclosed by brackets [] replaced by your own
 *  identifying information: "Portions Copyrighted [year]
 *  [name of copyright owner]"
 *
 *  Contributor(s):
 *
 *  If you wish your version of this file to be governed by only the CDDL or
 *  only the GPL Version 2, indicate your decision by adding "[Contributor]
 *  elects to include this software in this distribution under the [CDDL or GPL
 *  Version 2] license."  If you don't indicate a single choice of license, a
 *  recipient has the option to distribute your version of this file under
 *  either the CDDL, the GPL Version 2 or to extend the choice of license to
 *  its licensees as provided above.  However, if you add GPL Version 2 code
 *  and therefore, elected the GPL Version 2 license, then the option applies
 *  only if the new code is made subject to such option by the copyright
 *  holder.
 */
/*
 *  ConfigProperty.java
 */
package com.sun.jbi.jsf.bean;

import java.util.List;

/**
 * Defines when and how a configuration property is to be 
 * initialized, displayed, edited, and validated based on
 * the option Open ESB configuration extension elements (e.g.
 * <code>&lt;config:Property ...&gt;</code> in a JBI
 * component's jbi.xml descriptor.
 *
 * @author   Sun Microsystems Inc.
 */
public class ConfigProperty
{
    // === static utility methods ===

    /**
     * Determines if the specified property is to be displayed during
     * component installation configuration
     *
     * @param aConfigProperty  a property to be checked
     * @return                 true for installation configuration display
     *                         (i.e. showDisplay of <code>all</code> or
     *                         <code>install</code>)
     */
    public static boolean isInstallTime(ConfigProperty aConfigProperty)
    {
        boolean result = false;
        String showDisplay =
            aConfigProperty.getShowDisplay();
        if ((SHOW_DISPLAY_TYPE_ALL.equals(showDisplay))
            || (SHOW_DISPLAY_TYPE_INSTALL.equals(showDisplay)))
            {
                result = true;
            }
        return result;
    }

    /**
     * Determines if the specified property is to be displayed during
     * component runtime configuration
     *
     * @param aConfigProperty  a property to be checked
     * @return                 true for runtime configuration display
     *                         (i.e. showDisplay of <code>all</code> or
     *                         <code>runtime</code>)
     */
    public static boolean isRuntime(ConfigProperty aConfigProperty)
    {
        boolean result = false;
        String showDisplay =
            aConfigProperty.getShowDisplay();
        if ((SHOW_DISPLAY_TYPE_ALL.equals(showDisplay))
            || (SHOW_DISPLAY_TYPE_RUNTIME.equals(showDisplay)))
            {
                result = true;
            }
        return result;
    }

    // === constructor(s) ===

    /**
     * Empty Constructor (use setters after instantiation)
     */
    public ConfigProperty()
    {
    }

    // === getters ===

    /**
     * Gets the Display Name
     *
     * @return   The name to be displayed in user interface
     */
    public String getDisplayName()
    {
        return mDisplayName;
    }

    /**
     * Gets the Default Numeric Value attribute, if any
     *
     * @return   The DefaultNumericValue value or null if none
     */
    public int getDefaultNumericValue()
    {
        return mDefaultNumericValue;
    }

    /**
     * Gets the Default String Value attribute, if any
     *
     * @return   The DefaultStringValue value or null if none
     */
    public String getDefaultStringValue()
    {
        return mDefaultStringValue;
    }

    /**
     * Gets the Display Description 
     *
     * @return   The DisplayDescription I18n value to be used for 
     *           field help or tool tips
     */
    public String getDisplayDescription()
    {
        return mDisplayDescription;
    }

    /**
     * Gets the Constraint Enumeration if any
     *
     * @return   A List of allowable values for a selection widget
     *           or null if the field is to be manually entered
     */
    public List<String> getConstraintEnumeration()
    {
        return mConstraintEnumeration;
    }

    /**
     * Gets the Constraint Max Inclusive attribute if applicable
     *
     * @return   The max allowed value for a numeric field
     */
    public int getConstraintMaxInclusive()
    {
        return mConstraintMaxInclusive;
    }

    /**
     * Gets the Constraint Min Inclusive attribute if applicable
     *
     * @return   The ConstraintMinInclusive value
     */
    public int getConstraintMinInclusive()
    {
        return mConstraintMinInclusive;
    }

    /**
     * Gets the Application Restart Required attribute
     *
     * @return  true if changing this property requires the display
     *          of an application-restart-is-required message
     */
    public boolean getIsAppRestartRequired()
    {
        return mIsAppRestartRequired;
    }

    /**
     * Gets the Component Restart Required attribute
     *
     * @return   true if changing this property requires the display
     *           of a component-restart-is-required message
     */
    public boolean getIsCompRestartRequired()
    {
        return mIsCompRestartRequired;
    }

    /**
     * Gets the Is Encrypted attribute
     *
     * @return   true if this property is to be hidden when viewing or editing
     */
    public boolean getIsEncrypted()
    {
        return mIsEncrypted;
    }

    /**
     * Gets the Is Required attribute
     *
     * @return   true if a value is required for this property
     */
    public boolean getIsRequired()
    {
        return mIsRequired;
    }

    /**
     * Gets the Is Server Restart Required attribute
     *
     * @return   true if changing this property requires the display
     *           of a server-restart-is-required message
     */
    public boolean getIsServerRestartRequired()
    {
        return mIsServerRestartRequired;
    }

    /**
     * Gets the Is Variable Array attribute
     *
     * @return   true if this property is to be displayed/edited
     * in a table with add/remove buttons.
     */
    public boolean getIsVariableArray()
    {
        return mIsVariableArray;
    }

    /**
     * Gets the Name attribute
     *
     * @return   The non-I18n Name of this property
     */
    public String getName()
    {
        return mName;
    }

    /**
     * Gets the On Change Message attribute, if any
     *
     * @return   an I18n message to be displayed if this value is changed
     *           or null if there is no on-change-message for this property
     */
    public String getOnChangeMessage()
    {
        return mOnChangeMessage;
    }

    /**
     * Gets the Show Display attribute 
     *
     * @return   one of: <code>all</code>, <code>install</code>, or
     *           <code>runtime</code>  
     */
    public String getShowDisplay()
    {
        return mShowDisplay;
    }

    /**
     * Gets the Type attribute
     *
     * @return   The XSD type of value for this property (which determines
     *           whether it is boolean, numeric, or string). one of: 
     *           <code>xsd:boolean</code>, <code>xsd:int</code>, or 
     *           <code>xsd:string</code>.
     */
    public String getXSDType()
    {
        return mXSDType;
    }

    /**
     * Gets the Value attribute
     *
     * @return   The current value of the property, if any
     * Note that if this is a Variable Array, this returns
     * a List.
     */
    public Object getValue()
    {
        return mValue;
    }

    // === setters ===

    /**
     * Sets the Constraint Enumeration values
     *
     * @param aConstraintEnumeration  A list of display strings to be used
     *                                in a selection widget (or null if
     *                                this is a manually entered value)
     */
    public void setConstraintEnumeration(List<String> aConstraintEnumeration)
    {
        mConstraintEnumeration = aConstraintEnumeration;
    }

    /**
     * Sets the Constraint Max Inclusive attribute
     *
     * @param aConstraintMaxInclusive  The maximum allowed numeric value
     */
    public void setConstraintMaxInclusive(int aConstraintMaxInclusive)
    {
        mConstraintMaxInclusive = aConstraintMaxInclusive;
    }

    /**
     * Sets the Constraint Min Inclusive attribute
     *
     * @param aConstraintMinInclusive  The minimum allowed numeric value
     */
    public void setConstraintMinInclusive(int aConstraintMinInclusive)
    {
        mConstraintMinInclusive = aConstraintMinInclusive;
    }

    /**
     * Sets the Display Name attribute
     *
     * @param aDisplayName  The I18n String to be displayed in the 
     *                      user interface
     */
    public void setDisplayName(String aDisplayName)
    {
        mDisplayName = aDisplayName;
    }

    /**
     * Sets the Default Numeric Value attribute
     *
     * @param aDefaultNumericValue  The default value to display, if numeric
     *                              or null if non-numeric
     */
    public void setDefaultNumericValue(int aDefaultNumericValue)
    {
        mDefaultNumericValue = aDefaultNumericValue;
    }

    /**
     * Sets the Default String Value attribute
     *
     * @param aDefaultStringValue  The new DefaultStringValue value
     */
    public void setDefaultStringValue(String aDefaultStringValue)
    {
        mDefaultStringValue = aDefaultStringValue;
    }

    /**
     * Sets the Display Description attribute
     *
     * @param aDisplayDescription  an I18n String to describe this property
     *                             e.g. for inline help or tooltip
     */
    public void setDisplayDescription(String aDisplayDescription)
    {   
    	if (aDisplayDescription != null) {
            mDisplayDescription = aDisplayDescription;
    	}
    }

    /**
     * Sets the Is Application Restart Required attribute 
     *
     * @param isAppRestartRequired  true if changing this property
     *                              should result in an
     *                              application-restart-required message
     */
    public void setIsAppRestartRequired(boolean isAppRestartRequired)
    {
        mIsAppRestartRequired = isAppRestartRequired;
    }

    /**
     * Sets the IsCompRestartRequired attribute of the ConfigProperty object
     *
     * @param isCompRestartRequired true if changing this property
     *                              should result in an
     *                              component-restart-required message
     */
    public void setIsCompRestartRequired(boolean isCompRestartRequired)
    {
        mIsCompRestartRequired = isCompRestartRequired;
    }

    /**
     * Sets the Is Encrypted attribute 
     *
     * @param isEncrypted  true if this property's value is to be hidden
     *                     when viewed or prompted-for (e.g. password field)
     *                     (also hidden during logging)
     */
    public void setIsEncrypted(boolean isEncrypted)
    {
        mIsEncrypted = isEncrypted;
    }

    /**
     * Sets the Is Required attribute 
     *
     * @param isRequired  true if a value is required for this property
     */
    public void setIsRequired(boolean isRequired)
    {
        mIsRequired = isRequired;
    }

    /**
     * Sets the IsServerRestartRequired attribute of the ConfigProperty object
     *
     * @param isServerRestartRequired true if changing this property
     *                                should result in an
     *                                component-restart-required message
     */
    public void setIsServerRestartRequired(boolean isServerRestartRequired)
    {
        mIsServerRestartRequired = isServerRestartRequired;
    }

    /**
     * Sets the Is Variable Array attribute 
     *
     * @param isVariableArray true if this property is to be displayed/edited
     * in a table property, with add/remove buttons
     */
    public void setIsVariableArray(boolean isVariableArray)
    {
        mIsVariableArray = isVariableArray;
    }

    /**
     * Sets the Name attribute
     *
     * @param aName  a non-I18n String to name this property
     */
    public void setName(String aName)
    {
        mName = aName;
    }

    /**
     * Sets the On Change Message attribute 
     *
     * @param anOnChangeMessage an I18n String with a message to be
     *                          displayed if this property's value is changed
     *                          (or null if there is no such message)
     */
    public void setOnChangeMessage(String anOnChangeMessage)
    {
        mOnChangeMessage = anOnChangeMessage;
    }

    /**
     * Sets the Show Display attribute
     *
     * @param aShowDisplay an I18n String to display in the user-interface
     */
    public void setShowDisplay(String aShowDisplay)
    {
        mShowDisplay = aShowDisplay;
    }

    /**
     * Sets the Type attribute to specify if this is a boolean, numeric, or 
     * string property
     *
     * @param anXSDType  one of: <code>xsd:boolean</code>, <code>xsd:int</code>,
     * or <code>xsd:string</code>
     */
    public void setXSDType(String anXSDType)
    {
        mXSDType = anXSDType;
    }

    /**
     * Sets the Value attribute 
     *
     * @param aValue  The value of this property to be viewed, edited,
     *                and passed to the component's installation 
     *                configuration and/or runtime configuration MBean.
     * Note that if this is a Variable Array, this should be a List.
     */
    public void setValue(Object aValue)
    {
        mValue = aValue;
    }

    /**
     * Converts to a String representation of the object.
     *
     * @return   A non-I18n String for logging the state of this object
     *           (for an "encrypted" property,
     *           the value is <code>*hidden*</code>)
     */
    public String toString()
    {
        StringBuffer result =
            new StringBuffer(CN);
        result.append(", mConstraintEnumeration=[");
        if (null != mConstraintEnumeration)
            {
                boolean first = true;
                for (int i = 0; i < mConstraintEnumeration.size(); ++i)
                    {
                        if (!first)
                            {
                                result.append(", ");
                            }
                        else
                            {
                                first = false;
                            }
                        result.append(mConstraintEnumeration.get(i));
                    }
            }
        result.append("], mConstraintMaxInclusive=");
        result.append(mConstraintMaxInclusive);
        result.append(", mConstraintMinInclusive=");
        result.append(mConstraintMinInclusive);
        result.append(", mDefaultNumericValue=");
        result.append(mDefaultNumericValue);
        result.append(", mDefaultStringValue=");
        result.append(mDefaultStringValue);
        result.append(", mDisplayDescription=");
        result.append(mDisplayDescription);
        result.append(", mDisplayName=");
        result.append(mDisplayName);
        result.append(", mIsAppRestartRequired=");
        result.append(mIsAppRestartRequired);
        result.append(", mIsCompRestartRequired=");
        result.append(mIsCompRestartRequired);
        result.append(", mIsEncrypted=");
        result.append(mIsEncrypted);
        result.append(", mIsRequired=");
        result.append(mIsRequired);
        result.append(", mIsServerRestartRequired=");
        result.append(mIsServerRestartRequired);
        result.append(", mIsVariableArray=");
        result.append(mIsVariableArray);
        result.append(", mName=");
        result.append(mName);
        result.append(", mOnChangeMessage=");
        result.append(mOnChangeMessage);

        result.append(", mValue=");
        if (mIsEncrypted)
            {
                result.append("*hidden*");
            }
        else
            {
                result.append(mValue);
            }
        result.append(", mShowDisplay=");
        result.append(mShowDisplay);
        result.append(", mXSDType=");
        result.append(mXSDType);

        return result.toString();
    }

    // === static fields ===

    private static final String CN = ConfigProperty.class.getName();

    private static final String SHOW_DISPLAY_TYPE_ALL = "all";
    private static final String SHOW_DISPLAY_TYPE_INSTALL = "install";
    private static final String SHOW_DISPLAY_TYPE_RUNTIME = "runtime";

    // === member variables ===

    private int mDefaultNumericValue;
    private String mDefaultStringValue;
    private String mDisplayDescription = "";
    private String mDisplayName;
    private List<String> mConstraintEnumeration = null;
    private int mConstraintMaxInclusive;
    private int mConstraintMinInclusive;
    private boolean mIsAppRestartRequired;
    private boolean mIsCompRestartRequired;
    private boolean mIsEncrypted;
    private boolean mIsRequired;
    private boolean mIsServerRestartRequired;
    private boolean mIsVariableArray;
    private String mName;
    private String mOnChangeMessage;
    private String mShowDisplay;
    private Object mValue;
    private String mXSDType;
}

