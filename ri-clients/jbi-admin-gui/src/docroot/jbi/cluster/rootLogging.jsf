<!--
 DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 
 Copyright 1997-2008 Sun Microsystems, Inc. All rights reserved.
 
 The contents of this file are subject to the terms of either the GNU
 General Public License Version 2 only ("GPL") or the Common Development
 and Distribution License("CDDL") (collectively, the "License").  You
 may not use this file except in compliance with the License. You can obtain
 a copy of the License at https://glassfish.dev.java.net/public/CDDL+GPL.html
 or glassfish/bootstrap/legal/LICENSE.txt.  See the License for the specific
 language governing permissions and limitations under the License.
 
 When distributing the software, include this License Header Notice in each
 file and include the License file at glassfish/bootstrap/legal/LICENSE.txt.
 Sun designates this particular file as subject to the "Classpath" exception
 as provided by Sun in the GPL Version 2 section of the License file that
 accompanied this code.  If applicable, add the following below the License
 Header, with the fields enclosed by brackets [] replaced by your own
 identifying information: "Portions Copyrighted [year]
 [name of copyright owner]"
 
 Contributor(s):
 
 If you wish your version of this file to be governed by only the CDDL or
 only the GPL Version 2, indicate your decision by adding "[Contributor]
 elects to include this software in this distribution under the [CDDL or GPL
 Version 2] license."  If you don't indicate a single choice of license, a
 recipient has the option to distribute your version of this file under
 either the CDDL, the GPL Version 2 or to extend the choice of license to
 its licensees as provided above.  However, if you add GPL Version 2 code
 and therefore, elected the GPL Version 2 license, then the option applies
 only if the new code is made subject to such option by the copyright
 holder.
-->
<!-- jbi/cluster/rootLogging.jsf -->

<sun:page>

    <!beforeCreate 
    
setResourceBundle(key="i18n" bundle="com.sun.jbi.jsf.resources.Bundle")
setResourceBundle(key="i18n2"  bundle="com.sun.enterprise.tools.admingui.resources.Strings")
setResourceBundle(key="help"  bundle="com.sun.enterprise.tools.admingui.resources.Helplinks");
   
//------------------------------------------------------------------------------------
//  If not set, initialize the dropDown to domain (Happens when page is first displayed)
//------------------------------------------------------------------------------------
if (!$session{jbiRuntimeLoggerTargetValue}){
    setSessionAttribute(key="jbiRuntimeLoggerTargetValue", value="server" );
}

//------------------------------------------------------------------------------------
//  Retrieve the list of running instances that will be displayed in the dropdown and update listbox.
//------------------------------------------------------------------------------------
jbiGetAllInstances (
  addDomainFlag="$boolean{false}"
  instancesList=>$page{instancesList}
)

//------------------------------------------------------------------------------------
//  This will be true when the "Save" button is clicked.  When this happens, we need
//  to update the log levels for all the selected entries in the update listbox.
//------------------------------------------------------------------------------------
if ($attribute{hasResults}) {
    jbiSaveJBIRuntimeLogLevels (propertySheetParentId     = "$session{propertySheetId}"
                                propertySheetId           = "jbiSetJBIRuntimeLogLevelsPropertySheet"
                                propertySheetSectionIdTag = "jbiSetJBIRuntimeLogLevelPropertySheetSection"
                                propertyIdTag             = "jbiSetJBIRuntimeLogLevelProperty"
                                dropDownIdTag             = "jbiDropDown"
                                hiddenFieldIdTag          = "jbiHiddenField"
                                targetName                = "#{sessionScope.jbiRuntimeLoggerTargetValue}"
                                instanceName              = "#{sessionScope.jbiRuntimeLoggerTargetValue}"
                                isAlertNeeded => $session{isJbiAlertNeeded}
                                alertSummary  => $session{jbiAlertSummary}
                                alertDetails  => $session{jbiAlertDetails});
    
    jbiIncrementAlertCountIfNeeded(isAlertNeeded='$session{isJbiAlertNeeded}');
    setAttribute(key="hasResults" value="" );
}

if (!$session{loadDefaultLevel}) {
    setSessionAttribute(key="jbiDropDownDefaultLevel", value="");
}

if ($session{loadDefaultLevel}) {
    setSessionAttribute(key="jbiDropDownDefaultLevel", value="DEFAULT");
    setSessionAttribute(key="loadDefaultLevel", value="" );
}
    <!-- display an alert if the selected target is not running -->
jbiAlertIfTargetNotRunning(
 targetName="#{sessionScope.jbiRuntimeLoggerTargetValue}"
 isAlertNeeded=>$session{isJbiAlertNeeded}
 alertSummary=>$session{jbiAlertSummary}
 alertDetails=>$session{jbiAlertDetails}
)

    />

    <sun:html>
        <sun:head id="jbiRootLoggingHead" />
        <sun:body>
        
             <sun:form id="rootTabsForm">
             
#include treeBreadcrumbs.inc
#include "jbi/scripts/jbiscripts.js"
#include "jbi/cluster/inc/rootTabs.inc"

                 <sun:hidden id="helpKey" value="$resource{help.jbi.cluster.rootLogging}" />
             </sun:form>
             
#include "jbi/cluster/inc/alertBox.inc"

             <sun:form id="loggersSelectionForm"> 

                 <sun:title id="title1"
                     style = "text-indent: 8pt"
                     title = "$resource{i18n.jbi.runtime.loggers.page.title}"
                     helpText="$resource{i18n.jbi.runtime.loggers.page.help}">
                     >    
                     <!facet pageButtonsTop>
                         <sun:panelGroup id="topButtons">
                             <sun:button id="saveButton" 
                                 text="$resource{i18n2.button.Save}" 
                                 disabled="#{LoggingBean.saveButtonDisabled}" >
                                 onClick="javascript: 
                                     if ( guiValidate('#{reqMsg}','#{reqInt}','#{reqPort}'))
                                        submitAndDisable(this, '$resource{i18n2.button.Processing}');
                                     return false; "
                                 >
                                 <!command
                                     setAttribute(key="hasResults" value="true");
                                     navigate(page="jbi/cluster/rootLogging.jsf");
                                 />
                             </sun:button>
                         </sun:panelGroup>
                     </facet>
                 </sun:title>
                
                 "<br />
                 <sun:image id="indentDropDown"
                     align  = "top"
                     height = "$int{10}"
                     url    = "/resource/images/dot.gif"
                     width  = "$int{8}"
                 />
                 <sun:dropDown id="FromTarget"
                     immediate  = "#{true}"
                     items      = "$pageSession{instancesList}"
                     label      = "$resource{i18n.jbi.runtime.loggers.instance.dropdown.label}"
                     selected   = "#{sessionScope.jbiRuntimeLoggerTargetValue}"
                     submitForm = "#{true}"
                     >
                     <!command
                         setAttribute(key="click" value="$this{component}");
                         setAttribute(key="targetVal" value="#{click.selected}")
                         setSessionAttribute(key="jbiRuntimeLoggerTargetValue", value="$attribute{targetVal}" );
                         navigate(page="jbi/cluster/rootLogging.jsf");
                     />
                 </sun:dropDown>
                 
<!-- Help text may go below the drop down.
                 <sun:helpInline id="loadDefaultsHelpInline"
                     style="margin-left: 12px" 
                     text="#{'domain'==sessionScope.jbiRuntimeLoggerTargetValue ? 'Domain Help Text Goes Here' :  'The Non-Domain Help Text Goes Here' }" 
                 />
-->
                
                 "<br />
                 "<br />
                 <sun:button id="loadDefaults" 
                     immediate="#{true}"
                     primary="#{false}" 
                     style="margin-left: 8pt"  
                     text="$resource{i18n2.button.LoadDefaults}" 
                     disabled="#{LoggingBean.saveButtonDisabled}"
                     >
                     <!command
                         setSessionAttribute(key="loadDefaultLevel", value="true" );
                         navigate(page="jbi/cluster/rootLogging.jsf");
                     />
                 </sun:button>
                            
                 <dynamicPropertySheet id="jbiDynamicProperySheet"
                     propertySheetId           = "jbiSetJBIRuntimeLogLevelsPropertySheet"
                     propertySheetSectionIdTag = "jbiSetJBIRuntimeLogLevelPropertySheetSection"
                     propertyIdTag             = "jbiSetJBIRuntimeLogLevelProperty"
                     staticTextIdTag           = "jbiStaticText"
                     dropDownIdTag             = "jbiDropDown"
                     dropDownDefaultLevel      = "#{sessionScope.jbiDropDownDefaultLevel}"
                     hiddenFieldIdTag          = "jbiHiddenField"
                     componentName             = "#{sessionScope.sharedShowName}"
                     targetName                = "#{sessionScope.jbiRuntimeLoggerTargetValue}"
                     instanceName              = "#{sessionScope.jbiRuntimeLoggerTargetValue}"
		     configFile                = "/com/sun/jbi/config/LoggerConfig.xml"                  
                     propertySheetAdaptorClass = "com.sun.jbi.jsf.util.JBIRuntimeLogLevelsPropertySheetAdaptor"
                     >
                     <!beforeCreate
                         getClientId(component="$this{component}" clientId=>$session{propertySheetId});
                     />
                 </dynamicPropertySheet>

                 <sun:title id="title2">    
                     <!facet pageButtonsBottom>
                         <sun:panelGroup id="bottomButtons">
                             <sun:button id="saveButton2" 
                                 text="$resource{i18n2.button.Save}" 
                                 disabled="#{LoggingBean.saveButtonDisabled}" >
                                 onClick="javascript: 
                                     if ( guiValidate('#{reqMsg}','#{reqInt}','#{reqPort}'))
                                        submitAndDisable(this, '$resource{i18n2.button.Processing}');
                                     return false; "
                                 >
                                 <!command
                                     setAttribute(key="hasResults" value="true");
                                     navigate(page="jbi/cluster/rootLogging.jsf");
                                 />
                             </sun:button>
                         </sun:panelGroup>
                     </facet>
                 </sun:title>

             </sun:form>           
         </sun:body> 
     </sun:html>  

 </sun:page>


