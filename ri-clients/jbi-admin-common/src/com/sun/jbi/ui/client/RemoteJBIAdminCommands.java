/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RemoteJBIAdminCommands.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.client;

import com.sun.jbi.ui.common.JBIRemoteException;
import java.util.Properties;

/** This interface defined the mehtods that will be implemeted to exeute a remote commands
 * that involves a file upload to the server. If any implemetation does not suport the
 * fuctinoality, it should return Unspported exception wrapped in JBIRemoteException.
 * RI implements a default implementation that throws the unsupported exception.
 *
 * @author Sun Microsystems, Inc.
 */

public interface RemoteJBIAdminCommands
{
    /**
     * remotely deploys service assembly by uploading the file to server side.
     * Not supported in RI. Using this interface throws unsupported exception in RI.
     * @return result as a management message xml text
     * @param zipFilePath fie path
     * @throws JBIRemoteException on error
     */
    public String remoteDeployServiceAssembly(String zipFilePath) throws JBIRemoteException ;
    /**
     * remotely install service engine or binding component by uploading the file server side
     * Not supported in RI. Using this interface throws unsupported exception in RI.
     * @return result as a management message xml text
     * @param paramProps Properties object contains name,value pair for parameters.
     * @param zipFilePath fie path
     * @throws JBIRemoteException on error
     */
    public String remoteInstallComponent(String zipFilePath, Properties paramProps) throws JBIRemoteException ;
    /**
     * remotely install service engine or binding component by uploading the file server side
     * Not supported in RI. Using this interface throws unsupported exception in RI.
     * @return result as a management message xml text
     * @param zipFilePath fie path
     * @throws JBIRemoteException on error
     */
    public String remoteInstallComponent(String zipFilePath) throws JBIRemoteException ;
    /**
     * remotely install shared library by uploading the file to server side
     * Not supported in RI. Using this interface throws unsupported exception in RI.
     * @return result as a management message xml text
     * @param zipFilePath fie path
     * @throws JBIRemoteException on error
     */
    public String remoteInstallSharedLibrary(String zipFilePath) throws JBIRemoteException ;
    
    
}
