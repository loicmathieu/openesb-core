/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkStatisticsData.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.helper.FrameworkStatisticsDataWriter;

/**
 * @author graj
 *
 */
public class FrameworkStatisticsData implements Serializable {
    static final long          serialVersionUID                   = -1L;
    
    public static final String INSTANCENAME_KEY           = "InstanceName";
    public static final String STARTUPTIME_KEY           = "StartupTime (ms)";
    public static final String UPTIME_KEY           = "UpTime (ms)";
    
    String instanceName;
    long startupTime;
    long upTime;
    
    /**
     * Generate Tabular Data for this object
     * @return tabular data of this object
     */
    static public TabularData generateTabularData(Map<String /* instanceName */, FrameworkStatisticsData> map) {
        TabularData tabularData = null;
        try {
            tabularData = FrameworkStatisticsDataCreator.createTabularData(map);
        } catch (ManagementRemoteException e) {
        }
        return tabularData;
    }
    
    /**
     * Retrieves the Framework Statistics Data Map
     * 
     * @param tabularData
     * @return Framework Statistics Data
     */
    @SuppressWarnings("unchecked")
    static public Map<String /* instanceName */, FrameworkStatisticsData> retrieveDataMap(
            TabularData tabularData) {
        FrameworkStatisticsData data = null;
        Map<String /* instanceName */, FrameworkStatisticsData> dataMap = null;
        dataMap = new HashMap<String /* instanceName */, FrameworkStatisticsData>();
        for (Iterator dataIterator = tabularData.values().iterator(); dataIterator
                .hasNext();) {
            CompositeData compositeData = (CompositeData) dataIterator.next();
            CompositeType compositeType = compositeData.getCompositeType();
            data = new FrameworkStatisticsData();
            for (Iterator<String> itemIterator = compositeType.keySet()
                    .iterator(); itemIterator.hasNext();) {
                String item = (String) itemIterator.next();
                if (true == item
                        .equals(FrameworkStatisticsData.INSTANCENAME_KEY)) {
                    String value = (String) compositeData.get(item);
                    data.setInstanceName(value);
                }
                if (true == item
                        .equals(FrameworkStatisticsData.STARTUPTIME_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setStartupTime(value.longValue());
                }
                if (true == item.equals(FrameworkStatisticsData.UPTIME_KEY)) {
                    Long value = (Long) compositeData.get(item);
                    data.setUpTime(value.longValue());
                }
            }
            dataMap.put(data.getInstanceName(), data);
            
        }
        return dataMap;
    }
    
    /**
     * Converts a Framework Statistics Data to an XML String
     * 
     * @param map
     * @return XML string representing a Framework Statistics Datum
     * @throws ManagementRemoteException
     */
    static public String convertDataMapToXML(
            Map<String /* instanceName */, FrameworkStatisticsData> map)
            throws ManagementRemoteException {
        String xmlText = null;
        try {
            xmlText = FrameworkStatisticsDataWriter.serialize(map);
        } catch (ParserConfigurationException e) {
            throw new ManagementRemoteException(e);
        } catch (TransformerException e) {
            throw new ManagementRemoteException(e);
        }
        
        return xmlText;
        
    }
    
    
    /**
     * @return the instanceName
     */
    public String getInstanceName() {
        return this.instanceName;
    }


    /**
     * @param instanceName the instanceName to set
     */
    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }


    /**
     * @return the startupTime
     */
    public long getStartupTime() {
        return this.startupTime;
    }


    /**
     * @param startupTime the startupTime to set
     */
    public void setStartupTime(long startupTime) {
        this.startupTime = startupTime;
    }


    /**
     * @return the upTime
     */
    public long getUpTime() {
        return this.upTime;
    }


    /**
     * @param upTime the upTime to set
     */
    public void setUpTime(long uptime) {
        this.upTime = uptime;
    }

    /**
     * 
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n  Instance Name" + "=" + this.getInstanceName());
        buffer.append("\n  Startup Time" + "=" + this.getStartupTime());
        buffer.append("\n  Up Time" + "=" + this.getUpTime());
        buffer.append("\n  ========================================\n");
        return buffer.toString();
        
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
