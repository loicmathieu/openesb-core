/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JavaEEVerifierReport.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data;

import javax.management.openmbean.TabularData;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.OpenDataException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * This class holds information about JavaEEVerificationReport
 * for a single service unit
 */
public class JavaEEVerifierReport implements Serializable {
    static final long serialVersionUID = -1L;
    
    String            serviceUnitName;
    
    ArrayList<JavaEEReportItem> reportTable = new ArrayList<JavaEEReportItem>();
    
    /**
     * Constructor
     */
    public JavaEEVerifierReport() {
    }
    
    /**
     * get the service unit name
     * @return the serviceunit name
     */
    public String getServiceUnitName() {
        return this.serviceUnitName;
    }
    
    /**
     * set the service unit name
     * @param serviceUnitName
     *            the serviceUnitName to set
     */
    public void setServiceUnitName(String serviceUnitName) {
        this.serviceUnitName = serviceUnitName;
    }
    
    /**
     * get the verifier report for this service unit
     * @return the serviceUnitName
     */
    public ArrayList getJavaEEVerifierReport() {
        return this.reportTable;
    }
    
    
    /**
     * set the verifier report for this service unit
     * @return the serviceUnitName
     */
    public void setJavaEEVerifierReport(TabularData javaEEReportTable) 
    {

        if (javaEEReportTable != null)
        {
            for (Iterator dataIterator = javaEEReportTable.values().iterator(); 
                dataIterator.hasNext();) 
            {
                CompositeData cdata = (CompositeData) dataIterator.next();
                JavaEEReportItem reportItem = new JavaEEReportItem();
                HashMap<String, String> items = new HashMap<String, String>();
                Set keys = cdata.getCompositeType().keySet();
                for(Object key : keys)
                {
                    String keyString = (String)key;
                    items.put(keyString, (String)cdata.get(keyString).toString());
                }
                reportItem.setReportItems(items);
                reportTable.add(reportItem);
            }
        }
    }
    
    /**
     * this method is used to set a given row of java ee verifier report
     * read by the xml reader into this instance of javaee verifier report
     */
    public void addJavaEEVerifierReportItem(JavaEEReportItem tableItem)
    {
        reportTable.add(tableItem);
    }

    
    /**
     * This method is used to create a composite data to represent this 
     * JavaEEVerifierReport
     * @return CompositeData representing the data in this instance
     */
    public CompositeType getCompositeType()
    throws OpenDataException
    {
        if (reportTable == null || reportTable.size() == 0 )
        {
            return null;
        }
        TabularType reportTableType = 
                new TabularType(
                    "JavaEEVerifierTableType",
                    "Java EE Verification Report Table Type",
                    reportTable.get(0).getCompositeType(), //get the first entry in the array list
                    reportTable.get(0).getNames());
        
        String[] javaEEVerifierReportItemNames = new String[]
        {
            "ServiceUnitName",
            "JavaEEVerifierReport"
        };
        
        String[] javaEEVerifierReportItemDesc = new String[]
        {
            "Service Unit Name",
            "Java EE Verifier Report"
        };
       
        OpenType[] javaEEVerifierReportItemTypes = new OpenType[]
        {
            SimpleType.STRING,
            reportTableType
        };                
        
        return 
                new CompositeType(
                "JavaEEVerifierReport",
                "Java EE Verifier Report",
                javaEEVerifierReportItemNames,
                javaEEVerifierReportItemDesc,
                javaEEVerifierReportItemTypes);        
        

    }

    /**
     * This method is used to create a composite data to represent this 
     * JavaEEVerifierReport
     * @return CompositeData representing the data in this instance
     */
    public CompositeData getCompositeData()
    throws OpenDataException
    {
        
        if (reportTable == null || reportTable.size() == 0 )
        {
            return null;
        }
        
        TabularType reportTableType = 
                new TabularType(
                    "JavaEEVerifierTableType",
                    "Java EE Verification Report Table Type",
                    reportTable.get(0).getCompositeType(), //get the first entry in the array list
                    reportTable.get(0).getNames());
        
        String[] javaEEVerifierReportItemNames = new String[]
        {
            "ServiceUnitName",
            "JavaEEVerifierReport"
        };
        
        String[] javaEEVerifierReportItemDesc = new String[]
        {
            "Service Unit Name",
            "Java EE Verifier Report"
        };
       
        OpenType[] javaEEVerifierReportItemTypes = new OpenType[]
        {
            SimpleType.STRING,
            reportTableType
        };                
        
        CompositeType javaEEVerifierReport = 
                new CompositeType(
                "JavaEEVerifierReport",
                "Java EE Verifier Report",
                javaEEVerifierReportItemNames,
                javaEEVerifierReportItemDesc,
                javaEEVerifierReportItemTypes);        
        
        TabularData table = new TabularDataSupport(reportTableType);        
        for(JavaEEReportItem item : reportTable)
        {
            table.put(item.getCompositeData());
        }
        
        return new CompositeDataSupport(
                javaEEVerifierReport,
                javaEEVerifierReportItemNames,
                new Object[]
                { getServiceUnitName(), table});
        
    }
    
    /**
     * Return a displayable string of values
     * @return
     */
    public String getDisplayString() {
        StringBuffer buffer = new StringBuffer();
        buffer.append("\n    Service Unit Name" + "=" + this.getServiceUnitName());
        buffer.append("\n    VerifierReport\n");
        for (JavaEEReportItem item : reportTable)
        {
            buffer.append("\n " + item.getDisplayString());
        }
        return buffer.toString();
    }
    
    
    /**
     * This class holds details about the JavaEEVerifierReport
     */
    public class JavaEEReportItem {
        
        /**
         * map of i18n-ed keys to values
         */
        HashMap<String, String> mReportItemValues = new HashMap<String, String>();
        
        
        /**
         * get the values in JavaEEReportItem
         * @return String
         */
        public HashMap<String, String> getReportItems() {
            return this.mReportItemValues;
        }
        
        /**
         * set the values in JavaEEReportItem 
         * @param HashMap<String, String> values
         */
        public void setReportItems(HashMap<String, String> values) {
            this.mReportItemValues=values;
        }

        /**
         * get display string
         */
        private String getDisplayString()
        {
            StringBuffer buffer = new StringBuffer();
            for (String key : mReportItemValues.keySet())
            {
                buffer.append("\n  " + key + "=" + mReportItemValues.get(key));
                buffer.append("\n");                    
            }
            return buffer.toString();            
        }
        
        /**
         * This method is used to get a list of the keys in the report
         * item
         * @return String[] list of keys
         */
        private String[] getNames()
        {
            Set<Map.Entry<String, String>> entrySet = mReportItemValues.entrySet();
            String[] names = new String[entrySet.size()];
            int i = 0;
            for (Map.Entry entry : entrySet)
            {
                names[i++] = (String)entry.getKey();
            }
            return names;
        }
        
        /**
         * This method is used to get a list of the values in the report
         * item
         * @return String[] list of values
         */
        private String[] getValues()
        {
            Set<Map.Entry<String, String>> entrySet = mReportItemValues.entrySet();
            String[] values = new String[entrySet.size()];
            int i = 0;
            for (Map.Entry entry : entrySet)
            {
                values[i++] = (String)entry.getValue();
            }
            return values;
        }        
        
        /**
         * This method is used to get a list of the types in the report
         * item
         * @return String[] list of values
         */
        private OpenType[] getTypes()
        {
            Set<Map.Entry<String, String>> entrySet = mReportItemValues.entrySet();
            OpenType[] types = new OpenType[entrySet.size()];
            for (int i=0; i<types.length; i++)
            {
                types[i]=SimpleType.STRING;
            }
            return types;
        }                
        
        /** This method is used to create a CompositeData with the 
         * given values of a JavaEEReportItem 
         * @retutn CompositeData representing the data in this instance
         */
        public CompositeType getCompositeType()
        throws OpenDataException
        {
            return new CompositeType(
                      "JavaEEVerifierReportItem",
                      "JavaEE Verifier Report Item",
                      getNames(),
                      getNames(),
                      getTypes());        
        }
        /** This method is used to create a CompositeData with the 
         * given values of a JavaEEReportItem 
         * @retutn CompositeData representing the data in this instance
         */
        public CompositeData getCompositeData()
        throws OpenDataException
        {
            return new CompositeDataSupport(
                    new CompositeType(
                      "JavaEEVerifierReportItem",
                      "JavaEE Verifier Report Item",
                      getNames(),
                      getNames(),
                      getTypes()),   
                    getNames(),
                    getValues());
                    
        }
  
    } //end of the inner class
    
 
}
