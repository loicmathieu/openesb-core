/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EndpointStatisticsDataWriter.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.sun.esb.management.common.data.ConsumingEndpointStatisticsData;
import com.sun.esb.management.common.data.IEndpointStatisticsData;
import com.sun.esb.management.common.data.PerformanceData;
import com.sun.esb.management.common.data.ProvisioningEndpointStatisticsData;

/**
 * @author graj
 * 
 */
public class EndpointStatisticsDataWriter implements
        EndpointStatisticsDataXMLConstants, Serializable {
    
    static final long   serialVersionUID = -1L;
    
    static final String FILE_NAME_KEY    = "EndpointStatisticsData.xml";
    
    /** Constructor - Creates an EndpointStatisticsDataWriter */
    public EndpointStatisticsDataWriter() {
    }
    
    /**
     * 
     * @param document
     * @param directoryPath
     * @throws TransformerConfigurationException
     * @throws TransformerException
     * @throws Exception
     */
    public static void writeToFile(Document document, String directoryPath)
            throws TransformerConfigurationException, TransformerException,
            Exception {
        File file = new File(directoryPath);
        if ((file.isDirectory() == false) || (file.exists() == false)) {
            throw new Exception("Directory Path: " + directoryPath
                    + " is invalid.");
        }
        String fileLocation = file.getAbsolutePath() + File.separator
                + FILE_NAME_KEY;
        System.out.println("Writing out to file: " + fileLocation);
        File outputFile = new File(fileLocation);
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StreamResult result = new StreamResult(outputFile);
        
        // indent the Output to make it more legible...
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        
        transformer.transform(source, result);
    }
    
    /**
     * Change the contents of text file in its entirety, overwriting any
     * existing text. This style of implementation throws all exceptions to the
     * caller.
     * 
     * @param aFile
     *            is an existing file which can be written to.
     * @throws IllegalArgumentException
     *             if param does not comply.
     * @throws FileNotFoundException
     *             if the file does not exist.
     * @throws IOException
     *             if problem encountered during write.
     */
    public static void setContents(File aFile, String aContents)
            throws FileNotFoundException, IOException {
        if (aFile == null) {
            throw new IllegalArgumentException("File should not be null.");
        }
        if (!aFile.exists()) {
            aFile.createNewFile();
        }
        if (!aFile.isFile()) {
            throw new IllegalArgumentException("Should not be a directory: "
                    + aFile);
        }
        if (!aFile.canWrite()) {
            throw new IllegalArgumentException("File cannot be written: "
                    + aFile);
        }
        
        // declared here only to make visible to finally clause; generic
        // reference
        Writer output = null;
        try {
            // use buffering
            // FileWriter always assumes default encoding is OK!
            output = new BufferedWriter(new FileWriter(aFile));
            output.write(aContents);
        } finally {
            // flush and close both "aspectOutput" and its underlying FileWriter
            if (output != null) {
                output.close();
            }
        }
    }
    
    /**
     * 
     * @param NMRStatisticsData
     *            data
     * @return XML string
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public static String serialize(
            Map<String /* instanceName */, IEndpointStatisticsData> dataMap)
            throws ParserConfigurationException, TransformerException {
        Document document = null;
        EndpointStatisticsDataWriter writer = new EndpointStatisticsDataWriter();
        if (dataMap != null) {
            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            document = builder.newDocument(); // Create from whole cloth
            
            // ////////////////////////////////
            // <EndpointStatisticsDataList>
            Element root = (Element) document
                    .createElement(EndpointStatisticsDataXMLConstants.ENDPOINT_STATISTICS_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/EndpointStatisticsData"
            root
                    .setAttribute(
                            EndpointStatisticsDataXMLConstants.ENDPOINT_NAMESPACE_KEY,
                            EndpointStatisticsDataXMLConstants.ENDPOINT_NAMESPACE_VALUE);
            // version = "1.0"
            root.setAttribute(
                    EndpointStatisticsDataXMLConstants.ENDPOINT_VERSION_KEY,
                    EndpointStatisticsDataXMLConstants.ENDPOINT_VERSION_VALUE);
            
            for (String instanceName : dataMap.keySet()) {
                IEndpointStatisticsData data = dataMap.get(instanceName);
                if (data.isProvisioningEndpoint() == true) {
                    // ////////////////////////////////
                    // <ProvisioningEndpointStatisticsData>
                    Element endpointStatisticsDataElementChild = writer
                            .createProvisioningEndpointStatisticsDataElement(
                                    document,
                                    (ProvisioningEndpointStatisticsData) data);
                    // </ProvisioningEndpointStatisticsData>
                    root.appendChild(endpointStatisticsDataElementChild);
                    // ////////////////////////////////
                } else {
                    // ////////////////////////////////
                    // <ConsumingEndpointStatisticsData>
                    Element endpointStatisticsDataElementChild = writer
                            .createConsumingEndpointStatisticsDataElement(
                                    document,
                                    (ConsumingEndpointStatisticsData) data);
                    // </ConsumingEndpointStatisticsData>
                    root.appendChild(endpointStatisticsDataElementChild);
                    // ////////////////////////////////
                }
            }
            
            // ////////////////////////////////
            // </EndpointStatisticsDataList>
            document.appendChild(root);
            // ////////////////////////////////
            
        }
        return writer.writeToString(document);
    }
    
    /**
     * 
     * @param document
     * @param data
     * @return
     */
    protected Element createProvisioningEndpointStatisticsDataElement(
            Document document,
            ProvisioningEndpointStatisticsData provisioningData) {
        Element endpointStatisticsDataElement = null;
        if ((document != null) && (provisioningData != null)) {
            
            // <ProvisioningEndpointStatisticsData>
            endpointStatisticsDataElement = document
                    .createElement(EndpointStatisticsDataXMLConstants.PROVISIONING_ENDPOINT_STATISTICS_DATA_KEY);
            
            // Write all the Provisioning Endpoints first
            Element activationTimeElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.ACTIVATION_TIME_KEY);
            if (activationTimeElementChild != null) {
                activationTimeElementChild.setTextContent(provisioningData
                        .getActivationTime()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(activationTimeElementChild);
                       
            Element numberOfActiveExchangesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_ACTIVE_EXCHANGES_KEY);
            if (numberOfActiveExchangesElementChild != null) {
                numberOfActiveExchangesElementChild
                        .setTextContent(provisioningData
                                .getNumberOfActiveExchanges()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfActiveExchangesElementChild);
            
            Element numberOfReceivedRequestsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_REQUESTS_KEY);
            if (numberOfReceivedRequestsElementChild != null) {
                numberOfReceivedRequestsElementChild
                        .setTextContent(provisioningData
                                .getNumberOfReceivedRequests()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedRequestsElementChild);
            
            Element numberOfSentRepliesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_REPLIES_KEY);
            if (numberOfSentRepliesElementChild != null) {
                numberOfSentRepliesElementChild.setTextContent(provisioningData
                        .getNumberOfSentReplies()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentRepliesElementChild);
            
            Element uptimeElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.UPTIME_KEY);
            if (uptimeElementChild != null) {
                uptimeElementChild.setTextContent(provisioningData.getUptime()
                        + "");
            }
            endpointStatisticsDataElement.appendChild(uptimeElementChild);
            
            Element messageExchangeResponseTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_RESPONSE_TIME_AVERAGE_KEY);
            if (messageExchangeResponseTimeAverageElementChild != null) {
                messageExchangeResponseTimeAverageElementChild
                        .setTextContent(provisioningData
                                .getMessageExchangeResponseTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeResponseTimeAverageElementChild);
            
            // Write all the base Endpoint Statistics Data.
            Element instanceNameElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.INSTANCE_NAME_KEY);
            if (instanceNameElementChild != null) {
                instanceNameElementChild.setTextContent(provisioningData
                        .getInstanceName());
            }
            endpointStatisticsDataElement.appendChild(instanceNameElementChild);
            
            Element componentNameElementChild = document
            .createElement(EndpointStatisticsDataXMLConstants.COMPONENT_NAME_KEY);
            if (componentNameElementChild != null) {
                componentNameElementChild.setTextContent(provisioningData
                        .getComponentName());
            }
            endpointStatisticsDataElement
                    .appendChild(componentNameElementChild);
            
            Element extendedTimingStatisticsFlagEnabledElementChild = document
            .createElement(EndpointStatisticsDataXMLConstants.EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY);
            if (extendedTimingStatisticsFlagEnabledElementChild != null) {
                extendedTimingStatisticsFlagEnabledElementChild
                        .setTextContent(provisioningData.isExtendedTimingStatisticsFlagEnabled()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(extendedTimingStatisticsFlagEnabledElementChild);
            
            
            Element messageExchangeComponentTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY);
            if (messageExchangeComponentTimeAverageElementChild != null) {
                messageExchangeComponentTimeAverageElementChild
                        .setTextContent(provisioningData
                                .getMessageExchangeComponentTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeComponentTimeAverageElementChild);
            
            Element messageExchangeDeliveryChannelTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY);
            if (messageExchangeDeliveryChannelTimeAverageElementChild != null) {
                messageExchangeDeliveryChannelTimeAverageElementChild
                        .setTextContent(provisioningData
                                .getMessageExchangeDeliveryChannelTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeDeliveryChannelTimeAverageElementChild);
            
            Element messageExchangeServiceTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_SERVICE_TIME_AVERAGE_KEY);
            if (messageExchangeServiceTimeAverageElementChild != null) {
                messageExchangeServiceTimeAverageElementChild
                        .setTextContent(provisioningData
                                .getMessageExchangeServiceTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeServiceTimeAverageElementChild);
            
            Element numberOfReceivedDonesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_DONES_KEY);
            if (numberOfReceivedDonesElementChild != null) {
                numberOfReceivedDonesElementChild
                        .setTextContent(provisioningData
                                .getNumberOfReceivedDones()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedDonesElementChild);
            
            Element numberOfReceivedErrorsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_ERRORS_KEY);
            if (numberOfReceivedErrorsElementChild != null) {
                numberOfReceivedErrorsElementChild
                        .setTextContent(provisioningData
                                .getNumberOfReceivedErrors()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedErrorsElementChild);
            
            Element numberOfReceivedFaultsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_FAULTS_KEY);
            if (numberOfReceivedFaultsElementChild != null) {
                numberOfReceivedFaultsElementChild
                        .setTextContent(provisioningData
                                .getNumberOfReceivedFaults()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedFaultsElementChild);
            
            Element numberOfSentDonesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_DONES_KEY);
            if (numberOfSentDonesElementChild != null) {
                numberOfSentDonesElementChild.setTextContent(provisioningData
                        .getNumberOfSentDones()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentDonesElementChild);
            
            Element numberOfSentErrorsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_ERRORS_KEY);
            if (numberOfSentErrorsElementChild != null) {
                numberOfSentErrorsElementChild.setTextContent(provisioningData
                        .getNumberOfSentErrors()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentErrorsElementChild);
            
            Element numberOfSentFaultsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_FAULTS_KEY);
            if (numberOfSentFaultsElementChild != null) {
                numberOfSentFaultsElementChild.setTextContent(provisioningData
                        .getNumberOfSentFaults()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentFaultsElementChild);
            
            // ////////////////////////////////
            // <PerformanceMeasurementDataList>
            Element performanceDataListElement = (Element) document
                    .createElement(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/PerformanceMeasurementDataList"
            performanceDataListElement.setAttribute(PerformanceDataXMLConstants.NAMESPACE_KEY, 
                    PerformanceDataXMLConstants.NAMESPACE_VALUE);
            // version = "1.0"
            performanceDataListElement.setAttribute(PerformanceDataXMLConstants.VERSION_KEY, 
                    PerformanceDataXMLConstants.VERSION_VALUE);
            Map<String, PerformanceData> dataMap = provisioningData.getCategoryToPerformanceDataMap();

            for (String category : dataMap.keySet()) {
                PerformanceData data = dataMap.get(category);
                // ////////////////////////////////
                // <PerformanceMeasurementData>
                Element performanceMeasurementDataElementChild = this.createPerformanceMeasurementDataElement(document, data);
                // </PerformanceMeasurementData>
                performanceDataListElement.appendChild(performanceMeasurementDataElementChild);
                // ////////////////////////////////
            }
            // </PerformanceMeasurementDataList>
            endpointStatisticsDataElement.appendChild(performanceDataListElement);
            // ////////////////////////////////
        }
        
        return endpointStatisticsDataElement;
    }
    
    /**
     * 
     * @param document
     * @param data
     * @return
     */
    protected Element createConsumingEndpointStatisticsDataElement(
            Document document, ConsumingEndpointStatisticsData consumingData) {
        Element endpointStatisticsDataElement = null;
        if ((document != null) && (consumingData != null)) {
            
            // <ConsumingEndpointStatisticsData>
            endpointStatisticsDataElement = document
                    .createElement(EndpointStatisticsDataXMLConstants.CONSUMING_ENDPOINT_STATISTICS_DATA_KEY);
            // Write all the Consuming Endpoints.
            Element numberOfReceivedRepliesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_REPLIES_KEY);
            if (numberOfReceivedRepliesElementChild != null) {
                numberOfReceivedRepliesElementChild
                        .setTextContent(consumingData
                                .getNumberOfReceivedReplies()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedRepliesElementChild);
            
            Element numberOfSentRequestsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_REQUESTS_KEY);
            if (numberOfSentRequestsElementChild != null) {
                numberOfSentRequestsElementChild.setTextContent(consumingData
                        .getNumberOfSentRequests()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentRequestsElementChild);
            
            Element messageExchangeStatusTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_STATUS_TIME_AVERAGE_KEY);
            if (messageExchangeStatusTimeAverageElementChild != null) {
                messageExchangeStatusTimeAverageElementChild
                        .setTextContent(consumingData
                                .getMessageExchangeStatusTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeStatusTimeAverageElementChild);
            
            // Write all the base Endpoint Statistics Data.
            Element instanceNameElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.INSTANCE_NAME_KEY);
            if (instanceNameElementChild != null) {
                instanceNameElementChild.setTextContent(consumingData
                        .getInstanceName());
            }
            endpointStatisticsDataElement.appendChild(instanceNameElementChild);
            
            Element componentNameElementChild = document
            .createElement(EndpointStatisticsDataXMLConstants.COMPONENT_NAME_KEY);
            if (componentNameElementChild != null) {
                componentNameElementChild.setTextContent(consumingData
                        .getComponentName());
            }
            endpointStatisticsDataElement
                    .appendChild(componentNameElementChild);
            
            Element extendedTimingStatisticsFlagEnabledElementChild = document
            .createElement(EndpointStatisticsDataXMLConstants.EXTENDED_TIMING_STATISTICS_FLAG_ENABLED_KEY);
            if (extendedTimingStatisticsFlagEnabledElementChild != null) {
                extendedTimingStatisticsFlagEnabledElementChild
                        .setTextContent(consumingData.isExtendedTimingStatisticsFlagEnabled()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(extendedTimingStatisticsFlagEnabledElementChild);

            
            Element messageExchangeComponentTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_COMPONENT_TIME_AVERAGE_KEY);
            if (messageExchangeComponentTimeAverageElementChild != null) {
                messageExchangeComponentTimeAverageElementChild
                        .setTextContent(consumingData
                                .getMessageExchangeComponentTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeComponentTimeAverageElementChild);
            
            Element messageExchangeDeliveryChannelTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_DELIVERY_CHANNEL_TIME_AVERAGE_KEY);
            if (messageExchangeDeliveryChannelTimeAverageElementChild != null) {
                messageExchangeDeliveryChannelTimeAverageElementChild
                        .setTextContent(consumingData
                                .getMessageExchangeDeliveryChannelTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeDeliveryChannelTimeAverageElementChild);
            
            Element messageExchangeServiceTimeAverageElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.MESSAGE_EXCHANGE_SERVICE_TIME_AVERAGE_KEY);
            if (messageExchangeServiceTimeAverageElementChild != null) {
                messageExchangeServiceTimeAverageElementChild
                        .setTextContent(consumingData
                                .getMessageExchangeServiceTimeAverage()
                                + "");
            }
            endpointStatisticsDataElement
                    .appendChild(messageExchangeServiceTimeAverageElementChild);
            
            Element numberOfReceivedDonesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_DONES_KEY);
            if (numberOfReceivedDonesElementChild != null) {
                numberOfReceivedDonesElementChild.setTextContent(consumingData
                        .getNumberOfReceivedDones()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedDonesElementChild);
            
            Element numberOfReceivedErrorsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_ERRORS_KEY);
            if (numberOfReceivedErrorsElementChild != null) {
                numberOfReceivedErrorsElementChild.setTextContent(consumingData
                        .getNumberOfReceivedErrors()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedErrorsElementChild);
            
            Element numberOfReceivedFaultsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_RECEIVED_FAULTS_KEY);
            if (numberOfReceivedFaultsElementChild != null) {
                numberOfReceivedFaultsElementChild.setTextContent(consumingData
                        .getNumberOfReceivedFaults()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfReceivedFaultsElementChild);
            
            Element numberOfSentDonesElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_DONES_KEY);
            if (numberOfSentDonesElementChild != null) {
                numberOfSentDonesElementChild.setTextContent(consumingData
                        .getNumberOfSentDones()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentDonesElementChild);
            
            Element numberOfSentErrorsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_ERRORS_KEY);
            if (numberOfSentErrorsElementChild != null) {
                numberOfSentErrorsElementChild.setTextContent(consumingData
                        .getNumberOfSentErrors()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentErrorsElementChild);
            
            Element numberOfSentFaultsElementChild = document
                    .createElement(EndpointStatisticsDataXMLConstants.NUMBER_OF_SENT_FAULTS_KEY);
            if (numberOfSentFaultsElementChild != null) {
                numberOfSentFaultsElementChild.setTextContent(consumingData
                        .getNumberOfSentFaults()
                        + "");
            }
            endpointStatisticsDataElement
                    .appendChild(numberOfSentFaultsElementChild);
            
            // ////////////////////////////////
            // <PerformanceMeasurementDataList>
            Element performanceDataListElement = (Element) document
                    .createElement(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_LIST_KEY);
            // xmlns="http://java.sun.com/xml/ns/esb/management/PerformanceMeasurementDataList"
            performanceDataListElement.setAttribute(PerformanceDataXMLConstants.NAMESPACE_KEY, 
                    PerformanceDataXMLConstants.NAMESPACE_VALUE);
            // version = "1.0"
            performanceDataListElement.setAttribute(PerformanceDataXMLConstants.VERSION_KEY, 
                    PerformanceDataXMLConstants.VERSION_VALUE);
            Map<String, PerformanceData> dataMap = consumingData.getCategoryToPerformanceDataMap();

            for (String category : dataMap.keySet()) {
                PerformanceData data = dataMap.get(category);
                // ////////////////////////////////
                // <PerformanceMeasurementData>
                Element performanceMeasurementDataElementChild = this.createPerformanceMeasurementDataElement(document, data);
                // </PerformanceMeasurementData>
                performanceDataListElement.appendChild(performanceMeasurementDataElementChild);
                // ////////////////////////////////
            }
            // </PerformanceMeasurementDataList>
            endpointStatisticsDataElement.appendChild(performanceDataListElement);
            // ////////////////////////////////
        }
        return endpointStatisticsDataElement;
    }
    
    /**
    *
    * @param document
    * @param data
    * @return
    */
   protected Element createPerformanceMeasurementDataElement(
           Document document, PerformanceData data) {
       Element performanceDataElement = null;
       if ((document != null) && (data != null)) {
           // <PerformanceMeasurementData>
           performanceDataElement = document
                   .createElement(PerformanceDataXMLConstants.PERFORMANCE_MEASUREMENT_DATA_KEY);

           // <category>
           Element categoryElementChild = document.createElement(PerformanceDataXMLConstants.CATEGORY_KEY);
           if (categoryElementChild != null) {
               categoryElementChild.setTextContent(data.getCategory());
           }
           // </category>
           performanceDataElement.appendChild(categoryElementChild);

           // <endpoint>
           Element endpointElementChild = document.createElement(PerformanceDataXMLConstants.ENDPOINT_KEY);
           if (endpointElementChild != null) {
               endpointElementChild.setTextContent(data.getEndpoint());
           }
           // </endpoint>
           performanceDataElement.appendChild(endpointElementChild);

           // <sourceClassName>
           Element sourceClassNameElementChild = document
                   .createElement(PerformanceDataXMLConstants.SOURCE_CLASS_NAME_KEY);
           if (sourceClassNameElementChild != null) {
               sourceClassNameElementChild.setTextContent(data
                       .getSourceClassName());
           }
           // </sourceClassName>
           performanceDataElement.appendChild(sourceClassNameElementChild);

           // <numberOfMeasurementObjects>
           Element numberOfMeasurementObjectsElementChild = document
                   .createElement(PerformanceDataXMLConstants.NUMBER_OF_MEASUREMENT_OBJECTS_KEY);
           if (numberOfMeasurementObjectsElementChild != null) {
               numberOfMeasurementObjectsElementChild.setTextContent(data
                       .getNumberOfMeasurementObjects()
                       + "");
           }
           // </numberOfMeasurementObjects>
           performanceDataElement
                   .appendChild(numberOfMeasurementObjectsElementChild);

           // <numberOfMeasurements>
           Element numberOfMeasurementsElementChild = document
                   .createElement(PerformanceDataXMLConstants.NUMBER_OF_MEASUREMENTS_KEY);
           if (categoryElementChild != null) {
               numberOfMeasurementsElementChild.setTextContent(data
                       .getNumberOfMeasurements()
                       + "");
           }
           // </numberOfMeasurements>
           performanceDataElement
                   .appendChild(numberOfMeasurementsElementChild);

           // <average>
           Element averageElementChild = document.createElement(PerformanceDataXMLConstants.AVERAGE_KEY);
           if (averageElementChild != null) {
               averageElementChild.setTextContent(data.getAverage() + "");
           }
           // </average>
           performanceDataElement.appendChild(averageElementChild);

           // <averageWithoutFirstMeasurement>
           Element averageWithoutFirstMeasurementElementChild = document
                   .createElement(PerformanceDataXMLConstants.AVERAGE_WITHOUT_FIRST_MEASUREMENT_KEY);
           if (averageWithoutFirstMeasurementElementChild != null) {
               averageWithoutFirstMeasurementElementChild.setTextContent(data
                       .getAverageWithoutFirstMeasurement()
                       + "");
           }
           // </averageWithoutFirstMeasurement>
           performanceDataElement
                   .appendChild(averageWithoutFirstMeasurementElementChild);

           // <firstMeasurementTime>
           Element firstMeasurementTimeElementChild = document
                   .createElement(PerformanceDataXMLConstants.FIRST_MEASUREMENT_TIME_KEY);
           if (firstMeasurementTimeElementChild != null) {
               firstMeasurementTimeElementChild.setTextContent(data
                       .getFirstMeasurementTime()
                       + "");
           }
           // </firstMeasurementTime>
           performanceDataElement
                   .appendChild(firstMeasurementTimeElementChild);

           // <load>
           Element loadElementChild = document.createElement(PerformanceDataXMLConstants.LOAD_KEY);
           if (loadElementChild != null) {
               loadElementChild.setTextContent(data.getLoad() + "");
           }
           // </load>
           performanceDataElement.appendChild(loadElementChild);

           // <median>
           Element medianElementChild = document.createElement(PerformanceDataXMLConstants.MEDIAN_KEY);
           if (medianElementChild != null) {
               medianElementChild.setTextContent(data.getMedian() + "");
           }
           // </median>
           performanceDataElement.appendChild(medianElementChild);

           // <throughput>
           Element throughputElementChild = document
                   .createElement(PerformanceDataXMLConstants.THROUGHPUT_KEY);
           if (throughputElementChild != null) {
               throughputElementChild
                       .setTextContent(data.getThroughput() + "");
           }
           // </throughput>
           performanceDataElement.appendChild(throughputElementChild);

           // <timeTaken>
           Element timeTakenElementChild = document
                   .createElement(PerformanceDataXMLConstants.TIME_TAKEN_KEY);
           if (timeTakenElementChild != null) {
               timeTakenElementChild.setTextContent(data.getTimeTaken() + "");
           }
           // </timeTaken>
           performanceDataElement.appendChild(timeTakenElementChild);

           // <totalTime>
           Element totalTimeElementChild = document
                   .createElement(PerformanceDataXMLConstants.TOTAL_TIME_KEY);
           if (totalTimeElementChild != null) {
               totalTimeElementChild.setTextContent(data.getTotalTime() + "");
           }
           // </totalTime>
           performanceDataElement.appendChild(totalTimeElementChild);
       }
       return performanceDataElement;
   }
    
    
    /**
     * @param document
     * @return
     * @throws TransformerException
     */
    protected String writeToString(Document document)
            throws TransformerException {
        // Use a Transformer for aspectOutput
        TransformerFactory tFactory = TransformerFactory.newInstance();
        Transformer transformer = tFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        
        // indent the aspectOutput to make it more legible...
        transformer.setOutputProperty(
                "{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
        
        return result.getWriter().toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        String uri = "C:/test/schema/endpointstatistics/EndpointStatisticsData.xml";
        try {
            Map<String /* instanceName */, IEndpointStatisticsData> map = null;
            map = EndpointStatisticsDataReader.parseFromFile(uri);
            for (String instanceName : map.keySet()) {
                System.out.println(map.get(instanceName).getDisplayString());
            }
            
            String content = EndpointStatisticsDataWriter.serialize(map);
            System.out.println(content);
            EndpointStatisticsDataWriter.setContents(new File(uri), content);
            
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
    
}
