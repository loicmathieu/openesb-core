/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceAssemblyStatisticsDataCreator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.common.data.helper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeDataSupport;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenDataException;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularDataSupport;
import javax.management.openmbean.TabularType;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.esb.management.common.data.ServiceAssemblyStatisticsData;
import com.sun.esb.management.common.data.ServiceUnitStatisticsData;

/**
 * @author graj
 *
 */
public class ServiceAssemblyStatisticsDataCreator {
    
    protected static CompositeType serviceUnitStatsType;
    
    protected static CompositeType serviceAssemblyStatsType;
    
    /**  table index   */
    static String[]                STATS_TABLE_INDEX     = new String[] { "InstanceName" };
    
    /**
     * item names for service unit stats
     */
    protected static String[]      SU_STATS_ITEMS        = { "ServiceUnitName",
            "ServiceUnitStartupTime Avg (ms)", "ServiceUnitStopTime Avg (ms)",
            "ServiceUnitShutdownTime Avg (ms)", "Endpoints" };
    
    /**
     * descriptions for service unit stats
     */
    protected static String[]      SU_STATS_DESCRIPTIONS = {
            "Service Unit Name", "Service Unit StartupTime Avg (ms)",
            "Service Unit StopTime Avg (ms)",
            "Service Unit ShutdownTime Avg (ms)", "Endpoints List" };
    
    /** 
     * item names for SA stats
     */
    protected static String[]      SA_STATS_ITEMS        = { "InstanceName",
            "ServiceAssemblyName", "LastStartupTime", "StartupTime Avg (ms)",
            "StopTime Avg (ms)", "ShutdownTime Avg (ms)", "UpTime",
            "ServiceUnitStatistics"                     };
    
    /**
     * item descriptions for SA stats
     */
    protected static String[]      SA_STATS_DESCRIPTIONS = {
            "JBI Instance Name", "Service Assembly Name", "Last Startup Time",
            "Startup Time Avg (ms)", "Stop Time Avg (ms)", "Up Time (ms)",
            "Shutdown Time Avg (ms)", "ServiceUnit Statistics" };
    
    /**
     * 
     */
    public ServiceAssemblyStatisticsDataCreator() {
    }
    
    /**
     * 
     * @param data
     * @return
     * @throws ManagementRemoteException
     */
    public static TabularData createTabularData(
            Map<String /* instanceName */, ServiceAssemblyStatisticsData> map)
            throws ManagementRemoteException {
        TabularType saStatsTableType = null;
        TabularData saStatsTable = null;
        Set<String> instances = map.keySet();
        CompositeData[] saStats = new CompositeData[instances.size()];
        
        try {
            //All the stats provided by deployment service are 
            //exposed. So we don't have a static composite type.
            
            int index = 0;
            for (String instance : instances) {
                ServiceAssemblyStatisticsData data = map.get(instance);
                if (data != null) {
                    
                    saStats[index++] = getCompositeData(data);
                }
            }
            //the tabular type could be constructed only after we have the component stats composite type
            //so we need atleast one entry
            if (index > 0 && saStats[0] != null) {
                saStatsTableType = new TabularType(
                        "DeploymentServiceStatistics",
                        "Deployment Service Statistics", saStats[0]
                                .getCompositeType(), STATS_TABLE_INDEX);
                saStatsTable = new TabularDataSupport(saStatsTableType);
                
                for (int innerIndex = 0; innerIndex < index; innerIndex++) {
                    saStatsTable.put(saStats[innerIndex]);
                }
            }
        } catch (OpenDataException ex) {
            throw new ManagementRemoteException(ex);
            
        }
        return saStatsTable;
    }
    
    /**
     * return the stats as composite data
     */
    protected static CompositeData getCompositeData(
            ServiceAssemblyStatisticsData data) throws OpenDataException {
        List<ServiceUnitStatisticsData> serviceUnitStatisticsList = null;
        serviceUnitStatisticsList = data.getServiceUnitStatisticsList();
        
        CompositeData[] serviceUnitStats = new CompositeData[serviceUnitStatisticsList
                .size()];
        
        int index = 0;
        for (ServiceUnitStatisticsData unitData : serviceUnitStatisticsList) {
            serviceUnitStats[index++] = getCompositeData(unitData);
        }
        Object values[] = { data.getInstanceName(), data.getName(),
                data.getLastStartupTime(), data.getStartupTimeAverage(),
                data.getStopTimeAverage(), data.getShutdownTimeAverage(),
                serviceUnitStats };
        return new CompositeDataSupport(serviceAssemblyStatsType,
                SA_STATS_ITEMS, values);
    }
    
    /** 
     * This method is used to obtain a CompositeData represenation
     * of the collected statistics 
     * @return CompositeData
     */
    protected static CompositeData getCompositeData(
            ServiceUnitStatisticsData data) throws OpenDataException {
        
        Object values[] = new Object[] { data.getName(),
                data.getStartupTimeAverage(), data.getStopTimeAverage(),
                data.getShutdownTimeAverage(), data.getEndpointNameArray() };
        return new CompositeDataSupport(serviceUnitStatsType, SU_STATS_ITEMS,
                values);
        
    }
    
    /**
     * This is a helper method that is used to create the 
     * composite types for SA stats and SU stats
     */
    protected void createCompositeTypes() {
        try {
            OpenType[] SU_STATS_ITEM_TYPES = new OpenType[] {
                    SimpleType.STRING, SimpleType.LONG, SimpleType.LONG,
                    SimpleType.LONG, new ArrayType(1, SimpleType.STRING) };
            
            //CompositeType for Service Unit stats
            serviceUnitStatsType = new CompositeType("ServiceUnitStatistics",
                    "Service Unit Statistics", SU_STATS_ITEMS,
                    SU_STATS_DESCRIPTIONS, SU_STATS_ITEM_TYPES);
            
            //Item types for SA stats
            OpenType[] SA_STATS_TYPES = new OpenType[] { SimpleType.STRING,
                    SimpleType.STRING, SimpleType.DATE, SimpleType.LONG,
                    SimpleType.LONG, SimpleType.LONG, SimpleType.LONG,
                    new ArrayType(1, serviceUnitStatsType) };
            
            //CompositeType for SA stats
            serviceAssemblyStatsType = new CompositeType(
                    "ServiceAssemblyStatistics", "Service Assembly Statistics",
                    SA_STATS_ITEMS, SA_STATS_DESCRIPTIONS, SA_STATS_TYPES);
        } catch (OpenDataException ode) {
            
        }
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
