/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.base.services;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.QueryExp;
import javax.management.ReflectionException;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.common.JBIRemoteException;

/**
 * @author graj
 *
 */
public abstract class AbstractServiceImpl implements Serializable {
    static final long                         serialVersionUID = -1L;
    
    protected static String CANNED_RESPONSE = 
        "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" +
           "<jbi-task version=\"1.0\" xmlns=\"http://java.sun.com/xml/ns/jbi/management-message\">" +
                "<jbi-task-result><frmwk-task-result>" +
                            "<frmwk-task-result-details>" +
                                "<task-result-details>" +
                                        "<task-id>OPERATION</task-id>" +
                                        "<task-result>SUCCESS</task-result>" +
                                        "<message-type>INFO</message-type>" +
                                "</task-result-details>" +
                                "<locale>en</locale>" +
                            "</frmwk-task-result-details>" +
                "</frmwk-task-result></jbi-task-result>" +
             "</jbi-task>";    

    /** i18n */
    protected static I18NBundle       I18NBUNDLE      = null;


    /** remote MBeanServer connection */
    protected transient MBeanServerConnection remoteConnection;

    /** is this a local or remote connection */
    protected boolean                         isRemoteConnection;
    
    /** Constructor - Constructs a new instance of AbstractServiceImpl */
    public AbstractServiceImpl() {
        this(null, false);
    }

    /**
     * Constructor - Constructs a new instance of AbstractServiceImpl
     *
     * @param serverConnection
     */
    public AbstractServiceImpl(MBeanServerConnection serverConnection) {
        this(serverConnection, false);
    }

    /**
     * Constructor - Constructs a new instance of AbstractServiceImpl
     *
     * @param serverConnection
     * @param isRemoteConnection
     */
    public AbstractServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        this.remoteConnection = serverConnection;
        this.isRemoteConnection = isRemoteConnection;
    }

    /**
     * is this a local or remote connection
     *
     * @return true if remote, false if local
     */
    protected boolean isRemoteConnection() {
        return this.isRemoteConnection;
    }


    // //////////////////////////////////////////////////////
    // -- Common Operations --
    // //////////////////////////////////////////////////////
    /**
     * gives the I18N bundle
     *
     * @return I18NBundle object
     */
    protected I18NBundle getI18NBundle() {
        // lazzy initialize the Client
        if (I18NBUNDLE == null) {
            I18NBUNDLE = new I18NBundle("com.sun.jbi.ui.client");
        }
        return I18NBUNDLE;
    }

    /**
     * returns mbean server connection.
     *
     * @throws IllegalStateException
     *             on error
     * @return mbeanserver interface
     */
    protected MBeanServerConnection getMBeanServerConnection()
            throws IllegalStateException {
        if (this.remoteConnection == null) {
            throw new IllegalStateException(
                    "caps.management.client.jmx.connection.not.open");
        }
        return this.remoteConnection;
    }

    /**
     * Test whether an mbean is registered.
     *
     * @param objectName
     * @return true when the mbean is registered, false otherwise
     * @throws JBIRemoteException
     */
    protected boolean isRegistered(ObjectName objectName) {
        boolean result = false;
        Boolean resultObject = null;
        try {
            resultObject = (Boolean) this.remoteConnection
                    .isRegistered(objectName);
            if (resultObject != null) {
                result = resultObject.booleanValue();
            }
        } catch (IOException e) {
            result = false;
        } catch (RuntimeException exception) {
            result = false;
        }

        return result;
    }

    /**
     * return Administration Service MBean object name
     * @return object name of the Administration Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getAdministrationServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsAdministrationServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }

    /**
     * return Configuration Service MBean object name
     * @return object name of the Configuration Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getConfigurationServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsConfigurationServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }
    
    /**
     * return Deployment Service MBean object name
     * @return object name of the Deployment Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getDeploymentServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsDeploymentServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }

    /**
     * return Installation Service MBean object name
     * @return object name of the Installation Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getInstallationServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsInstallationServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }

    /**
     * return Runtime Management Service MBean object name
     * @return object name of the Runtime Management Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getRuntimeManagementServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsRuntimeManagementServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }

    /**
     * return Performance Measurement Service MBean object name
     * @return object name of the Performance Measurement Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getPerformanceMeasurementServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsPerformanceMeasurementServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }

    /**
     * return Notification Service MBean object name
     * @return object name of the Notification Service UI MBean
     * @throws ManagementRemoteException
     */
    protected ObjectName getNotificationServiceMBeanObjectName() throws ManagementRemoteException {
        try {
            return JBIJMXObjectNames.getJavaCapsNotificationServiceMBeanObjectName();
        } catch (MalformedObjectNameException e) {
            throw ManagementRemoteException.filterJmxExceptions(e);
        }
    }
    
    /**
     * get a list of object names matching the pattern/query
     * @param pattern   pattern string
     * @param query     query expression 
     * @return a set of  object names matching the pattern and query 
     */
    @SuppressWarnings("unchecked")
    protected Set<ObjectName> queryNames(String pattern, QueryExp query)  throws ManagementRemoteException {
        Set<ObjectName> objectNames = null;
        if ( remoteConnection != null ) {
            try {
                objectNames = (Set<ObjectName>)remoteConnection.queryNames(new ObjectName(pattern),query);
            } catch(Exception e) {
                 throw ManagementRemoteException.filterJmxExceptions(e);
            }
        }
        return objectNames;
    }

    /**
     * Returns a map of target names to an array of target instance names.
     * In the case cluster targets, the key contains the cluster target name, 
     * and the the value contains an array of the "target instance" names.
     * If it is not a cluster target, the key contains the targetName, and 
     * the value is null.
     * 
     * @return map of target names to array of target instance names
     * @throws ManagementRemoteException
     */
    @SuppressWarnings("unchecked")
    protected Map<String /*targetName*/, String[] /*targetInstanceNames*/> listTargetNames() throws ManagementRemoteException {
        Map<String /*targetName*/, String[] /*targetInstanceNames*/> targetNameToTargetInstanceNameMap = null;
        ObjectName mbeanName = this.getAdministrationServiceMBeanObjectName();
        Object[] params = null;
        String[] signature = null;
        targetNameToTargetInstanceNameMap = (Map<String /*targetName*/, String[] /*targetInstanceNames*/>) this.invokeMBeanOperation(mbeanName,
                "listTargetNames", params, signature);
        return targetNameToTargetInstanceNameMap;
    }
    

    /**
     * open connection, invokes the operation on mbean and closes connection.
     * This should not be used if the connection is already opened or not to be
     * close after the invoke operation.
     *
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param params
     *            parameters
     * @param signature
     *            signature of the parameters
     * @throws JBIRemoteException
     *             on error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName, Object[] params, String[] signature)
            throws ManagementRemoteException {

        Object result = null;
        if (this.remoteConnection != null) {
            try {
                result = this.remoteConnection.invoke(objectName,
                        operationName, params, signature);
            } catch (InstanceNotFoundException jmxException) {
                throw ManagementRemoteException.filterJmxExceptions(jmxException);
            } catch (MBeanException jmxException) {
                throw ManagementRemoteException.filterJmxExceptions(jmxException);
            } catch (ReflectionException jmxException) {
                throw ManagementRemoteException.filterJmxExceptions(jmxException);
            } catch (IOException jmxException) {
                throw ManagementRemoteException.filterJmxExceptions(jmxException);
            } finally {
            }
        }

        return result;

    }

    /**
     * Single param operation invocation.
     *
     * @return result object
     * @param objectName
     *            object name
     * @param operationName
     *            operation name
     * @param param
     *            String parameter
     * @throws ManagementRemoteException
     *             on error
     */
    protected Object invokeMBeanOperation(ObjectName objectName,
            String operationName, String param)
            throws ManagementRemoteException {

        Object[] params = new Object[1];
        params[0] = param;

        String[] signature = new String[1];
        signature[0] = "java.lang.String";

        return this.invokeMBeanOperation(objectName, operationName, params,
                signature);
    }
}
