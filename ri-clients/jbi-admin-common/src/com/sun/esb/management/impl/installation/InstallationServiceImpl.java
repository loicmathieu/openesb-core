/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.installation;

import java.io.Serializable;
import java.util.Map;
import java.util.Properties;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;

import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.base.services.BaseServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines client operations for common installation services for the client.
 * 
 * @author graj
 */
public class InstallationServiceImpl extends BaseServiceImpl implements
        Serializable, InstallationService {
    
    static final long serialVersionUID = -1L;
    
    /** Constructor - Constructs a new instance of InstallationServiceImpl */
    public InstallationServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of InstallationServiceImpl
     * 
     * @param serverConnection
     */
    public InstallationServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of InstallationServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public InstallationServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }
    
    /**
     * installs component (service engine, binding component)
     * 
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installComponent(java.lang.String, java.util.Properties, java.lang.String)
     */
    public String installComponent(String zipFilePath, Properties paramProps,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        if (this.isRemoteConnection() == true) {
            zipFilePath = uploadFile(zipFilePath);
        }
        
        Object[] params = new Object[3];
        params[0] = zipFilePath;
        params[1] = paramProps;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Properties";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName, "installComponent",
                params, signature);
        
        this.removeUploadedFile(); // if uploaded
        
        return resultObject.toString();        
    }
    
    /**
     * installs component (service engine, binding component)
     * 
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installComponent(java.lang.String, java.lang.String)
     */
    public String installComponent(String zipFilePath, String targetName)
            throws ManagementRemoteException {
        return this.installComponent(zipFilePath, new Properties(), targetName);
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @return name of the component as map of [targetName,string].
     * @throws ManagementRemoteException
     *             on error     * 
     * @see com.sun.esb.management.api.installation.InstallationService#installComponent(java.lang.String, java.util.Properties, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> installComponent(String zipFilePath,
            Properties paramProps, String[] targetNames)
            throws ManagementRemoteException {
        Map<String, String> resultObject = null;
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        
        Object[] params = new Object[3];
        params[0] = zipFilePath;
        params[1] = paramProps;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Properties";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "installComponent", params, signature);
        return resultObject;
    }
    
    /**
     * installs shared library
     * 
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @return shared library name as map of [targetName,string].
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installComponent(java.lang.String, java.lang.String[])
     */
    public Map<String, String> installComponent(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException {
        return installComponent(zipFilePath, new Properties(), targetNames);
    }
    
    /**
     * installs component (service engine, binding component) from the domain
     * target
     * 
     * @param componentName
     *            name of the component.
     * @param component
     *            configuration properties
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#installComponentFromDomain(java.lang.String, java.util.Properties, java.lang.String)
     */
    public String installComponentFromDomain(String componentName,
            Properties properties, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = properties;
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Properties";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "installComponentFromDomain", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * installs component (service engine, binding component) from the domain
     * target
     * 
     * @param componentName
     *            name of the component.
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installComponentFromDomain(java.lang.String, java.lang.String)
     */
    public String installComponentFromDomain(String componentName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "installComponentFromDomain", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * installs component from Domain( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installComponentFromDomain(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> installComponentFromDomain(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "installComponentFromDomain", params, signature);
        return resultObject;
    }
    
    /**
     * installs component from Domain( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param properties
     *            configuration properties
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installComponentFromDomain(java.lang.String, java.util.Properties, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> installComponentFromDomain(String componentName,
            Properties properties, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = properties;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.util.Properties";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "installComponentFromDomain", params, signature);
        return resultObject;
    }
    
    /**
     * installs shared library
     * 
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installSharedLibrary(java.lang.String, java.lang.String)
     */
    public String installSharedLibrary(String zipFilePath, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        if (this.isRemoteConnection() == true) {
            zipFilePath = uploadFile(zipFilePath);
        }
        
        Object[] params = new Object[2];
        params[0] = zipFilePath;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "installSharedLibrary", params, signature);
        
        this.removeUploadedFile(); // if uploaded
        
        return resultObject.toString();
    }
    
    /**
     * installs shared library
     * 
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @return shared library name as map of [targetName,string].
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#installSharedLibrary(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> installSharedLibrary(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        Object[] params = new Object[2];
        params[0] = zipFilePath;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "installSharedLibrary", params, signature);
        
        return resultObject;
    }
    
    /**
     * installs shared library from domain target
     * 
     * @param libraryName
     *            Shared Library Name
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installSharedLibraryFromDomain(java.lang.String, java.lang.String)
     */
    public String installSharedLibraryFromDomain(String libraryName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = libraryName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "installSharedLibraryFromDomain", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * installs shared library
     * 
     * @param libraryName
     *            name of the library
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#installSharedLibraryFromDomain(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> installSharedLibraryFromDomain(
            String libraryName, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = libraryName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "installSharedLibraryFromDomain", params, signature);
        return resultObject;
    }
    
    /**
     * uninstalls component (service engine, binding component) with option to
     * retain in domain target and option to forcibly remove from specified
     * target
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String, boolean, boolean, java.lang.String)
     */
    public String uninstallComponent(String componentName, boolean forceDelete,
            boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = Boolean.valueOf(forceDelete);
        params[2] = Boolean.valueOf(retainInDomain);
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        signature[3] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "uninstallComponent", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * uninstalls component (service engine, binding component) with forcibly
     * remove option
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String, boolean, java.lang.String)
     */
    public String uninstallComponent(String componentName, boolean forceDelete,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = Boolean.valueOf(forceDelete);
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "uninstallComponent", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * uninstalls component (service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     *            name of the target for this operation
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String, java.lang.String)
     */
    public String uninstallComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "uninstallComponent", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @return name of the component as [targetName, String] map.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> uninstallComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "uninstallComponent", params, signature);
        
        return resultObject;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String, boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> uninstallComponent(String componentName,
            boolean forceDelete, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = forceDelete;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "uninstallComponent", params, signature);
        return resultObject;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String, boolean, boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> uninstallComponent(String componentName,
            boolean forceDelete, boolean retainInDomain, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = forceDelete;
        params[2] = retainInDomain;
        params[3] = targetNames;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        signature[3] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "uninstallComponent", params, signature);
        return resultObject;
    }
    
    /**
     * uninstalls shared library with option to retain in domain target and
     * option to forcibly remove from specified target
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String, boolean, boolean, java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = sharedLibraryName;
        params[1] = Boolean.valueOf(forceDelete);
        params[2] = Boolean.valueOf(retainInDomain);
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        signature[3] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "uninstallSharedLibrary", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * uninstalls shared library with option to forcibly remove
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String, boolean, java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = sharedLibraryName;
        params[1] = Boolean.valueOf(forceDelete);
        params[2] = targetName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "uninstallSharedLibrary", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String, java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            String targetName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = sharedLibraryName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName,
                "uninstallSharedLibrary", params, signature);
        
        return resultObject.toString();
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param targetNames
     * @return shared library name as [targetName, string] map.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> uninstallSharedLibrary(String sharedLibraryName,
            String[] targetNames) throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = sharedLibraryName;
        params[1] = targetNames;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "uninstallSharedLibrary", params, signature);
        
        return resultObject;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetNames
     *            list of names of targets for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String, boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[3];
        params[0] = sharedLibraryName;
        params[1] = forceDelete;
        params[2] = targetNames;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "uninstallSharedLibrary", params, signature);
        return resultObject;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String, boolean, boolean, java.lang.String[])
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, boolean retainInDomain, String[] targetNames)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Map<String, String> resultObject = null;
        
        Object[] params = new Object[4];
        params[0] = sharedLibraryName;
        params[1] = forceDelete;
        params[2] = retainInDomain;
        params[3] = targetNames;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "boolean";
        signature[3] = targetNames.getClass().getName();
        
        resultObject = (Map<String, String>) this.invokeMBeanOperation(
                mbeanName, "uninstallSharedLibrary", params, signature);
        return resultObject;
    }
    
    /**
     * upgrades component (service engine, binding component) Upgrades a
     * component in a way that actually involves the component. During the
     * upgrade processing, the component's implementation of the new upgrade SPI
     * will be invoked to give the component the opportunity to perform any
     * special processing necessary to complete the upgrade. Components which do
     * not provide an implementation of the upgrade SPI can still be updated
     * using the updateComponent API.
     * 
     * Also, in the upgrade implementation, changes in the component's
     * installation descriptor will be allowed, with the exception of the
     * component name (for obvious reasons). This allows new shared library
     * dependencies, changes to the class names of the component's SPI
     * implementations, and changes to the component's class loading preferences
     * (class path and class loading order). These changes are allowed
     * regardless of whether or not the component provides an implementation of
     * the new upgrade SPI.
     * 
     * @param componentName
     *            Name of the component to update.
     * @param zipFilePath
     *            archive file in a zip format
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     *             
     * @see com.sun.esb.management.api.installation.InstallationService#upgradeComponent(java.lang.String, java.lang.String)
     */
    public String upgradeComponent(String componentName, String zipFilePath)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getInstallationServiceMBeanObjectName();
        Object resultObject = null;
        
        if (this.isRemoteConnection() == true) {
            zipFilePath = uploadFile(zipFilePath);
        }
        
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = zipFilePath;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = this.invokeMBeanOperation(mbeanName, "upgradeComponent",
                params, signature);
        
        this.removeUploadedFile(); // if uploaded
        
        return resultObject.toString();
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
