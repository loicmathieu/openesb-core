/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PerformanceMeasurementServiceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.impl.performance;

import java.io.Serializable;

import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.TabularData;

import com.sun.esb.management.api.performance.PerformanceMeasurementService;
import com.sun.esb.management.base.services.AbstractServiceImpl;
import com.sun.esb.management.common.ManagementRemoteException;

/**
 * Defines client operations to measure performance statistics. 
 * e.g., time taken to normalize/denormalize, encode/decode, wire-to-NMR 
 * on the endpoints, etc.
 * 
 * @author graj
 *
 */
public class PerformanceMeasurementServiceImpl extends AbstractServiceImpl
        implements Serializable, PerformanceMeasurementService {
    
    static final long serialVersionUID = -1L;
    
    /** Constructor - Constructs a new instance of PerformanceMeasurementServiceImpl */
    public PerformanceMeasurementServiceImpl() {
        super(null, false);
    }
    
    /**
     * Constructor - Constructs a new instance of PerformanceMeasurementServiceImpl
     * 
     * @param serverConnection
     */
    public PerformanceMeasurementServiceImpl(MBeanServerConnection serverConnection) {
        super(serverConnection, false);
    }
    
    /**
     * Constructor - Constructs a new instance of PerformanceMeasurementServiceImpl
     * 
     * @param serverConnection
     * @param isRemoteConnection
     */
    public PerformanceMeasurementServiceImpl(MBeanServerConnection serverConnection,
            boolean isRemoteConnection) {
        super(serverConnection, isRemoteConnection);
    }
    
    /**
     * Resets the performance measurements on the endpoint.
     * @param componentName
     * @param endpoint
     * @param targetName
     * @param targetInstanceName 
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#clearPeformaceInstrumentationMeasurement(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public void clearPeformaceInstrumentationMeasurement(String componentName,
            String endpoint, String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = endpoint;
        params[2] = targetName;
        params[3] = targetInstanceName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        this.invokeMBeanOperation(mbeanName,
                "clearPeformaceInstrumentationMeasurement", params, signature);
    }
    
    /**
     * Retrieves the performance measurement enabling flag.
     * @param componentName
     * @param targetName
     * @param targetInstanceName 
     * @return true if measurement enabled, false if not
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#getPerformanceInstrumentationEnabled(java.lang.String, java.lang.String, java.lang.String)
     */
    public boolean getPerformanceInstrumentationEnabled(String componentName,
            String targetName, String targetInstanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        Boolean resultObject = null;
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = targetInstanceName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        
        resultObject = (Boolean) this.invokeMBeanOperation(mbeanName,
                "getPerformanceInstrumentationEnabled", params, signature);
        
        return resultObject;
    }
    
    /**
     * Retrieves the performance measurement data for the specified endpoint.
     * @param componentName
     * @param endpoint
     * @param targetName
     * @param targetInstanceName 
     * @return XML String representing PerformanceData Map
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#getPerformanceInstrumentationMeasurement(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
     */
    public String getPerformanceInstrumentationMeasurement(
            String componentName, String endpoint, String targetName, String targetInstanceName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String resultObject = null;
        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = endpoint;
        params[2] = targetName;
        params[3] = targetInstanceName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        resultObject = (String) this.invokeMBeanOperation(mbeanName,
                "getPerformanceInstrumentationMeasurement", params, signature);
        
        return resultObject;
    }
    
    /**
     * Retrieves the performance statistics categories. Each item in the array is the key to the composite performance data, which also indicates the type (e.g. normalization) of measurement.
     * @param componentName
     * @param targetName
     * @param targetInstanceName 
     * @return array of performance measurement categories
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#getPerformanceMeasurementCategories(java.lang.String, java.lang.String, java.lang.String)
     */
    public String[] getPerformanceMeasurementCategories(String componentName,
            String targetName, String targetInstanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String[] resultObject = null;
        Object[] params = new Object[3];
        params[0] = componentName;
        params[1] = targetName;
        params[2] = targetInstanceName;
        
        String[] signature = new String[3];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        signature[2] = "java.lang.String";
        
        resultObject = (String[]) this.invokeMBeanOperation(mbeanName,
                "getPerformanceMeasurementCategories", params, signature);
        
        return resultObject;
    }
    
    /**
     * Sets the performance measurement enabling flag.
     * @param componentName
     * @param flag
     * @param targetName
     * @param targetInstanceName 
     * @throws ManagementRemoteException
     * 
     * @see com.sun.esb.management.api.performance.PerformanceMeasurementService#setPerformanceInstrumentationEnabled(java.lang.String, boolean, java.lang.String, java.lang.String)
     */
    public void setPerformanceInstrumentationEnabled(String componentName,
            boolean flag, String targetName, String targetInstanceName) throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        Object[] params = new Object[4];
        params[0] = componentName;
        params[1] = flag;
        params[2] = targetName;
        params[3] = targetName;
        
        String[] signature = new String[4];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        signature[2] = "java.lang.String";
        signature[3] = "java.lang.String";
        
        this.invokeMBeanOperation(mbeanName,
                "setPerformanceInstrumentationEnabled", params, signature);
    }
    
    /**
     * This method is used to provide JBIFramework statistics in the
     * given target.
     * @param target target name.
     * @return String table of framework statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getFrameworkStatistics(String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = (String)this.invokeMBeanOperation(mbeanName,
                "getFrameworkStatistics", params, signature);
        return resultObject;
    }
    
    /**
     * This method is used to provide JBIFramework statistics in the
     * given target.
     * @param target target name.
     * @return TabularData table of framework statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getFrameworkStatisticsAsTabularData(String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getFrameworkStatisticsAsTabularData", params, signature);
        return resultObject;
    }

    /**
     * This method is used to provide statistics for the given component
     * in the given target
     * @param targetName target name
     * @param componentName component name
     * @return String table of component statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     *
     */
    public String getComponentStatistics(String componentName, String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String resultObject = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (String)this.invokeMBeanOperation(mbeanName,
                "getComponentStatistics", params, signature);
        return resultObject;
    }
    
    /**
     * This method is used to provide statistics for the given component in the
     * given target
     * 
     * @param targetName
     *            target name
     * @param componentName
     *            component name
     * @return TabularData table of component statistics
     * 
     * If the target is a standalone instance the table will have one entry. If
     * the target is a cluster the table will have an entry for each instance.
     * 
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     * 
     */
    public TabularData getComponentStatisticsAsTabularData(
            String componentName, String targetName)
            throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getComponentStatisticsAsTabularData", params, signature);
        return resultObject;
    }
    

    
                
    /**
     * This method is used to provide statistic information about the given 
     * endpoint in the given target
     * @param targetName target name
     * @param endpointName the endpoint Name
     * @return String table of endpoint statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getEndpointStatistics(String endpointName, String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String resultObject = null;

        Object[] params = new Object[2];
        params[0] = endpointName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (String)this.invokeMBeanOperation(mbeanName,
                "getEndpointStatistics", params, signature);
        return resultObject;
    }
    
    /**
     * This method is used to provide statistic information about the given 
     * endpoint in the given target
     * @param targetName target name
     * @param endpointName the endpoint Name
     * @return TabularData table of endpoint statistics
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getEndpointStatisticsAsTabularData(String endpointName, String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[2];
        params[0] = endpointName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getEndpointStatisticsAsTabularData", params, signature);
        return resultObject;
    }

    /**
     * This method is used to provide statistics about the message service in the
     * given target.
     * @param target target name.
     * @return String table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getNMRStatistics(String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = (String)this.invokeMBeanOperation(mbeanName,
                "getNMRStatistics", params, signature);
        return resultObject;
    }

    /**
     * This method is used to provide statistics about the message service in the
     * given target.
     * @param target target name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getNMRStatisticsAsTabularData(String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getNMRStatisticsAsTabularData", params, signature);
        return resultObject;
    }
    
    
    /**
     * This method is used to provide statistics about a Service Assembly
     * in the given target.
     * @param target target name.
     * @param assemblyName the service assembly name.
     * @return String table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public String getServiceAssemblyStatistics(String assemblyName, String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        String resultObject = null;

        Object[] params = new Object[2];
        params[0] = assemblyName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (String)this.invokeMBeanOperation(mbeanName,
                "getServiceAssemblyStatistics", params, signature);
        return resultObject;
    }

    /**
     * This method is used to provide statistics about a Service Assembly
     * in the given target.
     * @param target target name.
     * @param assemblyName the service assembly name.
     * @return TabularData table of NMR statistics in the given target.
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * For more information about the type of the entries in table please refer
     * to <code>JBIStatisticsMBean</code>
     */
    public TabularData getServiceAssemblyStatisticsAsTabularData(String componentName, String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getServiceAssemblyStatisticsAsTabularData", params, signature);
        return resultObject;
    }

   /**
     * This method is used to provide a list of consuming endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of consuming endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getConsumingEndpointsForComponentAsTabularData(String componentName, String targetName)
    throws ManagementRemoteException
    {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getConsumingEndpointsForComponentAsTabularData", params, signature);
        return resultObject;        
    }

    
    /**
     * This method is used to provide a list of provisioning endpoints for a component.
     * @param componentName component name
     * @param target target name.
     * @return TabularData list of provisioning endpoints 
     *
     * If the target is a standalone instance the table will have one entry.
     * If the target is a cluster the table will have an entry for each instance.
     *
     * Each entry in this tabular data is of the following composite type
     * 
     *  String            - "InstanceName",
     *  String[]          - "Endpoints",
     */
    public TabularData getProvidingEndpointsForComponentAsTabularData(String componentName, String targetName)
    throws ManagementRemoteException
    {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        TabularData resultObject = null;

        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = targetName;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        resultObject = (TabularData)this.invokeMBeanOperation(mbeanName,
                "getProvidingEndpointsForComponentAsTabularData", params, signature);
        return resultObject;            
    }
        
    
    /**
     * This method is used to enable monitoring of timing 
     * information about message exchanges
     * @param targetName the target name
     */
    public void enableMessageExchangeMonitoring(String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        this.invokeMBeanOperation(mbeanName,
                "enableMessageExchangeMonitoring", params, signature);
    }
    
    
    /**
     * This method is used to disable monitoring of timing 
     * information about message exchanges
     * @param targetName the target name
     */
    public void disableMessageExchangeMonitoring(String targetName)
    throws ManagementRemoteException {
        ObjectName mbeanName = this.getPerformanceMeasurementServiceMBeanObjectName();
        
        Object[] params = new Object[1];
        params[0] = targetName;
        
        String[] signature = new String[1];
        signature[0] = "java.lang.String";
        
        this.invokeMBeanOperation(mbeanName,
                "disableMessageExchangeMonitoring", params, signature);
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        
    }
    
}
