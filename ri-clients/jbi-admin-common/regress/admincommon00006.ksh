#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)admincommon00006.ksh - test of common-client component configuration commands
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

testname=`echo "$0" | sed 's/^.*\///' | sed 's/\..*//'`
echo "${testname} : Test of common-client component configuration commands."

#regress setup
. ./regress_defs.ksh

run_test()
{
    ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml pkg.test.component
 
    COMPONENT_ARCHIVE=$UI_REGRESS_DIST_DIR/component-with-custom-mbean.jar
    COMPONENT_NAME=admin-common-binding-1
    TARGET1=server
    TARGET2=ccfg-cluster
  
    INSTANCE1=${TARGET2}-instance-1

    # this test just does not work property with NODEAGENT=agent1
    NODEAGENT=commonagent1

    # cluster and member instance setup
    asadmin create-node-agent -t --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $NODEAGENT
    asadmin create-cluster  --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $TARGET2
    asadmin create-instance --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords --cluster $TARGET2 --nodeagent $NODEAGENT $INSTANCE1
    
    asadmin start-node-agent  -t --passwordfile $JV_AS8BASE/passwords $NODEAGENT
    asadmin start-instance  --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $INSTANCE1

    # component setup
    $JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE -Djbi.target=$TARGET1 install-component
    $JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE -Djbi.target=$TARGET2 install-component

    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET1 start-component
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET2 start-component

    # test 
    ant -q -emacs -Djbi.task.fail.on.error=false -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml -Dcomponent.name=$COMPONENT_NAME test.component.configuration.up

    # shut down the cluster and standalone instance, and run the test again
    #asadmin stop-instance   --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $INSTANCE1

    #ant -q -emacs -Djbi.task.fail.on.error=false -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $testname.xml -Dcomponent.name=$COMPONENT_NAME test.component.configuration.down

    # component cleanup
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET1 stop-component
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET2 stop-component
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET1 shut-down-component
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET2 shut-down-component
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET1 uninstall-component
    $JBI_ANT -Djbi.component.name=$COMPONENT_NAME -Djbi.target=$TARGET2 uninstall-component
   

    # cluster and member cleanup
    asadmin stop-instance   --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $INSTANCE1
    asadmin delete-instance --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $INSTANCE1
    asadmin delete-cluster  --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $TARGET2
    asadmin delete-node-agent-config --port $ASADMIN_PORT --passwordfile $JV_AS8BASE/passwords $NODEAGENT
    # asadmin stop-node-agent   $NODEAGENT
    asadmin delete-node-agent $NODEAGENT

}

################## MAIN ##################
####
# Execute the test
####

local_test_setup

#this is to correct for differences in ant behavior from version 1.5->1.6.  RT 6/18/05
run_test | tr -d '\r' | sed 's/en_US/en/' | sed -e '/^$/d' | grep -v "_PORT" | grep -v "port=" | grep -v "^Redirecting "
local_test_cleanup

exit 0
