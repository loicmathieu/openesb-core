#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)admincommon00004.ksh - test that issue 60 remains fixed.
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


echo "admincommon00008: Test the application configuration management operations."

#regress setup
. ./regress_defs.ksh

COMPONENT_ARCHIVE=$UI_REGRESS_DIST_DIR/component-with-custom-mbean.jar
COMPONENT_NAME=admin-common-binding-1

# component setup
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f admincommon00008.xml pkg.test.component
$JBI_ANT -Djbi.install.file=$COMPONENT_ARCHIVE  install-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME start-component

# test
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml list.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml add.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml list.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml set.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml list.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml delete.app.config
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -Dcomponent.name=$COMPONENT_NAME -Dtarget="server" -f admincommon00008.xml list.app.config

# component cleanup
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT -Djbi.component.name=$COMPONENT_NAME uninstall-component

exit 0
