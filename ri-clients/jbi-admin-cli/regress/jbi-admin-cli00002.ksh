#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbi-admin-cli00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#


#----------------------------------------------------------------------------------
# Install the shared library and the components
#----------------------------------------------------------------------------------
test_install()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-shared-library cli-test-sns1.jar"
  $AS8BASE/bin/asadmin install-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-sns1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install binding component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-binding1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-binding1.jar 
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Install service engine cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "install-jbi-component cli-test-engine1.jar"
  $AS8BASE/bin/asadmin install-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-engine1.jar 
}


#----------------------------------------------------------------------------------
# Deploy the service assembly
#----------------------------------------------------------------------------------
test_deploy()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Deploy the service assembly cli_test_assembly_unit_1"
  echo "-------------------------------------------------------------------"
  echo "deploy-jbi-service-assembly cli-test-au1.zip"
  $AS8BASE/bin/asadmin deploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS $UI_REGRESS_DIST_DIR/cli-test-au1.zip 
}


#----------------------------------------------------------------------------------
# Undeploy the Service Assembly
#----------------------------------------------------------------------------------
test_undeploy()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Undeploy the service assembly cli_test_assembly_unit_1"
  echo "-------------------------------------------------------------------"
  echo "undeploy-jbi-service-assembly cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin undeploy-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
}

#----------------------------------------------------------------------------------
# Uninstall the components and shared library
#----------------------------------------------------------------------------------
test_uninstall()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the component cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin uninstall-jbi-component      --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Uninstall the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "uninstall-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin uninstall-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
}

#----------------------------------------------------------------------------------
# Start the components
#----------------------------------------------------------------------------------
test_start_component()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the component cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin start-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
}

#----------------------------------------------------------------------------------
# Stop the components
#----------------------------------------------------------------------------------
test_stop_component()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the component cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-component cli_test_engine1"
  $AS8BASE/bin/asadmin stop-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
}

#----------------------------------------------------------------------------------
# Shut Down the components
#----------------------------------------------------------------------------------
test_shutdown_component()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-component cli_test_binding1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the component cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-componen cli_test_engine1"
  $AS8BASE/bin/asadmin shut-down-jbi-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
}

#----------------------------------------------------------------------------------
# Start the service assembly
#----------------------------------------------------------------------------------
test_start_service_assembly()
{  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Start the Service Assembly"
  echo "-------------------------------------------------------------------"
  echo "start-jbi-service-assembly cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin start-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
}

#----------------------------------------------------------------------------------
# Stop the service assembly
#----------------------------------------------------------------------------------
test_stop_service_assembly()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Stop the Service Assembly"
  echo "-------------------------------------------------------------------"
  echo "stop-jbi-service-assembly cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin stop-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
}

#----------------------------------------------------------------------------------
# Shut Down the service assembly
#----------------------------------------------------------------------------------
test_shutdown_service_assembly()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Shut Down the Service Assembly"
  echo "-------------------------------------------------------------------"
  echo "shut-down-jbi-service-assembly cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin shut-down-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
}

#----------------------------------------------------------------------------------
# Show the servcie assembly
#----------------------------------------------------------------------------------
test_show_service_assembly()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Service Assembly cli_test_assembly_unit_1"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-assembly cli_test_assembly_unit_1"
  $AS8BASE/bin/asadmin show-jbi-service-assembly --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_assembly_unit_1
}

#----------------------------------------------------------------------------------
# Show the components
#----------------------------------------------------------------------------------
test_show_component()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Binding Component cli_test_binding1"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-binding-component cli_test_binding1"
  $AS8BASE/bin/asadmin show-jbi-binding-component --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_binding1
  
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the Service Engine cli_test_engine1"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-service-engine cli_test_engine1"
  $AS8BASE/bin/asadmin show-jbi-service-engine --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_engine1
}


#----------------------------------------------------------------------------------
# Show the shared libraries
#----------------------------------------------------------------------------------
test_show_libraries()
{
  echo ""
  echo "-------------------------------------------------------------------"
  echo " Show the shared library cli_test_sns1"
  echo "-------------------------------------------------------------------"
  echo "show-jbi-shared-library cli_test_sns1"
  $AS8BASE/bin/asadmin show-jbi-shared-library --port $ADMIN_PORT --user $ADMIN_USER $ASADMIN_PW_OPTS cli_test_sns1
}

#----------------------------------------------------------------------------------
# Test the list commands filtering on each name that should be in the list.
#----------------------------------------------------------------------------------
test_list_all()
{
  list_service_assembly cli_test_assembly_unit_1
  list_service_engines cli_test_engine1
  list_binding_components cli_test_binding1
  list_shared_libraries cli_test_sns1
}

#----------------------------------------------------------------------------------
# Perform any necessary cleanup to restore the repository back to its initial state.
#----------------------------------------------------------------------------------
test_cleanup()
{
  cleanup server
}

#----------------------------------------------------------------------------------
# Main function called that will run the test
#----------------------------------------------------------------------------------
run_test()
{
 initilize_test 
 
 # Clean up the (Remove uninstall everything so this regression test will work
 #test_cleanup
 test_list_all
 
 # Install the components, shared library and deploy the service assembly
 test_install
 test_start_component
 test_deploy
 test_list_all
 
 # Show the componets
 test_show_component
 
 # Show the libraries
 test_show_libraries
 
 # Test that undeploy works
 test_undeploy
 
 # Shut down the components so we can uninstall them
 test_shutdown_component
 
 # Uninstall the components and the shared library
 test_uninstall
 
 # Reinstall the component and deploy the sa again so we can test stop and shutdown
 test_install
 test_start_component
 test_deploy
  
 # Stop the components and show state
 test_stop_component
 test_show_component
 
 # Start the component and show state
 test_start_component
 test_show_component
 
 # Shutdown the component and show state
 test_shutdown_component
 test_show_component
   
 # List everything again
 test_list_all
 
}

#----------------------------------------------------------------------------------
# Perform the regression setup and call the function that will run the test
#----------------------------------------------------------------------------------
TEST_NAME="jbi-admin-cli00002"
TEST_DESCRIPTION="Test Install, Deploy and Life Cycle Operations"
. ./regress_defs.ksh
run_test

exit 0


