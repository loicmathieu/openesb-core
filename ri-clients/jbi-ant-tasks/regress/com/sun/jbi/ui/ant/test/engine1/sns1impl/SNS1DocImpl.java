/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SNS1DocImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant.test.engine1.sns1impl;

import com.sun.jbi.ui.ant.test.sns1.SNS1Doc;

/**
 *
 * @author Sun Microsystems, Inc.
 */
public class SNS1DocImpl implements SNS1Doc {
    
    /** Creates a new instance of HelloSNSImpl */
    public SNS1DocImpl() {
    }
    
    public String getContentAsString() {
        return "SNS1 Doc from SharedNamespace1 Impl in Ant Test Binding1";
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        SNS1Doc sns1Doc = new SNS1DocImpl();
        System.out.println(sns1Doc.getContentAsString());
    }
    
}
