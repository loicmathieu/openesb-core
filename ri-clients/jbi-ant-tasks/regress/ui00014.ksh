#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00001.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# Verifier Test
# A SA is provided to the verifier. Verifier finds out that a component
# needed is not installed in the selected target and generates a verification
# report accordingly.
####

echo "ui00014 : Verify application and export application configuration."

. ./regress_defs.ksh

mkdir ${JV_SVC_BLD}/regress/testdata/tmp
cd ${JV_SVC_BLD}/regress/testdata/test-su1
jar -cvf ${JV_SVC_BLD}/regress/testdata/sa1/test-su1.jar *
cd ${JV_SVC_BLD}/regress/testdata/test-su2
jar -cvf ${JV_SVC_BLD}/regress/testdata/sa1/test-su2.jar *
cd ${JV_SVC_BLD}/regress/testdata/sa1
jar -cvf sa1.jar *
cd ${JV_SVC_BLD}/regress/testdata/test-su2
jar -cvf ${JV_SVC_BLD}/regress/testdata/sa2/test-su2.jar *
cd ${JV_SVC_BLD}/regress/testdata/sa2
jar -cvf sa2.jar *
cd  ${JV_SVC_ROOT}

# Package and install a test binding component
ant -q -emacs -propertyfile "$JV_JBI_DOMAIN_PROPS" -lib "$REGRESS_CLASSPATH" -f $JV_SVC_REGRESS/ui00014.xml pkg.test.component1

run_test_against_server()
{
$JBI_ANT -Djbi.install.file=${JV_SVC_BLD}/regress/dist/test-component1.jar install-component

$JBI_ANT -Djbi.include.deploy.command=true -Djbi.deploy.file="${JV_SVC_BLD}/regress/testdata/sa1/sa1.jar" -Djbi.template.dir="${JV_SVC_BLD}/regress/testdata/tmp" verify-application-environment

$JBI_ANT -Djbi.component.name=test-component1 start-component

$JBI_ANT -Djbi.component.name=test-component1 -Djbi.app.variables.file=$JV_SVC_BLD/regress/testcomponent1/test-component1-app-var.properties create-application-variable

$JBI_ANT -Djbi.component.name=test-component1 -Djbi.app.config.params.file=$JV_SVC_BLD/regress/testcomponent1/test-component1-app-config2.properties -Djbi.app.config.name=MyAppConfig3 create-application-configuration

$JBI_ANT -Djbi.include.deploy.command=true -Djbi.deploy.file="${JV_SVC_BLD}/regress/testdata/sa2/sa2.jar" -Djbi.template.dir="${JV_SVC_BLD}/regress/testdata/tmp" verify-application-environment

$JBI_ANT -Djbi.deploy.file="${JV_SVC_BLD}/regress/testdata/sa2/sa2.jar" deploy-service-assembly

$JBI_ANT -Djbi.service.assembly.name=SA2 -Djbi.config.dir="${JV_SVC_BLD}/regress/testdata/tmp" export-application-environment

$JBI_ANT -Djbi.service.assembly.name=SA2 undeploy-service-assembly
$JBI_ANT -Djbi.component.name=test-component1 stop-component
$JBI_ANT -Djbi.component.name=test-component1 shut-down-component
$JBI_ANT -Djbi.component.name=test-component1 uninstall-component
}

run_test()
{
$JBI_ANT -Djbi.install.file=${JV_SVC_BLD}/regress/dist/test-component1.jar -Djbi.target=$TARGET install-component

$JBI_ANT -Djbi.target=$TARGET -Djbi.include.deploy.command=true -Djbi.deploy.file="${JV_SVC_BLD}/regress/testdata/sa1/sa1.jar" -Djbi.template.dir="${JV_SVC_BLD}/regress/testdata/tmp" verify-application-environment

$JBI_ANT -Djbi.component.name=test-component1 -Djbi.target=$TARGET start-component

$JBI_ANT -Djbi.component.name=test-component1 -Djbi.app.variables.file=$JV_SVC_BLD/regress/testcomponent1/test-component1-app-var.properties -Djbi.target=$TARGET create-application-variable

$JBI_ANT -Djbi.component.name=test-component1 -Djbi.app.config.params.file=$JV_SVC_BLD/regress/testcomponent1/test-component1-app-config2.properties -Djbi.app.config.name=MyAppConfig3 -Djbi.target=$TARGET create-application-configuration

$JBI_ANT -Djbi.include.deploy.command=true -Djbi.deploy.file="${JV_SVC_BLD}/regress/testdata/sa2/sa2.jar" -Djbi.template.dir="${JV_SVC_BLD}/regress/testdata/tmp" -Djbi.target=$TARGET verify-application-environment

$JBI_ANT -Djbi.deploy.file="${JV_SVC_BLD}/regress/testdata/sa2/sa2.jar" -Djbi.target=$TARGET deploy-service-assembly

$JBI_ANT -Djbi.target=$TARGET -Djbi.service.assembly.name=SA2 -Djbi.config.dir="${JV_SVC_BLD}/regress/testdata/tmp" export-application-environment

$JBI_ANT -Djbi.service.assembly.name=SA2 -Djbi.target=$TARGET undeploy-service-assembly
$JBI_ANT -Djbi.component.name=test-component1 -Djbi.target=$TARGET stop-component
$JBI_ANT -Djbi.component.name=test-component1 -Djbi.target=$TARGET shut-down-component
$JBI_ANT -Djbi.component.name=test-component1 -Djbi.target=$TARGET uninstall-component
}

run_test_against_server

# Run against cluster
TARGET=ant_cluster
run_test

# Run against standalone instance
TARGET=instance13
run_test

# Run against clustered instance
# TARGET=instance11
# run_test
