/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiQueryTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;
import org.apache.tools.ant.BuildException;


/** This class is a base class that has the helper methods the extended classes of the
 * query interface need to implement. All concreate query tasks extend directly from this
 * or from a derived class of this.
 *
 * @author Sun Microsystems, Inc.
 */
public abstract class JbiQueryTask extends JbiTargetTask
{
    /** query information for one or more objects **/
    protected static final int LIST_QUERY_RESULT_TYPE = 1;
    /** query information for a single object **/
    protected static final int SINGLE_QUERY_RESULT_TYPE = 2;
    
    /** started state */
    protected static final String STARTED_STATE = "started";
    /** stopped state */
    protected static final String STOPPED_STATE = "stopped";
    /** shutdown state */
    protected static final String SHUTDOWN_STATE = "shutdown";
    /** unknown state */
    protected static final String UNKNOWN_STATE = "unknown";
    /** any state */
    protected static final String ANY_STATE = "any";
    
    /** i18n text for installed state for shared libs */
    private static String sDisplayInstalledState = null;
    /** i18n text for started state */
    private static String sDisplayStartedState = null;
    /** i18n text for stopped state */
    private static String sDisplayStoppedState = null;
    /** i18n text for shutdown state */
    private static String sDisplayShutdownState = null;
    /** i18n text for unknown state */
    private static String sDisplayUnknownState = null;
    
    /**
     * query result type. single or list.
     */
    private int mQueryResultType = LIST_QUERY_RESULT_TYPE;
    
    
    /** Holds value of property xmlOutput. */
    private String mXmlOutput = null;
    
    /**
     * base constructor.
     */
    public JbiQueryTask()
    {
        super();
        initStateDisplayValues();
    }
    /**
     * loads the i18n text for the states
     */
    private void initStateDisplayValues()
    {
        
        if ( sDisplayInstalledState == null )
        {
            sDisplayInstalledState = getI18NBundle().getMessage(
                "jbi.ui.ant.print.state.installed");
        }
        
        if ( sDisplayStartedState == null )
        {
            sDisplayStartedState = getI18NBundle().getMessage(
                "jbi.ui.ant.print.state.started");
        }
        
        if ( sDisplayStoppedState == null )
        {
            sDisplayStoppedState = getI18NBundle().getMessage(
                "jbi.ui.ant.print.state.stopped");
        }
        
        if ( sDisplayShutdownState == null )
        {
            sDisplayShutdownState = getI18NBundle().getMessage(
                "jbi.ui.ant.print.state.shutdown");
        }
        
        if ( sDisplayUnknownState == null )
        {
            sDisplayUnknownState = getI18NBundle().getMessage(
                "jbi.ui.ant.print.state.unknown");
        }
        
    }
    
    /** Getter for property XmlOutput.
     * @return Value of XmlOutput.
     */
    public String getXmlOutput()
    {
        return this.mXmlOutput;
    }
    
    /**
     * Setter for property XmlOutput.
     * @param xmlOutput xml text.
     */
    public void setXmlOutput(String xmlOutput)
    {
        this.mXmlOutput = xmlOutput;
    }
    
    /** Getter for property QueryType.
     * @return Value of QueryType.
     */
    protected int getQueryResultType()
    {
        return this.mQueryResultType;
    }
    
    /** Setter for property QueryType to LIST_QUERY_RESULT_TYPE.
     *
     */
    protected void setListQueryResultType()
    {
        this.mQueryResultType = LIST_QUERY_RESULT_TYPE;
    }
    
    /** Setter for property QueryType to SINGLE_QUERY_RESULT_TYPE.
     *
     */
    protected void setSingleQueryResultType()
    {
        this.mQueryResultType = SINGLE_QUERY_RESULT_TYPE;
    }
    
    /**
     * returns i18n key
     * the i18n key
     * @return i18n key
     */
    protected abstract String getQueryResultHeaderI18NKey();
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected abstract String getQueryResultHeaderSeparatorI18NKey();
    /**
     * returns i18n key
     * the i18n key
     * @return i18n key
     */
    protected abstract String getQueryResultPageSeparatorI18NKey();
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected abstract String getEmptyQueryResultI18NKey();
    /**
     * returns i18n key
     * @return the i18n key
     */
    protected abstract String getEmptyQueryResultI18NMessage();
    
    /**
     * converts the state to the display value
     * @return text for displaying the state
     * @param stateValue text representing the state .
     * @param compType component type in the JBIComponentInfo
     */
    private String toComponentInfoStateDisplayValue(String stateValue, String compType)
    {
        String stateDisplay = this.sDisplayUnknownState;
        
        if ( STARTED_STATE.equalsIgnoreCase(stateValue) )
        {
            stateDisplay = sDisplayStartedState;
        }
        else if ( STOPPED_STATE.equalsIgnoreCase(stateValue) )
        {
            stateDisplay = sDisplayStoppedState;
        }
        else if ( SHUTDOWN_STATE.equalsIgnoreCase(stateValue) )
        {
            if ( JBIComponentInfo.SHARED_LIBRARY_TYPE.equalsIgnoreCase(compType))
            {
                stateDisplay = sDisplayInstalledState;
            }
            else
            {
                stateDisplay = sDisplayShutdownState;
            }
        }
        else
        {
            stateDisplay = sDisplayUnknownState;
        }
        
        return stateDisplay;
        
    }
    
    /**
     * converts to state
     * @param taskPropState value passed from task attribute state
     * @return state value
     */
    protected String toJbiComponentInfoState(String taskPropState)
    {
        String compInfoState = JBIComponentInfo.UNKNOWN_STATE;
        
        if ( taskPropState == null || taskPropState.length() == 0)
        {
            return compInfoState;
        }
        
        if ( STARTED_STATE.equalsIgnoreCase(taskPropState) )
        {
            compInfoState = JBIComponentInfo.STARTED_STATE;
        }
        else if ( STOPPED_STATE.equalsIgnoreCase(taskPropState) )
        {
            compInfoState = JBIComponentInfo.STOPPED_STATE;
        }
        else if ( SHUTDOWN_STATE.equalsIgnoreCase(taskPropState) )
        {
            compInfoState = JBIComponentInfo.SHUTDOWN_STATE;
        }
        
        return compInfoState;
    }

    /**
     * converts the state to the display value
     * @return text for displaying the state
     * @param stateValue text representing the state .
     */
    private String toServiceAssemblyInfoStateDisplayValue(String stateValue)
    {
        String stateDisplay = this.sDisplayUnknownState;
        
        if ( STARTED_STATE.equalsIgnoreCase(stateValue) )
        {
            stateDisplay = sDisplayStartedState;
        }
        else if ( STOPPED_STATE.equalsIgnoreCase(stateValue) )
        {
            stateDisplay = sDisplayStoppedState;
        }
        else if ( SHUTDOWN_STATE.equalsIgnoreCase(stateValue) )
        {
                stateDisplay = sDisplayShutdownState;
        }
        else
        {
            stateDisplay = sDisplayUnknownState;
        }
        
        return stateDisplay;
        
    }
    
    /**
     * converts the state to the display value
     * @return text for displaying the state
     * @param stateValue text representing the state .
     */
    private String toServiceUnitInfoStateDisplayValue(String stateValue)
    {
        String stateDisplay = this.sDisplayUnknownState;
        
        if ( STARTED_STATE.equalsIgnoreCase(stateValue) )
        {
            stateDisplay = sDisplayStartedState;
        }
        else if ( STOPPED_STATE.equalsIgnoreCase(stateValue) )
        {
            stateDisplay = sDisplayStoppedState;
        }
        else if ( SHUTDOWN_STATE.equalsIgnoreCase(stateValue) )
        {
                stateDisplay = sDisplayShutdownState;
        }
        else
        {
            stateDisplay = sDisplayUnknownState;
        }
        
        return stateDisplay;
        
    }
    
    
    /**
     * converts to state
     * @param taskPropState value passed from task attribute state
     * @return state value
     */
    protected String toServiceAssemblyInfoState(String taskPropState)
    {
        String compInfoState = ServiceAssemblyInfo.UNKNOWN_STATE;
        
        if ( taskPropState == null || taskPropState.length() == 0)
        {
            return compInfoState;
        }
        
        if ( STARTED_STATE.equalsIgnoreCase(taskPropState) )
        {
            compInfoState = ServiceAssemblyInfo.STARTED_STATE;
        }
        else if ( STOPPED_STATE.equalsIgnoreCase(taskPropState) )
        {
            compInfoState = ServiceAssemblyInfo.STOPPED_STATE;
        }
        else if ( SHUTDOWN_STATE.equalsIgnoreCase(taskPropState) )
        {
            compInfoState = ServiceAssemblyInfo.SHUTDOWN_STATE;
        }
        
        return compInfoState;
    }
    
    
    /**
     * validates the state value
     * @param state value
     * @throws BuildException on invalid state value
     */
    protected void validateTaskPropertyForStateValue(String state) throws BuildException
    {
        
        if ( state == null || state.length() == 0 )
        {
            // all sates
            return;
        }
        String stateValue = state;
        
        if ( !( STARTED_STATE.equalsIgnoreCase(stateValue) ||
            STOPPED_STATE.equalsIgnoreCase(stateValue) ||
            SHUTDOWN_STATE.equalsIgnoreCase(stateValue) ) )
        {
            throwTaskBuildException(
                "jbi.ui.ant.task.error.invalid.state", state);
        }
        
    }
    /**
     * use to clear the xml output if set, to empty list before executing the
     * query. This way, failred query will have the xml output set to empty list.
     * @param xmlText xml text to set to the output xml property value
     */
    protected void initXmlOutput(String xmlText) 
    {
        printResultsToXmlOutput(xmlText);
    }
    
    /**
     * set the xmlResult as a value of the property from getXmlOutput
     * @param xmlResult results in xml text
     * @return true if the result is saved in the property else false.
     */
    protected boolean printResultsToXmlOutput(String xmlResult )
    {
        String xmlOutputProp = this.getXmlOutput();
        if ( xmlOutputProp != null )
        {
            this.getProject().setProperty(xmlOutputProp, xmlResult);
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /** prints result
     * @param aResult result to print
     */
    protected void printComponentQueryResults(String aResult)
    {
        if ( printResultsToXmlOutput(aResult) )
        {
            return;
        }
        
        String header =
            getI18NBundle().getMessage( getQueryResultHeaderI18NKey(), getValidTarget() );
        String headerSeparator =
            getI18NBundle().getMessage( getQueryResultHeaderSeparatorI18NKey() );
        String pageSeparator =
            getI18NBundle().getMessage( getQueryResultPageSeparatorI18NKey() );
        String emptyResult = getEmptyQueryResultI18NMessage();
        
        List list = JBIComponentInfo.readFromXmlText(aResult);
        
        StringWriter stringWriter = new StringWriter();
        PrintWriter msgWriter = new PrintWriter(stringWriter);
        
        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);
        
        if ( list.size() <= 0 )
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
        }
        else
        {
            for ( Iterator itr = list.iterator(); itr.hasNext();)
            {
                JBIComponentInfo info = (JBIComponentInfo) itr.next();
                
                String status = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.comp.info.state",
                    toComponentInfoStateDisplayValue(info.getState(), info.getType()));
                
                String name = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.comp.info.name",
                    info.getName());
                
                String desc = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.comp.info.desc",
                    info.getDescription());
               
                String compVersion = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.comp.info.version",
                    info.getComponentVersion());
               
                String buildNumber = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.comp.info.build.number",
                    info.getBuildNumber());
               
                msgWriter.println(name);
                if ((info.getComponentVersion() != null) &&
                    (info.getComponentVersion().compareTo("") != 0))
                {
                    msgWriter.println(compVersion);
                }
                if ((info.getBuildNumber() != null) &&
                    (info.getBuildNumber().compareTo("") != 0))
                {
                    msgWriter.println(buildNumber);
                }
                msgWriter.println(status);
                msgWriter.println(desc);
                msgWriter.println(pageSeparator);
            }
        }
        msgWriter.close();
        printMessage(stringWriter.toString());
        
    }
    
    /** prints result
     * @param aResult result to print
     */
    protected void printDeploymentQueryResults(String aResult)
    {
        
        if ( printResultsToXmlOutput(aResult) )
        {
            return;
        }
        
        String header =
            getI18NBundle().getMessage( getQueryResultHeaderI18NKey(), getValidTarget() );
        String headerSeparator =
            getI18NBundle().getMessage( getQueryResultHeaderSeparatorI18NKey() );
        String pageSeparator =
            getI18NBundle().getMessage( getQueryResultPageSeparatorI18NKey() );
        String emptyResult = getEmptyQueryResultI18NMessage();
        
        List list = ServiceAssemblyInfo.readFromXmlTextWithProlog(aResult);
        
        StringWriter stringWriter = new StringWriter();
        PrintWriter msgWriter = new PrintWriter(stringWriter);
        
        msgWriter.println(headerSeparator);
        msgWriter.println(header);
        msgWriter.println(headerSeparator);
        
        if ( list.size() <= 0 )
        {
            msgWriter.println(emptyResult);
            msgWriter.println(pageSeparator);
        }
        else
        {
            for ( Iterator itr = list.iterator(); itr.hasNext();)
            {
                ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
                List suInfoList = saInfo.getServiceUnitInfoList();
                
                String saName = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.sasm.info.name",
                    saInfo.getName());
                
                String saState = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.sasm.info.state",
                    toServiceAssemblyInfoStateDisplayValue(saInfo.getState()));
                
                String saDesc = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.sasm.info.desc",
                    saInfo.getDescription());
                
                String suInfoListDesc = getI18NBundle().getMessage(
                    "jbi.ui.ant.print.sasm.info.sunit.list.size",
                    Integer.toString(suInfoList.size()));
                
                msgWriter.println(saName);
                msgWriter.println(saState);
                msgWriter.println(saDesc);
                msgWriter.println(suInfoListDesc);
                
                for ( Iterator suItr = suInfoList.iterator(); suItr.hasNext(); )
                {
                    ServiceUnitInfo suInfo = (ServiceUnitInfo ) suItr.next();
                    
                    String suState = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.sunit.info.state",
                        toServiceUnitInfoStateDisplayValue(suInfo.getState()));
                    
                    String deployedOn = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.sunit.info.deployed.on",
                        suInfo.getDeployedOn());
                    
                    String suName = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.sunit.info.name",
                        suInfo.getName());
                    
                    String suDesc = getI18NBundle().getMessage(
                        "jbi.ui.ant.print.sunit.info.desc",
                        suInfo.getDescription());
                    
                    msgWriter.println(suName);
                    msgWriter.println(suState);
                    msgWriter.println(deployedOn);
                    msgWriter.println(suDesc);
                }
                
                msgWriter.println(pageSeparator);
            }
        }
        
        msgWriter.close();
        printMessage(stringWriter.toString());
        
    }
    
    /**
     * This class is a base class for the component query tasks ( list-bindings and
     * list-engines) that has the common attributes
     */
    public static abstract class JbiComponentQueryTask extends JbiQueryTask
    {
        /**
         * Holds value of property component state.
         */
        private String mState;
        
        /**
         * Holds value of property shared library name.
         */
        private String mSharedLibraryName;
        
        /**
         * Holds value of property service assembly name.
         */
        private String mServiceAssemblyName;
        
        /**
         * constructor
         */
        public JbiComponentQueryTask()
        {
            super();
        }
        /**
         * Getter for property state.
         * @return Value of property state.
         */
        public String getState()
        {
            return this.mState;
        }
        
        /**
         * Setter for property state.
         * @param state New value of property state.
         */
        public void setState(String state)
        {
            this.mState = state;
        }
        /**
         * Getter for property sharedLibraryName.
         * @return Value of property sharedLibraryName.
         */
        public String getSharedLibraryName()
        {
            return this.mSharedLibraryName;
        }
        
        /**
         * Setter for property sharedLibraryName.
         * @param sharedLibraryName shared Library Name
         */
        public void setSharedLibraryName(String sharedLibraryName)
        {
            this.mSharedLibraryName = sharedLibraryName;
        }
        
        /**
         * Getter for property serviceAssemblyName.
         * @return Value of property serviceAssemblyName.
         */
        public String getServiceAssemblyName()
        {
            return this.mServiceAssemblyName;
        }
        
        /**
         * Setter for property serviceAssemblyName.
         * @param serviceAssemblyName New value of property serviceAssemblyName.
         */
        public void setServiceAssemblyName(String serviceAssemblyName)
        {
            this.mServiceAssemblyName = serviceAssemblyName;
        }
    }
    
}
