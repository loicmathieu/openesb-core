/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiUpdateApplicationConfigurationTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Properties;
import org.apache.tools.ant.BuildException;

/** This class is an ant task for updating service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiUpdateApplicationConfigurationTask extends JbiTargetTask
{
    /**
     * appconfig success msg key
     */
    private static final String APPCONFIG_SUCCESS_STATUS_KEY = "jbi.ui.ant.set.appconfig.successful";

    /**
     * appconfig failure msg key
     */
    private static final String APPCONFIG_FAILED_STATUS_KEY = "jbi.ui.ant.set.appconfig.failed";
   
    /**
     * appconfig success msg key
     */
    private static final String APPCONFIG_PARTIAL_SUCCESS_STATUS_KEY = "jbi.ui.ant.set.appconfig.partial.success";
    
    /** Holds Param Nested elements */
    private List mParamList = null;
    
    /** Holds Params File **/
    private String mParamsFile = null;

    /** Holds value of property componentName. */
    private String mComponentName = null;
    
    /** Holds value of property name. */
    private String mName = null;
    
    /**
     * Getter for property componentName.
     * @return Value of property componentName.
     */
    public String getComponentName()
    {
        return this.mComponentName;
    }
    
    /**
     * Setter for property componentName.
     * @param componentName name of the component.
     */
    public void setComponentName(String componentName)
    {
        this.mComponentName = componentName;
    }
    
    /**
     * Getter for property name (i.e, the name of application configuration).
     * @return Value of property name.
     */
    public String getName()
    {
        return this.mName;
    }
   
    /**
     * Setter for property name (i.e, the name of application configuration).
     * @param name of the application configuration.
     */
    public void setName(String name)
    {
        this.mName = name;
    }

    /** Getter for property Params.
     * @return Value of property Params.
     *
     */
    public String getParams()
    {
        return this.mParamsFile;
    }
    
    /**
     * Sets the params file location to the absolute filename of the
     * given file. If the value of this attribute is an absolute path, it
     * is left unchanged (with / and \ characters converted to the
     * current platforms conventions). Otherwise it is taken as a path
     * relative to the project's basedir and expanded.
     * @param paramsFile path to set
     */
    
    public void setParams(String paramsFile)
    {
        this.mParamsFile = paramsFile;
    }
    
    private void debugPrintParams(Properties params) {
        if( params == null ) {
            this.logDebug("Set Configuration params are NULL");
            return;
        }
        StringWriter stringWriter = new StringWriter();
        PrintWriter out = new PrintWriter(stringWriter);
        params.list(out);
        out.close();
        this.logDebug(stringWriter.getBuffer().toString());
    }
    
    private String createFormatedSuccessJbiResultMessage(String i18nKey, Object[] args) {
        
        String msgCode = getI18NBundle().getMessage(i18nKey + ".ID");
        String msg = getI18NBundle().getMessage(i18nKey, args);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("JBI_ANT_TASK_SET_CONFIG",
            JBIResultXmlBuilder.SUCCESS_RESULT,
            JBIResultXmlBuilder.INFO_MSG_TYPE,
            msgCode, msg, args, null);
        
        JBIManagementMessage mgmtMsg = null;
        mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        return (mgmtMsg != null) ? mgmtMsg.getMessage() : msg ;
    }
    
    private void executeUpdateApplicationConfiguration(String componentName)
	throws BuildException
    {

        try
        {
	    // Go throught the static/constant configuration paramter elements
            String appConfigName = this.getName();
            
	    String	target		= getValidTarget();

            if ((componentName == null) || (componentName.compareTo("") == 0))
            {
                String errMsg = createFailedFormattedJbiAdminResult(
                                                "jbi.ui.ant.task.error.nullCompName",
                                                null);
                throw new BuildException(errMsg);
            }

            if ((appConfigName == null) || (appConfigName.compareTo("") == 0))
            {
                String errMsg = createFailedFormattedJbiAdminResult(
                                                "jbi.ui.ant.set.appconfig.no.name.error",
                                                null);
                throw new BuildException(errMsg);
            }

            this.logDebug("Executing Set Application Configuration ....");

	    List	paramList = this.getParamList();
            Properties appConfigParamProps = new Properties();

	    Iterator itr = paramList.iterator();
	    while (itr.hasNext())
	    {
		Param param = (Param) itr.next();
                if ((""+param.getName()).compareTo("") != 0)
                {
		    if ((param.getValue() != null) &&
			(param.getValue().compareTo("") != 0))
		    {
		        appConfigParamProps.setProperty(param.getName(),
						param.getValue());
		    }
		    else if ((param.getAppVariable() != null) &&
                        (param.getAppVariable().compareTo("") != 0))
		    {
		        appConfigParamProps.setProperty(param.getName(),
						"$" + param.getValue());
		    }
		    else
		    {
		        // Default one
		        appConfigParamProps.setProperty(param.getName(),
						param.getValue());
		    }
                }
                else
                {
                    this.logDebug("the input param name is empty.");
                }
	    }

            if (appConfigParamProps.size() == 0)
            {
                this.logDebug("No nested param element is found in input.");
            }

            String paramsFile = this.getParams();
	    if ((paramsFile != null ) && (paramsFile.compareTo("") != 0))
            {
                appConfigParamProps.putAll(loadParamsFromFile(new File(paramsFile)));
            }
            else
            {
                this.logDebug("No File based Parameters passed to set application configuration Task via nested params element");
            }

            if (appConfigParamProps.size() == 0)
            {
                String msg =
                    createFailedFormattedJbiAdminResult(
                        "jbi.ui.ant.list.no.input.appconfig.param.data.found",
                        null);
                throw new BuildException(msg,getLocation());
            }

            this.logDebug("component name: " + componentName +
			" Application Configuration name: " + appConfigName +
                        " target: " + target);
	    debugPrintParams(appConfigParamProps);
            String rtnXml =
		this.getJBIAdminCommands().setApplicationConfiguration(componentName,
							target,
							appConfigName,
							appConfigParamProps);

	    JBIManagementMessage mgmtMsg =
			JBIManagementMessage.createJBIManagementMessage(rtnXml);
            if ( mgmtMsg.isFailedMsg() )
            {
                 throwBuildException(mgmtMsg);
            }
            else
            {
                // print success message
                printTaskSuccess(mgmtMsg);
            }
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }                
    }


    /** executes the install task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {        
        this.logDebug("Executing Set Configuration Task....");
        String compName = getComponentName();
        
        executeUpdateApplicationConfiguration(compName);
    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
	return APPCONFIG_FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return APPCONFIG_SUCCESS_STATUS_KEY;
    }
    /**
     * return i18n key for the partial success
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return APPCONFIG_PARTIAL_SUCCESS_STATUS_KEY;
    }    
    /**
     * returns appconfig element list
     * @return Paramter List
     */
    protected List getParamList()
    {
        if ( this.mParamList == null )
        {
            this.mParamList = new ArrayList();
        }
        return this.mParamList;
    }

    /**
     * load properties from a file
     * @return the Loaded properties
     * @param file file to load
     * @throws BuildException on error
     */
    protected Properties loadParamsFromFile(File file) throws BuildException
    {
        String absFilePath = null;
        String fileName = null;
        if ( file != null )
        {
            absFilePath = file.getAbsolutePath();
            fileName = file.getName();
        }
        if ( file == null || !file.exists() )
        {
            String msg =
                createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.config.params.file.not.exist",
                new Object[] {fileName});
            throw new BuildException(msg,getLocation());
        }
        
        if ( file.isDirectory() )
        {
            String msg =
                createFailedFormattedJbiAdminResult("jbi.ui.ant.task.error.config.params.file.is.directory", null);
            throw new BuildException(msg,getLocation());
        }
        
        Properties props = new Properties();
        this.logDebug("Loading " + file.getAbsolutePath());
        try
        {
            FileInputStream fis = new FileInputStream(file);
            try
            {
                props.load(fis);
            }
            finally
            {
                if (fis != null)
                {
                    fis.close();
                }
            }
            return props;
        }
        catch (IOException ex)
        {
            throw new BuildException(ex, getLocation());
        }
    }

    /**
     * factory method for creating the nested element &lt;param>
     * @return Param Object
     */
    public Param createParam()
    {
        Param param = new Param();
        this.getParamList().add(param);
        return param;
    }
}
