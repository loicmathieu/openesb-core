/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiVerifyApplicationEnv.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import org.apache.tools.ant.BuildException;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Set;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.TabularData;
import com.sun.jbi.ui.common.JBIVerifierReportItemNames;

/** This class is an ant task for stopping the service engine or binding component.
 *
 * @author Sun Microsystems, Inc.
 */
public class JbiVerifyApplicationEnvTask extends JbiTargetTask
{
    /**
     * success msg key
     */
    private static final String SUCCESS_STATUS_KEY = "jbi.ui.ant.verify.app.env.successful";
    /**
     * failure msg key
     */
    private static final String FAILED_STATUS_KEY = "jbi.ui.ant.verify.app.env.failed";
    
    /** Holds value of property application zip file. */
    private File mAppFile;

    /** Holds value of property generating deploy command flag. */
    private boolean mIncludeDeployFlag;

    /** Holds value of property templates dir. */
    private String  mTemplateDir;

    /** Getter for property application zip file.
     * @return Path of property application zip file.
     *
     */

    public File getFile()
    {
        return this.mAppFile;
    }

    /**
     * Setter for property application zip file.
     * @param file the file path to the appliction zip file.
     */
    public void setFile(File file)
    {
        this.mAppFile = file;
    }

    /** Getter for property deploy.
     * @return Value of property deploy flag.
     *
     */
    public boolean getIncludeDeploy()
    {
        return this.mIncludeDeployFlag;
    }
   
    /**
     * Setter for property deploy flag.
     * @param deployFlag the deploy flag.
     */
    public void setIncludeDeploy(boolean includeDeploy)
    {
        this.mIncludeDeployFlag = includeDeploy;
    }

    /** Getter for property templatesdir.
     * @return Value of property templatedir.
     *
     */
    public String getTemplateDir()
    {
        return this.mTemplateDir;
    }
   
    /**
     * Setter for property templatedir.
     * @param templateDir the dir indicating where the templates will be genearted.
     */
    public void setTemplateDir(String templateDir)
    {
        this.mTemplateDir = templateDir;
    }

    /** executes the stop componentnt task. Ant Task framework calls this method to
     * excute the task.
     * @throws BuildException if error or exception occurs.
     */
    public void executeTask() throws BuildException
    {

        String target = getValidTarget();
                
        try
        {
	    String templateFlagStr =
                          !((mTemplateDir == null) || (mTemplateDir.compareTo("") == 0)) ?
                          "True" : "False";
	    String deployFlagStr = mIncludeDeployFlag==true ? "True" : "False";
	    this.logDebug("app path: " + mAppFile.getPath() + 
			" The target: " + target +
			" The gen templates flag: " + templateFlagStr +
			" The templates dir: " + mTemplateDir +
			" The deploy flag: " + deployFlagStr);
            
            CompositeData result =
		this.getJBIAdminCommands().verifyApplication(
                    mAppFile.getPath(),
                    target,
                    !((mTemplateDir == null) || (mTemplateDir.compareTo("") == 0)),
                    mTemplateDir,
                    mIncludeDeployFlag);

	    printResultCompositeData(result);
        }
        catch (Exception ex )
        {
            processTaskException(ex);
        }
        
    }

    protected void printResultCompositeData(CompositeData compData)
        throws IOException
    {
        String[] attributeNames = {
            "ServiceAssemblyName",
            "ServiceAssemblyDescription",
            "NumServiceUnits", 
            "AllComponentsInstalled",
            "MissingComponentsList",
            "EndpointInfo",
            "TemplateZIPID",
            "JavaEEVerificationReport"
        };   

        String[] endpointNames = {
            "EndpointName",
            "ServiceUnitName",
            "ComponentName",
            "Status",
            "MissingApplicationVariables",
            "MissingApplicationConfigurations"
        };

        String saName = (String) compData.get(attributeNames[0]);
        String saDesc = (String) compData.get(attributeNames[1]);
        int numSUs    = ((Integer) compData.get(attributeNames[2])).intValue();
        boolean allComponentsInstalled = ((Boolean) compData.get(attributeNames[3])).booleanValue();
        String[] missingComponents = (String[]) compData.get(attributeNames[4]);
        CompositeData[] endpointInfo = (CompositeData[]) compData.get(attributeNames[5]);
        String templateZipID = (String) compData.get(attributeNames[6]);

        logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.info.header.separator"));
        logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.info.header", getValidTarget()));
        logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.info.separator"));
        logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.sa.name") + ": " + saName);
        logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.service.units") + ": " + numSUs);
        logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.sa.description") + ": " + (saDesc+"").trim());


        if (!allComponentsInstalled)
        {
            StringBuffer missingCompString = new StringBuffer();
            for (int i=0 ; i< missingComponents.length; i++)
            {
                missingCompString.append(" " + missingComponents[i]);
            }
            logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.commponents.not.installed") +
			": " + (missingCompString.toString()+"").trim());
        }

        for (int i=0; i<endpointInfo.length; i++)
        {
            logWarning("\n" + getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.endpoint.configuration"));
            logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.info.separator"));

            Object[] endpointValues = endpointInfo[i].getAll(endpointNames);
            logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.endpoint.name") +
			": " + (String)endpointValues[0]);
            logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.service.unit") +
			": " + (String)endpointValues[1]);
            logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.endpoint.component") +
			": " + (String)endpointValues[2]);

            if ("UNKNOWN".compareTo((""+endpointValues[3]).trim()) == 0)
            {
                logWarning(
                    getI18NBundle().getMessage(
                       "jbi.ui.ant.print.jbi.verify.app.env.endpoint.status") + ": " +
                    getI18NBundle().getMessage(
                       "jbi.ui.ant.print.jbi.verify.app.env.endpoint.status.unknown"));
            }
            else if ("RESOLVED".compareTo((""+endpointValues[3]).trim()) == 0)
            {
                logWarning(
                    getI18NBundle().getMessage(
                       "jbi.ui.ant.print.jbi.verify.app.env.endpoint.status") + ": " +
                    getI18NBundle().getMessage(
                       "jbi.ui.ant.print.jbi.verify.app.env.endpoint.status.resolved"));
            }
            else if ("UNRESOLVED".compareTo((""+endpointValues[3]).trim()) == 0)
            {
                logWarning(
                    getI18NBundle().getMessage(
                       "jbi.ui.ant.print.jbi.verify.app.env.endpoint.status") + ": " +
                    getI18NBundle().getMessage(
                       "jbi.ui.ant.print.jbi.verify.app.env.endpoint.status.unresolved"));
            }

            String [] appVars =(String [])endpointValues[4];
            if (appVars != null && appVars.length > 0)
            {
                logWarning(
                    getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.missing.app.vars"));
                for (int j=0; j<appVars.length; j++)
                {
                    logWarning("\t" + appVars[j]);
                }
                logWarning("");
            }

            String[] appConfigs = (String[]) endpointValues[5];
            if (appConfigs != null && appConfigs.length > 0)
            {
                logWarning(
                    getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.missing.app.configs"));
                for (int j=0; j<appConfigs.length; j++)
                {
                    logWarning("\t" + appConfigs[j]);
                }
                logWarning("");
            }
        }

        if (compData.containsKey(attributeNames[7]))
        {
            String [] eeReportFieldNames = { "ServiceUnitName", "JavaEEVerifierReport" };

            CompositeData[] javaEEVerifierReports = (CompositeData[]) compData.get(attributeNames[7]);
            if (javaEEVerifierReports !=null && javaEEVerifierReports.length >0)
            {
                logWarning("");
                logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.ee.verify.report"));
                logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.info.separator"));
            }

            StringWriter stringWriter = new StringWriter();
            PrintWriter msgWriter = new PrintWriter(stringWriter);

            for (int i=0; i<javaEEVerifierReports.length; i++)
            {
                String serviceUnitName = (String) javaEEVerifierReports[i].get(eeReportFieldNames[0]);
                logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.service.unit") +
                        ": " + serviceUnitName);
                TabularData reportTable = (TabularData) javaEEVerifierReports[i].get(eeReportFieldNames[1]);
                Set rows = reportTable.keySet();
                for ( Object row : rows )
                {
                    
                    Object[] key = ( (java.util.List) row).toArray();
                    CompositeData rptCompData = reportTable.get(key);
                    msgWriter.println();                    

                    //the javaeeverifier report will not contain complex types
                    //parse the report and do not display items starting with "_" 
                    //which are machine readable non-i18ned items
                    if (rptCompData !=null)
                    {
                        CompositeType suCompType = rptCompData.getCompositeType();
                        Set suCompItemSet = suCompType.keySet();
                        Iterator itr = suCompItemSet.iterator();

                        while (itr.hasNext())
                        {          
                            String keyName = (String) itr.next();
                            if (!keyName.startsWith(JBIVerifierReportItemNames.NON_I18N_KEY_PREFIX))
                            {
                                String tabStr = "";
                                msgWriter.println(tabStr + keyName + ": " + rptCompData.get(keyName));
                            }
                        }
                    }
                }
                printMessage(stringWriter.getBuffer().toString());
                stringWriter.flush();
            }

            msgWriter.close();
            stringWriter.close();
        }

        if (!((mTemplateDir == null) || (mTemplateDir.compareTo("") == 0)))
        {
            logWarning("");
            logWarning(getI18NBundle().getMessage("jbi.ui.ant.print.jbi.verify.app.env.info.separator"));
            logWarning( getI18NBundle().getMessage(
                                "jbi.ui.ant.print.jbi.verify.app.env.templates.create.msg",
                                new String [] { mTemplateDir }) );
        }

    }
    
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected String getTaskFailedStatusI18NKey()
    {
        return FAILED_STATUS_KEY;
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    protected String getTaskSuccessStatusI18NKey()
    {
        return SUCCESS_STATUS_KEY;
    }
}
