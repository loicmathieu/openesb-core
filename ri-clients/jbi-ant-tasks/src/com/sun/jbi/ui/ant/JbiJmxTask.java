/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JbiJmxTask.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.ui.ant;

import com.sun.jbi.ui.client.ConnectionType;
import com.sun.jbi.ui.client.JBIAdminCommandsClientFactory;
import com.sun.jbi.ui.client.JMXConnectionProperties;
import com.sun.jbi.ui.common.I18NBundle;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIManagementMessage;
import com.sun.jbi.ui.common.JBIRemoteException;
import com.sun.jbi.ui.common.JBIResultXmlBuilder;
import com.sun.jbi.ui.common.JMXConnectionException;
import com.sun.jbi.ui.common.JBIManagementMessage.JBIManagementMessageException;
import java.util.Properties;
import java.util.Set;
import java.util.Iterator;
import java.io.IOException;
import java.io.PrintWriter;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.OpenType;
import javax.management.openmbean.ArrayType;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.SimpleType;
import javax.management.openmbean.TabularType;
import javax.management.openmbean.TabularData;

import java.io.FileInputStream;
import java.io.File;

/** This class is the base class for all the jbi admin tasks that uses JBIAdminCommands
 * interface implementation to execute the jbi admin tasks.
 * This class has a set of common properties for all the jbi admin tasks. These common properties
 * are related to jmx connection informaton ( host, port, username, passwrod )
 * This class also has helper methods to format the display message and handling the errors
 * and the management status message.
 *
 * @author Sun Microsystems, Inc.
 */

public abstract class JbiJmxTask extends Task
{
    /** default ant prop */
    public static final String DEF_USERNAME_PROP = "jbi.default.username";
    /** default ant prop */
    public static final String DEF_PASSWORD_PROP = "jbi.default.password";
    /** default ant prop */
    public static final String DEF_HOST_PROP = "jbi.default.host";
    /** default ant prop */
    public static final String DEF_PORT_PROP = "jbi.default.port";
    /** default ant prop */
    public static final String DEF_URL_PROP = "jbi.default.url";
    /**default ant prop**/
    public static final String DEF_JMX_PROPERTIES_FILE = "jmx.default.properties.file";
    
    /**
     * i18n bundle
     */
    private static I18NBundle sI18NBundle = null;
    
    /** Holds value of property Secure. */
    private Boolean mSecure = null;
        
    /** Holds value of property host. */
    private String mHost = null;
    
    /** Holds value of property port. */
    private String mPort = null;
    
    /** Holds value of property username. */
    private String mUsername = null;
    
    /** Holds value of property password. */
    private String mPassword = null;
    
    /** Holds value of property passwordFile. */
    private File mPasswordFile = null;
    
    /** Holds value of property failOnError. */
    private boolean mFailOnError = true;
    
    /** Jboss strings **/
    private String mPropertiesFile = null;
    
    /** jbi commands client interface */
    private JBIAdminCommands mJbiAdminCommands = null;
    
    /** Getter for property failOnError.
     * @return Value true if failOnError else false.
     */
    public boolean isFailOnError()
    {
        return this.mFailOnError;
    }
    
    /** Setter for property failOnError.
     * @param failOnError New value of property failOnError.
     */
    public void setFailOnError(boolean failOnError)
    {
        this.mFailOnError = failOnError;
    }
    
    /** Getter for property Secure.
     * @return Value true if Secure else false.
     */
    public Boolean getSecure()
    {
        return this.mSecure;
    }
    
    /** Setter for property Secure.
     * @param secure New value of property Secure.
     */
    public void setSecure(Boolean secure)
    {
        this.mSecure = secure;
    }
        
    /** Getter for Default Username.
     * @return Value of Default Username.
     *
     */
    private String getDefaultUsername()
    {
        return this.getProject().getProperty(this.DEF_USERNAME_PROP);
    }
    
    /** Getter for property Username.
     * @return Value of property Username.
     *
     */
    public String getUsername()
    {
        if ( this.mUsername == null )
        {
            // try to get the default value from ant prop
            return getDefaultUsername();
        }
        return this.mUsername;
    }
    /**
     * Setter for property Username.
     * @param aUsername user name
     */
    public void setUsername(String aUsername)
    {
        // remove the old jbi client
        this.mJbiAdminCommands = null;
        this.mUsername = aUsername;
    }
    /** Getter for Default Password.
     * @return Value of Default Password.
     *
     */
    public String getDefaultPassword()
    {
        return this.getProject().getProperty(this.DEF_PASSWORD_PROP);
    }
    /** Getter for property Password.
     * @return Value of property Password.
     *
     */
    public String getPassword()
    {
        if ( this.mPassword == null )
        {
            return getDefaultPassword();
        }
        return this.mPassword;
    }
    /**
     * Setter for property Password.
     * @param aPassword password
     */
    public void setPassword(String aPassword)
    {
        // remove the old jbi client
        this.mJbiAdminCommands = null;
        this.mPassword = aPassword;
    }
    
    /** Getter for property passwordFile.
     * @return Value of property  passwordFile.
     *
     */
    public File getPasswordfile()
    {
        return this.mPasswordFile;
    }
    
    /** Setter for property  passwordFile.
     * @param passwordFile New value of property  passwordFile.
     *
     */
    public void setPasswordfile(File passwordFile)
    {
        this.mPasswordFile = passwordFile;
    }
    
    /** Getter for Default host.
     * @return Value of Default host.
     *
     */
    public String getDefaultHost()
    {
        return this.getProject().getProperty(this.DEF_HOST_PROP);
    }
    
    /** Getter for property host.
     * @return Value of property host.
     *
     */
    public String getHost()
    {
        if ( this.mHost == null )
        {
            return getDefaultHost();
        }
        return this.mHost;
    }
    /** Setter for property host.
     * @param aHost New value of property host.
     *
     */
    public void setHost(String aHost)
    {
        // remove the old jbi client
        this.mJbiAdminCommands = null;
        this.mHost = aHost;
    }
    
    /** Setter for properties file.
     *
     * @param propertiesfile name of the properties file
     */
    public void setJmxpropertiesfile(String propertiesfile)
    {
        this.mPropertiesFile = propertiesfile;
    }
    
    /** Getter for properties file
     *
     * @return properties file name
     */
    public String getJmxpropertiesfile()
    {
        if ((mPropertiesFile == null) || (!(new File(mPropertiesFile)).exists()))
        {
            return getDefaultJmxpropertiesfile();
        }
        return mPropertiesFile;
    }
    
    
    /** Default Getter for properties file.
     *
     * @return default properties file name.
     */
    public String getDefaultJmxpropertiesfile()
    {
        return this.getProject().getProperty(this.DEF_JMX_PROPERTIES_FILE);
    }
    
    /**
     * Returns the properties from the file.
     *
     * @return properties
     */
    
    public Properties getProviderProperties()
    {
        Properties conprops = new Properties();
        if (mPropertiesFile != null)
        {
            try
            {
                File f = new File(mPropertiesFile);
                FileInputStream fio = new FileInputStream(f);
                conprops.load(fio);
            }
            catch (Exception ex)
            {
                ;
            }
        }
        return conprops;
    }
    /** Getter for property port.
     * @return Value of property port.
     *
     */
    public String getDefaultPort()
    {
        String port = this.getProject().getProperty(this.DEF_PORT_PROP);
        return port;
    }
    /** Getter for property port.
     * @return Value of property port.
     *
     */
    public String getPort()
    {
        if ( this.mPort == null )
        {
            return getDefaultPort();
        }
        return this.mPort;
    }
    /** Setter for property port.
     * @param aPort New value of property port.
     */
    public void setPort(String aPort)
    {
        // remove the old jbi client
        this.mJbiAdminCommands = null;
        this.mPort = aPort;
    }
    /** Getter for Default host.
     * @return Value of Default host.
     *
     */
    private String getDefaultUrl()
    {
        return this.getProject().getProperty(this.DEF_URL_PROP);
    }
    
    /**
     * returns properties object contain the name/value pairs
     * for jmx connection properties
     * @return Properties object
     */
    protected Properties getJmxConnectionProperties()
    {
        
        String username = this.getUsername();
        String password = this.getPassword();
        String host = this.getHost();
        String port = this.getPort();
        
        String url = this.getDefaultUrl();
        Properties provprops = getProviderProperties();
        
        return JMXConnectionProperties.getJMXConnectionPropertyMap(provprops, url,
            host, port, username, password );
        
    }
    /**
     * reads the password from the file according to the platform password file definition
     * @param pwFile path to the password file
     * @return Value of the password from file
     */
    protected String getPasswordFromFile(File pwFile) throws IOException
    {
        final String AS_ADMIN_PASSWORD_PROP = "AS_ADMIN_PASSWORD";
        String password = null;
        FileInputStream in = null;
        try
        {
            in = new FileInputStream(pwFile);
            Properties props = new Properties();
            props.load(in);
            password = props.getProperty(AS_ADMIN_PASSWORD_PROP);
            if ( password == null )
            {
                throw new IOException(createFailedFormattedJbiAdminResult(
                    "jbi.ui.ant.task.error.password.not.found.in.file", new Object[] {pwFile.getName()}));
            } else {
                return password;
            }
        }
        finally
        {
            if ( in != null )
            {
                try
                {
                    in.close();
                }
                catch (IOException ex)
                {
                    this.logDebug(ex.getMessage());
                }
            }
        }
    }
    /**
     * check if the password file is set. If set get the password from the password file, else
     * get the password from the task attribute "password".
     * @return Value of the password
     */
    protected String getPasswordFromFileOrAttribute() throws IOException
    {
        String password = null;
        File pwFile = this.getPasswordfile();
        if ( pwFile == null )
        {
            // No password file, get it from attribute "password"
            return this.getPassword();
        }
        this.logDebug("Getting password from the password file " + pwFile);
        if (!pwFile.exists())
        {
            this.logDebug("Password file path not found " + pwFile.getPath());
            throw new IOException(createFailedFormattedJbiAdminResult(
                "jbi.ui.ant.task.error.password.file.not.exist", new Object[] {pwFile.getName()}));
        }
        return password;
    }
    /**
     * returns JBIAdminCommands implementation
     * @return JBIAdminCommands interface
     * @throws JMXConnectionException on error
     */
    protected JBIAdminCommands getJBIAdminCommands() throws Exception
    {
        // lazzy initialize the mJbiAdminCommands
        if ( this.mJbiAdminCommands == null )
        {
            // validate jmx connection properties ( e.g. host, port, username, password etc )
            this.validateJmxConnectionPortValue(this.getPort());
            
            String portValue = this.getPort();
            if ( portValue == null )
            {
                portValue = "4848";
            }
            int port = Integer.parseInt(portValue);
            
            String host = this.getHost();
            if ( host == null )
            {
                host = "localhost";
            }
            
            String username = this.getUsername();
            String password = this.getPassword();
            
            ConnectionType connType = null;
            
            Boolean secure = this.getSecure();
            if ( secure != null && secure.booleanValue())
            {
                connType = ConnectionType.HTTPS;
                this.mJbiAdminCommands =
                    JBIAdminCommandsClientFactory.getInstance(host, port, username, password, connType);
            }
            else
            {
                //TODO: get the default connection type from the default settings from common client based on
                // platform ( PE or EE ) settings
                this.mJbiAdminCommands =
                    JBIAdminCommandsClientFactory.getInstance(host, port, username, password);
            }
            
        }
        return this.mJbiAdminCommands;
    }
    
    /** gives the I18N bundle for standard ant tasks. It is possible to
     * have different i18n bundle for extended tasks such as filtered query tasks
     *@return I18NBundle object
     */
    private static I18NBundle getStandardI18NBundle()
    {
        // lazzy initialize the JBI Client
        if ( sI18NBundle == null )
        {
            sI18NBundle = new I18NBundle("com.sun.jbi.ui.ant");
        }
        return sI18NBundle;
    }
    /** gives the I18N bundle for ant tasks
     * extended tasks override this if they want to provide different i18n bundle
     * for their messages.
     *@return I18NBundle object
     */
    protected I18NBundle getI18NBundle()
    {
        return getStandardI18NBundle();
    }
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the success status
     */
    protected abstract String getTaskSuccessStatusI18NKey();
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the failed status
     */
    /**
     * returns i18n key. tasks implement this method.
     * @return i18n key for the warning status
     */
    protected String getTaskWarningStatusI18NKey()
    {
        return null;
    }
    protected abstract String getTaskFailedStatusI18NKey();
    /**
     * returns i18n key. tasks implement this method to return the i18n key.
     * default implementation returns null indicating that the task
     * does not have partial success status message. If the tasks 
     * such as service assembly tasks have to display main partial success
     * message, they have to override and return the i18n key that is in the
     * i18n bundle.
     * 
     * use of this method should check for null return and then fall back to 
     * the getTaskSuccessStatusI18NKey for the main status message for partial
     * success.
     * @return i18n key for the partial success
     */
    protected String getTaskPartialSuccessStatusI18NKey() 
    {
        return null;
    }
    /**
     * logs as a INFO
     * @param anInfoMsg Message
     */
    protected void logInfo(String anInfoMsg)
    {
        log(anInfoMsg,Project.MSG_INFO);
    }
    /**
     * logs as a WARNING
     * @param aWarningMsg Message
     */
    protected void logWarning(String aWarningMsg)
    {
        log(aWarningMsg,Project.MSG_WARN);
    }
    /**
     * logs as a ERROR
     * @param aErrorMsg Message
     */
    protected void logError(String aErrorMsg)
    {
        log(aErrorMsg,Project.MSG_ERR);
    }
    /**
     * logs as a DEBUG
     * @param aDebugMsg Message
     */
    protected void logDebug(String aDebugMsg)
    {
        log(aDebugMsg,Project.MSG_DEBUG);
    }
    /**
     * logs as a VERBOSE
     * @param aVerboseMsg Message
     */
    protected void logVerbose(String aVerboseMsg)
    {
        log(aVerboseMsg,Project.MSG_VERBOSE);
    }
    /**
     * format message
     * @param statusMsg status message
     * @return formatted message
     */
    private String formatSuccessMessage(String statusMsg)
    {
        String key = "jbi.ui.ant.task.msg.success.no.args.format"; //NOI18N
        return getStandardI18NBundle().getMessage(key, statusMsg);
    }
    /**
     * returns formatted success message
     * @param statusMsg main message text
     * @param causeMsg cause message text
     * @return foramtted message
     */
    private String formatSuccessMessage( String statusMsg, String causeMsg )
    {
        if ( causeMsg == null )
        {
            return formatSuccessMessage(statusMsg);
        }
        String key = "jbi.ui.ant.task.msg.success.format";  //NOI18N
        return getStandardI18NBundle().getMessage(key,statusMsg,causeMsg);
    }
    /**
     * returns formated failed message
     * @param statusMsg main message text
     * @param causeMsg cause message text
     * @return formatted message
     */
    private String formatFailedMessage( String statusMsg, String causeMsg )
    {
        String key = "jbi.ui.ant.task.msg.failed.format";  //NOI18N
        return getStandardI18NBundle().getMessage(key,statusMsg,causeMsg);
    }
    
    /**
     * task success message
     * @return formated message
     */
    protected String formatTaskSuccessMessage()
    {
        String statusMsg = getI18NBundle().getMessage(getTaskSuccessStatusI18NKey());
        return formatSuccessMessage(statusMsg);
    }
    
    /**
     * returns formatted success message
     * @param causeMsg cause message
     * @return formated message
     */
    protected String formatTaskSuccessMessage(String causeMsg)
    {
        String statusMsg = getI18NBundle().getMessage(getTaskSuccessStatusI18NKey());
        return formatSuccessMessage(statusMsg, causeMsg);
    }
    
    /**
     * returns formatted failed message
     * @param causeMsg cause message text
     * @return formatted message
     */
    protected String formatTaskFailedMessage(String causeMsg )
    {
        String statusMsg = getI18NBundle().getMessage(getTaskFailedStatusI18NKey());
        return formatFailedMessage(statusMsg, causeMsg);
    }
    /**
     * throws BuildException
     * @param causeMsg cuased message text
     * @throws BuildException with cause
     */
    protected void throwBuildException(String causeMsg)
    throws BuildException
    {
        String expMsg = formatTaskFailedMessage(causeMsg);
        throw new BuildException(expMsg, location);
    }
    
    /**
     * throws BuildException
     * @param aCause a exception to embed
     * @throws BuildException with cause
     */
    protected void throwBuildException(Throwable aCause)
    throws BuildException
    {
        String causeMsg = aCause.getMessage();
        if ( causeMsg == null )
        {
            causeMsg = aCause.toString();
        }
        String expMsg = formatTaskFailedMessage(causeMsg);
        throw new BuildException(expMsg,aCause,location);
    }
    /**
     * throws exception
     * @param aCauseMgmtMsg message object
     * @throws BuildException on error
     */
    protected void throwBuildException(
        JBIManagementMessage aCauseMgmtMsg ) throws BuildException
    {
        String causeMsg = aCauseMgmtMsg.getMessage();
        if ( causeMsg == null )
        {
            causeMsg = aCauseMgmtMsg.toString();
        }
        String expMsg = formatTaskFailedMessage(causeMsg);
        
        // printMessage("*** throwing Failed Result Mgmt Message in print Result " +
        // formattedMsg );
        JBIManagementMessageException aCauseMgmtMsgEx =
            new JBIManagementMessageException(expMsg, aCauseMgmtMsg);
        
        throw new BuildException(expMsg, aCauseMgmtMsgEx, location);
        
    }
    
    /**
     * throws exception
     * @param aCauseKey i18n key of the cause
     * @throws BuildException build exception
     */
    protected void throwTaskBuildException(String aCauseKey) throws BuildException
    {
//        String aCauseMsg = this.getI18NBundle().getMessage(aCauseKey);
        String aCauseMsg = createFailedFormattedJbiAdminResult(aCauseKey, null);
        throwBuildException(aCauseMsg);
    }
    
    /**
     * throws exception
     * @param aCauseKey i18n key of the cause
     * @param aCauseArg1 root cuase
     * @throws BuildException build exception
     */
    protected void throwTaskBuildException(String aCauseKey, String aCauseArg1)
    throws BuildException
    {
//        String aCauseMsg = this.getI18NBundle().getMessage(aCauseKey, aCauseArg1);
        String aCauseMsg = createFailedFormattedJbiAdminResult(aCauseKey, new Object[] {aCauseArg1});
        throwBuildException(aCauseMsg);
    }
    
    /**
     * prints result
     * @param result string to print
     */
    protected void printTaskSuccess(String result)
    {
        if ( result == null || (result.trim().length() <= 0) )
        {
            logWarning(formatTaskSuccessMessage());
        }
        else
        {
            logWarning(formatTaskSuccessMessage(result));
        }
    }
    
    /**
     * prints result
     * @param result string to print
     */
    protected void printTaskPartialSuccess(String result)
    {
        String statusMsg = getI18NBundle().getMessage(getTaskPartialSuccessStatusI18NKey());
        
        String formatterKey = "jbi.ui.ant.task.msg.success.format"; //NOI18N
        
        String formattedResult = null;
        
        if ( result == null || (result.trim().length() <= 0) ) {
            formatterKey = "jbi.ui.ant.task.msg.success.no.args.format"; //NOI18N
            formattedResult = getStandardI18NBundle().getMessage(formatterKey, statusMsg);
        } else {
            formattedResult = getStandardI18NBundle().getMessage(formatterKey, statusMsg, result);
        }
        
        logWarning(formattedResult);
    }
    
    /**
     * prints result
     * @param mgmtMsg message object
     */
    protected void printTaskSuccess(JBIManagementMessage mgmtMsg )
    {
        String msg = mgmtMsg.getMessage();
        
        String partialSuccessI18NKey = this.getTaskPartialSuccessStatusI18NKey();
        
        if ( partialSuccessI18NKey != null && mgmtMsg.isWarningMsg() )
        {
            // isWarningMsge = partial success ( success with warnings )
            // if partail success i18n key available and is a partial success message,
            // then print partail success msg
            printTaskPartialSuccess(msg);
        }
        else
        {
            // print regular success message.            
            printTaskSuccess(msg);
        }
    }
    /** print message
     * @param aMsg a message string
     */
    protected void printMessage(String aMsg)
    {
        logWarning(aMsg);
    }
    /**
     * retrieves the exception message and try to construct the jbi mgmt message
     * @param ex Exception from which the message should be constructed
     * @return JBIManagementMessage object if the excpetion contains the xml text else return null.
     */
    protected JBIManagementMessage extractJBIManagementMessage(Exception ex )
    {
        JBIManagementMessage mgmtMsg = null;
        if ( ex instanceof JBIRemoteException )
        {
            JBIRemoteException rEx = (JBIRemoteException)ex;
            mgmtMsg = rEx.extractJBIManagementMessage();
            // logDebug(rEx.getMessage());
        }
        else
        {
            String exMessage = ex.getMessage();
            mgmtMsg = JBIManagementMessage.createJBIManagementMessage(exMessage);
            // logDebug(exMessage);
        }
        
        return mgmtMsg;
    }
    /**
     * here we check for the management message in the exception and process
     * it for the error, warning or success information. If it is error throw
     * the build exception, for all other cases print exception results with warnings
     * @param ex Exception from the task execution
     * @throws BuildException if the exception status is error
     */
    protected void processTaskException(Exception ex) throws BuildException
    {
        JBIManagementMessage mgmtMsg = extractJBIManagementMessage(ex);
        
        if ( mgmtMsg == null  )
        {
            // not a management message
            throwBuildException(ex);
        }
        else
        {
            // check the mgmt msg is a success, warning or error
            if ( mgmtMsg.isFailedMsg() )
            {
                throwBuildException(mgmtMsg);
            }
            else
            {
                // print success message
                printTaskSuccess(mgmtMsg);
            }
        }
    }
    
    /**
     * processes the result string as a management message for for the error, warning or
     * success information. If it is error throw the build exception, for all other cases
     * print success results with warnings
     * @param result results text
     * @throws BuildException if the exception status is error
     */
    protected void processTaskResult(String result) throws BuildException
    {
        JBIManagementMessage mgmtMsg = null;
        
        if ( result != null )
        {
            mgmtMsg = JBIManagementMessage.createJBIManagementMessage(result);
        }
        
        if ( mgmtMsg == null )
        {
            printTaskSuccess(result);
            return;
        }
        
        // check the mgmt msg is a success, warning or error
        if ( mgmtMsg.isFailedMsg() )
        {
            throwBuildException(mgmtMsg);
        }
        else
        {
            // print success or partial success message
            printTaskSuccess(mgmtMsg);
        }
        
//        if ( mgmtMsg.isSuccessMsg())
//        {
//            printTaskSuccess(mgmtMsg);
//        }
//        else
//        {
//            throwBuildException(mgmtMsg);
//        }
        
        
    }
    
    /**
     * this is where the task exection code gets implemented.
     * this will be called from execute method that preprocesses the build exception
     * before throwing it to ant runtime to process.
     * @throws org.apache.tools.ant.BuildException on error
     */
    protected abstract void executeTask() throws BuildException;
    
    protected void logDebugConnectionAttributes() {
        logDebug("values of jbi task connection attributes: (" +
            " host=" + this.getHost() +
            ", port=" + this.getPort() +
            ", secure=" + this.getSecure() +
            ", username=" + this.getUsername() + ")");        
    }
    
    /** executes the ant task. Ant Task framework calls this method to
     * excute the task. this method intern calls the executeTask where the task execution
     * code gets implemented. This methods processes the failOnError before throwing back
     * the BuildException to the ant environment. If the failOnError is false, it just
     * prints the exception message and returns otherwise it rethrows the build exception.
     * @throws BuildException if error or exception occurs.
     */
    public void execute() throws BuildException
    {
        logDebugConnectionAttributes();
        try
        {
            executeTask();
        }
        catch ( BuildException bEx)
        {
            if ( this.isFailOnError() )
            {
                throw bEx;
            }
            else
            {
                logWarning(bEx.getMessage());
            }
        }
    }
    /**
     * converts the i18n error message to the jbi result display format.
     * @param i18nKey String represneting i18n key
     * @param args arguments to the i18n string
     * @return the string in the jbi result display format of jbi mgmt message.
     */
    protected String createFailedFormattedJbiAdminResult(String i18nKey, Object[] args)
    {
        
        String msgCode = getI18NBundle().getMessage(i18nKey + ".ID");
        String msg = getI18NBundle().getMessage(i18nKey,args);
        
        String jbiResultXml =
            JBIResultXmlBuilder.getInstance()
            .createJbiResultXml("JBI_ANT_TASKS_VALIDATION",
            JBIResultXmlBuilder.FAILED_RESULT,
            JBIResultXmlBuilder.ERROR_MSG_TYPE,
            msgCode, msg, args, null);
        
        JBIManagementMessage mgmtMsg = null;
        mgmtMsg = JBIManagementMessage.createJBIManagementMessage(jbiResultXml);
        
        if ( mgmtMsg != null )
        {
            return mgmtMsg.getMessage();
        }
        else
        {
            return msg;
        }
    }
    /**
     * validate
     *
     * @param port port
     *
     * @throws JMXConnectionException on error
     */
    protected void validateJmxConnectionPortValue(String port)
    throws JMXConnectionException
    {
        if (port == null)
        {
            return;
        }
        
        int portValue = -1;
        
        try
        {
            portValue = Integer.parseInt(port);
        }
        catch (NumberFormatException ex)
        {
            // pass null to the cause as you have already serialized the cause to jbi mgmt xml
            throw new JMXConnectionException(createFailedFormattedJbiAdminResult(
                "jbi.ui.ant.task.error.invalid.jmx.port", new Object[] {port}), null);
        }
    }
    /**
     * return the trimed component name or throws build exception
     * @param name component name string as passed to the attribute
     * @return trimed component name
     * @throws BuildException if the compName is null or empty
     */
    protected String getValidName(String name, String errorMsgI18nKey ) throws BuildException
    {
        String validName = name;
        if ( validName != null )
        {
            validName = validName.trim();
        }
        
        if ( validName == null || validName.length() == 0 )
        {
            throwTaskBuildException(errorMsgI18nKey);
        }
        
        return name;
    }
    /**
     * return the trimed component name or throws build exception
     * @param compName component name string as passed to the attribute
     * @return trimed component name
     * @throws BuildException if the compName is null or empty
     */
    protected String getValidComponentName(String compName) throws BuildException
    {
        return getValidName(compName, "jbi.ui.ant.task.error.nullCompName");
    }
    /**
     * return the trimed component name or throws build exception
     * @param slibName shared library name string as passed to the attribute
     * @return trimed component name
     * @throws BuildException if the compName is null or empty
     */
    protected String getValidSharedLibraryName(String slibName) throws BuildException
    {
        return getValidName(slibName, "jbi.ui.ant.task.error.nullSLibName");
    }
    
    /**
     * return the trimed component name or throws build exception
     * @param saName service assembly name string as passed to the attribute
     * @return trimed component name
     * @throws BuildException if the compName is null or empty
     */
    protected String getValidServiceAssemblyName(String saName) throws BuildException
    {
        return getValidName(saName, "jbi.ui.ant.task.error.nullSAName");
    }
   
    /**
     * parse and print the tabular data
     */
    protected void printTabularData(TabularData statsReport, PrintWriter msgWriter, String tabStr)
    {
        Set names = statsReport.keySet();
        for ( Object name : names )
        {
            Object[] key = ( (java.util.List) name).toArray();
            CompositeData compData = statsReport.get(key);
            printMessage("");
            printCompositeDataRecursively(compData, msgWriter, tabStr);
        }
    }
 
    /**
     * @param compositeData 
     * @param msgWriter
     */
    protected void printCompositeDataRecursively (CompositeData compositeData,
                                               PrintWriter msgWriter, String tabStr)
    {
        if (compositeData == null)
        {
            return;
        }

        CompositeType compType = compositeData.getCompositeType();
        Set openTypes = compType.keySet();
        Iterator itr = openTypes.iterator();
        while  (itr.hasNext())
        {
            String itemName = (String) itr.next();
            OpenType openType = (OpenType) compType.getType(itemName);
            if (openType != null)
            {
                if (openType instanceof SimpleType)
                {
                    msgWriter.println(tabStr + itemName + ": " +
                                          (compositeData.get(itemName)));
                }
                else if (openType instanceof TabularType)
                {
                    TabularData theTabularData = (TabularData) compositeData.get(itemName);
                    printTabularData(theTabularData, msgWriter, tabStr+"\t");
                }
                else if (openType instanceof CompositeType)
                {
                    msgWriter.println(tabStr + itemName + ":");
                    printCompositeDataRecursively((CompositeData) compositeData.get(itemName),
                                                      msgWriter, tabStr+"\t");
                }
                else if (openType instanceof ArrayType)
                {
                    msgWriter.println(tabStr + itemName + ":");
                    OpenType arrayElementType = ((ArrayType) openType).getElementOpenType();
                    Object[] arrayData = (Object[]) compositeData.get(itemName);
                    for (int i=0; i < arrayData.length; ++i)
                    {
                        if (arrayElementType instanceof SimpleType)
                        {
                            msgWriter.println(tabStr + "\t" + arrayData[i]);
                        }
                        else if (arrayElementType instanceof TabularType)
                        {
                            TabularData theTbData = (TabularData) arrayData[i];
                            printTabularData(theTbData, msgWriter, tabStr+"\t");
                        }
                        else if (arrayElementType instanceof CompositeType)
                        {
                            printCompositeDataRecursively((CompositeData) arrayData[i],
                                                               msgWriter, tabStr + "\t");
                        }
                        else if (arrayElementType instanceof ArrayType)
                        {
                            printArrayDataRecursively((Object[]) arrayData[i], arrayElementType,
                                                         msgWriter, tabStr + "\t");
                        }
                    }
                }
            }
        }
    }

    /**
     * @param arrayData
     * @param elementType
     * @param msgWriter
     */
    protected void printArrayDataRecursively (Object[] arrayData, OpenType elementType,
                                                 PrintWriter msgWriter, String tabStr)
    {
        for (int i=0; i < arrayData.length; ++i)
        {
            if (elementType instanceof SimpleType)
            {
                msgWriter.println(tabStr + arrayData[i].toString());
            }
            else if (elementType instanceof TabularType)
            {
                msgWriter.println(tabStr + arrayData[i].toString());
            }
            else if (elementType instanceof CompositeType)
            {
                printCompositeDataRecursively((CompositeData) arrayData[i], msgWriter, tabStr);
            }
            else if (elementType instanceof ArrayType)
            {
                msgWriter.println(tabStr + ((ArrayType) elementType).getTypeName() + ":");
                printArrayDataRecursively((Object[]) arrayData[i], elementType,
                                                                msgWriter, tabStr + "\t");
            }
        }
   }
}
