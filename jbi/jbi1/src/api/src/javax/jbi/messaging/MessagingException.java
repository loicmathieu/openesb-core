/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessagingException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package javax.jbi.messaging;

/** Generic exception used to report messaging related errors in the Normalized 
 *  Message Service.
 *
 * @author JSR208 Expert Group
 */
public class MessagingException extends javax.jbi.JBIException
{
    /** Create a new MessagingException.
     * @param msg error detail
     */
    public MessagingException(String msg)
    {
        super(msg);
    }
    
    /** Create a new MessagingException with the specified cause and error text.
     * @param msg error detail
     * @param cause underlying error
     */
    public MessagingException(String msg, Throwable cause)
    {
        super(msg, cause);
    }
    
    /** Create a new MessagingException with the specified cause.
     * @param cause underlying error
     */   
    public MessagingException(Throwable cause)
    {
        super(cause);
    }
}
