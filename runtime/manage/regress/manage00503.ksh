#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)manage00503.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#manage00503 - test the autoinstall and autodeploy functionality.
#              reproduce issuezilla case 42 -- trying to autouninstall
#              a shared library that has been manually uninstalled.
#the stand-alone JBI container version of this test is manage00513.ksh.
#
# autoinstall components
# manually uninstall components
# manuall reinstall components
# autoreinstall components
# manually uninstall components
# autoreinstall components
# manually uninstall components
# manuall reinstall components
# autouninstall components

echo testname is manage00503
. ./regress_defs.ksh

list_files()
{
    $JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa list-shared-libraries

    echo "File count: $autoInstallDir"
    pls -l $autoInstallDir | wc -l
    echo "File count: $statusDir"
    pls -l $statusDir | wc -l
    echo "autoinstalled area success files"
    pls -l $autoInstallDir | grep _installed | wc -l
    pls -l $autoInstallDir | grep _uninstalled | wc -l
    echo "autoinstalled area failed files"
    pls -l $autoInstallDir | grep _notInstalled | wc -l
    pls -l $autoInstallDir | grep _notUninstalled | wc -l
    echo "recursive listing"
    cd $autoInstallDir
    find . -type f -print | sort
}

auto_install()
{
    echo "Copying component and shared library installers into autoinstall directory."
    cp $JV_JBI_HOME/components/jbicalc/installers/calc.jar $autoInstallDir
    cp $JV_JBI_HOME/components/sharedlibraries/sns1.jar $autoInstallDir
    chmod +w $autoInstallDir/*.jar
    autoInstallDelay
    list_files
}

auto_uninstall()
{
    echo "removing installers from autoinstall directory."
    rm $autoInstallDir/calc.jar $autoInstallDir/sns1.jar
    autoInstallDelay
    list_files
}

manually_uninstall()
{
    echo "manually uninstalling components"
    $JBI_ANT -Djbi.shared.library.name=b3c97e99-bcb2-4ee0-95e4-60e1cf1408aa uninstall-shared-library
    uninstallComponentDelay
    $JBI_ANT -Djbi.component.name=JbiCalculator uninstall-component
    uninstallComponentDelay
    list_files
}

manually_reinstall()
{
    echo "manually reinstalling components"
    $JBI_ANT -Djbi.install.file=$JV_JBI_HOME/components/sharedlibraries/sns1.jar install-shared-library
    installComponentDelay
    $JBI_ANT -Djbi.install.file=$JV_JBI_HOME/components/jbicalc/installers/calc.jar install-component
    installComponentDelay
    list_files
}

echo "Before first autoinstall ..."

autoInstallDir="$JBI_DOMAIN_ROOT/jbi/autoinstall"
statusDir="$autoInstallDir/.autoinstallstatus"

rm -rf $autoInstallDir
mkdir -p $autoInstallDir

list_files
#test really begins here
auto_install
manually_uninstall
manually_reinstall
auto_install
manually_uninstall
auto_install
manually_uninstall
manually_reinstall
auto_uninstall
