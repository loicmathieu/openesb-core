/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XmlReaderMain.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

/**
 * XmlReaderMain -- read in and print out expected values from the specified
 * files (supposedly the jbi.xml file from the archive).
 *
 * @author Sun Microsystems, Inc.
 *
 */
public class XmlReaderMain
{

    /**
     * Main routine, called by external user.
     *
     * @param aSetOfArguments list of XML file(s)
     * @throws Exception unexpectedly.
     */
    public static void main(String[] aSetOfArguments)
        throws Exception
    {        
        XmlReader xmlReader = new XmlReader();
        for (int i = 0; i < aSetOfArguments.length; i++)
        {
            System.out.println("\nEvaluating " + aSetOfArguments[i] + "...");
            xmlReader.loadAndParse(aSetOfArguments[i], false);
        }
    }
}
