/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestFileHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.util;

import java.io.File;

/**
 *
 * @author Sun Microsystems, Inc.
 */

public class TestFileHelper
    extends junit.framework.TestCase
{
    private String mConfigDir = null;
    private String GOOD_COMP               = "component.zip";
    private String MODIFIED_GOOD_COMP      = "component-modified.zip";
    
    public TestFileHelper(String testName)
    {
        super(testName);
        String srcroot = System.getProperty("junit.srcroot");
        String manage = "/runtime/manage";        // open-esb build
        mConfigDir = srcroot + manage + "/target/test-classes/testdata/";

        java.io.File f = new java.io.File(srcroot + manage);
        if (! f.exists())
        {
            manage = "/shasta/manage";       // mainline/whitney build
            mConfigDir = srcroot + manage + "/target/regress/testdata/";
        }
    }
    
    /**
     * Test comparing a file with itself, should be identical
     */
    public void testAreFilesIdentical()
        throws Exception
    {
        File testComp = new File(mConfigDir, GOOD_COMP); 
        assertTrue(FileHelper.areFilesIdentical(testComp, testComp));
    }
    
    /**
     * Test comparing with a file with a slight change to jbi.xml, should not be
     * Identical
     */
    public void testAreFilesIdenticalNegative()
        throws Exception
    {
        File testComp1 = new File(mConfigDir, GOOD_COMP); 
        File testComp2 = new File(mConfigDir, MODIFIED_GOOD_COMP); 
        assertFalse(FileHelper.areFilesIdentical(testComp1, testComp2));
    }
    
    /**
     * This method is used to test copy method in FileHelper class
     * @throws Exception if the test could not be executed
     */
    public void testCopy()
    throws Exception
    {
        String testClasses = 
                System.getProperty("junit.srcroot") +
                "/runtime/manage/target/test-classes/com/sun/jbi/management/internal/support";
        String dest =
                System.getProperty("junit.srcroot") +
                "/runtime/manage/target/test-classes/filehelper_test/";
        File srcDir = new File(testClasses);
        File destDir = new File(dest);       
        
        String[] srcFiles = srcDir.list();
        java.util.Arrays.sort(srcFiles);
        

        FileHelper.copy(testClasses, dest);
        
        //verify that the dir has been created
        assertTrue(destDir.exists());
        
        String[] destFiles = destDir.list();
        java.util.Arrays.sort(destFiles);
                
        //verify that all the files have been copied
        assertEquals(srcFiles.length, destFiles.length);
        
        //verify that all the files are copied correctly
        for(int i=0; i<srcFiles.length; i++)
        {
            assertEquals(srcFiles[i].toString(), destFiles[i].toString());
            assertTrue(
                    FileHelper.areFilesIdentical(
                        new File(testClasses, srcFiles[i]), 
                        new File(dest, destFiles[i])));
        }
        assertTrue(FileHelper.cleanDirectory(destDir));        
    }
    
    /**
     * Test reading the file into a String.
     */
    public void testReadFile()
        throws Exception
    {
        File testXmlFile = new File(mConfigDir, "DOMUtil-ns-good.xml");
        
        String fileStr = FileHelper.readFile(testXmlFile);
        
        assertTrue(fileStr.indexOf("/good-namespace>") != -1);
    }
    
}
