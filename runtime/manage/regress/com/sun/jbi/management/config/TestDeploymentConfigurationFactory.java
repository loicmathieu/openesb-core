/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDeploymentConfigurationFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.management.system.ScaffoldEnvironmentContext;
import java.util.Properties;

import javax.management.Descriptor;
import javax.management.modelmbean.ModelMBeanAttributeInfo;

public class TestDeploymentConfigurationFactory 
    extends junit.framework.TestCase
{

    ScaffoldEnvironmentContext mEnvCtx;
    
    public TestDeploymentConfigurationFactory (String aTestName)
        throws Exception
    {
        super(aTestName);
        
        // The ConfigurationFactory gets the Env. Ctx from Environment Access
        // Instantiating Env. Ctx here causes the context to be set in EnvironmentAccess
        mEnvCtx = new ScaffoldEnvironmentContext();
        mEnvCtx.setJbiInstanceRoot("C:/as8/domains/CAS/jbi");
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
    }

    public void tearDown()
        throws Exception
    {
    }
    
    /**
     * Test the creation of the MBean Attribute Info
     */
    public void testCreateMBeanAttributeInfo()
           throws Exception
    {
        Properties emptyProps = new Properties();
        
        DeploymentConfigurationFactory dFactory = new DeploymentConfigurationFactory(emptyProps);
       
        ModelMBeanAttributeInfo[] mbeanInfos = dFactory.createMBeanAttributeInfo();
        
        for ( ModelMBeanAttributeInfo mbeanInfo : mbeanInfos )
        {
            // -- test getting the serviceUnitTimeout attribute
            if ( mbeanInfo.getName().equals("serviceUnitTimeout") )
            {
                Descriptor descr = mbeanInfo.getDescriptor();
                assertEquals(new Integer(0),(Integer) descr.getFieldValue("default"));
            }
        }
    }
    
    
    /**
     * Test the creation of the MBean Attribute Info with defualt overrides
     */
    public void testCreateMBeanAttributeInfoWithOverrides()
           throws Exception
    {
        Properties defProps = new Properties();
        defProps.setProperty("deployment.serviceUnitTimeout", "1000");
        
        DeploymentConfigurationFactory dFactory = new DeploymentConfigurationFactory(defProps);
       
        ModelMBeanAttributeInfo[] mbeanInfos = dFactory.createMBeanAttributeInfo();
        
        for ( ModelMBeanAttributeInfo mbeanInfo : mbeanInfos )
        {
            // -- test getting the serviceUnitTimeout attribute overriden value
            if ( mbeanInfo.getName().equals("serviceUnitTimeout") )
            {
                Descriptor descr = mbeanInfo.getDescriptor();
                assertEquals(new Integer(1000), (Integer) descr.getFieldValue("default") );
            }
        }
    }
}
