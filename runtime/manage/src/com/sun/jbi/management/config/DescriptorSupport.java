/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DescriptorSupport.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.config;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.LocalStringKeys;
import java.util.logging.Logger;

/**
 * DescriptorSupport is an extension of the JMX ModelMBean DescriptorSupport.
 * It provides additional setters to set fields specific to the JBI Configuration.
 * Additionaly, it overrrides the isValid operation to check for the presence of 
 * all required fields. 
 *
 * @author Sun Microsystems, Inc.
 */
public class DescriptorSupport 
   extends  javax.management.modelmbean.DescriptorSupport
{
    private static final String DESCRIPTOR_TYPE       = "descriptorType";
    private static final String DESCRIPTOR_TYPE_VALUE = "attribute";
    
    private StringTranslator         mTranslator;
    private Logger mLog;
    
    /*-------------------------------------------------------------------------------*\
     *                             Mandatory Fields                                  *
    \*-------------------------------------------------------------------------------*/
    
    public enum RequiredFieldName 
    {
        /** Attribute name */
        ATTR_NAME("name"),
        /** Indicates whether the attribute is static or dynamic */
        IS_STATIC ("isStatic"),
        /** Default value of the attribute. JMX Standard field name */
        DEFAULT("default"),
        /** Display name for the field */
        DISPLAY_NAME("displayName"),
        /** The I18N Token for the Display namefield */
        DISPLAY_NAME_ID("displayNameId"),
        /** The I18N Token for the description of the attribute provided in the attribute info */
        DESCRIPTION_ID("descriptionId"),
        /** Tool tip */
        TOOL_TIP("toolTip"),
        /** The I18N Token for the  Tool tip*/
        TOOL_TIP_ID("tootTipId"),
        /** Resource Bundle Name */
        RESOURCE_BUNDLE_NAME("resourceBundleName"),
        /** Indicates whether the attribute is a passwordfield */
        IS_PASSWORD("isPassword"); 

        /** field name instance variable */
        private String mFieldName;
        
        /**
         * @param name - descriptor field name.
         */
        private RequiredFieldName(String name)
         {
             mFieldName = name;
         }
        
        /**
         * @return the string value for the field name
         */
        public String getFieldName()
        {
            return mFieldName;
        }
    }
        
    /*-------------------------------------------------------------------------------*\
     *                             Optional Fields                                   *
    \*-------------------------------------------------------------------------------*/    
    
    public enum OptionalFieldName
    {   
        /** Minimum value of an attribute */
        MIN_VALUE("minValue"),
        /** Maximum value of an attribute */
        MAX_VALUE("maxValue"),
        /** Range of allowed values for an attribute */
        ENUM_VALUE("enumValue"),
        /** Unit of mesurement for an attribute */
        UNIT("unit");

        /** field name instance variable */
        private String mFieldName;
        
        /**
         * @param name - descriptor field name.
         */
        private OptionalFieldName(String name)
         {
             mFieldName = name;
         }
        
        /**
         * @return the string value for the field name
         */
        public String getFieldName()
        {
            return mFieldName;
        }  
    };

    /**
     * Default constructor
     */
    public DescriptorSupport()
    {
        super();
        setUnmutableFields();
    }
    
    /**
     * Descriptor constructor taking a Descriptor as parameter. 
     *
     * @param inDescriptor - the descriptor to initialize from
     * @see javax.jbi.management.modelmbean.DescriptorSupport
     */
    DescriptorSupport(DescriptorSupport inDescr) 
    {
        super(inDescr);
        setUnmutableFields();
    }
          
    /**
     * Descriptor constructor. 
     *
     * @param initNumFields - initial number of fields
     * @see javax.jbi.management.modelmbean.DescriptorSupport
     */
    DescriptorSupport(int initNumFields) 
        throws javax.management.MBeanException
    {
        super(initNumFields);
        setUnmutableFields();
    }
          
    /**
     * Descriptor constructor taking an XML String. 
     * @param inStr - XML string 
     * @see javax.jbi.management.modelmbean.DescriptorSupport
     */
    DescriptorSupport(String inStr) 
        throws javax.management.MBeanException,
               javax.management.modelmbean.XMLParseException
    {
        super(inStr);
        setUnmutableFields();
    }
    
    /**
     * Constructor taking fields in the fieldName=fieldValue format. 
     *
     * @param fileds - fields in the fieldName=fieldValue format. 
     * @see javax.jbi.management.modelmbean.DescriptorSupport
     */
    DescriptorSupport(String[] fields) 
    {
        super(fields);
        setUnmutableFields();
    }

    /**
     * Constructor taking field names and field values. 
     */
    DescriptorSupport(String[] fieldNames, Object[] fieldValues)
    {
        super(fieldNames, fieldValues);
        setUnmutableFields();
    }

    /*-------------------------------------------------------------------------------*\
     *                             Setters                                           *
    \*-------------------------------------------------------------------------------*/    
    
    /**
     * @param name - identification for the attribute
     */
     public void setAttributeName(String name)
     {
         setField(RequiredFieldName.ATTR_NAME.getFieldName(), name);
     }
         
     /**
      * @param isStatic - if true, the JBI runtime has to be restarted 
      *                   for changes to apply.
      */
     public void setIsStatic(Boolean isStatic)
     {
         setField(RequiredFieldName.IS_STATIC.getFieldName(), isStatic);
     }
     
     /**
      * @param isStatic - if true, this field is to be encrypted before
      *                   persisting it and the value is masked when retrieved.
      */
     public void setIsPassword(Boolean isPassword)
     {
         setField(RequiredFieldName.IS_PASSWORD.getFieldName(), isPassword);
     }
     
     /**
      * @param def - default value for the object
      */
     public void setDefault(Object def)
     {
        setField(RequiredFieldName.DEFAULT.getFieldName(), def);
     }
     
     /**
      * @param displayName - display name string for the attribute
      */
     public void setDisplayName(String displayName)
     {
        setField(RequiredFieldName.DISPLAY_NAME.getFieldName(), displayName);
     }

     /**
      * @param id - localzation token for display name.
      */
     public void setDisplayNameId(String id)
     {
        setField(RequiredFieldName.DISPLAY_NAME_ID.getFieldName(), id);
     }
     
     /**
      * @param id - localzation token for description.
      */
     public void setDescriptionId(String id)
     {
        setField(RequiredFieldName.DESCRIPTION_ID.getFieldName(), id);
     }
     
     /**
      * @param toolTip - long description for the attribute.
      */
     public void setToolTip(String toolTip)
     {
         setField(RequiredFieldName.TOOL_TIP.getFieldName(), toolTip);
     }
     
     /**
      * @param toolTipId - localization id for the tool tip string.
      */
     public void setToolTipId(String toolTipId)
     {
         setField(RequiredFieldName.TOOL_TIP_ID.getFieldName(), toolTipId);
     }
     
     /**
      * @param bundleName - resource bundle identification
      */
     public void setResourceBundleName(String bundleName)
     {
         setField(RequiredFieldName.RESOURCE_BUNDLE_NAME.getFieldName(), bundleName);
     }
     
     /**
      * @param value - minumum value allowed
      */
     public void setMinValue(Object value)
     {
         setField(OptionalFieldName.MIN_VALUE.getFieldName(), value);
     }     

     /**
      * @param value - maximum value allowed
      */
     public void setMaxValue(Object value)
     {
         setField(OptionalFieldName.MAX_VALUE.getFieldName(), value);
     }     
     
     /**
      * @param value - emumeration of allowed values
      */
     public void setEnumValue(String value)
     {
         setField(OptionalFieldName.ENUM_VALUE.getFieldName(), value);
     }     
     
     /**
      * @param unit - unit as a string
      */
     public void setUnit(String unit)
     {
         setField(OptionalFieldName.UNIT.getFieldName(), unit);
     } 

     /**
      * For a decriptor to be valid the following has to be true :
      *
      * <ul>
      *     <li> The basic isValid() of the base javax.management.modelmbean.DescriptorSupport should pass. </li>
      *     <li> All required fields should have been set </li>
      * </ul>
      */
     public boolean isValid()
        throws javax.management.RuntimeOperationsException
     {
         boolean isValid = false;
        if (super.isValid())
        {
            java.util.List<String> names = java.util.Arrays.asList(this.getFieldNames());

            for ( RequiredFieldName fieldName : RequiredFieldName.values() )
            {
                if ( !names.contains(fieldName.getFieldName()) )
                {
                    String exMsg = getTranslator().getString(
                        LocalStringKeys.CS_DESCR_REQ_FIELD_MISSING,
                        fieldName.getFieldName(), (String) getFieldValue("name"));
                    getLogger().warning(exMsg);
                    isValid = false;
                    break;
                }
                isValid = true;
            }
        }
        return isValid;
        
     }
    
     /**
      * Set the default values for the fields
      */
     private void setUnmutableFields()
     {
        setField(DESCRIPTOR_TYPE, DESCRIPTOR_TYPE_VALUE);
     }
     
     /**
      * @return the management String translator
      */
     private StringTranslator getTranslator()
     {
        if ( mTranslator == null )
        {
            com.sun.jbi.EnvironmentContext envCtx = com.sun.jbi.util.EnvironmentAccess.getContext();
            mTranslator = envCtx.getStringTranslator("com.sun.jbi.management");
        }
        return mTranslator;
     }
     
     /**
      * @return the management logger
      */
     private Logger getLogger()
     {
         if ( mLog  == null )
         {
             mLog =Logger.getLogger("com.sun.jbi.management");
         }
         return mLog;
     }
}
