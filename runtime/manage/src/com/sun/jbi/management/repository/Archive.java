/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Archive.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

import com.sun.jbi.management.descriptor.Jbi;
import com.sun.jbi.management.descriptor.ServiceUnit;

import java.io.File;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.Marshaller;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.OutputKeys;

import com.sun.jbi.management.system.ManagementContext;
/** 
 *  Data object containing information about an archive in the ESB repsository.
 */
public class Archive
{
    private static final String JBI_XML      = "jbi.xml";
    private static final String JBI_XML_BASE = "jbi_";
    private static final String JBI_XML_EXT = ".xml";
    private static final String JBI_XML_DIR = "META-INF/";
    private static final String JBI_XML_PATH = JBI_XML_DIR + JBI_XML;

    /**
     * registry schema file
     */
    public static final String JBI_DESCRIPTOR_SCHEMA = "jbi.xsd";
    
    /**
     * registry schema subdir in JBI_HOME
     */
    public static final String JBI_DESCRIPTOR_SCHEMA_DIR = "schemas";
    
    /** JAXB content model for jbi.xml. */
    private Jbi         mJbiXml;
    /** JAXB content model for localized jbi.xml. */
    private Jbi         mJbiXmlLocalized;
    /** The String version of jbi.xml */
    private String      mJbiXmlString = null;
    /** The localized String version of jbi.xml */
    private String      mJbiXmlStringLocalized;
    /** The type of the archive. */
    private ArchiveType mType;
    /** Path to the archive. */
    private String      mPath;
    /** The JBI name of the archive, as specified in jbi.xml. */
    private String      mJbiName;
    /** The original file name for the archive. */
    private String      mFileName;
    /** Time added to the repository. */
    private Calendar    mUploadTimestamp;
    /** Last modified timestamp of jbi.xml file in archive package. */
    private Calendar    mJbiXmlTimestamp;
    /** Size of the archive. */
    private BigInteger  mSize;
    /** Map containing child archives -- archives within this archive. */
    private HashMap     mChildren;
    /** Flag indicating that the archive jbi.xml should be validated against
     *  the JBI schema. */
    private boolean     mRequiresValidation;
    /** Used to load JAXB object model for jbi.xml */
    private Unmarshaller mReader;
    private Marshaller  mWriter;

    /**
     * descriptor schema 
     */
    private File mDescSchema;
            
    /** Create a new Archive object from the specified archive package. */
    public Archive(File archiveZip, boolean validate)
        throws java.io.IOException, RepositoryException
    {
        mChildren           = new HashMap();
        mRequiresValidation = validate;        
        
        try
        {
            // setup JAXB
            JAXBContext jc = JAXBContext.newInstance( "com.sun.jbi.management.descriptor", 
                    Class.forName ("com.sun.jbi.management.descriptor.Jbi").getClassLoader());
            mReader = jc.createUnmarshaller();

            com.sun.jbi.EnvironmentContext envCtx = com.sun.jbi.util.EnvironmentAccess.getContext();
            File schemaDir = new File(envCtx.getJbiInstallRoot(), JBI_DESCRIPTOR_SCHEMA_DIR);
            mDescSchema = new File(schemaDir, JBI_DESCRIPTOR_SCHEMA);            
            
            mReader.setSchema(
                    javax.xml.validation.SchemaFactory.newInstance(
                        javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(mDescSchema));
            
            mWriter = jc.createMarshaller();
            mWriter.setProperty("jaxb.formatted.output", new Boolean(true));
            
        }
        catch (Exception ex)
        {
            throw new RepositoryException(ex.toString());
        }
        
        parseArchive(archiveZip);

        // Get the String for the archive descriptor
        initJbiXmlString(false);
        initJbiXmlString(true);
    }

    /** Return type of archive. */
    public ArchiveType getType()
    {
        return mType;
    }
    
    /** Return the name of the archive as specified in jbi.xml. */
    public String getJbiName()
    {
        return mJbiName;
    }
    
    /** Return the original name of the archive file. */
    public String getFileName()
    {
        return mFileName;
    }
    
    /** Return the absolute path to the archive file. */
    public String getPath()
    {
        return mPath;
    }
    
    /** Return a reference to the JAXB object model of the archive's
     *  deployment descriptor.
     * @param localized set to <code>true</code> to return the localized
     * version or <code>false</code> to return the default version.
     *  NOTE: Currently, only the server locale is supported.
     */
    public Jbi getJbiXml(boolean localized)
    {
        if ( localized )
        {
            return mJbiXmlLocalized;
        }
        else
        {
            return mJbiXml;
        }
    }
 
    /** Return the XML String for the JBI XML
     * @param localized set to <code>true</code> to return the localized
     * version or <code>false</code> to return the default version.
     *  NOTE: Currently, only the server locale is supported.
     */
    public String getJbiXmlString(boolean localized)
    {
        if ( localized )
        {
            return mJbiXmlStringLocalized;
        }
        else
        {
	    return mJbiXmlString;
        }
    }
    
    /** Return the date that the archive was added to the repository. */
    public java.util.Calendar getUploadTimestamp()
    {
        return mUploadTimestamp;
    }
    
    /** Return the size of the archive. */
    public java.math.BigInteger getSize()
    {
        return mSize;
    }    

    /** Return the timestamp of the jbi.xml file in the archive package. */
    public java.util.Calendar getJbiXmlTimestamp()
    {
        return mJbiXmlTimestamp;
    }
    
    /** Sets the JBI name/id of this archive. */
    public void setJbiName(String jbiName)
    {
        mJbiName = jbiName;
    }
    
    /** Sets the repository upload timestamp. */
    public void setUploadTimestamp(Calendar timestamp)
    {
        mUploadTimestamp = timestamp;
    }
    
    /** Sets the path for the archive package. */
    public void setPath(String path)
    {
        mPath = path;
    }
        
    /** Indicates whether this archive contains other archives.  For example,
     *  a service assembly contains one or more service units.
     */
    public boolean hasChildren()
    {
        return !mChildren.isEmpty();
    }
    
    /** Lists the names of child archives, if any, contained within this 
     *  archive. 
     */
    public Iterator listChildren()
    {
        return mChildren.keySet().iterator();
    }
    
    /** Returns the path to the specified child archive.
     *  @param name the JBI name/id of the child archive
     */
    public String getChildPath(String name)
    {
        return (String)mChildren.get(name);
    }
    
    /**
     * Given an archive zip file, return the entry for the jbi.xml for the
     * specified locale, if one is available. If none is available, return the
     * entry for the base jbi.xml. The search order is:
     * 
     * 1. jbi_LL_CC.xml where "LL" is the language code and "CC" is the
     *    country code
     * 2. jbi_LL.xml where "LL" is the language code
     * 3. jbi.xml
     *
     * For information on the language and country codes, see the javadoc for
     * java.util.ResourceBundle.
     *
     * @param zip the ZipFile representing the archive.
     * @param locale the Locale for which the jbi.xml is to be obtained, or null
     * to request the base jbi.xml file.
     * @return the ZipEntry representing the jbi.xml that was found. If none
     * was found, return value is null.
     * @throws java.io.IOException if an I/O error occurs.
     */
    private ZipEntry getJbiXmlEntry(ZipFile archive, Locale locale)
        throws java.io.IOException
    {
        ZipEntry    jbiXmlEntry;
        String      jbiXmlPath;

        if ( null != locale )
        {
            jbiXmlPath = JBI_XML_DIR + JBI_XML_BASE + locale.toString() +
                JBI_XML_EXT;
            jbiXmlEntry = archive.getEntry(jbiXmlPath);
            if ( null == jbiXmlEntry )
            {
                jbiXmlPath = JBI_XML_DIR + JBI_XML_BASE + locale.getLanguage() +
                    JBI_XML_EXT;
                jbiXmlEntry = archive.getEntry(jbiXmlPath);
                if ( null == jbiXmlEntry )
                {
                    jbiXmlEntry = archive.getEntry(JBI_XML_PATH);
                }
            }
        }
        else
        {
            jbiXmlEntry = archive.getEntry(JBI_XML_PATH);
        }
        return jbiXmlEntry;
    }
    
    private void parseArchive(File archiveFile)
        throws java.io.IOException, RepositoryException
    {
        ZipFile     zip;
        ZipEntry    jbiXmlEntry;
        ZipEntry    jbiXmlEntryLocalized;
        
        zip         = new ZipFile(archiveFile);
        mPath       = archiveFile.getAbsolutePath();  
        mFileName   = archiveFile.getName();
        
        jbiXmlEntry = getJbiXmlEntry(zip, null);
        jbiXmlEntryLocalized = getJbiXmlEntry(zip, Locale.getDefault());

        // determine the size of the archive
        mSize = BigInteger.valueOf(archiveFile.length());
        
        // process JBI metadata
        try
        {
            if (jbiXmlEntry == null)
            {
                throw new java.io.FileNotFoundException(
                        archiveFile.getName() + " : " + JBI_XML_PATH);
            }
            
            mJbiXml = loadJbiXml(archiveFile.getName(),
                zip.getInputStream(jbiXmlEntry));
            if ( jbiXmlEntry == jbiXmlEntryLocalized )
            {
                mJbiXmlLocalized = mJbiXml;
            }
            else
            {
                mJbiXmlLocalized = loadJbiXml(archiveFile.getName(),
                    zip.getInputStream(jbiXmlEntryLocalized));
            }

            parseJbiXml();
            
            if (mType.equals(ArchiveType.SERVICE_ASSEMBLY))
            {
                processServiceUnits(zip);
            }
        }
        catch (java.io.IOException ioex)
        {
            throw new RepositoryException(ioex.toString());
        }
        finally
        {
            if ( zip != null )
            {
                zip.close();
            }
        }
                
        // set the jbi.xml timestamp value to its modified date in the zip
        mJbiXmlTimestamp = Calendar.getInstance();
        mJbiXmlTimestamp.setTimeInMillis(jbiXmlEntry.getTime());        
    }
    
    /** Parse the archive's jbi.xml using JAXB.  Appropriate member variables
     *  are set based on jbi.xml content.
     */
    private void parseJbiXml()
    {        
        // figure out what we are dealing with -- there's really not an elegant
        // way to do this.
        if (mJbiXml.getComponent() != null)
        {
            mJbiName = mJbiXml.getComponent().getIdentification().getName();
            mType    = ArchiveType.COMPONENT;
        }
        else if (mJbiXml.getSharedLibrary() != null)
        {
            mJbiName = mJbiXml.getSharedLibrary().getIdentification().getName();
            mType    = ArchiveType.SHARED_LIBRARY;
        }        
        else if (mJbiXml.getServiceAssembly() != null)
        {
            mJbiName = mJbiXml.getServiceAssembly().getIdentification().getName();
            mType    = ArchiveType.SERVICE_ASSEMBLY;
        }
        else
        {
            mType = ArchiveType.SERVICE_UNIT;
        }
    }
    
    /** Find service unit archives and store them as children. */
    private void processServiceUnits(ZipFile zip)
        throws java.io.IOException, RepositoryException
    {
        List        suList;
        ServiceUnit su;
        String      suName;
        String      suPath;
        ZipEntry    suZip;
        InputStream suStream;
        
        suList = mJbiXml.getServiceAssembly().getServiceUnit();
        for (int i = 0; i < suList.size(); i++)
        {
            su      = (ServiceUnit)suList.get(i);
            suName  = su.getIdentification().getName();
            suPath  = su.getTarget().getArtifactsZip();
            suZip   = zip.getEntry(suPath);
                        
            if (suZip == null)
            {
                throw new java.io.FileNotFoundException(
                        localName(zip) + " : " + suPath);
            }
            
            // load the jbi.xml strictly for validation purposes
            suStream = getZipStream(zip.getInputStream(suZip), JBI_XML_PATH);
            if (suStream.available() <= 0)
            {                    
                throw new java.io.FileNotFoundException(
                        suPath + " : " + JBI_XML_PATH);
            }

            loadJbiXml(suPath, suStream);
            
            // looks good, add SU archive to the list of children
            mChildren.put(suName, suPath);
        }
    }
    
    /** Given an input stream, this method returns a zip input stream with the 
     *  pointer at the position of the specified entry.
     */
    private InputStream getZipStream(InputStream zipStream, String name)
        throws java.io.IOException
    {
        ZipInputStream  zis;
        ZipEntry        entry;
        
        zis = new ZipInputStream(zipStream);
        while ( (entry = zis.getNextEntry()) != null)
        {
            if (entry.getName().equals(name))
            {
                // stream pointer is set to beginning of targeted entry
                break;
            }
        }
        
        return zis;
    }
    
    private Jbi loadJbiXml(String entryName, InputStream jbiXmlStream)
        throws RepositoryException
    {
        Jbi jbiXml;
        
        try
        {          
            jbiXml = (Jbi)mReader.unmarshal(jbiXmlStream);
     
        }        
        catch (javax.xml.bind.JAXBException jEx)
        {   
            String message = 
                    jEx.getLinkedException() != null ?
                    jEx.getLinkedException().getMessage() : jEx.getMessage(); 
            
            if (message == null)
            {
                message = jEx.toString();
            }
            // schema validation failed         
            throw new RepositoryException("Schema validation failed for "
                        + entryName + " : " + JBI_XML + ".  " + message);
        }
        
        return jbiXml;
    }
    
    /** Trims the absolute path provided by ZipFile.getName() down to
     *  the name of the file.
     */
    private String localName(ZipFile zip)
    {
        return new File(zip.getName()).getName();
    }    
    
    /** Initialize the the XML String for the JBI XML
     * @param localized set to <code>true</code> to initialize the localized
     * string or <code>false</code> to initialize the default string.
     */
    private void initJbiXmlString(boolean localized)
        throws RepositoryException
    {
	StringWriter sw = null;
	ZipFile zip = null;
	StreamSource source = null;
	StreamResult result = null;
	try
	{
	    zip = new ZipFile(new File(mPath));
	    ZipEntry jbiXmlEntry;
            if ( localized )
            {
	        jbiXmlEntry = getJbiXmlEntry(zip, Locale.getDefault());
            }
            else
            {
	        jbiXmlEntry = getJbiXmlEntry(zip, null);
            }

	    TransformerFactory tF = TransformerFactory.newInstance();
	    Transformer tr = tF.newTransformer();
	    tr.setOutputProperty(OutputKeys.INDENT, "yes");

	    source = new StreamSource(zip.getInputStream(jbiXmlEntry));
	    sw = new StringWriter();
	    result = new StreamResult(sw);
	    tr.transform(source, result);
            if (sw != null)
	    {
                if ( localized )
                {
                    mJbiXmlStringLocalized = sw.toString();
                }
                else
                {
		    mJbiXmlString = sw.toString();
                }
	    }
	}
        catch (Exception ex)
	{
		throw new RepositoryException(ex.toString());
	}
	finally
	{
	   try
	   {
		if (zip != null)
		{
		    zip.close();
		}

		if (sw != null)
		{
		    sw.close();
		}

		if (source != null && source.getInputStream() != null)
		{
		    source.getInputStream().close();
		}

		if (result != null && result.getOutputStream() != null)
		{
		   result.getOutputStream().close();
		}
	   }
	   catch (Exception ex)
	   {
		;
	   }
	}
    }
}
