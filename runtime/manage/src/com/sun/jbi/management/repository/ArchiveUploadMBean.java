/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ArchiveUploadMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.repository;

/**
 *  Uploads archive bytes to a temporary store in the ESB repository.
 */
public interface ArchiveUploadMBean
{
    public static final String ARCHIVE_NAME = 
            "com.sun.jbi.management.archiveName";
    
    /** Initiates an upload session.  The Object returned from this method
     *  is used as an identifier for uploading bytes with the 
     *  <code>uploadBytes()</code> method.
     *  @return a unique id used for uploading bytes
     */
    Object initiateUpload(String archiveName)
        throws java.io.IOException;
    
    /** Uploads a chunk of bytes to an existing temporary archive file.
     *  @param id the upload identifier
     *  @param bytes the content to upload
     */
    void uploadBytes(Object id, byte[] bytes)
        throws java.io.IOException;
        
    /** Used to indicate that the upload session with the specified id is
     *  complete.
     */
    void terminateUpload(Object id, Long timestamp)
        throws java.io.IOException;
    
    /** Returns the location of the uploaded archive.
     *  @param id the upload identifier
     *  @return the location of the archive as a String URL, or null
     *  if the specified archive id does not exist in the repository.
     */
    String getArchiveURL(Object id);
    
    /** Returns the absolute path to the location of the uploaded archive.
     *  @param id the upload identifier
     *  @return the absolute path to the location of the archive as a String , or null
     *  if the specified archive id does not exist in the repository.
     */
    String getArchiveFilePath(Object id);
    
    /** Delete the uploaded archive.
     *
     * @return true if the archive is deleted succcessfully, false otherwise
     */
    public boolean removeArchive(Object id);    
}
