/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ZipFactory.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;


import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;

import java.util.Enumeration;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;


public class ZipFactory
{
    
    
    /* Buffer to read data 
    *
    */
    private byte[] mBuffer;
    
    /* Unzip Directory 
    *
    */
    private String mDir;
    
    /**
     * This object provides utility methods to unzip files.
     * @param dir Directory to unzip 
     */
    public ZipFactory (String dir)
    {
        
        this.mDir = dir;
        this.mBuffer = new byte[8092];
        
    }
    
    /**
     * Unzips a file.
     *
     * @param zpFile File to be unzipped
     * @throws IOException if error trying to read entry.
     * @throws java.util.zip.ZipException if error trying to read zip file.
     */
    public void unZip (File zpFile) throws IOException, ZipException
    {
        
        if (!zpFile.getName ().endsWith (".zip"))
        {
            throw new ZipException ("Not a zip file? " + zpFile.getName ());
        }
        
        // process all entries in that JAR file
        ZipFile zp = new ZipFile (zpFile);
        Enumeration all = zp.entries ();
        while (all.hasMoreElements ())
        {
            getEntry (zp, ((ZipEntry) (all.nextElement ())));
        }
        
        zp.close ();
    }
    
    /**
     * Gets one file <code>entry</code> from <code>zpFile</code>.
     *
     * @param zp the ZIP file reference to retrieve <code>entry</code> from.
     * @param entry the file from the ZIP to extract.
     * @throws IOException if error trying to read entry.
     */
    private void getEntry (ZipFile zp, ZipEntry entry) throws IOException
    {
        
        String entryName = entry.getName ();
        // if a directory, mkdir it (remember to 
        // create intervening subdirectories if needed!)
        if (entryName.endsWith ("/"))
        {
            new File (mDir, entryName).mkdirs ();
            return;
        }
        
        File f = new File (mDir, entryName);
        
        if (!f.getParentFile ().exists ())
        {
            f.getParentFile ().mkdirs ();
        }
        
        // Must be a file; create output stream to the file
        FileOutputStream fostream = new FileOutputStream (f);
        InputStream istream = zp.getInputStream (entry);
        
        // extract files
        int n = 0;
        while ((n = istream.read (mBuffer)) > 0)
        {
            fostream.write (mBuffer, 0, n);
        }
        
        try
        {
            istream.close ();
            fostream.close ();
        }
        catch (IOException e)
        {
            Logger.getLogger("com.sun.jbi.management.internal.support").log(
                Level.WARNING, e.getClass().getName(), e);
        }
    }
    
    
    
}
