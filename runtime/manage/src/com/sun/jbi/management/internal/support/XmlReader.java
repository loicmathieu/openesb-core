/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)XmlReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.internal.support;

import com.sun.jbi.component.InstallationContext;
import com.sun.jbi.management.ComponentInstallationContext;
import com.sun.jbi.EnvironmentContext;

import java.io.File;
import java.io.FileInputStream;

import java.util.Vector;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import com.sun.org.apache.xpath.internal.XPathAPI;

import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Notation;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

import org.xml.sax.SAXParseException;
import org.xml.sax.InputSource;
import java.io.CharArrayReader;

/**
 * XmlReader -- read in and print out expected values from the specified
 * file (supposedly the jbi.xml file from the archive).
 *
 * @author Sun Microsystems, Inc.
 *
 */
public class XmlReader
    implements org.xml.sax.ErrorHandler, org.xml.sax.EntityResolver
{

// ============================= member variables =============================

    /**
     * The Document being parsed
     */
    private Document mDocument;

    /**
     * The DocumentFramgment representing the component (part of the spec)
     */
    private DocumentFragment mComponentFragment;

    /**
     * The DocumentFramgment representing the ID extensions (NOT part of the spec)
     */
    private DocumentFragment mIdentificationFragment;

    /**
     * Set to true when an error is found
     */
    private boolean mErrorsFound;

    /**
     * Set to true when a binding was found
     */
    private boolean mSawBinding;

    /**
     * Set to true when an engine was found
     */
    private boolean mSawEngine;

    /**
     * Set to true when a service deployment was found
     */
    private boolean mSawDeployment;

    /**
     * Set to true when a shared library (definition) was found
     */
    private boolean mSawLibrary;

    /**
     * Save the description on parsing.
     */
    private String mSavedDescription;

    /**
     * Save the ID on parsing.
     */
    private String mSavedId;

    /**
     * Save the list of shared libraries
     */
    private Vector mSavedSharedLibraryIds;

    /**
     * Save the list of life cycle class names
     */
    private Vector mSavedLifecycleClassNames;

    /**
     * Save the Lifecycle class path
     */
    private Vector mSavedLifecycleClassPath;

    /**
     * Save the Bootstrap class name
     */
    private String mSavedBootstrapClassName;

    /**
     * Save the Bootstrap class path
     */
    private Vector mSavedBootstrapClassPath;

    /**
     * Save the Bootstrap class loader delegation, if specified
     */
    private boolean mSavedBootstrapClassLoaderDelegation;

    /**
     * Save the Component class loader delegation, if specified
     */
    private boolean mSavedComponentClassLoaderDelegation;

    /**
     * Save the Shared Library class loader delegation, if specified
     */
    private boolean mSavedSharedLibraryClassLoaderDelegation;

    /**
     * A pointer to a global environment context
     */
    private EnvironmentContext mEnvironmentContext;

    /**
     * Path to the jbi.xsd file (from JBI_HOME)
     */
    private static final String PATH_TO_SCHEMA_FILE = "/schemas/jbi.xsd";

    /**
     * Path to the managementMessage.xsd file (from JBI_HOME)
     */
    private static final String PATH_TO_MM_SCHEMA_FILE = "/schemas/managementMessage.xsd";

    /**
     * Flag indicating management message validation
     */
    private boolean mIsValidateManagementMsg = false;

// ============================== constructors ==============================

    /**
     * Default constructor.
     */
    public XmlReader()
    {
        mDocument = null;
        mComponentFragment = null;
        mIdentificationFragment = null;
        mEnvironmentContext = getEnvironmentContext();
        mErrorsFound = false;
        mSawBinding = false;
        mSawEngine = false;
        mSawDeployment = false;
        mSawLibrary = false;
    }

// ============================== methods ==============================

    /**
     * Extract all information from the jbi.xml file.
     * @param aFileName the specified "jbi.xml" file
     * @param isOnlyIdNeeded true if we want to return Id (only)
     * @return the UUID of the JBI component
     * @throws Exception when unable to process a valid
     * configuration file.
     */
    public String loadAndParse(String aFileName, boolean isOnlyIdNeeded)
        throws Exception
    {
        try
        {
            File configFile = new File(aFileName);

            DocumentBuilderFactory docBuilderFactory =
                DocumentBuilderFactory.newInstance();

            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            docBuilder.setErrorHandler(this);
            mDocument = docBuilder.parse( configFile );

            if ( null == mDocument )
            {
                return null;
            }

            mSavedId = null;  // clear out any previously saved id.
            jbiParse(!isOnlyIdNeeded);

            if (!isOnlyIdNeeded)
            {
                tellWhatWeFound();
            }
            return mSavedId;
        }
        catch (java.io.FileNotFoundException nfe)
        {
            throw(nfe);
        }
        catch (org.xml.sax.SAXParseException spe)
        {
            throw(spe);
        }
        catch (Exception e)
        {
            throw(e);
        }
        // return null;
    }

    /**
     * Validate the jbi.xml file.
     * @param istr is the InputStream to the "jbi.xml" file
     * @return true iff the file passes validation
     * @throws Exception when unable to process a valid
     * configuration file.
     */
    public boolean validate(java.io.InputStream istr)
        throws Exception
    {
        try
        {
            DocumentBuilderFactory docBuilderFactory =
                DocumentBuilderFactory.newInstance();

            docBuilderFactory.setValidating(true);
            docBuilderFactory.setNamespaceAware(true);
            docBuilderFactory.setAttribute(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema");
            String jbiHome = mEnvironmentContext.getJbiInstallRoot();
            // convert any '\'s into '/'
            if ((null == jbiHome) || ("".equals(jbiHome)))
            {
                return false;
            }
            jbiHome = jbiHome.replace('\\', '/');
            String schema = jbiHome + PATH_TO_SCHEMA_FILE;
            docBuilderFactory.setAttribute(
                "http://java.sun.com/xml/jaxp/properties/schemaSource",
                schema);

            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            docBuilder.setErrorHandler(this);
            docBuilder.setEntityResolver(this);

            mDocument = docBuilder.parse( istr );
            istr.close();
            return ( null != mDocument );

        }
        catch (org.xml.sax.SAXParseException spe)
        {
            throw(spe);
        }
        catch (Exception e)
        {
            throw(e);
        }
        // return false;
    }

    /**
     * Validate the jbi.xml file.
     * @param aFileName the specified "jbi.xml" file
     * @return true iff the file passes validation
     * @throws Exception when unable to process a valid
     * configuration file.
     */
    public boolean validate(String aFileName)
        throws Exception
    {
        try
        {
            File configFile = new File(aFileName);
            java.io.InputStream config = new FileInputStream(configFile);
            return validate(config);
        }
        catch (java.io.FileNotFoundException nfe)
        {
            throw(nfe);
        }
        catch (Exception e)
        {
            throw(e);
        }
        // return false;
    }

    /**
     * Validate management message.
     * @param aMMsgStr management message string
     * @return true iff the file passes validation
     * @throws Exception when unable to process a valid
     * configuration file.
     */
    public boolean validateManagementMessage(String aMMsgStr)
        throws Exception
    {
        try
        {
            DocumentBuilderFactory docBuilderFactory =
                DocumentBuilderFactory.newInstance();

            mIsValidateManagementMsg = true;
            docBuilderFactory.setValidating(true);
            docBuilderFactory.setNamespaceAware(true);
            docBuilderFactory.setAttribute(
                "http://java.sun.com/xml/jaxp/properties/schemaLanguage",
                "http://www.w3.org/2001/XMLSchema");
            String jbiHome = mEnvironmentContext.getJbiInstallRoot();
            // convert any '\'s into '/'
            if ((null == jbiHome) || ("".equals(jbiHome)))
            {
                return false;
            }
            jbiHome = jbiHome.replace('\\', '/');
            String schema = jbiHome + PATH_TO_MM_SCHEMA_FILE;
            docBuilderFactory.setAttribute(
                "http://java.sun.com/xml/jaxp/properties/schemaSource",
                schema);

            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            docBuilder.setErrorHandler(this);
            docBuilder.setEntityResolver(this);

            InputSource isrc = new InputSource (
                    (new CharArrayReader (aMMsgStr.toCharArray ())));
            mDocument = docBuilder.parse( isrc );
            return ( null != mDocument );
        }
        catch (java.io.FileNotFoundException nfe)
        {
            throw(nfe);
        }
        catch (org.xml.sax.SAXParseException spe)
        {
            throw(spe);
        }
        catch (Exception e)
        {
            throw(e);
        }
        // return false;
    }

    /**
     * This is a callback from the XML parser used to handle errors.
     * @param aSaxException SAXParseException is the error.
     * @throws org.xml.sax.SAXException when finished logging.
     */
    public void error(SAXParseException aSaxException)
        throws org.xml.sax.SAXException
    {
        throw aSaxException;
    }

    /**
     * This is a callback from the XML parser used to handle errors.
     * @param aSaxException SAXParseException is the error.
     * @throws org.xml.sax.SAXException when finished logging.
     */
    public void fatalError(SAXParseException aSaxException)
        throws org.xml.sax.SAXException
    {
        throw aSaxException;
    }

    /**
     * The resolveEntity method resolves schema references only,  a reference
     * to the XML definition of the XML configuration file is resolved to return
     * an InputSource created for the local schema definition file. The schema definition
     * file should be present in the EntityResolvers work directory. If the referenced
     * entity is not the schema definition a null value is returned such that the XML
     * parser can apply the default entity resolution logic.
     *
     * @param publicId - The public identifier of the external entity being referenced,
     * or null if none was supplied.
     * @param systemId - The system identifier of the external entity being referenced.
     * @return An InputSource object describing the new input source, or null to request
     * that the parser open a regular URI connection to the system identifier
     *
     * @see org.xml.sax.EntityResolver
     */
    public InputSource resolveEntity(String publicId, String systemId)
    {
        InputSource mIpSrc = null;

        String jbiHome = mEnvironmentContext.getJbiInstallRoot();
        // convert any '\'s into '/'
        if ((null == jbiHome) || ("".equals(jbiHome)))
        {
            return mIpSrc;
        }
        jbiHome = jbiHome.replace('\\', '/');

        try
        {
            if(mIsValidateManagementMsg)
            {
                mIpSrc = new InputSource(new FileInputStream(
                    new File(jbiHome + PATH_TO_MM_SCHEMA_FILE) ));
            }
            else
            {
                mIpSrc = new InputSource(new FileInputStream(
                    new File(jbiHome + PATH_TO_SCHEMA_FILE) ));
            }
        }
        catch (java.io.FileNotFoundException nfe)
        {
            // just drop it and return null.
        }

        return mIpSrc;
    }

    /**
     * This is a callback from the XML parser used to handle warnings.
     * @param aSaxException SAXParseException is the warning.
     * @throws org.xml.sax.SAXException when finished logging.
     */
    public void warning(SAXParseException aSaxException)
        throws org.xml.sax.SAXException
    {
        throw aSaxException;
    }

    /**
     * display the information found.
     * @param isRegressTest true iff being run from regress test (println is ok)
     */
    void jbiParse(boolean isRegressTest)
    {
        String type = null;
        mSavedId = null;
        mSavedDescription = null;
        mSavedBootstrapClassName = null;
        mSavedBootstrapClassPath = null;
        mSavedLifecycleClassNames = null;
        mSavedLifecycleClassPath = null;
        mSavedSharedLibraryIds = null;
        mSawBinding = false;
        mSawEngine = false;
        mSawDeployment = false;
        mSawLibrary = false;
        mComponentFragment = null;
        mIdentificationFragment = null;
        mSavedBootstrapClassLoaderDelegation = false;
        mSavedComponentClassLoaderDelegation = false;
        mSavedSharedLibraryClassLoaderDelegation = false;

        try
        {
            if ( null != (strip(XPathAPI.selectNodeList(mDocument, "jbi/component") )))
            {
                mSavedId = stripString("jbi/component/identification/name");
                mSavedDescription = stripString(
                                      "jbi/component/identification/description");
                mSavedLifecycleClassNames = stripArray(
                                      "jbi/component/component-class-name");
                mSavedLifecycleClassPath = stripArray(
                                      "jbi/component/component-class-path/path-element");
                mSavedBootstrapClassName = stripString(
                                      "jbi/component/bootstrap-class-name");
                mSavedBootstrapClassPath = stripArray(
                                      "jbi/component/bootstrap-class-path/path-element");
                mSavedSharedLibraryIds = stripArray(
                                      "jbi/component/shared-library");

                NodeList componentTree = XPathAPI.selectNodeList(mDocument,
                    "jbi/component");
                if (null != componentTree.item(0))
                {
                    // skip up to the bootstrap-class-path entry

                    NodeList nl = componentTree.item(0).getChildNodes();
                    boolean foundBootstrap = false;
                    for ( int i = 0; i < nl.getLength(); i++ )
                    {
                        Node n = nl.item(i);
                        if ( n.getNodeType() == Node.ELEMENT_NODE )
                        {
                            Element elem = (Element) n;
                            String nn = elem.getTagName();
                            if (!foundBootstrap)
                            {
                                foundBootstrap = ("bootstrap-class-path".equals(nn));
                            }
                            // skip the shared-library elements
                            else if ("shared-library".equals(nn))
                            {
                                // do nothing
                            }
                            else
                            {
                                if (null == mComponentFragment)
                                {
                                    mComponentFragment =
	mDocument.createDocumentFragment();
                                }
                                mComponentFragment.appendChild(n);
                            }
                        }
                    }
                }

                // was an optional BootstrapClassLoaderDelegation specified?
                Node typeNode = componentTree.item(0).getAttributes().
                    getNamedItem("bootstrap-class-loader-delegation");
                if (null != typeNode)
                {
                    type = typeNode.getNodeValue();
                    mSavedBootstrapClassLoaderDelegation =
                        ("self-first".equals(type));
                    // reset type for later
                    type = null;
                }

                // was an optional ComponentClassLoaderDelegation specified?
                typeNode = componentTree.item(0).getAttributes().
                    getNamedItem("component-class-loader-delegation");
                if (null != typeNode)
                {
                    type = typeNode.getNodeValue();
                    mSavedComponentClassLoaderDelegation =
                        ("self-first".equals(type));
                    // reset type for later
                    type = null;
                }

                // make sure that either a Binding or Engine was specified
                typeNode = componentTree.item(0).getAttributes().
                    getNamedItem("type");
                if (null != typeNode)
                {
                    type = typeNode.getNodeValue();
                }
                if ("binding-component".equals(type))
                {
                    mSawBinding = true;
                }
                else if ("service-engine".equals(type))
                {
                    mSawEngine = true;
                }
                else
                {
                    // error
                    if (isRegressTest)
                    {
                        System.out.println("Invalid Component subtype: " + type);
                    }
                    mSavedId = null;
                }

                componentTree = XPathAPI.selectNodeList(mDocument,
                    "jbi/component/identification");
                if (null != componentTree.item(0))
                {
                    // skip name and description entries

                    NodeList nl = componentTree.item(0).getChildNodes();
                    for ( int i = 0; i < nl.getLength(); i++ )
                    {
                        Node n = nl.item(i);
                        if ( n.getNodeType() == Node.ELEMENT_NODE )
                        {
                            Element elem = (Element) n;
                            String nn = elem.getTagName();
                            if ("name".equals(nn))
                            {
                                // skip it
                            }
                            else if ("description".equals(nn))
                            {
                                // skip that one too
                            }
                            else
                            {
                                if (null == mIdentificationFragment)
                                {
                                    mIdentificationFragment =
	mDocument.createDocumentFragment();
                                }
                                mIdentificationFragment.appendChild(n);
                            }
                        }
                    }
                }
            }

            else if ( null != (strip(XPathAPI.selectNodeList(mDocument, "jbi/service-assembly") )))
            {
                mSawDeployment = true;
                mSavedId = stripString(
                              "jbi/service-assembly/identification/component-name");
                mSavedDescription = stripString(
                              "jbi/service-assembly/identification/description");
                NodeList componentTree = XPathAPI.selectNodeList(mDocument,
                    "jbi/service-assembly/identification");
                if (null != componentTree.item(0))
                {
                    // skip name and description entries

                    NodeList nl = componentTree.item(0).getChildNodes();
                    for ( int i = 0; i < nl.getLength(); i++ )
                    {
                        Node n = nl.item(i);
                        if ( n.getNodeType() == Node.ELEMENT_NODE )
                        {
                            Element elem = (Element) n;
                            String nn = elem.getTagName();
                            if ("name".equals(nn))
                            {
                                // skip it
                            }
                            else if ("description".equals(nn))
                            {
                                // skip that one too
                            }
                            else
                            {
                                if (null == mIdentificationFragment)
                                {
                                    mIdentificationFragment =
	mDocument.createDocumentFragment();
                                }
                                mIdentificationFragment.appendChild(n);
                            }
                        }
                    }
                }
            }
            else if ( null != (strip(XPathAPI.selectNodeList(mDocument, "jbi/shared-library") )))
            {
                mSawLibrary = true;
                mSavedId = stripString("jbi/shared-library/identification/name");
                mSavedDescription = stripString(
                              "jbi/shared-library/identification/description");
                mSavedLifecycleClassPath = stripArray(
                             "jbi/shared-library/shared-library-class-path/path-element");
                // was an optional ComponentClassLoaderDelegation specified?
                NodeList componentTree = XPathAPI.selectNodeList(mDocument,
                    "jbi/shared-library");
                Node typeNode = componentTree.item(0).getAttributes().
                    getNamedItem("class-loader-delegation");
                if (null != typeNode)
                {
                    type = typeNode.getNodeValue();
                    mSavedSharedLibraryClassLoaderDelegation =
                        ("self-first".equals(type));
                }
                componentTree = XPathAPI.selectNodeList(mDocument,
                    "jbi/shared-library/identification");
                if (null != componentTree.item(0))
                {
                    // skip name and description entries

                    NodeList nl = componentTree.item(0).getChildNodes();
                    for ( int i = 0; i < nl.getLength(); i++ )
                    {
                        Node n = nl.item(i);
                        if ( n.getNodeType() == Node.ELEMENT_NODE )
                        {
                            Element elem = (Element) n;
                            String nn = elem.getTagName();
                            if ("name".equals(nn))
                            {
                                // skip it
                            }
                            else if ("description".equals(nn))
                            {
                                // skip that one too
                            }
                            else
                            {
                                if (null == mIdentificationFragment)
                                {
                                    mIdentificationFragment =
	mDocument.createDocumentFragment();
                                }
                                mIdentificationFragment.appendChild(n);
                            }
                        }
                    }
                }
            }
            else
            {
                // error
                if (isRegressTest)
                {
                    System.out.println("Invalid Component type.");
                }
            }
        }
        catch (javax.xml.transform.TransformerException te)
        {
            if (isRegressTest)
            {
                System.out.println("Unexpected Exception: " + te.toString());
            }
            // throw(te);
        }
        catch (Throwable t)
        {
            if (isRegressTest)
            {
                System.out.println("Unexpected Exception: " + t.getClass().getName());
                t.printStackTrace();
            }
        }

    }

    /**
     * display the information found.
     *
     * Note: This routine is *not* called from anywhere that records to the
     * server.log file (it's only used by regression test manage00002), so the
     * println messages in this routine are known to not be I18N'd.
     */
    void tellWhatWeFound()
    {
        int totalMatches = 0;
        int matched;

        String type = null;
        try
        {
            if ( null != (strip(XPathAPI.selectNodeList(mDocument, "jbi/component") )))
            {
                NodeList componentTree = XPathAPI.selectNodeList(mDocument,
                           "jbi/component");
                Node typeNode = componentTree.item(0).getAttributes().
                    getNamedItem("type");
                if (null != typeNode)
                {
                    type = typeNode.getNodeValue();
                }
            }
            else if ( null != (strip(XPathAPI.selectNodeList(mDocument, "jbi/service-assembly") )))
            {
                type = "service-assembly";
            }
            else if ( null != (strip(XPathAPI.selectNodeList(mDocument, "jbi/shared-library") )))
            {
                type = "shared-library";
            }
            else
            {
                type = "invalid";
            }
        }
        catch (javax.xml.transform.TransformerException te)
        {
            System.out.println("Unexpected Exception: " + te.toString());
            // throw(te);
        }

        if (mSawEngine || mSawBinding)
        {
            System.out.println("JBI Type:" + type);
            System.out.println("COMPONENT CLASS LOADER DELEGATION: " +
                delegationToString(mSavedComponentClassLoaderDelegation));
            System.out.println("BOOTSTRAP CLASS LOADER DELEGATION: " +
                delegationToString(mSavedBootstrapClassLoaderDelegation));
            System.out.println("ID: " + mSavedId);
            System.out.println("DESCRIPTION: " + getDescription());
            System.out.println("COMPONENT CLASS: " + mSavedLifecycleClassNames);
            System.out.println("COMPONENT CLASSPATH: " + mSavedLifecycleClassPath);
            System.out.println("BOOTSTRAP CLASS: " + mSavedBootstrapClassName);
            System.out.println("BOOTSTRAP CLASSPATH: " + mSavedBootstrapClassPath);
            System.out.println("SHARED LIBRARY ID:" + mSavedSharedLibraryIds);
            if (null != mComponentFragment)
            {
                System.out.println("COMPONENT DOCUMENT FRAGMENT:" +
                    documentFragmentToString(mComponentFragment));
            }
            if (null != mIdentificationFragment)
            {
                System.out.println("IDENTIFICATION DOCUMENT FRAGMENT:" +
                    documentFragmentToString(mIdentificationFragment));
            }
        }
        else if (mSawDeployment)
        {
            System.out.println("JBI Type:" + type);
            System.out.println("ID: " + mSavedId);
            System.out.println("DESCRIPTION: " + getDescription());
            if (null != mIdentificationFragment)
            {
                System.out.println("IDENTIFICATION DOCUMENT FRAGMENT:" +
                    documentFragmentToString(mIdentificationFragment));
            }
        }
        else if (mSawLibrary)
        {
            System.out.println("JBI Type:" + type);
            System.out.println("CLASS LOADER DELEGATION: " +
                delegationToString(mSavedSharedLibraryClassLoaderDelegation));
            System.out.println("ID: " + mSavedId);
            System.out.println("DESCRIPTION: " + getDescription());
            System.out.println("COMPONENT CLASSPATH: " + mSavedLifecycleClassPath);
            if (null != mIdentificationFragment)
            {
                System.out.println("IDENTIFICATION DOCUMENT FRAGMENT:" +
                    documentFragmentToString(mIdentificationFragment));
            }
        }
        else
        {
            System.out.println("VALIDATION ERROR.");
        }
    }

    /**
     * Function to specify if the parsed component was a binding.
     * @return true iff the last component parsed was a binding
     */
    public boolean isBinding()
    {
        return mSawBinding;
    }

    /**
     * Function to specify if the parsed component was an engine.
     * @return true iff the last component parsed was an engine
     */
    public boolean isEngine()
    {
        return mSawEngine;
    }

    /**
     * Function to specify if the parsed component was a deployment.
     * @return true iff the last component parsed was a deployment
     */
    public boolean isDeployment()
    {
        return mSawDeployment;
    }

    /**
     * Function to specify if the parsed component was a shared library.
     * @return true iff the last component parsed was a shared library
     */
    public boolean isSharedLibrary()
    {
        return mSawLibrary;
    }

    /**
     * Function to return the last description parsed.
     * @return the last description we saw
     */
    public String getDescription()
    {
        return mSavedDescription;
    }

    /**
     * Function to return the last component ID parsed.
     * @return the last component ID we saw
     */
    public String getId()
    {
        return mSavedId;
    }

    /**
     * Function to return the last bootstrap class parsed.
     * @return the bootstrap class name
     */
    public String getBootstrapClassName()
    {
        return mSavedBootstrapClassName;
    }

    /**
     * Function to return the last bootstrap classpath parsed.
     * @return the bootstrap class path
     */
    public Vector getBootstrapClassPath()
    {
        return mSavedBootstrapClassPath;
    }

    /**
     * Function to return the last lifecycle classes parsed.
     * @return the lifecycle classes names
     */
    public Vector getLifecycleClassNames()
    {
        return mSavedLifecycleClassNames;
    }

    /**
     * Function to return the last lifecycle classpath parsed.
     * @return the lifecycle class path
     */
    public Vector getLifecycleClassPath()
    {
        return mSavedLifecycleClassPath;
    }

    /**
     * Function to return the last list of shared library ids parsed.
     * @return the shared Library Ids list
     */
    public Vector getSharedLibraryIds()
    {
        return mSavedSharedLibraryIds;
    }

    /**
     * Function to return the installation context for the component.
     * @return the installation context for the component.
     */
    public ComponentInstallationContext getInstallationContext()
    {
        String className = null;
        if (null != mSavedLifecycleClassNames && mSavedLifecycleClassNames.size() > 0)
        {
            className = (String) mSavedLifecycleClassNames.get(0);
        }
        ComponentInstallationContext ic = new ComponentInstallationContext(mSavedId,
            (mSawBinding ? InstallationContext.BINDING : InstallationContext.ENGINE),
            className,
            mSavedLifecycleClassPath,
            mComponentFragment);
        ic.setDescription(mSavedDescription);
        if (isAlternateBootstrapClassLoaderDelegation())
        {
            ic.setBootstrapClassLoaderSelfFirst();
        }
        if (isAlternateComponentClassLoaderDelegation())
        {
            ic.setComponentClassLoaderSelfFirst();
        }
        return ic;
    }

    /**
     * Function to specify if the parsed component had an alternate bootstrap
     * class loader delegation.
     * @return true iff the last component parsed had an alternate bootstrap
     * class loader delegation.
     */
    public boolean isAlternateBootstrapClassLoaderDelegation()
    {
        return mSavedBootstrapClassLoaderDelegation;
    }

    /**
     * Function to specify if the parsed component had an alternate component
     * class loader delegation.
     * @return true iff the last component parsed had an alternate component
     * class loader delegation.
     */
    public boolean isAlternateComponentClassLoaderDelegation()
    {
        return mSavedComponentClassLoaderDelegation;
    }

    /**
     * Function to specify if the parsed shared library had an alternate
     * class loader delegation.
     * @return true iff the last shared library parsed had an alternate
     * class loader delegation.
     */
    public boolean isAlternateSharedLibraryClassLoaderDelegation()
    {
        return mSavedSharedLibraryClassLoaderDelegation;
    }

    /**
     * Specify the global environment context.
     * @param aContext the global environment context
     */
    public void setEnvironmentContext(EnvironmentContext aContext)
    {
        mEnvironmentContext = aContext;
    }

    /**
     * Return the XML Document.
     * @return the document as a DOM tree
     */
    public Document getXmlDocument()
    {
        return mDocument;
    }

    /**
     * Return the XML Document Fragment from the component section.
     * @return the document fragment from the component section.
     */
    public DocumentFragment getComponentDocumentFragment()
    {
        return mComponentFragment;
    }

    /**
     * Return the XML Document Fragment from the identification section.
     * @return the document fragment from the identification section.
     */
    public DocumentFragment getIdentificationDocumentFragment()
    {
        return mIdentificationFragment;
    }

    /**
     * Convert the given node list to a Vector of strings.
     * @param aNodeList the list of nodes we want
     * @return the node list as a vector
     */
    Vector stripArray(NodeList aNodeList)
    {
        Vector elements = new Vector();
        for (int j = 0; j < aNodeList.getLength(); ++j)
        {
            Text text = (Text) aNodeList.item(j).getFirstChild();
            if (null == text)
            {
                continue;
            }
            String theData = text.getData();
            if (null != theData)
            {
                elements.addElement(theData.trim());
            }
        }
        return elements;
    }

    /**
     * Convert the given node list to a single string.
     * @param aNodeList the list of nodes (should be 1) we want
     * @return the element as a string
     */
    String strip(NodeList aNodeList)
    {
        if (aNodeList == null)
        {
            /* throw a parse error */
            return null;
        }
        if (aNodeList.getLength() == 1)
        {
            Text text = (Text) aNodeList.item(0).getFirstChild();
            if (null == text)
            {
                return null;
            }
            String theData = text.getData();
            if (null == theData)
            {
                return null;
            }
            else
            {
                return theData.trim();
            }
        }
        return null;
    }

    /**
     * Find the specified Xpath element, and return it as a string.
     * @param anXpath the path to the element we want
     * @return the element as a string
     * @throws javax.xml.transform.TransformerException unexpectedly.
     */
    String stripString(String anXpath)
        throws javax.xml.transform.TransformerException
    {
        return strip( XPathAPI.selectNodeList(mDocument, anXpath) );
    }

    /**
     * Find the specified Xpath element, and return it as an array.
     * @param anXpath the path to the element we want
     * @return the element as an array
     * @throws javax.xml.transform.TransformerException unexpectedly.
     */
    Vector stripArray(String anXpath)
        throws javax.xml.transform.TransformerException
    {
        return stripArray( XPathAPI.selectNodeList(mDocument, anXpath) );
    }

    /**
     * Present the class loader delegation as "parent-first" or "self-first"
     * @param aDelegation true if self-first, false if parent first.
     * @return the delegation as "parent-first" or "self-first".
     */
    private String delegationToString(boolean aDelegation)
    {
        if (aDelegation)
        {
            return "self-first";
        }
        else
        {
            return "parent-first";
        }
    }

    /**
     * Present a Document Fragment representing extension elements as a String.
     * @param aDocumentFragment the optional extension elements.
     * @return the String representation of the DocumentFragment.
     */
    private String documentFragmentToString(DocumentFragment aDocumentFragment)
    {
        return childrenAsString(aDocumentFragment);
    }

    /**
     * Present a Node as a String.
     * @param aNode the node to pretty print.
     * @return the String representation of the pretty-printed Node.
     */
    private String nodeToString(Node aNode)
    {
        short type = aNode.getNodeType();
        switch ( type )
        {
            case Node.DOCUMENT_NODE:
                return documentToString( (Document) aNode);

            case Node.DOCUMENT_FRAGMENT_NODE:
                return documentFragmentToString( (DocumentFragment) aNode);

            case Node.DOCUMENT_TYPE_NODE:
                return documentTypeToString( (DocumentType) aNode);

            case Node.ELEMENT_NODE:
                return elementToString( (Element) aNode);

            case Node.ATTRIBUTE_NODE:
                return attributeToString( (Attr) aNode);

            case Node.ENTITY_REFERENCE_NODE:
                // return entityReferenceToString( (EntityReference) aNode);
            break;

            case Node.ENTITY_NODE:
                // return entityToString( (Entity) aNode);
            break;

            case Node.NOTATION_NODE:
                return notationToString( (Notation) aNode);

            case Node.PROCESSING_INSTRUCTION_NODE:
                return procInstToString( (ProcessingInstruction) aNode);

            case Node.TEXT_NODE:
                return textToString( (Text) aNode);

            case Node.COMMENT_NODE:
                return commentToString( (Comment) aNode);

            case Node.CDATA_SECTION_NODE:
                return cDataToString( (CDATASection) aNode);
        }
        return null;
    }

    /**
     * Pretty-print the children of a Node as a String.
     * @param aNode the node to pretty print.
     * @return the String representation of the pretty-printed Node.
     */
    private String prettyPrintChildren(Node node)
    {
        StringBuffer sb = new StringBuffer();
        NodeList l = node.getChildNodes();
        int size = l.getLength();
        for ( int i = 0; i < size; i++ )
        {
            Node n = l.item(i);
            if ( n.getNodeType() == Node.TEXT_NODE )
            {
                Text t = (Text)n;
                sb.append("CHILD #" + i);
                sb.append(t.getData() + "\n");
            }
            else
            {
                sb.append("CHILD #" + i + "is not a Text node.\n");
                sb.append(nodeToString(n));
            }
        }
        return sb.toString();
   }

    /**
     * Return the children of a Node as a String.
     * @param aNode the node to pretty print.
     * @return the String representation of the children of the Node.
     */
    private String childrenAsString(Node aNode)
    {
        StringBuffer sb = new StringBuffer();
        NodeList l = aNode.getChildNodes();
        int size = l.getLength();
        for ( int i = 0; i < size; i++ )
        {
            Node n = l.item(i);
            sb.append(nodeToString(n));
        }
        return sb.toString();
    }

    /**
     * Present a Document representing extension elements as a String.
     * @param aDocument a Document Node.
     * @return the String representation of the Document.
     */
    private String documentToString(Document aDocument)
    {
        return childrenAsString(aDocument);
    }

    /**
     * Present a DocumentType as a String.
     * @param aDocumentType a DocumentType Node.
     * @return the String representation of the DocumentType.
     */
    private String documentTypeToString(DocumentType docType)
    {
        StringBuffer sb = new StringBuffer();
        sb.append("<!DOCTYPE " + docType.getName());
        String pubID = docType.getPublicId();
        String sysID = docType.getSystemId();
        if ( pubID != null )
        {
            sb.append(" PUBLIC " + pubID);
            if ( sysID != null )
            {
                sb.append(" " + sysID);
            }
        }
        else if ( sysID != null )
        {
            sb.append(" SYSTEM " + sysID);
        }
        String is = docType.getInternalSubset();
        if ( is != null )
        {
            sb.append(" [" + is + "]");
        }
        sb.append("\n");
        return sb.toString();
    }

    /**
     * Present an Element as a String.
     * @param aDocument a Element Node.
     * @return the String representation of the Element.
     */
    private String elementToString(Element elem)
    {
        StringBuffer sb = new StringBuffer();
        String n = elem.getTagName();
        sb.append("<" + n);

        NamedNodeMap a = elem.getAttributes();
        for ( int i = 0; i < a.getLength(); i++ )
        {
            Attr att = (Attr)a.item(i);
            sb.append(" ");
            sb.append(nodeToString(att));
        }

        if ( elem.hasChildNodes() )
        {
            sb.append(">" + childrenAsString(elem));
            sb.append("</" + n +">");
        }
        else
        {
            sb.append("/>");
        }
        return sb.toString();
    }

    /**
     * Present an Attribute as a String.
     * @param anAttribute an Attribute Node.
     * @return the String representation of the Attribute.
     */
    private String attributeToString(Attr anAttribute)
    {
        return anAttribute.getName() + "=\"" + anAttribute.getValue() + "\"";
    }

    /**
     * Present a Notation as a String.
     * @param notation a Notation Node.
     * @return the String representation of the Notation.
     */
    private String notationToString(Notation notation)
    {
        StringBuffer sb = new StringBuffer();
        sb.append("<!NOTATION " + notation.getNodeName());
        String pubID = notation.getPublicId();
        String sysID = notation.getSystemId();
        if ( pubID != null )
        {
            sb.append(" PUBLIC " + pubID);
            if ( sysID != null )
            {
                sb.append(" " + sysID);
            }
        }
        else if ( sysID != null )
        {
            sb.append(" SYSTEM " + sysID);
        }
        sb.append("\n");
        return sb.toString();
    }

    /**
     * Present a ProcessingInstruction as a String.
     * @param pi a ProcessingInstruction Node.
     * @return the String representation of the ProcessingInstruction.
     */
    private String procInstToString(ProcessingInstruction pi)
    {
        return "<?" + pi.getTarget() + " " + pi.getData() + "?>\n";
    }

    /**
     * Present a Text Node as a String.
     * @param text the Text Node.
     * @return the String representation of the Text node.
     */
    private String textToString(Text text)
    {
        return text.getData();
    }

    /**
     * Present a cDataSection Node as a String.
     * @param cdata the cDataSection Node.
     * @return the String representation of the cDataSection node.
     */
    private String cDataToString(CDATASection cdata)
    {
        return "<![CDATA[" + cdata.getData() + "]]>\n";
    }

    /**
     * Present a comment Node as a String.
     * @param comment the Comment Node.
     * @return the String representation of the Comment node.
     */
    private String commentToString(Comment comment)
    {
        return "<!--" + comment.getData() + "-->\n";
    }
    
    /**
     * Get the Environment Context
     */
    private EnvironmentContext getEnvironmentContext()
    {
        if ( mEnvironmentContext == null )
        {
            mEnvironmentContext = com.sun.jbi.util.EnvironmentAccess.getContext();
        }
        return mEnvironmentContext;
        
    }
}
