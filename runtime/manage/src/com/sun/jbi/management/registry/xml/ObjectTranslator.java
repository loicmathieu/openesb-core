/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ObjectTranslator.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  ObjectTranslator.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on August 23, 2006, 5:17 PM
 */

package com.sun.jbi.management.registry.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.ServiceUnitInfo;
import com.sun.jbi.ServiceUnitState;
import com.sun.jbi.platform.PlatformContext;
import javax.xml.bind.JAXBException;


import com.sun.jbi.management.registry.RegistryException;
import com.sun.jbi.management.registry.data.ComponentInfoImpl;
import com.sun.jbi.management.registry.data.ServiceUnitInfoImpl;
import com.sun.jbi.management.descriptor.ComponentDescriptor;
import com.sun.jbi.management.descriptor.SharedLibraryDescriptor;
import com.sun.jbi.management.util.PropertyFilter;
import com.sun.jbi.management.util.StringHelper;

/**
 * Helper class to translate from JAXB objects to Registry objects.
 *
 * @author Sun Microsystems, Inc.
 */
public class ObjectTranslator
{
    private static ObjectFactory sObjectFactory;
    private static String        NA = "NA";
    
    static {
        
        sObjectFactory = new ObjectFactory();
    }
    
    public static SharedLibraryRefType 
    getJaxbSharedLibraryRef(ComponentInfo slInfo)
        throws JAXBException
    {
        SharedLibraryRefType slRef = sObjectFactory.createSharedLibraryRefType();
        slRef.setNameRef(StringHelper.trim(slInfo.getName()));
        slRef.setInstallRoot(getRelativePath(StringHelper.trim(slInfo.getInstallRoot())));
        return slRef;
    }
   
    public static ServiceAssemblyRefType 
    getJaxbServiceAssemblyRef(String saName)
        throws JAXBException
    {
        ServiceAssemblyRefType saRef = sObjectFactory.createServiceAssemblyRefType();
        saRef.setNameRef(StringHelper.trim(saName));
        return saRef;
    }
    public static ComponentRefType 
    getJaxbComponentRef(ComponentInfo compInfo) 
        throws JAXBException
    {
        ComponentRefType compRef = sObjectFactory.createComponentRefType();

        compRef.setNameRef(StringHelper.trim(compInfo.getName()));
        compRef.setInstallRoot(getRelativePath(StringHelper.trim(compInfo.getInstallRoot())));
        compRef.setServiceUnits(getJaxbServiceUnitListType(compInfo.getServiceUnitList()));
        compRef.setState(LifeCycleStatusEnum.fromValue(compInfo.getStatus().toString()));
        compRef.setWorkspace(getRelativePath(StringHelper.trim(compInfo.getWorkspaceRoot())));
        updateJaxbComponentPropertyList(compInfo.getProperties(), compRef.getProperty());
        return compRef;
    }
    
    public static ServiceUnitListType
    getJaxbServiceUnitListType(List<ServiceUnitInfo> suInfoList)
        throws JAXBException
    {
        ServiceUnitListType suListType = sObjectFactory.createServiceUnitListType();
        
        for ( ServiceUnitInfo suInfo : suInfoList  )
        {
            ServiceUnitType suType = getJaxbServiceUnitType(suInfo);
            suListType.getServiceUnit().add(suType);
        }
        return suListType;
    }

    public static ServiceUnitType 
    getJaxbServiceUnitType(ServiceUnitInfo suInfo)
        throws JAXBException
    {
        ServiceUnitType suType = sObjectFactory.createServiceUnitType();

        suType.setName(StringHelper.trim(suInfo.getName()));
        suType.setServiceAssemblyRef(StringHelper.trim(suInfo.getServiceAssemblyName()));
        suType.setState(LifeCycleStatusEnum.fromValue(suInfo.getState().toString()));
        
        return suType;
    }
    
    public static void 
    updateJaxbComponentPropertyList(Map compProps, List<PropertyType> list)
        throws JAXBException
    {
        java.util.Iterator itr = compProps.keySet().iterator();
        while( itr.hasNext() )
        {
            PropertyType pt = sObjectFactory.createPropertyType();
            
            String key = (String) itr.next();
            pt.setName(StringHelper.trim(key));
            pt.setValue( StringHelper.trim((String) compProps.get(key)));
            
            list.add(pt);
        }
    }
    
    /*---------------------------------------------------------------------------------*\
     *          Operations to convert JAXB Objects to the Registry data objects        *
    \*---------------------------------------------------------------------------------*/
    
    public static ComponentInfo
    getRegistryComponent(ComponentRefType compRef, GenericQueryImpl genQuery)
        throws JAXBException, RegistryException
    {
        ComponentInfoImpl compInfo = null;
        
        
        if ( compRef != null )
        {
            String compName = StringHelper.trim(compRef.getNameRef());
            String compInstallRoot = StringHelper.trim(compRef.getInstallRoot());
            compInfo = new ComponentInfoImpl();

            compInfo.setName(compName);
            compInfo.setInstallRoot(getAbsolutePath(compInstallRoot));
            compInfo.setStatus(ComponentState.valueOfString(StringHelper.trim(compRef.getState().value())));
            compInfo.setWorkspaceRoot(getAbsolutePath(StringHelper.trim(compRef.getWorkspace())));
            compInfo.setTimestamp(genQuery.getComponentTimestamp(compName));
            compInfo.setUpgradeNumber(genQuery.getComponentUpgradeNumber(compName).intValue());
            compInfo.setServiceUnitList( getRegistryServiceUnitListType(
                compRef.getServiceUnits(), StringHelper.trim(compRef.getNameRef())) );
            updateRegistryComponentPropertyList(compInfo.getProperties(), 
                compRef.getProperty());
            updateRegistryComponentConfigInfo(compInfo, compRef.getComponentConfig());
            compInfo = updateComponentWithDescriptorData(compInfo, genQuery);
            
        }
        return compInfo;
    }
    
    /**
     * This operation is called for target="domain"
     */
    public static ComponentInfo
    getRegistryComponent(String componentName, GenericQueryImpl genQuery)
        throws JAXBException, RegistryException
    {
        ComponentInfoImpl compInfo = null;
        
        if ( componentName != null )
        {
            compInfo = new ComponentInfoImpl();

            compInfo.setName(componentName);
            compInfo.setInstallRoot(NA);
            compInfo.setStatus(ComponentState.UNKNOWN);
            compInfo.setWorkspaceRoot(NA);
            compInfo.setTimestamp(genQuery.getComponentTimestamp(componentName));
            compInfo.setUpgradeNumber(genQuery.getComponentUpgradeNumber(componentName).intValue());
            compInfo.setServiceUnitList(new ArrayList());

            compInfo = updateComponentWithDescriptorData(compInfo, genQuery);
        }
        return compInfo;
    }
    
    public static ComponentInfo
    getRegistrySharedLibrary(GenericQueryImpl genQuery, SharedLibraryRefType slRef)
        throws RegistryException
    {
        ComponentInfoImpl slInfo = null;
        
        
        if ( slRef != null )
        {
            String slName = slRef.getNameRef();
            
            slInfo = new ComponentInfoImpl();

            slInfo.setName(slName);
            slInfo.setInstallRoot(getAbsolutePath(slRef.getInstallRoot()));
            slInfo.setStatus(ComponentState.SHUTDOWN);
            slInfo.setComponentType(ComponentType.SHARED_LIBRARY);
            slInfo.setTimestamp(genQuery.getComponentTimestamp(slName));
            slInfo = updateSharedLibraryWithDescriptorData(slInfo, genQuery);
        }
        return slInfo;
    }
    
    
    public static ComponentInfo
    getRegistrySharedLibrary(GenericQueryImpl genQuery, String slName)
        throws RegistryException
    {
        ComponentInfoImpl slInfo = new ComponentInfoImpl();
        
        slInfo.setName(slName);
        slInfo.setInstallRoot(NA);
        slInfo.setStatus(ComponentState.SHUTDOWN);
        slInfo.setComponentType(ComponentType.SHARED_LIBRARY);
        slInfo.setTimestamp(genQuery.getComponentTimestamp(slName));        
        slInfo = updateSharedLibraryWithDescriptorData(slInfo, genQuery);
        return slInfo;
    }

    private static List<String>
    getClassPathList(String installRoot, List<String> classPathList)
    {
        List<String> absCp = new ArrayList();
        
        for ( String classpath : classPathList )
        {
            absCp.add( installRoot + java.io.File.separator + classpath );
        }
        
        return absCp;
    }
    
    private static List<ServiceUnitInfo>
    getRegistryServiceUnitListType(ServiceUnitListType suList, String targetComponent)
    {
        List suInfoList = new java.util.ArrayList();
         
        if ( suList != null )
        {
            List<ServiceUnitType> sus = suList.getServiceUnit();
            
            for ( ServiceUnitType suType : sus  )
            {
                ServiceUnitInfo suInfo = getRegistryServiceUnitType(suType, targetComponent);
                suInfoList.add(suInfo);
            }
        }
        return suInfoList;
    }
    
    
    private static void
    updateRegistryComponentPropertyList(Map compProps, List<PropertyType> list)
        throws JAXBException
    {
        for ( PropertyType pt : list )
        {
            compProps.put(pt.getName(), pt.getValue());
        }
    }
    
    /**
     * Populate the Component Info with all the configuration information ( i.e. 
     * all = static config, env vars and named configs )
     */
    private static void updateRegistryComponentConfigInfo(ComponentInfoImpl compInfo, 
        ComponentConfigType compCfg)
    {
        if ( compCfg != null )
        {
            // Populate the static configuration
            List<PropertyType> regProps = compCfg.getProperty();
            java.util.Properties props = new java.util.Properties();

            for ( PropertyType regProp : regProps )
            {
                props.put(regProp.getName(), regProp.getValue());
            }
            compInfo.setConfiguration(props);

            // Populate the application variables
            List<AppVariableType> appVars = compCfg.getApplicationVariable();
            com.sun.jbi.management.ComponentInfo.Variable[] vars = new 
                com.sun.jbi.management.ComponentInfo.Variable[appVars.size()];
            
            int i = 0;
            for ( AppVariableType appVar : appVars )
            {
                vars[i++] = new com.sun.jbi.management.ComponentInfo.Variable(
                    appVar.getName(), appVar.getValue(), appVar.getType());
            }
            compInfo.setVariables(vars);
            
            // populate the named configurations
            List<AppConfigType> appConfigs = compCfg.getApplicationConfiguration();
            for ( AppConfigType appConfig : appConfigs )
            {
                List<PropertyType> cfgProps = appConfig.getProperty();
                
                java.util.Properties appCfgProps = new java.util.Properties();

                for ( PropertyType cfgrop : cfgProps )
                {
                    appCfgProps.put(cfgrop.getName(), cfgrop.getValue());
                }
                compInfo.setApplicationConfiguration(
                    appCfgProps.getProperty(
                        com.sun.jbi.management.registry.Registry.APP_CONFIG_NAME_KEY), appCfgProps);
            }
        }
    }
    
    private static ServiceUnitInfo
    getRegistryServiceUnitType(ServiceUnitType suType, String targetComponent)
    {
        ServiceUnitInfoImpl suInfo = new ServiceUnitInfoImpl();

        suInfo.setName(StringHelper.trim(suType.getName()));
        suInfo.setTargetComponent(targetComponent);
        suInfo.setServiceAssemblyName(StringHelper.trim(suType.getServiceAssemblyRef()));
        suInfo.setState(ServiceUnitState.valueOfString(StringHelper.trim(suType.getState().value())));
        
        return suInfo;
    }
    
    /**
     * Get the Component data from jbi.xml and update the registry component
     * information
     */
    private static ComponentInfoImpl updateComponentWithDescriptorData(ComponentInfoImpl compInfo, 
        GenericQueryImpl genQuery)
            throws RegistryException
    {
        String compName        = compInfo.getName();
        String compInstallRoot = StringHelper.trim(compInfo.getInstallRoot());
        String compRoot = "";
        if ( !NA.equals(compInstallRoot) )
        {
            compRoot = getAbsolutePath(compInstallRoot);
        }
        
        
        // -- Get the data from the components jbi.xml
        compInfo.setInstallationDescriptor(genQuery.getComponentInstallationDescriptor(compName));
        com.sun.jbi.management.descriptor.Jbi jbi = genQuery.getComponentJbi(compName);

        if ( jbi != null )
        {
            ComponentDescriptor descr = new ComponentDescriptor(jbi);

            String compClass = descr.getComponentClassName();
            String bootClass = descr.getBootstrapClassName();
            compInfo.setComponentClassName(compClass);
            compInfo.setBootstrapClassName(bootClass);

            compInfo.setComponentType(descr.getComponentType());
            compInfo.setDescription(descr.getDescription());

            
            compInfo.setClassLoaderSelfFirst(descr.isComponentClassLoaderSelfFirst());
            compInfo.setBootstrapClassLoaderSelfFirst(descr.isBootstrapClassLoaderSelfFirst());
            
            // -- Get the shared libraries
            compInfo.setSharedLibraryNames(descr.getSharedLibraryIds());

            compInfo.setClassPathElements( 
                getClassPathList(compRoot, descr.getComponentClassPathElements()));
            compInfo.setBootstrapClassPathElements( 
                getClassPathList(compRoot, descr.getBootstrapClassPathElements()));
        }
        return compInfo;
    }
    
    private static ComponentInfoImpl updateSharedLibraryWithDescriptorData(ComponentInfoImpl slInfo, 
        GenericQueryImpl  genQuery) throws RegistryException
    {
        String slName = slInfo.getName();
        String slInstallRoot = StringHelper.trim(slInfo.getInstallRoot());
        String slRoot = "";
        if ( !NA.equals(slInstallRoot) )
        {
            slRoot = getAbsolutePath(slInstallRoot);
        }

        // -- jbi.xml content
        slInfo.setInstallationDescriptor(genQuery.getSharedLibraryInstallationDescriptor(slName));
        com.sun.jbi.management.descriptor.Jbi jbi = genQuery.getSharedLibraryJbi(slName);
        
        if ( jbi != null )
        {
            SharedLibraryDescriptor descr = new SharedLibraryDescriptor(jbi); 

            slInfo.setDescription(descr.getDescription());
            slInfo.setClassLoaderSelfFirst(descr.isSharedLibraryClassLoaderSelfFirst());
            // .. classpath .. InstallRoot + classpath entry in jbi.xml
            slInfo.setClassPathElements( 
                getClassPathList(slRoot, descr.getSharedLibraryClassPathElements()));
        }
        
        return slInfo;
    }
    
    /**
     * This operation parses the path string and replaces any occurences of
     * the value of a system property.
     */
    private static String getRelativePath(String path)
    {
       String resStr = 
            PropertyFilter.replacePropertyValues(path, PlatformContext.INSTANCE_ROOT_TOKEN);       
       resStr = 
            PropertyFilter.replacePropertyValues(resStr, PlatformContext.INSTALL_ROOT_TOKEN);    
       return resStr;
    }
    
    /**
     * This operation parses the path string and replaces any occurences of
     * a system  property with it's actual value.
     */
    private static String getAbsolutePath(String path)
    {
        return PropertyFilter.filterProperties(path);  
    }    
}
