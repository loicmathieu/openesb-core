/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)LoggingService.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.system;

import javax.management.ObjectName;
import com.sun.jbi.management.MBeanNames;
import com.sun.jbi.management.support.JbiNameInfo;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.StringTranslator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Logger;

/**
 * This is the JBI Framework Logging Service, which manages the loggers
 * for all components and system services.
 *
 * @author Sun Microsystems, Inc.
 */
public class LoggingService extends ModelSystemService
    implements LoggingServiceMBean
{
    /** our immutable name: */
    private final JbiNameInfo mJbiNameInfo =
            new JbiNameInfo("LoggingService");

    /**
     * Management context
     */
    private ManagementContext mContext = null;

    /**
     * Management Message Object
     */
    private BuildManagementMessageImpl mMMImpl = null;

    /**
     * Constructs a LoggingService.
     * @param anEnv is the ManagementContext.
     */
    public LoggingService(ManagementContext anEnv)
    {
        mContext = anEnv;

        /*
         * Local initialization of this service.
         * Local routine is responsible for calling super.initModelSystemService(..).
         */
        initModelSystemService(anEnv.getEnvironmentContext());

        mMMImpl = mContext.getManagementMessageObject();
    }

    /** local model init - called by constructor - create custom mbeans. */
    protected void initModelSystemService(com.sun.jbi.EnvironmentContext anEnv)
    {
        String loggerName = 
            com.sun.jbi.management.config.LoggerConfigurationFactory.LOGGING_LOGGER;
       
        Logger logger = Logger.getLogger(loggerName);
        
        //initialize the super.
        super.initModelSystemService(anEnv, logger, mJbiNameInfo);

        //add AdminService MBean to START/STOP mbean set:
        mStartMBeans.add(mLoggingServiceMBeanName, com.sun.jbi.management.system.LoggingServiceMBean.class, this);
    }

    /**
     * Lookup a system LoggerMBean by system service name.
     * @param aSvcName is the name of the system service
     * @return the JMX object name of the service LoggerMBean or null
     */
    public ObjectName getSystemLoggerMBean(String aSvcName)
    {
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.SERVICE_NAME_KEY + "=" + aSvcName;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + mbn.CONTROL_TYPE_LOGGER;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName[] names = queryHelper(tmp);

        if (names.length >= 1)
        {
            if (names.length > 1)
            {
                String statusMsg = mTranslator.getString(
                    LocalStringKeys.LS_GETSYSTEMLOGGERMBEAN_TOO_MANY_MBEANS,
                    tmp);
                mLogger.severe(statusMsg);
            }

            //always return the first mbean name, even if many:
            return names[0];
        }
        else
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.LS_GETSYSTEMLOGGERMBEAN_LOOKUP_FAILED,
                tmp);
            mLogger.warning(statusMsg);
        }

        return null;
    }

    /**
     * Looks up LoggerMBeans for all JBI Framework
     * System Services currently installed.
     * @return array of object names for all system service LoggerMBeans.
     * @return zero-length array if no services registered.
     */
    public ObjectName[] getSystemLoggerMBeans()
    {
        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_SYSTEM;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + mbn.CONTROL_TYPE_LOGGER;
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName[] names = queryHelper(tmp);

        //if no logger mbeans found for any system service...
        if (names.length <= 1)
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.LS_GETSYSTEMLOGGERMBEANS_NO_SERVICES);
            mLogger.severe(statusMsg);
            return (new ObjectName[0]);
        }

        return names;
    }

    /**
     * lookup a JBI Installable Component by its unique ID.
     * @param aComponentId is the unique ID of the BC or SE.
     * @return the JMX object name of the LifeCycle MBean for the component
     */
    public ObjectName getComponentLoggerMBeanById(String aComponentId)
    {
        /*
         * EXAMPLE:
com.sun.jbi:ComponentId=ABC,ControlType=Lifecycle,ComponentType=Installed,*
         */

        MBeanNames mbn = mContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + mbn.COMPONENT_ID_KEY + "=" + aComponentId;
        tmp += "," + mbn.COMPONENT_TYPE_KEY + "=" + mbn.COMPONENT_TYPE_INSTALLED;
        tmp += "," + mbn.CONTROL_TYPE_KEY + "=" + mbn.CONTROL_TYPE_LOGGER;
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName[] names = queryHelper(tmp);

/*
mLogger.info(
    "LoggingService.getComponentLoggerMBeanById: T1 names.length=" + names.length );
mLogger.info("LoggingService.getComponentLoggerMBeanById: T1 tmp=" + tmp);
*/

        if (names.length >= 1)
        {
            if (names.length > 1)
            {
                String statusMsg = mTranslator.getString(
                    LocalStringKeys.LS_GETCOMPONENTLOGGERMBEANBYID_TOO_MANY_MBEANS,
                    tmp);
                mLogger.severe(statusMsg);
            }

            //always return the first mbean name, even if many:
            return names[0];
        }
        else
        {
            String statusMsg = mTranslator.getString(
                LocalStringKeys.LS_GETCOMPONENTLOGGERMBEANBYID_LOOKUP_FAILED,
                tmp);
            mLogger.warning(statusMsg);
        }

        return null;
    }


/////////
//methods private to LoggingService
/////////

    /**
     * Stub that calls AdminService queryHelper.
     * @param qStr is a JMX formatted query string
     * @return array of JMX object names, possibly of zero length.
     */
    private ObjectName[] queryHelper(String qStr)
    {
        return mContext.getAdminServiceHandle().queryHelper(qStr);
    }
}
