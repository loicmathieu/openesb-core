/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.message;

import java.util.List;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.management.LocalStringKeys;
import com.sun.jbi.management.system.ManagementException;



/**
 * Helper class to format Management XML messages for logging
 */

public class MessageHelper
{
    
    private static JAXBContext  mJaxbContext = null;    
    private static Marshaller          mWriter;
    private static Unmarshaller        mReader;    
    
    private static String TAB = "\t";
    private static String NEWLINE = System.getProperty("line.separator");
    private static String SPACE = " ";
    private static String COLON = ":";
    
    /**
     * This method is used to extract a message in the
     * given exception. If the exception contains a XML string
     * then it is parsed to look for the real message.
     * @param Exception
     * @return the message in the exception
     */
    public static String getMsgString(Exception ex)
    {
        String message = ex.getMessage();
        if (!MessageBuilder.isXmlString(message))
        {
            return message;
        }
        else
        {
            return extractMessageFromXML(message, null);
        }
    }
    
    /**
     * This method is used to extract a message in the
     * given exception. If the exception contains a XML string
     * then it is parsed to look for the real message.
     * @param Exception
     * @param logLevel the logLevel, if the log level is fine or lower the
     * stack trace will be printed
     * @return the message in the exception
     */
    public static String getMsgString(Exception ex, Level logLevel)
    {
        String message = ex.getMessage();
        if (!MessageBuilder.isXmlString(message))
        {
            return message;
        }
        else
        {
            return extractMessageFromXML(message, logLevel);
        }
    }
    
    /**
     * This method is used to extract the message from a given
     * XML string
     * @param the XML string 
     * @param logLevel the logLevel, if the log level is fine or lower the
     * stack trace will be printed
     * @return the message in the XML
     */
    public static String extractMessageFromXML(String xmlString, Level logLevel)
    {
        StringBuffer messageContent = new StringBuffer();
        Logger mLog = null;
        try
        {
            mLog = Logger.getLogger("com.sun.jbi.management");
            
            StringBuffer strBuf = new StringBuffer(xmlString);
            JbiTask jbiTask = (JbiTask)unmarshal( new StreamSource( 
                new java.io.StringReader( strBuf.toString())));
            JbiTaskResultElement taskResult = jbiTask.getJbiTaskResult();            
            messageContent.append(NEWLINE);
            messageContent.append(getFrameWorkMessages(jbiTask, logLevel));
            messageContent.append(getComponentMessages(jbiTask, logLevel));            
            
            return messageContent.toString();
            
        }
        catch (Exception ex )
        {
            mLog.warning(ex.getMessage());
            return xmlString;
        }       
        
    }
    
    
    /**
     * This method is used to get the framework task result details
     * messges from a JbiTask object
     * @param jbiTask jbiTask
     * @param logLevel the logLevel, if the log level is fine or lower the
     * stack trace will be printed
     * @return the formatted message
     */
    private static String getFrameWorkMessages(JbiTask jbiTask, Level logLevel)
    throws Exception
    {
        StringBuffer result = new StringBuffer();

        JbiTaskResultElement taskResult = jbiTask.getJbiTaskResult();
        if (taskResult == null)
        {
            return result.toString();
        }
        FrmwkTaskResult frmwkTaskResult = taskResult.getFrmwkTaskResult();
        if (frmwkTaskResult == null)
        {
            return result.toString();
        }
        TaskResultDetails taskResultDetails = 
               frmwkTaskResult.getFrmwkTaskResultDetails();

        if (taskResultDetails == null)
        {
            return result.toString();
        }

        TaskResultDetailsElement taskResultDetailsElement = 
                taskResultDetails.getTaskResultDetails();

        if (taskResultDetailsElement == null)
        {
            return result.toString();
        }

        String taskId = taskResultDetailsElement.getTaskId();
        String taskResultString = taskResultDetailsElement.getTaskResult();

        if (taskId != null)
        {
            result.append(getLocalizedString(LocalStringKeys.TASK_ID) + SPACE + taskId + SPACE);
        }
        if (taskResultString != null)
        {
            result.append(taskResultString.toLowerCase());
        }
        result.append(NEWLINE);

        String taskStatusMessages = getTaskStatusMessages(taskResultDetailsElement);
        if (taskStatusMessages != null && taskStatusMessages.length() > 0)
        {

            String messageType = taskResultDetailsElement.getMessageType();
            if (messageType != null)
            {
                result.append(format(messageType + COLON + SPACE,1));
            }
            result.append(NEWLINE);
            result.append(format(taskStatusMessages,1));
            result.append(NEWLINE);
        }


        String exceptionMessages = getExceptionMessages(taskResultDetailsElement, logLevel);
        if (exceptionMessages != null && exceptionMessages.length() > 0 )
        {
            result.append(format(getLocalizedString(LocalStringKeys.EXCEPTION_INFO) + COLON,2));
            result.append(NEWLINE);                
            result.append(format(exceptionMessages,3));
            result.append(NEWLINE);                                
        }            
        return result.toString();
    }
                    

    /**
     * This method is used to extract the framework task status messages from
     * framework task result details element
     * @param taskResultDetailsElement
     * @return the formatted message
     */
    public static String getTaskStatusMessages(
            TaskResultDetailsElement taskResultDetailsElement) 
    throws Exception
    {
        StringBuffer result = new StringBuffer();

        if(taskResultDetailsElement == null)
        {
            return result.toString();
        }
        List<TaskStatusMsg> taskStatusMsgs = taskResultDetailsElement.getTaskStatusMsg();
        if (taskStatusMsgs != null) 
        {
            for (TaskStatusMsg taskStatusMsg : taskStatusMsgs)            
            {
                MsgLocInfo msgLoc = taskStatusMsg.getMsgLocInfo();
                if(msgLoc != null)
                {
                    if (result != null && result.length() > 0)
                    {
                        result.append(NEWLINE);
                    }
                    result.append(msgLoc.getLocMessage());
                }
            }
        }

        return result.toString();
    }
    
    /**
     * This method is used to get the framework exception messages
     * messges from a framework task result details element
     * @param taskResultDetailsElement
     * @param logLevel the logLevel, if the log level is fine or lower the
     * stack trace will be printed     
     * @return the formatted message
     */
    private static String getExceptionMessages(
            TaskResultDetailsElement taskResultDetailsElement,
            Level logLevel) 
    throws Exception
    {
        StringBuffer result = new StringBuffer();
        if (taskResultDetailsElement == null)
        {
            return result.toString();
        }

        List<ExceptionInfo> exceptionsInfo = taskResultDetailsElement.getExceptionInfo();
        if (exceptionsInfo != null) 
        {
            for (ExceptionInfo exceptionInfo : exceptionsInfo)            
            {
                MsgLocInfo msgLoc = exceptionInfo.getMsgLocInfo();
                if (msgLoc != null)
                {   
                    if (result != null && result.length() > 0)
                    {
                        result.append(NEWLINE);
                    }                        
                    result.append(msgLoc.getLocMessage());
                    
                    //if the log level is fine or below print the stack trace
                    if (logLevel != null &&
                        logLevel.intValue() <= Level.FINE.intValue())
                    {
                        result.append(NEWLINE);
                        result.append(exceptionInfo.getStackTrace());
                    }
                }
            }
        }

        return result.toString();
    }    
    
    /**
     * This method is used to get the framework task result details
     * messges from a JbiTask object
     * @param jbiTask
     * @param logLevel the logLevel, if the log level is fine or lower the
     * stack trace will be printed     
     * @return the formatted message
     */
    private static String getComponentMessages(JbiTask jbiTask, Level logLevel)
    throws Exception
    {
        StringBuffer result = new StringBuffer();
        JbiTaskResultElement taskResult = jbiTask.getJbiTaskResult();
        if (taskResult == null)
        {
            return result.toString();
        }
        List<ComponentTaskResult> compTaskResults = taskResult.getComponentTaskResult();
        for (ComponentTaskResult compTaskResult : compTaskResults)
        {
            if (compTaskResult != null)
            {
                TaskResultDetails compTaskResultDetails = 
                    compTaskResult.getComponentTaskResultDetails();

                if (compTaskResultDetails != null)
                {
                    TaskResultDetailsElement taskResultDetailsElement = 
                            compTaskResultDetails.getTaskResultDetails();

                    if (taskResultDetailsElement != null)
                    {
                        String taskId = taskResultDetailsElement.getTaskId();
                        String taskResultString = taskResultDetailsElement.getTaskResult();
                        String compName = compTaskResult.getComponentName();
                        if (compName != null)
                        {
                            result.append(format(getLocalizedString(LocalStringKeys.COMPONENT_NAME) + COLON + SPACE + compName, 3));
                            result.append(NEWLINE);
                        }
                        if (taskId != null)
                        {
                            result.append(format(getLocalizedString(LocalStringKeys.TASK_ID) + SPACE + taskId + SPACE, 4));
                        }
                        if (taskResultString != null)
                        {
                            result.append(taskResultString.toLowerCase());
                        }

                        result.append(NEWLINE);
                        String messageType = taskResultDetailsElement.getMessageType();
                        if (messageType != null)
                        {
                            result.append(format(messageType + COLON + SPACE, 5));
                        }
                        result.append(NEWLINE);

                        String taskStatusMessages = getTaskStatusMessages(taskResultDetailsElement);
                        if (taskStatusMessages != null && taskStatusMessages.length() > 0)
                        {
                            result.append(format(taskStatusMessages,5));
                            result.append(NEWLINE);                                
                        }

                        String exceptionMessages = getExceptionMessages(taskResultDetailsElement, logLevel);
                        if (exceptionMessages != null && exceptionMessages.length() > 0 )
                        {
                            result.append(format(getLocalizedString(LocalStringKeys.EXCEPTION_INFO) + COLON,5));
                            result.append(NEWLINE);                                
                            result.append(format(exceptionMessages,6));
                            result.append(NEWLINE);                                 
                        }
                    }
                }
            }
        }
        return result.toString();
    }
            
    /**
     * This method is used to format a given string using indentations
     * @param message the string to be formatted
     * @param indentation the number of tabs to be put before every line
     * @return the formatted message
     */
    private static String format(String message, int indentationlevel)
    {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < indentationlevel ; i++)
        {
            buffer.append(TAB);
        }
        for (int i = 0; i < message.length() ; i++)
        {
            buffer.append(message.charAt(i));
            if (message.charAt(i) == '\n')
            {
                for (int j=0; j < indentationlevel; j++)
                {
                    buffer.append(TAB);
                }
            }
            
        }
        return buffer.toString();
    }

    /**
     * This method is used to return localized messages that
     * correspond to the given key
     * @param key the key
     * @return the message
     */
    private static String getLocalizedString(String key)
    {
        StringTranslator mTranslator = 
                com.sun.jbi.util.EnvironmentAccess.getContext().getStringTranslator("com.sun.jbi.management");
        return mTranslator.getString(key);
    }
    
    /** 
     * Provides a reference to the JAXB context for management messages, 
     *  initializing one if necessary.
     * @throws Excepion if the JAXB Context cannot be initialized
     */
    private static synchronized JAXBContext getJaxbContext()
        throws Exception
    {
        if ( mJaxbContext == null )
        {
            ClassLoader cl = 
                    Class.forName(
                        "com.sun.jbi.management.message.JbiTaskResult").
                        getClassLoader();
            mJaxbContext = JAXBContext.newInstance( "com.sun.jbi.management.message", cl);
        }
        
        return mJaxbContext;
    }
    
    /** 
     * Synchronizes access to the non thread-safe Marshaller for this context.
     */
    private static synchronized void marshal(Object jaxbElement, java.io.Writer writer)
        throws Exception
    {
        if (mWriter == null)
        {
            mWriter = getJaxbContext().createMarshaller();
        }
        
        mWriter.marshal(jaxbElement, writer);
    }
    
    /** 
     * Synchronizes access to the non thread-safe Unmarshaller for this context.
     */
    private static synchronized Object unmarshal(StreamSource input)
        throws Exception
    {
        if (mReader == null)
        {
            mReader = getJaxbContext().createUnmarshaller();
        }
        
        return mReader.unmarshal(input);
    }    
        
}


        
 
