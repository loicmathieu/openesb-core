/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WsdlException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

import java.text.MessageFormat;

/**
 * The class represents a WSDL processing exception. The message will contain
 * details about the error condition encountered.
 *
 * @author Sun Microsystems, Inc.
 */
public class WsdlException extends Exception 
{
    /**
     * Construct a WSDL exception with a simple error message string.
     * @param message Message text explaining the error condition being raised.
     */
    public WsdlException(String message)
    {
        super(message);
    }

    /**
     * Construct a WSDL exception with a parameterised error message string.
     * @param template Message text template explaining the error condition being raised.
     * @param params   The parameters used to fill in the template parameters.
     */
    public WsdlException(String template, Object[] params)
    {
        this( replaceTemplateParameters(template, params));
    }

    /**
     * Helper function to replace the parameters given in the template given.
     * @param template Message text template.
     * @param params   The parameters used to fill in the template parameters.
     *
     * @return String with all parameters substituted in the template.
     */
    protected static String replaceTemplateParameters(String template, Object[] params)
    {
        return MessageFormat.format(template, params);
    }
}
