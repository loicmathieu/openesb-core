/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Include.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2;

/**
 * API for  WSDL 2.0 Include component.
 *
 * @author ApiGen AX.00
 */
public interface Include extends ExtensibleDocumentedComponent
{
    /**
     * Get location hint for the included definitions.
     *
     * @return Location hint for the included definitions
     */
    String getLocation();

    /**
     * Set location hint for the included definitions.
     *
     * @param theLocation Location hint for the included definitions
     */
    void setLocation(String theLocation);

    /**
     * Get the description included by this component.
     *
     * @return Description from included by this component, if any
     */
    Description getDescription();

    /**
     * Get the definitions included by this component.
     *
     * @deprecated - replaced by getDescription()
     * @return Definitions from included by this component, if any
     */
    Definitions getDefinitions();

}

// End-of-file: Include.java
