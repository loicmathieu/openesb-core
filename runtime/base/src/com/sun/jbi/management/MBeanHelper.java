/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MBeanHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management;

import java.util.logging.Logger;

/**
 * MBeanHelper is used to create standard mbeans for system services and components.
 *
 * @author Sun Microsystems, Inc.
 */
public interface MBeanHelper
{
    /**
     * Create a LoggerMBean and associate it with <CODE>aLogger</CODE>.
     * @param aService is the name of the system service that owns the logger.
     * @param aLogger is the Logger to associate with the LoggerMBean.
     * @return true if successful, otherwise false.
     */
    boolean createSystemServiceLoggerMBean(String aService, Logger aLogger);

    /**
     * Destroy the LoggerMBean registered for <CODE>aService</CODE>.
     * @param aService is the name of the system service that owns the logger.
     * @return true if successful, otherwise false.
     */
    boolean destroySystemServiceLoggerMBean(String aService);

    /**
     * Destroy a LoggerMBean registered for <CODE>aService</CODE>.
     * @param aService is the name of the system service that owns the logger.
     * @param aLogger is the Logger associated with the LoggerMBean.
     * @return true if successful, otherwise false.
     */
    boolean destroySystemServiceLoggerMBean(String aService, Logger aLogger);

}
