package com.sun.jbi.management;

import com.sun.jbi.ServiceLifecycle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public interface ExtensionService extends ServiceLifecycle {
    
}
