/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventNotifierMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import javax.management.NotificationEmitter;

/**
 * This interface defines the MBean that generates notifications for all events
 * affecting the state of JBI runtime. There is one of these MBeans per runtime
 * instance, and it emits notifications for all applicable events in the
 * runtime, such as startup and shutdown of the runtime, installation or
 * uninstallation of a component or shared library, deployment or undeployment
 * of a service assembly, and life cycle operations on components and service
 * assemblies.
 *
 * Notifications from this MBean are processed by a single DAS notifier MBean,
 * which in turn emits the notifications. This allows clients to receive
 * notifications for all instances controlled by the DAS from a single MBean.
 *
 * @author Mark S White
 */
public interface EventNotifierMBean
    extends NotificationEmitter
{
    /**
     * Disable event notifications.
     */
    void disableNotifications();

    /**
     * Enable event notifications.
     */
    void enableNotifications();
}
