/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PlatformEventListener.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.platform;


/**
 * This is a listener interface for getting notifications on platform activity.
 * For example, an event would be fired when an instance is added or removed
 * from the system.
 *
 * @author Sun Microsystems, Inc.
 */
public interface PlatformEventListener
{
    /**
     * Notification on the creation of a instance
     * @param instanceName - name of the  instance.
     */
    void createdInstance(String instanceName);
    
    /**
     * Notification on the deletion of a instance
     * @param instanceName - name of the instance.
     */
    void deletedInstance(String instanceName);
    
    /**
     * Notification on the creation of a cluster
     * @param clusterName - name of the cluster
     */
    void createdCluster(String clusterName);
    
    /**
     * Notification on the deletion of a cluster
     * @param clusterName - name of the cluster 
     */
    void deletedCluster(String clusterName);
    
}
