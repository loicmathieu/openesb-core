/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)FrameworkStatisticsMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.monitoring;

import java.util.Date;

/**
 * This interface defines the MBean for display of statistics from the
 * framework.
 *
 * @author Sun Microsystems, Inc.
 */
public interface FrameworkStatisticsMBean
{
    /**
     * Get the time of the last JBI framework restart.
     * @return The time of the last restart of the JBI framework.
     */
    Date getLastRestartTime();

    /**
     * Get the total time used by the last JBI framework restart.
     * @return The time in milliseconds of the last restart of the JBI framework.
     */
    long getStartupTime();

    /**
     * Get the count of component registry additions.
     * @return The total number of entries added to the component registry
     * since the last JBI startup.
     */
    long getRegistryAdds();

    /**
     * Get the count of component registry deletions.
     * @return The total number of entries deleted from the component registry
     * since the last JBI startup.
     */
    long getRegistryDeletes();
}
