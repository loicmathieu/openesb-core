/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)NMRTestBinding.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package TestBinding;

import com.sun.enterprise.ComponentInvocation;
import com.sun.enterprise.InvocationManager;
import com.sun.enterprise.Switch;

import com.sun.messaging.XAQueueConnectionFactory;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.io.StringWriter;

import java.util.Enumeration;

import java.util.logging.Logger;

import javax.jbi.component.Component;
import javax.jbi.component.ComponentContext;
import javax.jbi.component.ComponentLifeCycle;
import javax.jbi.component.ServiceUnitManager;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.Fault;
import javax.jbi.messaging.MessageExchangeFactory;
import javax.jbi.messaging.InOptionalOut;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.NormalizedMessage;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.TextMessage;
import javax.jms.QueueBrowser;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.MessageConsumer;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.XAQueueConnection;
import javax.jms.XAQueueSession;

import javax.naming.InitialContext;

import javax.transaction.Transaction;
import javax.transaction.TransactionManager;
import javax.transaction.xa.XAResource;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.stream.StreamSource;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.dom.DOMResult;


import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * This is a servlet that acts as a Binding Component and tests the dynamic
 * registration support provided by JBI. It registers itself as a binding and
 * then performs a fixed set of message exchanges with another servlet that
 * acts as a Service Engine. That servlet is EngineServlet.
 *
 * @author Sun Microsystems, Inc.
 */
public class NMRTestBinding
    implements Component, ComponentLifeCycle
{

   /**
    * Logger for the servlet.
    */
    private java.util.logging.Logger        mLog;

   /**
    * Handle to the XML Document Builder.
    */
    private DocumentBuilder                 mDocumentBuilder;

   /**
    * Binding channel for NMS communications.
    */
    private DeliveryChannel                 mChannel;

   /**
    * Component context from JBI.
    */
    private ComponentContext                mContext;
    
   
    /**
     * Transaction Manager.
     */
    
    private TransactionManager              mTM;
    
   /**
    * MessageExchangeFactory.
    */
    private MessageExchangeFactory          mFactory;

   /** SE Queue to send messages. */
    private Queue mDirectQueueBinding = null;
    private Queue mDirectQueueEngine = null;
    private Queue mJNDIQueueEngine = null;
    private Queue mJNDIQueueBinding = null;

    /** QueueConnection for JMS connection */
    private XAQueueConnection mQDirectConn = null;
    private QueueConnection mQJNDIConn = null;
    private XAQueueConnection mQJNDIXAConn = null;
    
    /** Factory of QueueConnections for JMS Tests */
    private XAQueueConnectionFactory mQDirectFactory = null;
    private QueueConnectionFactory mQJNDIFactory = null;

    /** QueueSession to be used for sending/receiving messages */
    private XAQueueSession mDirectQueueSession = null;
    private QueueSession mJNDIQueueSession = null;

   /**
    * The engine's service name.
    */
    private static final QName ENGINE_SERVICE =
        new QName("nmr-test-engine_service");

   /**
    * Qualified name for the binding's service.
    */
    private static final QName BINDING_SERVICE =
        new QName("nmr-test-binding_service");

   /**
    *  The binding's endpoint name.
    */
    private static final String BINDING_ENDPOINT = "nmr-test-binding_endpoint";

   /**
    * JBI component name
    */
    private static final String JBI_COMPONENT_NAME = "NMRTestBinding";

   /**
    * JBI component description
    */
    private static final String JBI_COMPONENT_DESC = "NMR TestBinding";

   /**
    * Runner Thread.
    */
    private Thread                      mRunnerThread;
    private boolean                    mRunning;
    private StringBuffer                mSB;

    public NMRTestBinding()
    {
        mLog = Logger.getLogger(this.getClass().getPackage().getName());
    }
    
//---------------------------- Component Methods ------------------------------

    /**
     * Get the ComponentLifeCycle instance.
     * @return The ComponentLifeCycle instance.
     */
    public ComponentLifeCycle getLifeCycle()
    {
        return this;
    }

    /**
     * Get the ServiceUnitManager instance.
     * @return The ServiceUnitManager instance, which is always null for
     * this test.
     */
    public ServiceUnitManager getServiceUnitManager()
    {
        mLog.info("NMRTestBinding: Component getServiceUnitManager() called");
        return null;
    }
    
    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually
     * perform the operation desired.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if its OK.
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        /* Should not always return true
         * The capabilities of this component has to be checked first.
         * Not implemented right now
         */
        return true;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually
     * interact with the the provider completely.
     *
     * @param endpoint
     * @param exchange
     *
     * @return  true if OK.
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        /* Should not always return true
         * The capabilities of this component has to be checked first.
         * Not implemented right now
         */
        return true;
    }

    /**
     * Returns a resolver for meta-data.
     *
     * @param ServiceEndpoint endpoint reference object.
     *
     * @return Descriptor for file binding.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint ServiceEndpoint)
    {
        org.w3c.dom.Document desc = null;

        try
        {
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return desc;
    }
    
    /**
     * Resolve the endpoint reference using the given capabilities of the
     * consumer. This is called by JBI when its trying to resove the given EPR
     * on behalf of the component.
     *
     * @param epr endpoint reference.
     *
     * @return  Service enpoint.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }

//------------------------ ComponentLifeCycle Methods -------------------------

    /**
     * Initializes the JBI LifeCycle. Because this servlet specified the
     * "self-started" option when it registered, this method does nothing.
     * @param context - the component context provided by the JBI framework.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void init(ComponentContext context) throws javax.jbi.JBIException
    {
        mLog.info("NMRTestBinding: LifeCycle init() called");
        mContext = context;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        try
        {
            mDocumentBuilder = dbf.newDocumentBuilder();
        }
        catch ( javax.xml.parsers.ParserConfigurationException pcEx )
        {
            throw new javax.jbi.JBIException(
                "Unable to create new DocumentBuilder instance: " + pcEx, pcEx);
        }
        try
        {
            mChannel = mContext.getDeliveryChannel();
            mFactory = mChannel.createExchangeFactory();
        }
        catch ( javax.jbi.messaging.MessagingException mEx )
        {
            throw new javax.jbi.JBIException(
                "Unable to obtain DeliveryChannel: " + mEx, mEx);
        }
    }

    /**
     * Get the Extension MBean name.
     * @return The JMX object name of the Extension MBean for this BC,
     * which is always null in this test.
     */
    public javax.management.ObjectName getExtensionMBeanName()
    {
        return null;
    }

    /**
     * Starts the JBI LifeCycle. Because this servlet specified the
     * "self-started" option when it registered, this method does nothing.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void start() throws javax.jbi.JBIException
    {
        mLog.info("NMRTestBinding: LifeCycle start() entered");
        
        //
        //  Get Various JMS resources needed for the test.
        //
        try
        {
            mQDirectFactory = (XAQueueConnectionFactory) Class.forName("com.sun.messaging.XAQueueConnectionFactory").newInstance();
            mQDirectFactory.setProperty("imqBrokerHostPort", "9000");
            mQDirectFactory.setProperty("imqBrokerHostName", "localhost");
        }
        catch (Exception e)
        {
            throw new javax.jbi.JBIException("Can't get MQ factory: " + e);            
        }
        try
        {
            //
            //  Try a direct connection to JMS that bypasses the JMS RA.
            //
            mQDirectConn = mQDirectFactory.createXAQueueConnection();
            mDirectQueueSession = mQDirectConn.createXAQueueSession();
            mDirectQueueBinding = new com.sun.messaging.Queue("QUEUE_NMR_TEST_BINDING");
            mDirectQueueEngine = new com.sun.messaging.Queue("QUEUE_NMR_TEST_ENGINE");
            
            //
            //  Get JNDI connection to JMS that uses the JMS RA.
            //
//            prepareThreadForXA();
//            mQJNDIFactory = (QueueConnectionFactory) mContext.getNamingContext().lookup("jms/NMRTestQueueFactory");
//            mQJNDIConn = mQJNDIFactory.createQueueConnection();
//            mJNDIQueueBinding = (Queue) mContext.getNamingContext().lookup("jms/QUEUE_NMR_TEST_BINDING2");
//            mJNDIQueueEngine = (Queue) mContext.getNamingContext().lookup("jms/QUEUE_NMR_TEST_ENGINE2");
//            mJNDIQueueSession = mQJNDIConn.createQueueSession(true, 0);
            
            //
            //  Get the Transaction Manager.
            //
            mTM = (TransactionManager)mContext.getTransactionManager();
        }
        catch (javax.jms.JMSException jEx)
        {
            throw new javax.jbi.JBIException("Cant' create JMS resources: " + jEx);
        }
//        catch (javax.naming.NamingException nEx)
//        {
//            throw new javax.jbi.JBIException("Cant' create JMS resources via JNDI: " + nEx);           
//        }

        mRunning = true;
        mRunnerThread = new Thread (new BindingRunner());
        mRunnerThread.setName("NMRTestBinding-Runner");
        mRunnerThread.setDaemon(true);
        mRunnerThread.start();
        mLog.info("NMRTestBinding: LifeCycle start() exited");
    }

    /**
     * Stops the JBI LifeCycle. Because this servlet specified the
     * "self-started" option when it registered, this method does nothing.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void stop() throws javax.jbi.JBIException
    {
        mLog.info("NMRTestBinding: LifeCycle stop() called");
        mRunning = false;
        mRunnerThread.interrupt();
        mLog.info("\nNMRTestBinding: Results =====================\n"
                + mSB.toString() +
                "NMRTestBinding: END Results =================\n");
    }

    /**
     * Shuts down the JBI LifeCycle. Because this servlet specified the
     * "self-started" option when it registered, this method does nothing.
     * @throws javax.jbi.JBIException if any error occurs.
     */
    public void shutDown() throws javax.jbi.JBIException
    {
        mLog.info("NMRTestBinding: LifeCycle shutDown() called");
    }
        
    
    public void prepareThreadForXA()
    {
        InvocationManager im;

        im = Switch.getSwitch().getInvocationManager();
        if (im.getCurrentInvocation() == null)
        {
            im.preInvoke(new ComponentInvocation(this, this));
        }
     }

    class BindingRunner
            implements java.lang.Runnable
    {
        public void run() 
        {            
            mLog.info("NMRTestBinding: Start processing requests");
            StringBuffer        sb = mSB = new StringBuffer();
           
            try
            {
//                prepareThreadForXA();
                if (mRunning)    sb.append(doInOutTest("Direct-Commit", mDirectQueueSession, mDirectQueueBinding) + "\n");
                if (mRunning)    sb.append(doInOutTest("Direct-Abort", mDirectQueueSession, mDirectQueueBinding) + "\n");
                if (mRunning)    sb.append("Binding Queue Contents:\n");
                if (mRunning)    sb.append(browseQueues((QueueConnection)mQDirectConn, (QueueSession)mDirectQueueSession, mDirectQueueEngine) + "\n");
//                if (mRunning)    sb.append(doInOutTest("JNDI-Commit", (XAQueueSession)mJNDIQueueSession, mJNDIQueueBinding) + "\n");
//                if (mRunning)    sb.append(doInOutTest("JNDI-Abort", (XAQueueSession)mJNDIQueueSession, mJNDIQueueBinding) + "\n");
//                if (mRunning)    sb.append(browseQueues((QueueConnection)mQJNDIConn, (QueueSession)mJNDIQueueSession, mJNDIQueueBinding) + "\n");
//                if (mRunning)    sb.append("Engine Queue Contents:\n");
            }      
            catch ( Exception ex )
            {
                mLog.warning("NMRTestBinding:Result: Exception: " + ex);
            }

            mLog.info("NMRTestBinding: Processing stopping");
        }
    }

//----------------------------- Private Methods ------------------------------

    /**
     * Use a direct instansiation JMS Local Transaction/
     */
    private String doInOutTest(String test, XAQueueSession session, Queue queue)
    {
        mLog.info("NMRTestBinding (" + test + ") start");
        String          respMsg = null;
        Transaction     xact;
        XAResource      resource;
        
        InOut io;
        try
        {
            mTM.begin();
            xact = mTM.getTransaction();
            resource = session.getXAResource();
            xact.enlistResource(resource);
            
            io = mFactory.createInOutExchange();
            io.setProperty(MessageExchange.JTA_TRANSACTION_PROPERTY_NAME, xact);

            io.setEndpoint(mContext.getEndpointsForService(ENGINE_SERVICE)[0]);
            io.setOperation(new QName(test));
            io.setInMessage(createMessage(io, "NMRTestBInding InOut " + test));
            
            //
            //  Queue message to our local resource.
            //
            MessageProducer mp = session.createProducer(queue);
            TextMessage t = session.createTextMessage();;
            t.setText("NMRTestBInding InOut " + test);
            mp.send(t);
            mp.close();

            mChannel.send(io);
            io = (InOut) mChannel.accept();
            if ( io.getStatus() == ExchangeStatus.ACTIVE )
            {
                respMsg = "NMRTestBinding:Result: sendInOut (" + test + ") Successful";
            }
            else
            {
                respMsg = "NMRTestBinding:Result: sendInOut (" + test + ") FAILED";
            }
            io.setStatus(ExchangeStatus.DONE);
            mChannel.send(io);
            
            xact.delistResource(resource, XAResource.TMSUCCESS);
            
            if (test.indexOf("Commit") >= 0)
            {
                xact.commit();
            }
            else
            {
                xact.rollback();
            }
        }
        catch ( Exception e)
        {
            respMsg = "NMRTestBinding:Result: (" + test + ") Exception: " + e;            
        }
        mLog.info("NMRTestBinding: (" + test +") end");
        
        return (respMsg);
    }


    /**
     * Use a direct instansiation JMS Local Transaction/
     */
    private String browseQueues(QueueConnection conn, QueueSession session, Queue queue)
    {
        StringBuilder   sb = new StringBuilder();
        
        InOut io;
        try
        {
            QueueBrowser qbrowser = session.createBrowser(queue);

            conn.start();
            Enumeration enum1 = qbrowser.getEnumeration();
            int count = 0;
            while (enum1.hasMoreElements())
            {
                TextMessage   m  = (TextMessage)enum1.nextElement();
                
                count++;
                sb.append("    ");
                sb.append(count);
                sb.append("    ");
                sb.append(m.getText());
                sb.append("\n");
            }
            conn.stop();
            qbrowser.close();
        }
        catch (Exception e)
        {
            sb.append("FAILED: ");
            sb.append(e);
            sb.append("\n");
        }
        
        return (sb.toString());
    }
    

    /**
     * Create a Normalized Message.
     * @param exchange - a MessageExchange for which a message is to be created.
     * @param content - a String containing the message content.
     * @return The NormalizedMessage instance with the provided content.
     */
    private NormalizedMessage createMessage(MessageExchange exchange,
                                            String content)
    {
        NormalizedMessage msg = null;
        try
        {
            StreamDataSource		fds = new StreamDataSource("Test", "text/plain");
            OutputStreamWriter		osw = new OutputStreamWriter(fds.getOutputStream());
            osw.write("This is the contents of the attachment.");
            osw.close();

            /*
            FileDataSource	fds = new FileDataSource("C:/temp/junk");
            */
            Document doc;
            Element elem;
            msg = exchange.createMessage();
            doc = mDocumentBuilder.newDocument();
            elem = doc.createElement("message");
            elem.appendChild(doc.createTextNode(content));
            doc.appendChild(elem);
            msg.setContent(new DOMSource(doc));
            msg.addAttachment("Attachment1", new DataHandler(fds));
        }
        catch ( java.io.IOException ioEx )
        {
            mLog.warning("IOException " + ioEx.toString());
        }
        catch ( javax.jbi.messaging.MessagingException ex )
        {
            mLog.warning("MessagingException " + ex.toString());
        }
        return msg;
    }
    
    /**
     * Create a Normalized Message.
     * @param exchange - a MessageExchange for which a message is to be created.
     * @param content - a String containing the message content.
     * @return The NormalizedMessage instance with the provided content.
     */
    private Fault createFault(MessageExchange exchange,
                                            String content)
    {
        Fault fault = null;
        try
        {
            Document doc;
            Element elem;
            fault = exchange.createFault();
            doc = mDocumentBuilder.newDocument();
            elem = doc.createElement("message");
            elem.appendChild(doc.createTextNode(content));
            doc.appendChild(elem);
            fault.setContent(new DOMSource(doc));
        }
        catch ( javax.jbi.messaging.MessagingException ex )
        {
            mLog.warning("MessagingException " + ex.toString());
        }
        return fault;
    }

}
