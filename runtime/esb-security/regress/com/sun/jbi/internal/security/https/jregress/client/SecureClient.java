/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecureClient.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecureClient.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on November 16, 2004, 4:33 PM
 */

package com.sun.jbi.internal.security.https.jregress.client;

import java.net.HttpURLConnection;
import java.net.URL;
import java.io.File;
import java.io.FileWriter;

import com.sun.jbi.internal.security.https.jregress.Helper;

/**
 * This is a client which will be used to connect to the TestSecurityServlet,
 * to test the Transport Level Security feature.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecureClient
{
    
    /** Creates a new instance of SecureClient */
    public SecureClient(URL url, String file)
        throws Exception
    {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        setExtendedHostVerifier(connection);
        Helper.getResponse(connection, new FileWriter(file, true));
    }
    
    public static void main(String[] args)
        throws Exception
    {
        try
        {
            if ( args.length != 2 )
            {
                System.out.println("Usage: SecureClient <url> <file>");
                return;
            }

            SecureClient client = new SecureClient( new URL(args[0]), args[1] ) ;
            
                
        }
        catch ( Exception ex )
        {
            //-- ex.printStackTrace();
            throw ex;
        }
        
    }
    
    /**
     * Set a Hostname Verifier, this is called when hostname verification fails,
     * Ignore the hostname mismatch as this is a test client.
     *
     * @param connection is the HttpURLConnection
     */
    private void setExtendedHostVerifier(HttpURLConnection connection)
    {
        if ( connection instanceof javax.net.ssl.HttpsURLConnection )
        {
             ( (javax.net.ssl.HttpsURLConnection) connection).setHostnameVerifier(
                new javax.net.ssl.HostnameVerifier() 
             {
                 public boolean verify(String hostname, javax.net.ssl.SSLSession ssn)
                 {
                     return true;
                 }
             } );
        }
             
    }
    
    
    
}
