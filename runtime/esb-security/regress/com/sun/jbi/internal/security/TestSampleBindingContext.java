/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSampleBindingContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security;

import java.io.File;
import org.xml.sax.EntityResolver;

public class TestSampleBindingContext 
    extends junit.framework.TestCase
{
    
    public TestSampleBindingContext (String aTestName)
    {
        super(aTestName);
    }

    public void setUp()
        throws Exception
    {
        super.setUp();
        
    }

    public void tearDown()
        throws Exception
    {
       
    }

    /**
     * Test to check if the String translator is created properly.
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testGetStringTranslator()
           throws Exception
    {
        String testname = "testGetStringTranslator";
        try 
        {
            SampleBindingContext bndCtx = new SampleBindingContext("sb-111");
            
            com.sun.jbi.StringTranslator st = 
                bndCtx.getStringTranslator(Constants.PACKAGE);
            
            assertNotNull(st);         
        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            fail(testname + ": failed due to -" + aEx.getMessage());
            throw aEx;
        }
    } 
    
     /**
     * Test to check if the string translator is returning the right values.
     *
     * @throws Exception if an unexpected error occurs
     */
    public void testGetStringTranslator2()
           throws Exception
    {
        String testname = "testGetStringTranslator";
        try 
        {
            SampleBindingContext bndCtx = new SampleBindingContext("sb-111");
            
            com.sun.jbi.StringTranslator st = 
                bndCtx.getStringTranslator("com.sun.jbi.internal.security");
            
            String str = st.getString("BC_ERR_INVALID_NS_URI", "testEndpoint"); 
            System.out.println(str);
            assertNotNull(str);   
            assertTrue(str.equals(
                "An invalid targetNamespace URI is specified for Endpoint testEndpoint."));
        }
        catch (Exception aEx)
        {
            aEx.printStackTrace();
            fail(testname + ": failed due to -" + aEx.getMessage());
            throw aEx;
        }
    } 
}
