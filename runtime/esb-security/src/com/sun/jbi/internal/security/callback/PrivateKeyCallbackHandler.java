/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)PrivateKeyCallbackHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  PrivateKeyCallbackHandler.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on February 23, 2005, 11:31 AM
 */

package com.sun.jbi.internal.security.callback;

import com.sun.enterprise.security.jauth.callback.PrivateKeyCallback;
import com.sun.jbi.internal.security.KeyStoreManager;

import java.io.IOException;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;



/**
 *
 * @author Sun Microsystems, Inc.
 */
public class PrivateKeyCallbackHandler
    implements CallbackHandler
{
    /** The reference to the KeyStoreManager. */
    private KeyStoreManager mKeyMgr;
    
    /** The X509 Certificate Type. */
    private static final String X509 = "X.509";
    
    /** 
     * Creates a new instance of PrivateKeyCallbackHandler.
     * 
     * @param mgr - KeyStoreManager instance which provides the handle
     * to the KeyStores.
     */
    public PrivateKeyCallbackHandler (KeyStoreManager mgr)
    {
        mKeyMgr = mgr;
    }
    
    /**
     * The implementation on the CallbackInterface. This method only handles 
     * PrivateKeyCallback.
     *
     * @param callbacks - array of Callbacks to be handled.
     * @throws IOException - if an input or output error occurs. 
     * @throws UnsupportedCallbackException - if the implementation of this method
     * does not support one or more of the Callbacks specified in the callbacks 
     * parameter.
     */
    public void handle(Callback[] callbacks)
        throws IOException, UnsupportedCallbackException
    {
        for (int i = 0; i < callbacks.length; i++) 
        {
            CallbackHandler handler = null;
            
            if  (callbacks[i] instanceof PrivateKeyCallback)
            {
                PrivateKeyCallback cb = (PrivateKeyCallback) callbacks[i];
                cb.setKey(null, null);
                Object req = cb.getRequest();
                
                if ( req == null )
                {
                    setAnyKey(cb);
                }
                if ( req instanceof PrivateKeyCallback.AliasRequest)
                {
                    handleAliasRequest( (PrivateKeyCallback.AliasRequest) req, cb);
                }
                else if ( req instanceof PrivateKeyCallback.IssuerSerialNumRequest)
                {
                    handleIssuerSerialNumRequest( 
                        (PrivateKeyCallback.IssuerSerialNumRequest) req, cb);
                }
                else if ( req instanceof PrivateKeyCallback.SubjectKeyIDRequest)
                {
                    handleSubjectKeyIdRequest( 
                        (PrivateKeyCallback.SubjectKeyIDRequest) req, cb);
                }           
            } 
            else
            {
                throw new UnsupportedCallbackException(callbacks[i]);
            }
        }
    }
    
    /**
     * Handle a request for a Private Key based on alias. Go through the KeyStore and
     * get the PrivateKey for the alias. If there is no key entry for the alias then
     * the key and cert chain in the Callback is null.
     *
     * @param req is the alias request from the PrivateKeyCallback
     * @param cb is the PrivateKeyCallback
     * @throws IOException - if an input or output error occurs. This would indicate
     * that the Key / Cert Chain could not be retrieved from the store.
     */
    private void handleAliasRequest(
        PrivateKeyCallback.AliasRequest req, PrivateKeyCallback cb)
        throws IOException
    {
        KeyStore ks = mKeyMgr.getKeyStore();
        
        if ( req.getAlias() == null )
        {
            setAnyKey(cb);   
        }
        
        try
        {
            if ( ks.containsAlias(req.getAlias()))
            {
                Key key = ks.getKey(req.getAlias(), 
                    mKeyMgr.getKeyStorePassword().toCharArray());
                if ( key instanceof PrivateKey )
                {
                    cb.setKey( (PrivateKey) key, 
                        ks.getCertificateChain (req.getAlias()));
                }
            }
        }
        catch ( Exception ex )
        {
            throw new IOException (ex.getMessage ());
        }
    }
    
    /**
     * Handle a request for a Private Key based on the Issuer Name and Serial Number 
     * for the corresponding Public Key. Basically get the Key Entry for the alias, 
     * then check if the Public Key Certificate's Issuer Name and Serial Number match 
     * those in the IssuerSerialNumRequest .
     *
     * @param req is the IssuerSerialNumRequest from the PrivateKeyCallback
     * @param cb is the PrivateKeyCallback
     * @throws IOException - if an input or output error occurs. This would indicate
     * that the Key / Cert Chain could not be retrieved from the store.
     */
    private void handleIssuerSerialNumRequest(
        PrivateKeyCallback.IssuerSerialNumRequest req, PrivateKeyCallback cb)
        throws IOException
    {
        KeyStore ks = mKeyMgr.getKeyStore();
         
        try
        {
            java.util.Enumeration aliases = ks.aliases();
            while ( aliases.hasMoreElements() )
            {
                String alias = (String) aliases.nextElement();
                if ( ks.isKeyEntry(alias) )
                {
                    Key key = ks.getKey( alias, 
                        mKeyMgr.getKeyStorePassword().toCharArray());

                    if ( key instanceof PrivateKey )
                    {
                        // -- Check if the Public Key Certificate matches request.
                        Certificate cert = ks.getCertificate (alias);
                        if (cert == null || !X509.equals (cert.getType()))
                        {
                            continue;
                        }
                        else
                        {
                            X509Certificate pkCert = (X509Certificate) cert;
                            if (req.getIssuer().equals(pkCert.getIssuerX500Principal()) &&
                                req.getSerialNum().equals(pkCert.getSerialNumber()) )  
                            {
                                // -- Match found
                                cb.setKey( (PrivateKey) key, 
                                    ks.getCertificateChain(alias));
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex )
        {
            throw new IOException (ex.getMessage ());
        }
        
    }
    
    /**
     * Handle a request for a Private Key based on alias.
     *
     * @param req is the SubjectKeyIdRequest from the PrivateKeyCallback
     * @param cb is the PrivateKeyCallback
     * @throws IOException - if an input or output error occurs. This would indicate
     * that the Key / Cert Chain could not be retrieved from the store.
     */
    private void handleSubjectKeyIdRequest(
        PrivateKeyCallback.SubjectKeyIDRequest req, PrivateKeyCallback cb)
        throws IOException
    {
        KeyStore ks = mKeyMgr.getKeyStore(); 
        
        try
        {
            java.util.Enumeration aliases = ks.aliases ();
            while (aliases.hasMoreElements ())
            {
                String alias = (String) aliases.nextElement ();
                if (ks.isKeyEntry (alias))
                {
                    Certificate cert = ks.getCertificate (alias);
                    if (cert == null || !X509.equals (cert.getType()))
                    {
                        continue;
                    }
                    X509Certificate x509Cert = (X509Certificate) cert;
                    byte[] keyId = CertificateHelper.getSubjectKeyIdentifier (x509Cert);
                    if (keyId == null)
                    {
                        // -- Cert does not contain a key identifier
                        continue;
                    }
                    if (java.util.Arrays.equals (req.getSubjectKeyID(), keyId))
                    {
                        // Asuumed key password same as the keystore password
                        cb.setKey( (PrivateKey)
                            ks.getKey(alias, mKeyMgr.getKeyStorePassword().toCharArray()),
                            ks.getCertificateChain(alias) );
                    }
                }
            }
        } 
        catch (Exception ex)
        {
            throw new IOException (ex.getMessage ());
        }
    }
    
    /**
     * If the request in the PrivateKeyCallback is null or the alias in the Alias request
     * is null, then we go through the KeyStore and return the entry for the first alias
     * with a private key entry.
     *
     * @param cb is the PrivateKeyCallback.
     * @throws java.io.IOException if an error occurs in retrieveing the Key.
     */
    private void setAnyKey(PrivateKeyCallback cb)
        throws java.io.IOException
    {
        KeyStore ks = mKeyMgr.getKeyStore();
         
        try
        {
            java.util.Enumeration aliases = ks.aliases();
            while ( aliases.hasMoreElements() )
            {
                String alias = (String) aliases.nextElement();
                if ( ks.isKeyEntry(alias) )
                {
                    Key key = ks.getKey( alias, 
                        mKeyMgr.getKeyStorePassword().toCharArray());

                    if ( key instanceof PrivateKey )
                    {
                        cb.setKey( (PrivateKey) key, ks.getCertificateChain(alias));
                    }
                }
            }
        }
        catch (Exception ex )
        {
            throw new java.io.IOException(ex.toString());
        }
    }
    
}
