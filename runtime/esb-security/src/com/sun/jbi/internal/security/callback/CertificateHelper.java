/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CertificateHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  CertificateHelper.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security.callback;

import java.security.cert.X509Certificate;

/**
 * Certificate Helper Class.
 * 
 * @author Sun Microsystems, Inc.
 */
public class CertificateHelper
{
    /** OID for Subject Key Identifier. */
    private static final String SUBJECT_KEY_IDENTIFIER_OID = "2.5.29.14";
    
    /** Offset */
    private static final int OFFSET = 4;
    
    /**
     * Get the Subject Key Identifier for the Certificate.
     * @param cert is the X509 Certificate
     * @return the subject key identifier of the certificate.
     */    
    protected static byte[] getSubjectKeyIdentifier (X509Certificate cert)
    {
        
        byte[] subjectKeyIdentifier =
            cert.getExtensionValue (SUBJECT_KEY_IDENTIFIER_OID);
        if (subjectKeyIdentifier == null)
        {
            return null;
        }
        byte[] dest = new byte[subjectKeyIdentifier.length - OFFSET];
        System.arraycopy (
            subjectKeyIdentifier, OFFSET, dest, 0, subjectKeyIdentifier.length - OFFSET);
        return dest;
    }        
}
