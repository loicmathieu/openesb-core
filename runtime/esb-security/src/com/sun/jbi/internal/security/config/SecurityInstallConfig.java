/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityInstallConfig.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  SecurityInstallConfig.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 *  Created on December 16, 2004, 2:49 PM
 */

package com.sun.jbi.internal.security.config;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.internal.security.Constants;
import com.sun.jbi.internal.security.LocalStringConstants;
import com.sun.jbi.internal.security.https.HttpConstants;
import com.sun.jbi.management.common.GenericConfigurationMBean;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.jbi.JBIException;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



/**
 * Class which encapsulates the installation configuration for the
 * Security Service.
 *
 * Remember : modifiers should be synchronized.
 *
 * @author Sun Microsystems, Inc.
 */
public class SecurityInstallConfig
    implements com.sun.jbi.internal.security.config.SecurityConfiguration
{
    /**
     * Missing Property String.
     */
    private final String mMissingProperty = "PROPERTY_NOT_FOUND";
    
    /** 
     * The User Domains map. This map contains the UserDomain information keyed by the
     * name of the domain. The Value is a Properties object that has all the attributes
     * specified for the domain
     */
    private HashMap mUserDomains;
    
    /** 
     * The KeyStoreManagers map. This map contains the KeyStoreManager information Keyed 
     * by the name of the manager. The Value is a Properties object that has all the 
     * attributes specified for the manager.
     */
    private HashMap mKeyStoreManagers;
    
    /** 
     * The SSL Context.
     */
    private Properties mTransportSecurityContext;
    
    /** Default UserDomain */
    private String mDefUD;
    
    /** Default Security Context */
    private String mDefSecCtx;
    
    /** Default KeyStore Manager */
    private String mDefKSMgr;
    
    /** The String Translator to use. */
    private StringTranslator mTranslator;
    
    /** The Logger. */
    private Logger mLogger;
    
    /** Flag indicating if the Security Configuration has changed. */
    private boolean mIsDirty;
    
    /** The DOM form of this configuration. */
    private Document mDom;
    
    /** Dot Pattern. */
    private static final String DOT_PATTERN = "\\x2E";
    
    /** UD Prefix. */
    private static final String UD_PREFIX = "uds.ud";
    
    /** UD pattern. */
    private static final String UD_PATTERN = "uds" + DOT_PATTERN + "ud";
    
    /** KM Prefix. */
    private static final String KM_PREFIX = "kms.km";
    
    /** KM pattern. */
    private static final String KM_PATTERN = "kms" + DOT_PATTERN + "km";
    
    /** Transport Prefix. */
    private static final String TRANSPORT_PREFIX = "transports";
    
    /** Default Suffix. */
    private static final String DEFAULT_SUFFIX = ".default";
    
    /** Generic Pattern. */
    private static final String ANY_PATTERN = ".*";
    
    /** Default User Domain Property. */
    private static final String DEF_UD_KEY = UD_PREFIX + DEFAULT_SUFFIX;
    
    /** Default Key Store Manager Property. */
    private static final String DEF_KM_KEY = KM_PREFIX + DEFAULT_SUFFIX;
    
    /** Name suffix. */
    private static final String NAME_SUFFIX = ".name";
    
    /** Domain suffix. */
    private static final String DOMAIN_SUFFIX = ".domain";
    
    /** Param suffix. */
    private static final String PARAM_SUFFIX = ".param";
    
    /** Param Pattern. */
    private static final String PARAM_PATTERN = "[[" + DOT_PATTERN + "]param]";
    
    /** Param suffix. */
    private static final String VALUE_SUFFIX = ".value";
    
    /** Manager suffix. */
    private static final String MANAGER_SUFFIX = ".manager";
    
    
    
    
    /** 
     * Creates a new instance of SecurityInstallConfig.
     *
     * @param translator is the StringTranslator.
     */
    public SecurityInstallConfig (StringTranslator translator)
    {
        mTranslator = translator;
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mUserDomains = new HashMap();
        mKeyStoreManagers = new HashMap();
        createDefaultTransportSecurityCtx();
        mIsDirty = true;
        mDom = null;
    }
    
    
    
    /**
     * As the name suggests generate a DOM from the Configuration data this object holds.
     *
     * @return a DOM based on the state of this object.
     * @throws JBIException on errors.
     */
    public synchronized org.w3c.dom.Document generateDocument()
        throws JBIException
    {
        if ( !mIsDirty )
        {
            return mDom;
        }
        
        try
        {
            validate();
            
            // -- Create a New Document
            mDom = DocumentBuilderFactory.newInstance().
                newDocumentBuilder().newDocument();
            
            // -- Add the root element
            Element root = mDom.createElement(Constants.ELEMENT_SECURITY_CONFIGURATION);
            mDom.appendChild(root);

            // -- Add the UserDomains
            addUserDomains(mDom);

            // -- Add the KeyStoreManagers
            addKeyStoreManagers(mDom);

            // -- Add the Security Contexts
            addSecurityContexts(mDom);
            
        }
        catch ( Exception ex )
        {
            throw new JBIException(ex);
        }
        return mDom;
    }
    
    /**
     * @param dom Document to which this element is to be added.
     * @throws Exception on errors.
     * 
     */
    private void addUserDomains(Document dom)
        throws Exception
    {
        Element uds =  dom.createElement(Constants.ELEMENT_USER_DOMAINS);
        Element defUd = dom.createElement(Constants.ELEMENT_DEFAULT);
        Text txtDefUd = dom.createTextNode(getDefaultUserDomainName());
        defUd.appendChild(txtDefUd);
        uds.appendChild(defUd);
        
        Iterator itr = getUserDomainContexts().keySet().iterator();
        
        while ( itr.hasNext() )
        {
            Element ud = dom.createElement(Constants.ELEMENT_USER_DOMAIN);
            
            String name = (String) itr.next();        
            Element elName = dom.createElement(Constants.ELEMENT_NAME);
            Text txtName = dom.createTextNode(name);
            elName.appendChild(txtName);
            
            
            Element domain = dom.createElement(Constants.ELEMENT_DOMAIN);
            
            Properties udProps = (Properties) getUserDomainContexts().get(name);
            
            for ( Enumeration e = udProps.propertyNames(); e.hasMoreElements();)
            {

                String key = (String) e.nextElement();
                String value = (String) udProps.getProperty(key);

                if (Constants.DOMAIN.equals(key))
                {
                    Text text = dom.createTextNode(value);
                    domain.appendChild(text);
                    ud.insertBefore( domain, ud.getFirstChild() );
                }
                else
                {
                    addParameter(dom, ud, key, value);
                }
            }
            ud.insertBefore(elName, ud.getFirstChild());
            uds.appendChild(ud);
        }
        dom.getDocumentElement().appendChild(uds);
    }
    
    /**
     * Add a parameter to a Element
     *
     * @param dom is the main Document to which the elements are added.
     * @param parent is the parent element to which the parameter is to be added.
     * @param key is the paramname.
     * @param value is the paramvalue.
     */
    private void addParameter(Document dom, Element parent, String key, String value)
    {
        Element param = dom.createElement(Constants.ELEMENT_PARAM);
        Element paramname = dom.createElement(Constants.ELEMENT_PARAM_NAME);
        Element paramvalue = dom.createElement(Constants.ELEMENT_PARAM_VALUE);
        
        Text txtName = dom.createTextNode(key);
        Text txtValue = dom.createTextNode(value);
        
        paramname.appendChild(txtName);
        paramvalue.appendChild(txtValue);
        
        param.appendChild(paramname);
        param.appendChild(paramvalue);
        
        parent.appendChild(param);
    }
     
    /**
     * @param dom Document to which this element is to be added.
     * @throws Exception on errors
     * 
     */
    private void addKeyStoreManagers(Document dom)
        throws Exception
    {
        Element kms =  dom.createElement(Constants.ELEMENT_KEYSTORE_MANAGERS);
        Element defKm = dom.createElement(Constants.ELEMENT_DEFAULT);
        Text txtDefKm = dom.createTextNode(getDefaultKeyStoreManagerName());
        defKm.appendChild(txtDefKm);
        kms.appendChild(defKm);
        
        Iterator itr = getKeyStoreContexts().keySet().iterator();
        
        while ( itr.hasNext() )
        {
            Element ks = dom.createElement(Constants.ELEMENT_KEYSTORE_MANAGER);
            
            String name = (String) itr.next();        
            Element elName = dom.createElement(Constants.ELEMENT_NAME);
            Text txtName = dom.createTextNode(name);
            elName.appendChild(txtName);
            
            Element manager = dom.createElement(Constants.ELEMENT_MANAGER);
            
            Properties ksProps = (Properties) getKeyStoreContexts().get(name);
            
            for ( Enumeration e = ksProps.propertyNames(); e.hasMoreElements();)
            {

                String key = (String) e.nextElement();
                String value = (String) ksProps.getProperty(key);

                if (Constants.MANAGER.equals(key))
                {
                    Text text = dom.createTextNode(value);
                    manager.appendChild(text);
                    ks.insertBefore(manager, ks.getFirstChild());
                }
                else
                {
                    addParameter(dom, ks, key, value);
                }
            }
            ks.insertBefore( elName, ks.getFirstChild() );
            kms.appendChild(ks);
        }
        dom.getDocumentElement().appendChild(kms);
    }
    
    /**
     * @param dom Document to which this element is to be added.
     */
    public void addSecurityContexts(Document dom)
    {

        Element ts =  dom.createElement(Constants.ELEMENT_TRANSPORT_SECURITY);
        Element server = dom.createElement(Constants.ELEMENT_SERVER);
        Element client = dom.createElement(Constants.ELEMENT_CLIENT);
        
        // -- Add the client Child elements
        client.appendChild(createTextElement(dom, Constants.ELEMENT_SSL_USE_DEFAULT, 
            Boolean.toString(getSSLClientUseDefault())));
        client.appendChild(createTextElement(dom, Constants.ELEMENT_SSL_PROTOCOL, 
            getSSLClientProtocol()));
        client.appendChild(createTextElement(dom, Constants.ELEMENT_SSL_CLIENT_ALIAS, 
            getSSLClientAlias()));
        
        // -- Add the server Child Elements
        server.appendChild(createTextElement(dom, Constants.ELEMENT_SSL_REQ_CLIENT_AUTH, 
            Boolean.toString(getSSLServerRequireClientAuth())));
        
        ts.appendChild(server);
        ts.appendChild(client);
        
        dom.getDocumentElement().appendChild(ts);
        
    }
    
    /**
     * create an Element with the Text value
     *
     * @param dom is the parent document.
     * @param elName is the Element Name.
     * @param textValue is the textValue
     * @return the created Element
     */
    private Element createTextElement(Document dom, String elName, String textValue)
    {
        Element el = dom.createElement(elName);
        Text txt = dom.createTextNode(textValue);
        el.appendChild(txt);
        return el;
    }
    
    
    
    /**
     * Validate the State of this Object.
     *
     * @throws JBIException if the state is invalid. The Message in the exception
     * should give an indication of the errorenous state.
     */
    public void validate()
        throws JBIException
    {
        // -- There should be atleast one of each User Domain, KeyStore Manager 
        // -- && Security Context defined.
        if ( mUserDomains.size() < 1 )
        {
            String msg = mTranslator.getString(
                LocalStringConstants.BC_ERR_MISSING_CONFIG_ELEMENT,
                    mTranslator.getString(LocalStringConstants.CONST_UD));
            throw new JBIException(msg);
        }
        
        if ( mKeyStoreManagers.size() < 1 )
        {
            String msg = mTranslator.getString(
                LocalStringConstants.BC_ERR_MISSING_CONFIG_ELEMENT,
                    mTranslator.getString(LocalStringConstants.CONST_KM));
            throw new JBIException(msg);
        }
        
        // -- Check if all the defaults exist.
        if ( !mUserDomains.containsKey(getDefaultUserDomainName()))
        {
            String msg = mTranslator.getString(
                LocalStringConstants.BC_ERR_DEFAULT_CONFIG_ELEMENT_MISSING,
                    mTranslator.getString(LocalStringConstants.CONST_UD), 
                        getDefaultUserDomainName());
            throw new JBIException(msg);
        }
        
        if ( !mKeyStoreManagers.containsKey(getDefaultKeyStoreManagerName()))
        {
            String msg = mTranslator.getString(
                LocalStringConstants.BC_ERR_DEFAULT_CONFIG_ELEMENT_MISSING,
                    mTranslator.getString(LocalStringConstants.CONST_KM), 
                        getDefaultKeyStoreManagerName());
            throw new JBIException(msg);
        }
        
    }
    
    
    /*-------------------------------------------------------------------------------*\
     *                     Accessors and Mutators                                    *
    \*-------------------------------------------------------------------------------*/
    
    // -- Set the Defaults
    
    /**
     * @param defUserDomain is the default User Domain.
     */
    public synchronized void setDefaultUserDomainName(String defUserDomain) 
    {
        mIsDirty = true;
        mDefUD = defUserDomain;
    }
    
    /**
     * @param defKSMgr is the default KeyStore Manager.
     */
    public synchronized void setDefaultKeyStoreManagerName(String defKSMgr) 
    {
        mIsDirty = true;
        mDefKSMgr = defKSMgr;
    }
    
    /**
     * Add a new User Domain. If a User Domain by the specified name exists, it is
     * overwritten with the new one.
     *
     * @param name of the domain.
     * @param domain is the implementation class that implements the UserDomain interface.
     */
    public synchronized void addUserDomain(String name, String domain)
    {
        mIsDirty = true;
        Properties props = new Properties();
        props.setProperty(Constants.DOMAIN, domain);

        mUserDomains.put(name, props);
    }
    
    /**
     * Remove a User Domain. 
     *
     * @param name of the domain.
     */
    public synchronized void removeUserDomain(String name)
    {
        mIsDirty = true;
        mUserDomains.remove(name);
    }
    /**
     * Add a parameter to a User Domain, if the domain does not exist, the parameter is
     * not added.
     *
     * @param domain name of the UserDomain.
     * @param name is the name of the parameter.
     * @param value is the value for the parameter.
     */
    public synchronized void addParameterToUserDomain(String domain,    
        String name, String value)
    {
        mIsDirty = true;
        if ( mUserDomains.containsKey(domain) )
        {
            Properties props = (Properties) mUserDomains.get(domain);
            props.setProperty(name, value);
            mUserDomains.put(domain, props);
        }
        else
        {
            mLogger.warning( mTranslator.getString(
                LocalStringConstants.BC_WRN_FAILED_TO_ADD_PARAM, 
                new String[]{
                    name, value,
                    mTranslator.getString(LocalStringConstants.CONST_UD), domain, }));
        }
    }
    
    /**
     * Remove a parameter to a User Domain.
     *
     * @param domain name of the UserDomain.
     * @param name is the name of the parameter.
     */
    public synchronized void removeParameterFromUserDomain(String domain,    
        String name)
    {
        mIsDirty = true;
        if ( mUserDomains.containsKey(domain) )
        {
            Properties props = (Properties) mUserDomains.get(domain);
            props.remove(name);
        }
    }
    
    /**
     * Add a new KeyStore Manager. If a KeyStoreManager by the specified name exists, 
     * it is overwritten with the new one.
     *
     * @param name of the manager.
     * @param manager is the implementation class that implements the KeyStoreManager 
     * interface.
     */
    public synchronized void addKeyStoreManager(String name, String manager)
    {
        mIsDirty = true;
        Properties props = new Properties();
        props.setProperty(Constants.MANAGER, manager);
        
        mKeyStoreManagers.put(name, props);
    }
    
    /**
     * Remove a KeyStore Manager. 
     *
     * @param name of the manager.
     */
    public synchronized void removeKeyStoreManager(String name)
    {
        mIsDirty = true;
        mKeyStoreManagers.remove(name);
    }
    
    /**
     * Add a parameter to a KeyStoreManager, if it does not exist, the parameter is
     * not added.
     *
     * @param manager is the name of the KeyStoreManager.
     * @param name is the name of the parameter.
     * @param value is the value for the parameter.
     */
    public synchronized void addParameterToKeyStoreManager(String manager, String name, 
        String value)
    {
        mIsDirty = true;
        if ( mKeyStoreManagers.containsKey(manager) )
        {
            Properties props = (Properties) mKeyStoreManagers.get(manager);
            props.setProperty(name, value);
        }
        else
        {
            mLogger.warning( mTranslator.getString(
                LocalStringConstants.BC_WRN_FAILED_TO_ADD_PARAM, 
                new String[]{
                    name, value,
                    mTranslator.getString(LocalStringConstants.CONST_KM), manager, }));
        }
    }
    
    /**
     * Remove a parameter from the KeyStoreManager.
     *
     * @param manager is the name of the KeyStoreManager.
     * @param name is the name of the parameter.
     */
    public synchronized void removeParameterFromKeyStoreManager(String manager, 
        String name)
    {
        mIsDirty = true;
        if ( mKeyStoreManagers.containsKey(manager) )
        {
            Properties props = (Properties) mKeyStoreManagers.get(manager);
            props.remove(name);
        }
    }            
    
    // -- Accessors
    
    /**
     * @return the default User Domain name.
     */
    public String getDefaultUserDomainName() 
    {
        return mDefUD;
    }
    
    /**
     * @return the default KeyStore Manager.
     */
    public String getDefaultKeyStoreManagerName() 
    {
        return mDefKSMgr;
    }

    /**
     * Get a Map of User Domain Contexts by their name.
     * [ Key = Name (string) : Value = UserDomain Contexts (Properties) ]
     * @return a Map of User Domain Contexts by their name
     */
    public HashMap getUserDomainContexts()
    {
        return mUserDomains;
    }


    /**
     * Get a Map of KeyStore Services keyed by their name.
     *
     * [ Key = Name (string) : Value = KeyStoreContexts (Properties) ]
     * @return a Map of KeyStore Manager keyed by their name.
     */
    public HashMap getKeyStoreContexts()
    {
        return mKeyStoreManagers;
    }
    
    /*---------------------------------------------------------------------------------*\
     *                     Create a Security Config from the dom                       *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Constructor to create SecurityConfiguration from DOM.
     *
     * @param translator is the StringTranslator
     * @param dom is the dom for the Security Config data.
     * @throws JBIException on Errors.
     */
    public SecurityInstallConfig(Document dom, StringTranslator translator)
        throws JBIException
    {
        mTranslator = translator;
        mLogger = Logger.getLogger(
            com.sun.jbi.internal.security.Constants.PACKAGE);
        mUserDomains = new HashMap();
        mKeyStoreManagers = new HashMap();
        createDefaultTransportSecurityCtx();
        createSecurityConfiguration(dom);
        mDom = dom;
        mIsDirty = false;
    }

    /** 
     * Create a TransportSecurityContext with default values.
     * 
     */
    private void createDefaultTransportSecurityCtx()
    {
        mTransportSecurityContext = new Properties();
        
        setSSLClientAlias(HttpConstants.DEFAULT_CLIENT_ALIAS);
        setSSLClientUseDefault(true);
        setSSLClientProtocol(HttpConstants.DEFAULT_SSL_PROTOCOL);
        
        setSSLServerRequireClientAuth(HttpConstants.DEFAULT_SSL_REQ_CLIENT_AUTH);
    }
    
    /**
     * Create a SecurityConfiguration object from the dom.
     *
     * @param dom is the dom for the Security Config data.
     * @throws JBIException on Errors.
     */
    public void createSecurityConfiguration(Document dom)
        throws JBIException
    {     
        getUserDomainsFromDom(this, dom.getDocumentElement());
        getKeyStoreManagersFromDom(this, dom.getDocumentElement());
        getTransportSecurityContextFromDom(this, dom.getDocumentElement());  
    }
    
    
    /**
     * @param config is the SecurityInstallConfig to update.
     * @param parent is the parent SecurityConfiguration element for the data.
     * @throws JBIException on Errors
     */
    private void getUserDomainsFromDom(SecurityInstallConfig config, Element parent)
        throws JBIException
    {
        try
        {
            // -- Should use XPath stuff from J2SE 1.5
            Element uds = (Element) 
                parent.getElementsByTagName(Constants.ELEMENT_USER_DOMAINS).item(0);

            config.setDefaultUserDomainName( ( (Text) 
                    uds.getElementsByTagName(Constants.ELEMENT_DEFAULT).item(0).
                        getFirstChild()).getData() );

            NodeList udList = uds.getElementsByTagName(Constants.ELEMENT_USER_DOMAIN);

            for ( int i = 0; i < udList.getLength(); i++ )
            {
                Element ud = (Element) udList.item(i);
                String name = ( (Text) 
                    ud.getElementsByTagName(Constants.ELEMENT_NAME).item(0).
                        getFirstChild()).getData();
                String domain = ( (Text) 
                    ud.getElementsByTagName(Constants.ELEMENT_DOMAIN).item(0).
                        getFirstChild()).getData();
                config.addUserDomain (name,  domain);
                
                NodeList params = ud.getElementsByTagName(Constants.ELEMENT_PARAM);
                
                for ( int p = 0; p < params.getLength(); p++ )  
                {
                    Element param = (Element) params.item(0);
                    String paramname = ( (Text) param.getElementsByTagName(
                            Constants.ELEMENT_PARAM_NAME).item(0).
                                getFirstChild()).getData();
                    String paramvalue = ( (Text) param.getElementsByTagName(
                            Constants.ELEMENT_PARAM_VALUE).item(0).
                                getFirstChild()).getData();
                    
                    config.addParameterToUserDomain (name, paramname, paramvalue);
                }
            }
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.toString(), ex);
        }  
    }
    
    /**
     * @param config is the SecurityInstallConfig to update.
     * @param parent is the parent SecurityConfiguration element for the data.
     * @throws JBIException on Errors
     */
    private void getKeyStoreManagersFromDom(SecurityInstallConfig config, Element parent)
        throws JBIException
    {
        try
        {
            // -- Should use XPath stuff from J2SE 1.5
            Element kms = (Element) 
                parent.getElementsByTagName(Constants.ELEMENT_KEYSTORE_MANAGERS).item(0);

            config.setDefaultKeyStoreManagerName( ( (Text) 
                    kms.getElementsByTagName(Constants.ELEMENT_DEFAULT).item(0).
                        getFirstChild()).getData() );

            NodeList kmList = kms.getElementsByTagName(
                Constants.ELEMENT_KEYSTORE_MANAGER);

            for ( int i = 0; i < kmList.getLength(); i++ )
            {
                Element km = (Element) kmList.item(i);
                String name = ( (Text) 
                    km.getElementsByTagName(Constants.ELEMENT_NAME).item(0).
                        getFirstChild()).getData();
                String manager = ( (Text) 
                    km.getElementsByTagName(Constants.ELEMENT_MANAGER).item(0).
                        getFirstChild()).getData();
                config.addKeyStoreManager(name,  manager);
                
                NodeList params = km.getElementsByTagName(Constants.ELEMENT_PARAM);
                
                for ( int p = 0; p < params.getLength(); p++ )  
                {
                    Element param = (Element) params.item(0);
                    String paramname = ( (Text) param.getElementsByTagName(
                            Constants.ELEMENT_PARAM_NAME).item(0).
                                getFirstChild()).getData();
                    String paramvalue = ( (Text) param.getElementsByTagName(
                            Constants.ELEMENT_PARAM_VALUE).item(0).
                                getFirstChild()).getData();
                    
                    config.addParameterToKeyStoreManager(name, paramname, paramvalue);
                }
            }
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.toString(), ex);
        }  
    }
    
    
    /**
     * @param config is the SecurityInstallConfig to update.
     * @param parent is the parent SecurityConfiguration element for the data.
     * @throws JBIException on Errors
     */
    private void getTransportSecurityContextFromDom(SecurityInstallConfig config, 
        Element parent)
        throws JBIException
    {
        try
        {
            // -- Should use XPath stuff from J2SE 1.5
            Element transportSec = (Element) parent.getElementsByTagName(
                Constants.ELEMENT_TRANSPORT_SECURITY).item (0);

            if ( transportSec != null )
            {
                Element client = (Element) transportSec.getElementsByTagName(
                    Constants.ELEMENT_CLIENT).item(0);

                // -- SSL Protocol value
                Element sslProtocol = (Element) client.getElementsByTagName(
                    Constants.ELEMENT_SSL_PROTOCOL).item(0);
                if ( sslProtocol != null )
                {
                    setSSLClientProtocol(((Text) sslProtocol.getFirstChild()).getData());
                }

                // -- Use Default
                Element useDef = (Element) client.getElementsByTagName(
                    Constants.ELEMENT_SSL_USE_DEFAULT).item(0);
                if ( useDef != null )
                {
                    setSSLClientUseDefault(
                        new Boolean(((Text) useDef.getFirstChild()).getData())
                            .booleanValue());
                }

                // -- Client Alias
                Element clientAlias = (Element) client.getElementsByTagName(
                    Constants.ELEMENT_SSL_CLIENT_ALIAS).item(0);
                if ( clientAlias != null )
                {
                    setSSLClientAlias(((Text) clientAlias.getFirstChild()).getData());
                }

                Element server = (Element) transportSec.getElementsByTagName(
                    Constants.ELEMENT_SERVER).item(0);

                if ( server != null )
                {
                    // -- Require Client Auth value
                    Element reqClientAuth = (Element) server.getElementsByTagName(
                        Constants.ELEMENT_SSL_REQ_CLIENT_AUTH).item(0);
                    if ( reqClientAuth != null )
                    {
                        setSSLServerRequireClientAuth(
                            new Boolean(((Text) reqClientAuth.getFirstChild()).getData())
                                .booleanValue());
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.toString(), ex);
        }  
        
    }
    
    /*---------------------------------------------------------------------------------*\
     *   Helper Methods which aid in the creation of a                                 *
     *   SecurityInstallConfig from properties and vice-versa                          *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Create a SecurityInstallConfig from a GenericConfigurationMBean.
     * @param configMBean is the GenericConfigurationMBean which has the Properties 
     * to be used for the creation of the SecurityInstallConfig object
     * @param translator is the StringTranslator
     * @throws JBIException if the configuration data is not sufficient/valid to
     * create the SecurityConfiguration information.
     * @return the created SecurityInstallConfig
     */
    public static SecurityInstallConfig createSecurityInstallConfig(
        GenericConfigurationMBean configMBean, StringTranslator translator)
        throws JBIException
    {
        SecurityInstallConfig secInstCfg = new SecurityInstallConfig(translator);
        
        secInstCfg.setDefaultUserDomainName(configMBean.getProperty(DEF_UD_KEY));
        secInstCfg.setDefaultKeyStoreManagerName(configMBean.getProperty(DEF_KM_KEY));
        
        secInstCfg.updateUserDomains(configMBean);
        secInstCfg.updateKeyStoreManagers(configMBean);
        secInstCfg.updateTransportSecurityContext(configMBean);
        secInstCfg.validate();
        
        return secInstCfg;
    }
    
    /**
     * @param configMBean is the GenericConfigurationMBean which has the cofig data
     */
    private void updateUserDomains(GenericConfigurationMBean configMBean)
    {
        mIsDirty = true;
        for ( int i = 1; true; i++ )
        {
            // -- Get all the parameters for a domain
            String currentDomainKey = UD_PREFIX + i;
           
            Properties udProps = SecurityInstallConfig.filterProperties(
                Pattern.compile(UD_PATTERN + i + DOT_PATTERN + ANY_PATTERN), 
                    configMBean);
            
            if ( udProps.size() == 0 )
            {
                break;
            }          

            // -- Create the UserDomain entry
            String name = configMBean.getProperty(currentDomainKey + NAME_SUFFIX);
            String domain = configMBean.getProperty(currentDomainKey + DOMAIN_SUFFIX);
            
            addUserDomain(name, domain);

            // -- Add parameters to the entry if any are specified
            String udParamKey = currentDomainKey + PARAM_SUFFIX;
            for ( int j = 1; true; j++ )
            {
                String currentUdParamKey = udParamKey + j;
                
                Properties udParams = SecurityInstallConfig.filterProperties(
                    Pattern.compile(UD_PATTERN + i + DOT_PATTERN + ANY_PATTERN 
                        + PARAM_PATTERN + j + DOT_PATTERN + ANY_PATTERN), 
                            udProps);
                
                if ( (udParams.size() == 0) || (udParams.size() != 2))
                {
                    break;
                }
                
                
                addParameterToUserDomain (name, 
                    configMBean.getProperty(currentUdParamKey + NAME_SUFFIX), 
                    configMBean.getProperty(currentUdParamKey + VALUE_SUFFIX)); 
            }
        }
        
    }
    
    /**
     * @param configMBean is the GenericConfigurationMBean which has the cofig data
     * @throws JBIException on errors
     */
    private void updateKeyStoreManagers(GenericConfigurationMBean configMBean)
        throws JBIException
    {
        mIsDirty = true;
        for ( int i = 1; true; i++ )
        {
            // -- Get all the parameters for a domain
            String currentManagerKey = KM_PREFIX + i;

            Properties kmProps = SecurityInstallConfig.filterProperties(
                Pattern.compile(KM_PATTERN + i + DOT_PATTERN + ANY_PATTERN), 
                    configMBean);

            if ( kmProps.size() == 0 )
            {
                break;
            }


            // -- Create the UserDomain entry
            String name = configMBean.getProperty(currentManagerKey + NAME_SUFFIX);
            String manager = configMBean.getProperty(currentManagerKey 
                + MANAGER_SUFFIX);
            
            addKeyStoreManager(name, manager);

            // -- Add parameters to the entry if any are specified
            String kmParamKey = currentManagerKey + PARAM_SUFFIX;
            for ( int j = 1; true; j++ )
            {
                String currentKmParamKey = kmParamKey + j;
                
                Properties kmParams = SecurityInstallConfig.filterProperties(
                    Pattern.compile(KM_PATTERN + i + DOT_PATTERN + ANY_PATTERN
                        + PARAM_PATTERN + j + DOT_PATTERN + ANY_PATTERN), 
                            kmProps);
                
                if ( (kmParams.size() == 0) || (kmParams.size() != 2))
                {
                    break;
                }
                
                addParameterToKeyStoreManager(name, 
                    configMBean.getProperty(currentKmParamKey + NAME_SUFFIX), 
                    configMBean.getProperty(currentKmParamKey + VALUE_SUFFIX)); 
            }
        }  
    }
    
    /**
     * @param configMBean is the GenericConfigurationMBean which has the cofig data
     */
    private void updateTransportSecurityContext(GenericConfigurationMBean configMBean)
    {
        mIsDirty = true;
        if ( !mMissingProperty.equals(configMBean.getProperty(TRANSPORT_PREFIX +
            HttpConstants.PARAM_SSL_CLIENT_ALIAS)) )
        {
            setSSLClientAlias(configMBean.getProperty(TRANSPORT_PREFIX +
                HttpConstants.PARAM_SSL_CLIENT_ALIAS));
        }
        
        if ( !mMissingProperty.equals(configMBean.getProperty(TRANSPORT_PREFIX +
            HttpConstants.PARAM_SSL_USE_DEFAULT)) )
        {
            setSSLClientUseDefault(new Boolean(configMBean.getProperty(TRANSPORT_PREFIX +
                        HttpConstants.PARAM_SSL_USE_DEFAULT)).booleanValue());
        }
        
        if ( !mMissingProperty.equals(configMBean.getProperty(TRANSPORT_PREFIX +
            HttpConstants.PARAM_SSL_PROTOCOL)) )
        {
            setSSLClientProtocol(configMBean.getProperty(TRANSPORT_PREFIX +
                HttpConstants.PARAM_SSL_PROTOCOL));
        }
        
        if ( !mMissingProperty.equals( configMBean.getProperty(TRANSPORT_PREFIX +
            HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH )) )
        {
            setSSLServerRequireClientAuth(new Boolean(
                    configMBean.getProperty(TRANSPORT_PREFIX +
                        HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH)).booleanValue());
        }
    }
    
    /**
     * Filter Property keys with the specified prefix.
     *
     * @param pattern is the pattern to be matched
     * @param configMBean is the source of the Properties
     * @return filtered Properties which start with the specified pattern.
     */
    public static Properties filterProperties(Pattern pattern, 
        GenericConfigurationMBean configMBean)
    {
        Properties props = new Properties();
        
        String[] keys = configMBean.getPropertyKeys();
        for ( int i = 0; i < keys.length; i++ )
        {
            if ( pattern.matcher(keys[i]).matches() )
            {
                props.setProperty(keys[i], configMBean.getProperty(keys[i]));
            }
        }
        return props;
    }
    
    /**
     * Filter Property keys with the specified prefix.
     *
     * @param pattern is the pattern to be matched.
     * @param srcProps are the Properties to search for a match.
     * @return filtered Properties which start with the specified pattern.
     */
    public static Properties filterProperties(Pattern pattern, 
        Properties srcProps)
    {
        Properties props = new Properties();
        
        
        for ( Enumeration keys = srcProps.propertyNames(); keys.hasMoreElements();)
        {
            String key = (String) keys.nextElement();
            if ( pattern.matcher(key).matches() )
            {
                props.setProperty(key, srcProps.getProperty(key));
            }
        }
        return props;
    }
    
    /**
     * Convert this SecurityInstallConfig instance to GenericConfigurationMBean 
     * Properties. This would ideally be called before the Configuration MBean is asked 
     * to save the data or before a the getPropertyKeys() is invoked.
     *
     * @param configMBean is the GenericConfigurationMBean which is to be updated.
     */
    public void updateConfigMBean(GenericConfigurationMBean configMBean)
    {  
        configMBean.setProperty(DEF_UD_KEY, getDefaultUserDomainName());
        configMBean.setProperty(DEF_KM_KEY, getDefaultKeyStoreManagerName());
        
        updateUserDomainProperties(configMBean);
        updateKeyStoreManagerProperties(configMBean);
        updateTransportSecurityCtxProperties(configMBean);
        
    }
    
    /**
     * @param configMBean is the Configuration MBean to be updated.
     */
    public void updateUserDomainProperties(GenericConfigurationMBean configMBean)
    {
        HashMap uds = getUserDomainContexts();
        
        int i = 1;
        for ( Iterator udNames = uds.keySet().iterator(); udNames.hasNext(); i++)
        {
            String currentUdPattern = UD_PREFIX + i;
            String udName = (String) udNames.next();
            configMBean.setProperty(currentUdPattern + NAME_SUFFIX, udName );
            Properties udProps = (Properties) uds.get( udName );
            
            int j = 1;
            for ( Enumeration params = udProps.propertyNames(); params.hasMoreElements();)
            {
                String paramname = (String) params.nextElement();
                String paramvalue =  udProps.getProperty(paramname);
                
                if ( paramname.equals(Constants.DOMAIN) )
                {
                    configMBean.setProperty(currentUdPattern + DOMAIN_SUFFIX, paramvalue);
                }
                else
                {
                    configMBean.setProperty(currentUdPattern + PARAM_SUFFIX + j 
                        + NAME_SUFFIX, paramname);
                    configMBean.setProperty(currentUdPattern + PARAM_SUFFIX + j++
                        + VALUE_SUFFIX, paramvalue);  
                }  
            }
        }   
    }
       
    /**
     * @param configMBean is the Configuration MBean to be updated.
     */
    public void updateKeyStoreManagerProperties(GenericConfigurationMBean configMBean)
    {
        HashMap kms = getKeyStoreContexts();
        
        int i = 1;
        for ( Iterator kmNames = kms.keySet().iterator(); kmNames.hasNext(); i++)
        {
            String currentKmPattern = KM_PREFIX + i;
            String kmName = (String) kmNames.next();
            configMBean.setProperty(currentKmPattern + NAME_SUFFIX, kmName );
            Properties kmProps = (Properties) kms.get( kmName );
            
            int j = 1;
            for ( Enumeration params = kmProps.propertyNames(); params.hasMoreElements();)
            {
                String paramname = (String) params.nextElement();
                String paramvalue =  kmProps.getProperty(paramname);
                
                if ( paramname.equals(Constants.MANAGER) )
                {
                    configMBean.setProperty(currentKmPattern + MANAGER_SUFFIX, 
                        paramvalue);
                }
                else
                {
                    configMBean.setProperty(currentKmPattern + PARAM_SUFFIX + j 
                        + NAME_SUFFIX, paramname);
                    configMBean.setProperty(currentKmPattern + PARAM_SUFFIX + j++
                        + VALUE_SUFFIX, paramvalue);  
                }  
            }
        }   
    }
    
           
    /**
     * @param configMBean is the Configuration MBean to be updated.
     */
    public void updateTransportSecurityCtxProperties(
        GenericConfigurationMBean configMBean)
    {
        
        configMBean.setProperty(TRANSPORT_PREFIX + HttpConstants.PARAM_SSL_CLIENT_ALIAS,
            getSSLClientAlias());
        configMBean.setProperty(TRANSPORT_PREFIX + HttpConstants.PARAM_SSL_PROTOCOL,
            getSSLClientProtocol());
        configMBean.setProperty(TRANSPORT_PREFIX + HttpConstants.PARAM_SSL_USE_DEFAULT,
            Boolean.toString((getSSLClientUseDefault())));
        
        configMBean.setProperty(
            TRANSPORT_PREFIX + HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH,
                Boolean.toString(getSSLServerRequireClientAuth()));
    }
    
   /*-------------------------------------------------------------------------------*\
    *         Operations to display Configuration information                       *
    * The GenericConfigurationMBean delegates all queries for information to this   *
    * class, the following methods return an XML string for the config information. *
   \*-------------------------------------------------------------------------------*/

    /**
     * Get an XML String which has the meta-data for a particular user domain
     * User Domain.
     *
     * @param name is the logical name of the UserDomain. If there is no such UserDomain
     * or the name is null then a empty string "" is returned.
     * @return an XML String with information for a particular UserDomain.
     */
    public String getUserDomainCtx(String name)
    {
        if ( name == null || "".equals(name) )
        {
            return "";
        }
        
        return getNamedNodeString(Constants.ELEMENT_USER_DOMAINS, 
            Constants.ELEMENT_USER_DOMAIN, name.trim());   
    }
    
    /**
     * Get an XML String listing all the UserDomains and their meta-data.
     *
     * @return an XML String with information for all the UserDomains.
     */
    public String getUserDomainCtxs()
    {
        
        try
        {
            Node uds = generateDocument().getElementsByTagName(
                Constants.ELEMENT_USER_DOMAINS).item(0);
            
            return getString(uds);
            
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    
    /**
     * Get an XML String which has the meta-data for a particular key store manager.
     *
     * @param name is the logical name of the KeyStoreManager. If there is no such 
     * KeyStoreManager or the name is null then a empty string "" is returned.
     * @return an XML String with information for a particular KeyStoreManager.
     */
    public String getKeyStoreCtx(String name)
    {
        if ( name == null || "".equals(name) )
        {
            return "";
        }
        
        return getNamedNodeString(Constants.ELEMENT_KEYSTORE_MANAGERS, 
            Constants.ELEMENT_KEYSTORE_MANAGER, name.trim());   
    }
    
    
    /**
     * Get an XML String listing all the KeyStoreManagers and their meta-data.
     *
     * @return an XML String with information for all the KeyStoreManagers.
     */
    public String getKeyStoreCtxs()
    {
        try
        {
            Node kms = generateDocument().getElementsByTagName(
                Constants.ELEMENT_KEYSTORE_MANAGERS).item(0);
            
            return getString(kms);
            
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    
    /**
     * Get an XML String for the TransportSecurityContext.
     *
     * @return an XML String with information for the TransportSecurity Context.
     */
    public String getTransportSecurityConfig()
    {
        try
        {
            Node ts = generateDocument().getElementsByTagName(
                Constants.ELEMENT_TRANSPORT_SECURITY).item(0);
            
            return getString(ts);
            
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    
    
    /**
     * @return an XML String for the dom rooted at the given node
     * @param node is the root node for the XML document.
     */
    private String getString(Node node)
    {
        java.io.StringWriter strWriter = new java.io.StringWriter();
        try
        { 
            Transformer xformer = TransformerFactory.newInstance().newTransformer();
            xformer.setOutputProperty(javax.xml.transform.OutputKeys.INDENT, "yes");
            xformer.transform( new javax.xml.transform.dom.DOMSource(node), 
                new javax.xml.transform.stream.StreamResult(strWriter));
        }
        catch (Exception ex)
        {
            return "";
        }
        return strWriter.toString();
    }
    
    /**
     *
     * @param collectionElementName is the collections element ( i.e User Domains, 
     * Key Store Managers, Security Contexts ).
     * @param elementName is the name of the sub-element ( User Domain, KeyStoreManager,
     * Security Context ).
     * @param searchName is the desired element name .
     * @return an XML String for the element whose name attribute is equal to the 
     * specified name.
     * the named element
     */
    private String getNamedNodeString(String collectionElementName,  String elementName,
        String searchName)
    {
        try
        {
            Element main = (Element) generateDocument().getElementsByTagName(
                collectionElementName).item(0);
            
            NodeList ctxs = main.getElementsByTagName(elementName);
            
            for ( int i = 0; i < ctxs.getLength(); i++ )
            {
                Node name = ( (Element) ctxs.item(i)).
                    getElementsByTagName(Constants.ELEMENT_NAME).item(0);
                if ( searchName.equals( ( (Text) name.getFirstChild()).getData() ) )
                {
                    return getString(ctxs.item(i));
                }
            }  
            return "";
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    
    // -- Set / Get the Client Alias from the TransportSecurity Context.
    /**
     * @param alias is the SSL Client alias.
     */
    public synchronized void setSSLClientAlias (java.lang.String alias)
    {
        mTransportSecurityContext.setProperty(
            HttpConstants.PARAM_SSL_CLIENT_ALIAS,
            alias);
        
    }  
    
    /**
     * @return the SSL Client alias.
     */
    public String getSSLClientAlias ()
    {
        return mTransportSecurityContext.getProperty(
            HttpConstants.PARAM_SSL_CLIENT_ALIAS);
    }   
    
    // -- Set / Get the Client SSL Protocol  from the TransportSecurity Context.
    /**
     * @param protocol is the SSL Client protocol, allowed values are SSLv3, 
     * TLS and TLSv1
     */
    public synchronized void setSSLClientProtocol (String protocol)
    {
        if ( protocol == null )
        {
            throw new IllegalArgumentException(protocol);
        }
        
        protocol = protocol.trim();
        
        if (HttpConstants.SSLV3.equals(protocol) ||
            HttpConstants.TLSV1.equals(protocol) ||
            HttpConstants.TLS.equals(protocol))
        {
            mTransportSecurityContext.setProperty(
                HttpConstants.PARAM_SSL_PROTOCOL,
                protocol);
        }
        else
        {
            throw new IllegalArgumentException(protocol);
        }   
    }  
    
    /**
     * @return the SSL Client Protocol.
     */
    public String getSSLClientProtocol()
    {
         return mTransportSecurityContext.getProperty(
             HttpConstants.PARAM_SSL_PROTOCOL);
    }   
    
    // -- Get / Set the SSL Client Use Default parameter
    
    /**
     * @param flag - true/false inducates whether to use the
     * default Application Server SSL context or not.
     */
    public synchronized void setSSLClientUseDefault(boolean flag)
    {
        mTransportSecurityContext.setProperty(
                HttpConstants.PARAM_SSL_USE_DEFAULT,
                Boolean.toString(flag));
    }
    
    /**
     * @return true/false inducates whether the
     * default Application Server SSL context is being used or not.
     */
    public boolean getSSLClientUseDefault()
    {
        return new Boolean(mTransportSecurityContext.getProperty(
                HttpConstants.PARAM_SSL_USE_DEFAULT)).booleanValue();
    }
    
    // -- Set / Get the SSL Server Req. Client Auth value
    
    /**
     * @param flag - true/false inducates whether Client Auth 
     * is required by default.
     */
    public synchronized void setSSLServerRequireClientAuth(boolean flag)
    {
        mTransportSecurityContext.setProperty(
                HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH,
                Boolean.toString(flag));
    }
    
    /**
     * @return true/false inducates whether Client Auth 
     * is required by default.
     */
    public boolean getSSLServerRequireClientAuth()
    {
        return new Boolean(mTransportSecurityContext.getProperty(
                HttpConstants.PARAM_SSL_REQ_CLIENT_AUTH)).booleanValue();
    }
    
    /**
     *
     * @return the Transport Security Properties as a Property Set.
     */
    public Properties getTransportSecurityContext ()
    {
        return mTransportSecurityContext;
    }
    
}
