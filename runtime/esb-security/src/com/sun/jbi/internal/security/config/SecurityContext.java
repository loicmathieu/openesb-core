/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SecurityContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.internal.security.config;

/**
 * The SecurityContext interface provides access to the Security Information configured
 * for an endpoint at deployment time.
 *
 * @author Sun Microsystems, Inc.
 */
public interface SecurityContext
    extends com.sun.jbi.binding.security.Context
{
    
    /**
     * Get the name of the User Domain to use with this Security Configuration.
     *
     * @return the name of the User Domain, if this parameter is missing in
     * the deployment security configuration an empty string is returned "".
     */
    String getUserDomainName();
    
    /**
      Get the name of the KeyStoreManager to use with this Security Configuration.
     *
     * @return the name of the User Domain, if the this parameter is missing in
     * the deployment security configuration an empty string is returned "".
     */
    String getKeyStoreManagerName();
    
    /**
     * Get the Provider Id. The Provider Id identifies the Message Security Handler
     * for the Endpoint.
     *
     * @return the Message Provider Id.
     */
    String getMessageProviderId ();
    
}
