/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)Include.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3.ns.wsdl.IncludeType;

/**
 * Abstract implementation of
 * WSDL 2.0 Include component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class Include extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.Include
{
    /**
     * Get the Xml bean for this component.
     *
     * @return The Xml bean for this component.
     */
    protected final IncludeType getBean()
    {
        return (IncludeType) this.mXmlObject;
    }

    /**
     * Construct an abstract Include implementation base component.
     * 
     * @param bean      The XML bean for this Include component
     */
    Include(IncludeType bean)
    {
        super(bean);
    }

    /**
     * Get the description included by this component
     *
     * @return Description from included by this component, if any
     */
    public abstract com.sun.jbi.wsdl2.Description getDescription();

    /**
     * Get the definitions included by this component
     *
     * @return Definitions from included by this component, if any
     */
    public abstract com.sun.jbi.wsdl2.Definitions getDefinitions();


}

// End-of-file: Include.java
