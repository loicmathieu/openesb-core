/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InterfaceFault.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import org.w3.ns.wsdl.InterfaceFaultType;

/**
 * Abstract implementation of
 * WSDL 2.0 Interface Fault component.
 *
 * @author Sun Microsystems, Inc.
 */
abstract class InterfaceFault extends ExtensibleDocumentedComponent
    implements com.sun.jbi.wsdl2.InterfaceFault
{
    /**
     * Get the XML bean for this interface fault component.
     *
     * @return The definitions XML bean for this component.
     */
    protected final InterfaceFaultType getBean()
    {
        return (InterfaceFaultType) this.mXmlObject;
    }

    /**
     * Construct an abstract Interface Fault implementation base component.
     * 
     * @param bean The XML bean for this Interface Fault component
     */
    InterfaceFault(InterfaceFaultType bean)
    {
        super(bean);
    }

}

// End-of-file: InterfaceFault.java
