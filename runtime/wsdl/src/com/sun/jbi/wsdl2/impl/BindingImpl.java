/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)BindingImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import java.io.StringWriter;

import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlOptions;

import org.w3.ns.wsdl.BindingFaultType;
import org.w3.ns.wsdl.BindingType;

import org.w3c.dom.DocumentFragment;

/**
 * Implementation of WSDL 2.0 Binding component.
 * 
 * @author Sun Microsystems, Inc.
 */
public final class BindingImpl extends Binding
{
    /** The definitions component this binding belongs to */
    private DescriptionImpl   mContainer;
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Construct a binding component implementation object from the given XML
     * bean.
     * 
     * @param bean The binding XML bean to use to construct this component.
     * @param defs The container for this binding component.
     */
    private BindingImpl(BindingType bean, DescriptionImpl defs)
    {
        super(bean);

        this.mContainer = defs;
    }
  
    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(BindingType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     * <p>
     * Implementation note: since this method is declared in the public API
     * <code>interface</code>, it has to be a member, not static. We delegate
     * to a class method to do the actual work.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }

    /**
     * Get the target namespace of this binding.
     * 
     * @return The target namespace of this binding's container.
     */
    public String getTargetNamespace()
    {
        return this.mContainer.getTargetNamespace();
    }
  
    /**
     * Get the qualified name of this binding component.
     * 
     * @return The qualified name of this binding component.
     */
    public QName getQName()
    {
        return new QName(this.mContainer.getTargetNamespace(), getName());
    }
  
    /**
     * Get interface being bound by this binding definition, if any.
     *
     * @return Interface being bound by this binding definition, if any
     */
    public com.sun.jbi.wsdl2.Interface getInterface()
    {
        return this.mContainer.findInterface(getBean().getInterface());
    }

    /**
     * Set interface being bound by this binding definition, if any.
     *
     * @param theInterface Interface being bound by this binding definition, if any
     */
    public void setInterface(com.sun.jbi.wsdl2.Interface theInterface)
    {
        if (theInterface != null)
        {
            getBean().setInterface(theInterface.getQName());
        }
        else
        {
            getBean().setInterface(null);
        }
    }

    /**
     * Get the number of BindingFault items in faults.
     *
     * @return The number of BindingFault items in faults
     */
    public int getFaultsLength()
    {
        return getBean().sizeOfFaultArray();
    }

    /**
     * Get faults bound by this binding definition by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Faults bound by this binding definition at given
     * <code>index</code> position.
     */
    public com.sun.jbi.wsdl2.BindingFault getFault(int index)
    {
        return BindingFaultImpl.Factory.getInstance(
            getBean().getFaultArray(index), 
            this.mContainer);
    }

    /**
     * Set faults bound by this binding definition by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theFault Item to add at position <code>index</code>.
     */
    public void setFault(
        int index, com.sun.jbi.wsdl2.BindingFault theFault)
    {
        getBean().setFaultArray(
            index, 
            theFault != null ? ((BindingFaultImpl) theFault).getBean() 
                             : null);
    }

    /**
     * Append an item to faults bound by this binding definition.
     *
     * @param theFault Item to append to faults
     */
    public void appendFault(
        com.sun.jbi.wsdl2.BindingFault theFault)
    {
        synchronized (getBean().monitor())
        {
            setFault(getFaultsLength(), theFault);
        }
    }

    /**
     * Remove faults bound by this binding definition by index position.
     *
     * @param index The index position of the fault to remove
     * @return The BindingFault removed, if any.
     */
    public com.sun.jbi.wsdl2.BindingFault removeFault(int index)
    {
        com.sun.jbi.wsdl2.BindingFault  result;
        
        synchronized (getBean().monitor())
        {
            result = getFault(index);
            getBean().removeFault(index);
        }
        
        return result;
    }

    /**
     * Get the number of BindingOperation items in operations.
     *
     * @return The number of BindingOperation items in operations
     */
    public int getOperationsLength()
    {
        return getBean().sizeOfOperationArray();
    }

    /**
     * Get named binding operations, if any, by indexed position.
     *
     * @param index Indexed position value 0..length-1
     * @return Named binding operations, if any at given <code>index</code> position.
     */
    public com.sun.jbi.wsdl2.BindingOperation getOperation(int index)
    {
        return BindingOperationImpl.Factory.getInstance(
            getBean().getOperationArray(index), 
            this.mContainer);
    }

    /**
     * Set named binding operations, if any by indexed position.
     *
     * @param index Indexed position value (0..length-1) of the item to set
     * @param  theOperation Item to add at position <code>index</code>.
     */
    public void setOperation(int index, com.sun.jbi.wsdl2.BindingOperation theOperation)
    {
        getBean().setOperationArray(
            index, 
            theOperation != null ? ((BindingOperationImpl) theOperation).getBean() 
                                 : null);
    }

    /**
     * Append an item to named binding operations, if any.
     *
     * @param theOperation Item to append to operations
     */
    public void appendOperation(com.sun.jbi.wsdl2.BindingOperation theOperation)
    {
        synchronized (getBean().monitor())
        {
            final int   length = getOperationsLength();

            setOperation(length, theOperation);
        }
    }

    /**
     * Remove named binding operations, if any, by index position.
     *
     * @param index The index position of the operation to remove
     * @return The BindingOperation removed, if any.
     */
    public com.sun.jbi.wsdl2.BindingOperation removeOperation(int index)
    {
        com.sun.jbi.wsdl2.BindingOperation result = getOperation(index);

        getBean().removeOperation(index);
      
        return result;
    }

    /**
     * Create a new operation, appending it to this binding's operations list.
     *
     * @return Newly created operation, appended to the operations list.
     */
    public com.sun.jbi.wsdl2.BindingOperation addNewOperation()
    {
        return BindingOperationImpl.Factory.getInstance(
            getBean().addNewOperation(),
            mContainer);
    }

    /**
     * Create a new binding fault, appending it to this binding's faults
     * list.
     *
     * @param ref Interface fault to which the new binding fault adds binding
     * information
     * @return Newly created binding fault, appended to the faults list.
     */
    public com.sun.jbi.wsdl2.BindingFault addNewBindingFault(
        com.sun.jbi.wsdl2.InterfaceFault ref)
    {
        BindingFaultType    bindingFaultBean = getBean().addNewFault();
        
        if (ref != null)
        {
            bindingFaultBean.setRef(ref.getQualifiedName());
        }
        
        return BindingFaultImpl.Factory.getInstance(
            bindingFaultBean,
            this.mContainer);
    }

    /**
    * Return this WSDL binding as an XML string.
    *
    * @return This binding, serialized as an XML string.
    */
    public String toXmlString()
    {
        String          result; 
        StringWriter    sw      = new StringWriter();
        XmlOptions      options = new XmlOptions();

        options.setSavePrettyPrint();
        options.setSavePrettyPrintIndent(Constants.XML_PRETTY_PRINT_INDENT);
        options.setSaveOuter();

        try
        {
            getBean().save(sw, options);
            sw.close();
        }
        catch (java.io.IOException ex)
        {
            sw.write("\n<!-- IO error: ");
            sw.write(ex.getMessage());
            sw.write("\n     Document fragment truncated. -->\n");
            // $TODO: log error
        }
        
        return sw.getBuffer().toString();
    }

    /**
    * Return this binding as a DOM document fragment.
    *
    * @return This binding, as a DOM document fragment.
    */
    public DocumentFragment toXmlDocumentFragment()
    {
        XmlOptions  options = new XmlOptions();

        options.setSaveOuter();
        return (DocumentFragment) getBean().newDomNode(options);
    }

    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static class Factory
    {
        /**
         * Find the WSDL binding component associated with the given XML
         * bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * Note: don't feel tempted to use the old double-checked lock trick to 
         * avoid the cost of always locking the read from the map. It doesn't work
         * in Java 1.4 and earlier. See <a href = 
         * "http://www.cs.umd.edu/~pugh/java/memoryModel/DoubleCheckedLocking.html">
         * The "Double-Checked Locking is Broken" Declaration</a>
         * for the details.
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL binding component for the given <code>bean</code>
         *         (null if the <code>bean</code> is null).
         */
        static BindingImpl getInstance(BindingType bean, DescriptionImpl defs)
        {
            BindingImpl   result = null;
      
            if (bean != null)
            {
                Map     map = defs.getBindingMap();
                
                synchronized (map)
                {
                    result = (BindingImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new BindingImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }

            return result;
        }
    }
}

// End-of-file: BindingImpl.java
