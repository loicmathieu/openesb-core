/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageReferenceImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.wsdl2.impl;

import com.sun.jbi.wsdl2.Direction;
import com.sun.jbi.wsdl2.MessageContentModel;

import java.util.Map;

import javax.xml.namespace.QName;

import org.apache.xmlbeans.XmlCursor;

import org.w3.ns.wsdl.MessageRefType;

/**
 * Implementation of WSDL 2.0 Message Reference Component.
 * 
 * @author Sun Microsystems, Inc.
 */
final class MessageReferenceImpl extends MessageReference
{
    /** The container for this component */
    private DescriptionImpl   mContainer;
  
    /**
     * Get the container for this component.
     * 
     * @return The component for this component
     */
    protected DescriptionImpl getContainer()
    {
        return this.mContainer;
    }
  
    /**
     * Construct a message reference implementation object from the given XML
     * bean.
     * @param bean The Message Reference XML bean to use to construct this
     *             component.
     * @param defs The container for this component
     */
    private MessageReferenceImpl(MessageRefType bean, DescriptionImpl defs)
    {
        super(bean);
        this.mContainer = defs;
    }
  
    /** Map of WSDL-defined attribute QNames. Keyed by QName.toString value */
    private static java.util.Map sWsdlAttributeQNames = null;

    /** 
     * Worker class method for {@link #getWsdlAttributeNameMap()}.
     * 
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    static synchronized java.util.Map getAttributeNameMap()
    {
        if (sWsdlAttributeQNames == null)
        {
            sWsdlAttributeQNames = XmlBeansUtil.getAttributesMap(
                MessageRefType.type);
        }

        return sWsdlAttributeQNames;
    }
  
    /**
     * Get map of WSDL-defined attribute QNames for this component, indexed by 
     * canonical QName string (see {@link javax.xml.namespace.QName#toString()}.
     *
     * @return Map of WSDL-defined attribute QNames for this component, 
     *         indexed by QName.toString()
     */
    public java.util.Map getWsdlAttributeNameMap()
    {
        return getAttributeNameMap();
    }
  
    /**
     * Get message exchange pattern role identifier.
     *
     * @return Message exchange pattern role identifier
     */
    public String getMessageLabel()
    {
        return getBean().getMessageLabel();
    }

    /**
     * Set message exchange pattern role identifier.
     *
     * @param theMessageLabel Message exchange pattern role identifier
     */
    public void setMessageLabel(String theMessageLabel)
    {
        if (theMessageLabel != null)
        {
            getBean().setMessageLabel(theMessageLabel);
        }
        else
        {
            getBean().unsetMessageLabel();
        }

        return;
    }

    /**
     * Get content or payload element type declaration.
     *
     * @return Content or payload element type declaration
     */
    public Object getElement()
    {
        return getBean().getElement();
    }

    /**
     * Set content or payload element type declaration.
     *
     * @param theElement Content or payload element type declaration
     */
    public void setElement(Object theElement)
    {
        if (theElement != null && 
            (theElement instanceof QName || theElement instanceof String))
        {
            getBean().setElement(theElement);
        }
    }

    /**
     * Get direction of this message in the exchange.
     *
     * @return Direction of this message in the exchange
     */
    public Direction getDirection()
    {
        XmlCursor     cursor   = getBean().newCursor();
        QName         name     = cursor.getName();

        cursor.dispose();

        return INPUT.equals(name.getLocalPart()) ? Direction.IN : Direction.OUT;
    }

    /**
     * Get content model type: #ANY, #NONE, or #ELEMENT.
     *
     * @return Content model type: #ANY, #NONE, or #ELEMENT
     */
    public MessageContentModel getMessageContentModel()
    {
        MessageContentModel result  = MessageContentModel.NONE;
        Object              element = getBean().getElement();

        if (element != null)
        {
            if (element instanceof QName)
            {
                result = MessageContentModel.ELEMENT;
            }

            //assert (element instanceof String);
            result = MessageContentModel.ANY;
        }

        return result;
    }

    /** WSDL element local name for an input message */
    private static final String INPUT  = "input";

    /** WSDL element local name for an output message */
    private static final String OUTPUT = "output";

    /**
     * Set direction of this message in the exchange.
     * <p>
     * Direction is encoded in name of the XML element representing this component.
     *
     * @param theDirection Direction of this message in the exchange
     */
    public void setDirection(Direction theDirection)
    {
        XmlCursor     cursor   = getBean().newCursor();
        QName         name     = cursor.getName();
        QName         newName  = new QName(name.getNamespaceURI(),
            theDirection == Direction.IN ? INPUT : OUTPUT);

        cursor.setName(newName);
        cursor.dispose();
    }

    /**
     * A factory class for creating / finding components for given XML beans.
     * <p>
     * This factory guarantees that there will only be one component for each
     * XML bean instance.
     */
    static final class Factory
    {
        /**
         * Find the WSDL message reference component associated with the given
         * XML bean, creating a new component if necessary.
         * <p>
         * This is thread-safe.<p>
         * 
         * @param bean The XML bean to find the component for.
         * @param defs The container for the component.
         * @return The WSDL message reference component for the given
         *         <code>bean</code> (null if the <code>bean</code> is null).
         */
        static MessageReferenceImpl getInstance(
            MessageRefType bean, 
            DescriptionImpl defs)
        {
            MessageReferenceImpl    result;

            if (bean != null)
            {
                Map         map = defs.getMessageReferenceMap();
                
                synchronized (map)
                {
                    result = (MessageReferenceImpl) map.get(bean);

                    if (result == null)
                    {
                        result = new MessageReferenceImpl(bean, defs);
                        map.put(bean, result);
                    }
                }
            }
            else
            {
                result = null;
            }

            return result;
        }
    } //  end inner class Factory
}

// End-of-file: MessageReferenceImpl.java
