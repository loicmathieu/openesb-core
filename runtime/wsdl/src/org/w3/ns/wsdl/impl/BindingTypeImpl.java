/*
 * XML Type:  BindingType
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.BindingType
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * An XML BindingType(@http://www.w3.org/ns/wsdl).
 *
 * This is a complex type.
 */
public class BindingTypeImpl extends org.w3.ns.wsdl.impl.ExtensibleDocumentedTypeImpl implements org.w3.ns.wsdl.BindingType
{
    
    public BindingTypeImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName OPERATION$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "operation");
    private static final javax.xml.namespace.QName FAULT$2 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "fault");
    private static final javax.xml.namespace.QName NAME$4 = 
        new javax.xml.namespace.QName("", "name");
    private static final javax.xml.namespace.QName TYPE$6 = 
        new javax.xml.namespace.QName("", "type");
    private static final javax.xml.namespace.QName INTERFACE$8 = 
        new javax.xml.namespace.QName("", "interface");
    
    
    /**
     * Gets array of all "operation" elements
     */
    public org.w3.ns.wsdl.BindingOperationType[] getOperationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(OPERATION$0, targetList);
            org.w3.ns.wsdl.BindingOperationType[] result = new org.w3.ns.wsdl.BindingOperationType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "operation" element
     */
    public org.w3.ns.wsdl.BindingOperationType getOperationArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationType target = null;
            target = (org.w3.ns.wsdl.BindingOperationType)get_store().find_element_user(OPERATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "operation" element
     */
    public int sizeOfOperationArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(OPERATION$0);
        }
    }
    
    /**
     * Sets array of all "operation" element
     */
    public void setOperationArray(org.w3.ns.wsdl.BindingOperationType[] operationArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(operationArray, OPERATION$0);
        }
    }
    
    /**
     * Sets ith "operation" element
     */
    public void setOperationArray(int i, org.w3.ns.wsdl.BindingOperationType operation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationType target = null;
            target = (org.w3.ns.wsdl.BindingOperationType)get_store().find_element_user(OPERATION$0, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(operation);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "operation" element
     */
    public org.w3.ns.wsdl.BindingOperationType insertNewOperation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationType target = null;
            target = (org.w3.ns.wsdl.BindingOperationType)get_store().insert_element_user(OPERATION$0, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "operation" element
     */
    public org.w3.ns.wsdl.BindingOperationType addNewOperation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingOperationType target = null;
            target = (org.w3.ns.wsdl.BindingOperationType)get_store().add_element_user(OPERATION$0);
            return target;
        }
    }
    
    /**
     * Removes the ith "operation" element
     */
    public void removeOperation(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(OPERATION$0, i);
        }
    }
    
    /**
     * Gets array of all "fault" elements
     */
    public org.w3.ns.wsdl.BindingFaultType[] getFaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            java.util.List targetList = new java.util.ArrayList();
            get_store().find_all_element_users(FAULT$2, targetList);
            org.w3.ns.wsdl.BindingFaultType[] result = new org.w3.ns.wsdl.BindingFaultType[targetList.size()];
            targetList.toArray(result);
            return result;
        }
    }
    
    /**
     * Gets ith "fault" element
     */
    public org.w3.ns.wsdl.BindingFaultType getFaultArray(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingFaultType target = null;
            target = (org.w3.ns.wsdl.BindingFaultType)get_store().find_element_user(FAULT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            return target;
        }
    }
    
    /**
     * Returns number of "fault" element
     */
    public int sizeOfFaultArray()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().count_elements(FAULT$2);
        }
    }
    
    /**
     * Sets array of all "fault" element
     */
    public void setFaultArray(org.w3.ns.wsdl.BindingFaultType[] faultArray)
    {
        synchronized (monitor())
        {
            check_orphaned();
            arraySetterHelper(faultArray, FAULT$2);
        }
    }
    
    /**
     * Sets ith "fault" element
     */
    public void setFaultArray(int i, org.w3.ns.wsdl.BindingFaultType fault)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingFaultType target = null;
            target = (org.w3.ns.wsdl.BindingFaultType)get_store().find_element_user(FAULT$2, i);
            if (target == null)
            {
                throw new IndexOutOfBoundsException();
            }
            target.set(fault);
        }
    }
    
    /**
     * Inserts and returns a new empty value (as xml) as the ith "fault" element
     */
    public org.w3.ns.wsdl.BindingFaultType insertNewFault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingFaultType target = null;
            target = (org.w3.ns.wsdl.BindingFaultType)get_store().insert_element_user(FAULT$2, i);
            return target;
        }
    }
    
    /**
     * Appends and returns a new empty value (as xml) as the last "fault" element
     */
    public org.w3.ns.wsdl.BindingFaultType addNewFault()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.BindingFaultType target = null;
            target = (org.w3.ns.wsdl.BindingFaultType)get_store().add_element_user(FAULT$2);
            return target;
        }
    }
    
    /**
     * Removes the ith "fault" element
     */
    public void removeFault(int i)
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_element(FAULT$2, i);
        }
    }
    
    /**
     * Gets the "name" attribute
     */
    public java.lang.String getName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "name" attribute
     */
    public org.apache.xmlbeans.XmlNCName xgetName()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$4);
            return target;
        }
    }
    
    /**
     * Sets the "name" attribute
     */
    public void setName(java.lang.String name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(NAME$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(NAME$4);
            }
            target.setStringValue(name);
        }
    }
    
    /**
     * Sets (as xml) the "name" attribute
     */
    public void xsetName(org.apache.xmlbeans.XmlNCName name)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlNCName target = null;
            target = (org.apache.xmlbeans.XmlNCName)get_store().find_attribute_user(NAME$4);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlNCName)get_store().add_attribute_user(NAME$4);
            }
            target.set(name);
        }
    }
    
    /**
     * Gets the "type" attribute
     */
    public java.lang.String getType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TYPE$6);
            if (target == null)
            {
                return null;
            }
            return target.getStringValue();
        }
    }
    
    /**
     * Gets (as xml) the "type" attribute
     */
    public org.apache.xmlbeans.XmlAnyURI xgetType()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(TYPE$6);
            return target;
        }
    }
    
    /**
     * Sets the "type" attribute
     */
    public void setType(java.lang.String type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(TYPE$6);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(TYPE$6);
            }
            target.setStringValue(type);
        }
    }
    
    /**
     * Sets (as xml) the "type" attribute
     */
    public void xsetType(org.apache.xmlbeans.XmlAnyURI type)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlAnyURI target = null;
            target = (org.apache.xmlbeans.XmlAnyURI)get_store().find_attribute_user(TYPE$6);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlAnyURI)get_store().add_attribute_user(TYPE$6);
            }
            target.set(type);
        }
    }
    
    /**
     * Gets the "interface" attribute
     */
    public javax.xml.namespace.QName getInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(INTERFACE$8);
            if (target == null)
            {
                return null;
            }
            return target.getQNameValue();
        }
    }
    
    /**
     * Gets (as xml) the "interface" attribute
     */
    public org.apache.xmlbeans.XmlQName xgetInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlQName target = null;
            target = (org.apache.xmlbeans.XmlQName)get_store().find_attribute_user(INTERFACE$8);
            return target;
        }
    }
    
    /**
     * True if has "interface" attribute
     */
    public boolean isSetInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            return get_store().find_attribute_user(INTERFACE$8) != null;
        }
    }
    
    /**
     * Sets the "interface" attribute
     */
    public void setInterface(javax.xml.namespace.QName xinterface)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.SimpleValue target = null;
            target = (org.apache.xmlbeans.SimpleValue)get_store().find_attribute_user(INTERFACE$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.SimpleValue)get_store().add_attribute_user(INTERFACE$8);
            }
            target.setQNameValue(xinterface);
        }
    }
    
    /**
     * Sets (as xml) the "interface" attribute
     */
    public void xsetInterface(org.apache.xmlbeans.XmlQName xinterface)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.apache.xmlbeans.XmlQName target = null;
            target = (org.apache.xmlbeans.XmlQName)get_store().find_attribute_user(INTERFACE$8);
            if (target == null)
            {
                target = (org.apache.xmlbeans.XmlQName)get_store().add_attribute_user(INTERFACE$8);
            }
            target.set(xinterface);
        }
    }
    
    /**
     * Unsets the "interface" attribute
     */
    public void unsetInterface()
    {
        synchronized (monitor())
        {
            check_orphaned();
            get_store().remove_attribute(INTERFACE$8);
        }
    }
}
