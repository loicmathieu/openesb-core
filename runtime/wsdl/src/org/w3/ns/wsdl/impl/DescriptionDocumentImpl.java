/*
 * An XML document type.
 * Localname: description
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.DescriptionDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one description(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class DescriptionDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.DescriptionDocument
{
    
    public DescriptionDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DESCRIPTION$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "description");
    
    
    /**
     * Gets the "description" element
     */
    public org.w3.ns.wsdl.DescriptionType getDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DescriptionType target = null;
            target = (org.w3.ns.wsdl.DescriptionType)get_store().find_element_user(DESCRIPTION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "description" element
     */
    public void setDescription(org.w3.ns.wsdl.DescriptionType description)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DescriptionType target = null;
            target = (org.w3.ns.wsdl.DescriptionType)get_store().find_element_user(DESCRIPTION$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.DescriptionType)get_store().add_element_user(DESCRIPTION$0);
            }
            target.set(description);
        }
    }
    
    /**
     * Appends and returns a new empty "description" element
     */
    public org.w3.ns.wsdl.DescriptionType addNewDescription()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DescriptionType target = null;
            target = (org.w3.ns.wsdl.DescriptionType)get_store().add_element_user(DESCRIPTION$0);
            return target;
        }
    }
}
