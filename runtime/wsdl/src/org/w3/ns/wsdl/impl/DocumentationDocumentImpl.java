/*
 * An XML document type.
 * Localname: documentation
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.DocumentationDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one documentation(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class DocumentationDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.DocumentationDocument
{
    
    public DocumentationDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName DOCUMENTATION$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "documentation");
    
    
    /**
     * Gets the "documentation" element
     */
    public org.w3.ns.wsdl.DocumentationType getDocumentation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().find_element_user(DOCUMENTATION$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "documentation" element
     */
    public void setDocumentation(org.w3.ns.wsdl.DocumentationType documentation)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().find_element_user(DOCUMENTATION$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.DocumentationType)get_store().add_element_user(DOCUMENTATION$0);
            }
            target.set(documentation);
        }
    }
    
    /**
     * Appends and returns a new empty "documentation" element
     */
    public org.w3.ns.wsdl.DocumentationType addNewDocumentation()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.DocumentationType target = null;
            target = (org.w3.ns.wsdl.DocumentationType)get_store().add_element_user(DOCUMENTATION$0);
            return target;
        }
    }
}
