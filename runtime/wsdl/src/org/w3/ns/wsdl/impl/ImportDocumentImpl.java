/*
 * An XML document type.
 * Localname: import
 * Namespace: http://www.w3.org/ns/wsdl
 * Java type: org.w3.ns.wsdl.ImportDocument
 *
 * Automatically generated - do not modify.
 */
package org.w3.ns.wsdl.impl;
/**
 * A document containing one import(@http://www.w3.org/ns/wsdl) element.
 *
 * This is a complex type.
 */
public class ImportDocumentImpl extends org.apache.xmlbeans.impl.values.XmlComplexContentImpl implements org.w3.ns.wsdl.ImportDocument
{
    
    public ImportDocumentImpl(org.apache.xmlbeans.SchemaType sType)
    {
        super(sType);
    }
    
    private static final javax.xml.namespace.QName IMPORT$0 = 
        new javax.xml.namespace.QName("http://www.w3.org/ns/wsdl", "import");
    
    
    /**
     * Gets the "import" element
     */
    public org.w3.ns.wsdl.ImportType getImport()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().find_element_user(IMPORT$0, 0);
            if (target == null)
            {
                return null;
            }
            return target;
        }
    }
    
    /**
     * Sets the "import" element
     */
    public void setImport(org.w3.ns.wsdl.ImportType ximport)
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().find_element_user(IMPORT$0, 0);
            if (target == null)
            {
                target = (org.w3.ns.wsdl.ImportType)get_store().add_element_user(IMPORT$0);
            }
            target.set(ximport);
        }
    }
    
    /**
     * Appends and returns a new empty "import" element
     */
    public org.w3.ns.wsdl.ImportType addNewImport()
    {
        synchronized (monitor())
        {
            check_orphaned();
            org.w3.ns.wsdl.ImportType target = null;
            target = (org.w3.ns.wsdl.ImportType)get_store().add_element_user(IMPORT$0);
            return target;
        }
    }
}
