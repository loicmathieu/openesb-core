#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)framework00006.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

# This test covers component and service unit startup logic in the JBI framework.
# Two components, test-fast-binding and test-slow-engine, are installed and 
# and started.  Two apps (one with a service connection and one without) are
# deployed to the components and started.  The test then bounces the server and
# queries the state of the components and service assemblies.  The SUs deployed
# to test-fast-binding will fail in start() if corresponding endpoints have not
# been activated during service unit init() in SlowEngine.  SlowEngine contains
# a sleep timer which should expose race conditions between init() and start()
# timing across components.

#regress setup
my_test_domain=domain1
. ./regress_defs.ksh

# package components
ant -emacs -q -f framework00006.xml package

# start our test domain
start_domain
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "cannot start domain $mytestdomain"
    exit 1
fi

# install components
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/slow-engine.jar install-component
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.install.file=$JV_FRAMEWORK_BLD_DIR/dist/fast-binding.jar install-component
installComponentDelay

# start components
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-slow-engine" start-component
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-fast-binding" start-component
startComponentDelay

# deploy service assemblies
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.deploy.file=$JV_FRAMEWORK_BLD_DIR/dist/restart-sa-1.jar deploy-service-assembly
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.deploy.file=$JV_FRAMEWORK_BLD_DIR/dist/restart-sa-2.jar deploy-service-assembly
deploySaDelay

# start service assemblies
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-1" start-service-assembly
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-2" start-service-assembly
startSaDelay

# Query the state of our components and service assemblies before restart
$JBI_ANT -Djbi.binding.component.name="test-fast-binding" list-binding-components
$JBI_ANT -Djbi.service.engine.name="test-slow-engine" list-service-engines
$JBI_ANT -Djbi.service.assembly.name="restart-sa-1" list-service-assemblies
$JBI_ANT -Djbi.service.assembly.name="restart-sa-2" list-service-assemblies

#shutdown the test domain
shutdown_domain
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "cannot shutdown domain $mytestdomain to restart"
    exit 1
fi

#restart the test domain
start_domain
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "cannot restart domain $mytestdomain"
    exit 1
fi

# wait a bit longer for autostart processing to complete
syncInstanceDelay

# Query the state of our components and service assemblies after restart
$JBI_ANT -Djbi.binding.component.name="test-fast-binding" list-binding-components
$JBI_ANT -Djbi.service.engine.name="test-slow-engine" list-service-engines
$JBI_ANT -Djbi.service.assembly.name="restart-sa-1" list-service-assemblies
$JBI_ANT -Djbi.service.assembly.name="restart-sa-2" list-service-assemblies

# shut down service assemblies
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-1" shut-down-service-assembly
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-2" shut-down-service-assembly
stopSaDelay

# undeploy service assemblies
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-1" undeploy-service-assembly
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.service.assembly.name="restart-sa-2" undeploy-service-assembly
undeploySaDelay

# shutdown components
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-slow-engine" shut-down-component
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-fast-binding" shut-down-component
stopComponentDelay

# uninstall components
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-slow-engine" uninstall-component
$JBI_ANT -Djbi.task.fail.on.error=false -Djbi.component.name="test-fast-binding" uninstall-component
uninstallComponentDelay

#shutdown the test domain
shutdown_domain
status=$?
if [ $status -ne 0 ]; then
    bldmsg -status $status -p $0 -error "cannot shutdown domain $mytestdomain"
    exit 1
fi

# ### END
