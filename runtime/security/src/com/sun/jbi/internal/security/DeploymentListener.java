/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeploymentListener.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
/**
 *  DeploymentListener.java
 *
 *  SUN PROPRIETARY/CONFIDENTIAL.
 *  This software is the proprietary information of Sun Microsystems, Inc.
 *  Use is subject to license terms.
 *
 */

package com.sun.jbi.internal.security;

import com.sun.jbi.binding.security.Endpoint;
import com.sun.jbi.binding.security.MessageHandlerException;
import com.sun.jbi.binding.security.InvalidConfigurationException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * No-op implementation of Deployment Listener.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeploymentListener
    implements com.sun.jbi.binding.security.DeploymentListener
{
    /**
     * Notification for a new Endpoint Deployment.
     *
     * @param endpoint is the Endpoint being deployed.
     * @throws InvalidConfigurationException If the Security Configuration information 
     * provided at deployment time is invalid.
     * @throws Exception on Errors.
     */
    public void addDeployment (Endpoint endpoint) 
        throws Exception 
    {
        // -- no-op
    }
    
    /**
     * Notification for removal of an Endpoint Deployment.
     *
     * @param endpoint is the Endpoint being undeployed.
     */
    public void removeDeployment(Endpoint endpoint)
    {
        // -- no-op
    }
 
}
