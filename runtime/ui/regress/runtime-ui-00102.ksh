#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)runtime-ui-00102.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

####
# Test Framework, NMR, Component, Endpoint, SA Stats in instance1
####
. ./regress_defs.ksh

echo "runtime-ui-00102 : Test Framework, NMR, Component, Endpoint, SA Stats in instance1."

echo "Setup server for getting some stats"
echo "-----------------------------------"
ant -q  -emacs -DAS_JMX_REMOTE_URL="$JBISE_JMX_URL" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml package-components
asadmin set-jbi-runtime-configuration --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 msgSvcTimingStatisticsEnabled="true"
asadmin deploy-jbi-service-assembly --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 $JV_SRCROOT/runtime/ui/bld/test-classes/dist/ping-sa.jar

echo "Service Assembly stats in instance1"
echo "-----------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dserviceassembly=PingApp -Dtarget=instance1 get-sa-stats

asadmin start-jbi-service-assembly --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 PingApp
asadmin install-jbi-component --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 $JV_SRCROOT/runtime/ui/bld/test-classes/dist/simpletestengine.jar
asadmin start-jbi-component --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 SimpleTestEngine

echo "Framework stats in instance1"
echo "----------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dtarget=instance1 get-framework-stats

echo "NMR stats in instance1"
echo "----------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dtarget=instance1 get-nmr-stats

echo "Component stats in instance1"
echo "----------------------------"
asadmin start-jbi-component --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 sun-http-binding
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dtarget=instance1 -Dcomponent=sun-http-binding get-component-stats

echo "List of providing endpoint for component"
echo "----------------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -DcomponentName=SimpleTestEngine -Dtarget=instance1 get-consuming-endpoints-list

echo "List of consuming endpoints for component"
echo "-----------------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -DcomponentName=sun-http-binding -Dtarget=instance1 get-providing-endpoints-list

echo "Providing endpoint stats in instance1"
echo "-------------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dendpoint="http://ping,PingService,PingPort" -Dtarget=instance1 get-endpoint-stats

echo "Consuming endpoint stats in instance1"
echo "-------------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dendpoint="http://ping,PingService,ConsumingPingPort" -Dtarget=instance1 get-endpoint-stats

echo "Service Assembly stats in instance1"
echo "-----------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dserviceassembly=PingApp -Dtarget=instance1 get-sa-stats

asadmin shut-down-jbi-service-assembly --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 PingApp

echo "Service Assembly stats in instance1"
echo "-----------------------------------"
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f runtime-ui-00101.xml -Dserviceassembly=PingApp -Dtarget=instance1 get-sa-stats

asadmin undeploy-jbi-service-assembly --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 PingApp
asadmin shut-down-jbi-component --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 SimpleTestEngine
asadmin uninstall-jbi-component --port $ASADMIN_PORT --user admin --passwordfile $JV_AS8BASE/passwords --target=instance1 SimpleTestEngine

