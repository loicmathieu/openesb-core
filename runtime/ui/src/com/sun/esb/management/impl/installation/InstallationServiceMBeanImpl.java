/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)InstallationServiceMBeanImpl.java
 * Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.management.impl.installation;

import java.io.File;
import java.io.Serializable;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.AttributeList;
import javax.management.ObjectName;

import com.sun.esb.management.api.installation.InstallationService;
import com.sun.esb.management.base.services.AbstractServiceMBeansImpl;
import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.common.JBIAdminCommands;
import com.sun.jbi.ui.common.JBIJMXObjectNames;

/**
 * Defines operations for common installation services.
 * 
 * @author graj
 */
public class InstallationServiceMBeanImpl extends AbstractServiceMBeansImpl
        implements InstallationService, Serializable {
    
    static final long serialVersionUID = -1L;
    
    /**
     * Constructor - Constructs a new instance of InstallationServiceMBeanImpl
     * 
     * @param anEnvContext
     */
    public InstallationServiceMBeanImpl(EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @return name of the component.
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#installComponent(java.lang.String,
     *      java.util.Properties, java.lang.String)
     */
    public String installComponent(String zipFilePath, Properties paramProps,
            String targetName) throws ManagementRemoteException {
        /*
         * ==================================================================
         * According to IN=100359 at
         * http://inf.central.sun.com/inf/integrationReport.jsp?id=100359
         * 
         * NOTE: Facade MBean operations for the InstallationService: When the
         * Installer is loaded for a component, it is added to the domain. When
         * the Installer is unloaded the component is removed from the domain if
         * it is not installed on any instances. For the domain targetName, the
         * ComponentLifeCycle MBean is not available.
         * ==================================================================
         */
        logDebug("Installing Component zip file " + zipFilePath);
 
        String fileURIString = null;
        
        File file = new File(zipFilePath);
        fileURIString = file.getAbsolutePath();
        
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        
        this.checkForValidTarget(installerServiceObjectName, targetName);
        
        ObjectName installerObjectName = null;
        ObjectName lifecycleObjectName = null;
        String componentName = null;
        
        logDebug("Install Step 1 : Calling loadNewInstaller on InstallerServiceMBean : "
                + installerServiceObjectName);
        // Load new installer
        installerObjectName = (ObjectName) this.invokeMBeanOperation(
                installerServiceObjectName, "loadNewInstaller", fileURIString);
        
        if (installerObjectName == null) {
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.install.comp.error.installerMBean", args, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        try {
            logDebug("Install Step 2 : Configuring Installer Configuration Parameters if present");
            // Configure InstallerConfigurationMBean
            this.configureComponentInstaller(installerObjectName, paramProps);
            
            logDebug("Install Step 3 : Calling Install on InstallerMbean : "
                    + installerObjectName);
            
            // invoke install on the component installer
            lifecycleObjectName = (ObjectName) this.invokeMBeanOperation(
                    (ObjectName) installerObjectName, "install");
            
            componentName = this
                    .getComponentNameFromJmxObjectName(installerObjectName);
            
            // Lifecycle object name is not available for the domain targetName
            if (JBIAdminCommands.DOMAIN_TARGET_KEY.equals(targetName) == false) {
                if (lifecycleObjectName == null) {
                    String[] args = { installerObjectName.toString() };
                    Exception exception = this.createManagementException(
                            "ui.mbean.install.comp.error.lifecycleMBean", args,
                            null);
                    throw new ManagementRemoteException(exception);
                    
                }
                
                // get the component id from the lifecycleObjectName
                componentName = this
                        .getComponentNameFromJmxObjectName(lifecycleObjectName);
                
                if (componentName == null) {
                    String[] args = { lifecycleObjectName.toString() };
                    Exception exception = this.createManagementException(
                            "ui.mbean.install.comp.error.compId", args, null);
                    throw new ManagementRemoteException(exception);
                    
                }
            }
        } finally {
            // // call unload installer to unregister the mbean
            boolean isRemoveComponent = false;
            boolean ignoreExceptions = false;
            if (componentName == null) {
                // install failed. ignore the unload exceptions to propagate the
                // install
                // fail exceptions to the client
                ignoreExceptions = true;
                isRemoveComponent = true; // since install failed, remove
                // component from disk also?
            }
            
            logDebug("Install Step 4 : unloading component installer "
                    + installerObjectName);
            
            this.unloadComponentInstaller(installerObjectName,
                    isRemoveComponent, ignoreExceptions, targetName);
        }
        
        return componentName;
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @return name of the component.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#installComponent(java.lang.String,
     *      java.lang.String)
     */
    public String installComponent(String zipFilePath, String targetName)
            throws ManagementRemoteException {
        Properties params = new Properties();
        return this.installComponent(zipFilePath, params, targetName);
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component.
     * @param component
     *            configuration properties
     * @param targetName
     *            name of the target for this operation
     * @return result.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#installComponentFromDomain(java.lang.String,
     *      java.util.Properties, java.lang.String)
     */
    public String installComponentFromDomain(String componentName,
            Properties properties, String targetName)
            throws ManagementRemoteException {
        if (true == targetName.equals(JBIAdminCommands.DOMAIN_TARGET_KEY)) {
            return componentName;
        }
        
        /*
         * ==================================================================
         * According to IN=100359 at
         * http://inf.central.sun.com/inf/integrationReport.jsp?id=100359
         * 
         * NOTE: Facade MBean operations for the InstallationService: When the
         * Installer is loaded for a component, it is added to the domain. When
         * the Installer is unloaded the component is removed from the domain if
         * it is not installed on any instances. For the domain targetName, the
         * ComponentLifeCycle MBean is not available.
         * ==================================================================
         */

        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        
        this.checkForValidTarget(installerServiceObjectName, targetName);
        
        ObjectName installerObjectName = null;
        ObjectName lifecycleObjectName = null;
        
        logDebug("Install Step 1 : "
                + "Calling loadInstallerFromRepository which internally "
                + "performs loadNewInstaller on InstallerServiceMBean : "
                + installerServiceObjectName);
        // Load new installer
        installerObjectName = (ObjectName) this.invokeMBeanOperation(
                installerServiceObjectName, "loadInstallerFromRepository",
                componentName);
        
        if (installerObjectName == null) {
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.install.comp.error.installerMBean", args, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        try {
            logDebug("Install Step 2 : Configuring Installer Configuration Parameters if present");
            // Configre InstallerConfigurationMBean
            this.configureComponentInstaller(installerObjectName, properties);
            
            logDebug("Install Step 3 : Calling Install on InstallerMbean : "
                    + installerObjectName);
            
            // invoke install on the component installer
            lifecycleObjectName = (ObjectName) this.invokeMBeanOperation(
                    (ObjectName) installerObjectName, "install");
            
            componentName = this
                    .getComponentNameFromJmxObjectName(installerObjectName);
            
            // Lifecycle object name is not available for the domain targetName
            if (JBIAdminCommands.DOMAIN_TARGET_KEY.equals(targetName) == false) {
                if (lifecycleObjectName == null) {
                    String[] args = { installerObjectName.toString() };
                    Exception exception = this.createManagementException(
                            "ui.mbean.install.comp.error.lifecycleMBean", args,
                            null);
                    throw new ManagementRemoteException(exception);
                    
                }
                
                // get the component id from the lifecycleObjectName
                componentName = this
                        .getComponentNameFromJmxObjectName(lifecycleObjectName);
                
                if (componentName == null) {
                    String[] args = { lifecycleObjectName.toString() };
                    Exception exception = this.createManagementException(
                            "ui.mbean.install.comp.error.compId", args, null);
                    throw new ManagementRemoteException(exception);
                    
                }
            }
        } finally {
            // // call unload installer to unregister the mbean
            boolean isRemoveComponent = false;
            boolean ignoreExceptions = false;
            if (componentName == null) {
                // install failed. ignore the unload exceptions to propagate the
                // install
                // fail exceptions to the client
                ignoreExceptions = true;
                isRemoveComponent = true; // since install failed, remove
                // component from disk also?
            }
            
            logDebug("Install Step 4 : unloading component installer "
                    + installerObjectName);
            
            this.unloadComponentInstaller(installerObjectName,
                    isRemoveComponent, ignoreExceptions, targetName);
        }
        
        return componentName;
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component.
     * @param targetName
     *            name of the target for this operation
     * @return result.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#installComponentFromDomain(java.lang.String,
     *      java.lang.String)
     */
    public String installComponentFromDomain(String componentName,
            String targetName) throws ManagementRemoteException {
        if (true == targetName.equals(JBIAdminCommands.DOMAIN_TARGET_KEY)) {
            return componentName;
        }
        Properties params = new Properties();
        return this.installComponentFromDomain(componentName, params,
                targetName);
    }
    
    /**
     * installs shared library
     * 
     * @return shared library name.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#installSharedLibrary(java.lang.String,
     *      java.lang.String)
     */
    public String installSharedLibrary(String zipFilePath, String targetName)
            throws ManagementRemoteException {
        logDebug("Installing Shared Library " + zipFilePath);
        
        File file = new File(zipFilePath);
        String fileURIString = null;
        fileURIString = file.getAbsolutePath();
        
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        
        this.checkForValidTarget(installerServiceObjectName, targetName);
        
        logDebug("Calling installSharedLibrary on Installation Service MBean = "
                + installerServiceObjectName);
        
        Object sharedLibName = null;
        
        sharedLibName = this.invokeMBeanOperation(installerServiceObjectName,
                "installSharedLibrary", fileURIString);
        
        if (sharedLibName == null) {
            Exception exception = this.createManagementException(
                    "ui.mbean.install.sns.error", null, null);
            throw new ManagementRemoteException(exception);
            
        }
        return sharedLibName.toString();
    }
    
    /**
     * installs shared library
     * 
     * @param libraryName
     *            Shared Library Name
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#installSharedLibraryFromDomain(java.lang.String,
     *      java.lang.String)
     */
    public String installSharedLibraryFromDomain(String libraryName,
            String targetName) throws ManagementRemoteException {
        if (true == targetName.equals(JBIAdminCommands.DOMAIN_TARGET_KEY)) {
            return libraryName;
        }
        
        ObjectName installerServiceObjectName = null;
        Object sharedLibName = null;
        
        installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        if (installerServiceObjectName == null) {
            String[] args = { targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.install.comp.error.installerMBean", args, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        this.checkForValidTarget(installerServiceObjectName, targetName);
        
        logDebug("Invoking installSharedLibraryFromRepository which "
                + "internally invokes installSharedLibrary on "
                + "Installation Service MBean = " + installerServiceObjectName);
        
        sharedLibName = this.invokeMBeanOperation(installerServiceObjectName,
                "installSharedLibraryFromRepository", libraryName);
        
        if (sharedLibName == null) {
            Exception exception = this.createManagementException(
                    "ui.mbean.install.sns.error", null, null);
            throw new ManagementRemoteException(exception);
            
        }
        return sharedLibName.toString();
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return component name string.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String,
     *      boolean, boolean, java.lang.String)
     */
    public String uninstallComponent(String componentName, boolean forceDelete,
            boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        return this.uninstallComponentInternal(componentName, forceDelete,
                retainInDomain, targetName);
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String uninstallComponent(String componentName, boolean forceDelete,
            String targetName) throws ManagementRemoteException {
        if (false == forceDelete) {
            return this.uninstallComponent(componentName, targetName);
        }
        return this.uninstallComponentInternal(componentName, forceDelete,
                targetName);
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @return name of the component.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallComponent(java.lang.String,
     *      java.lang.String)
     */
    public String uninstallComponent(String componentName, String targetName)
            throws ManagementRemoteException {
        boolean force = false;
        return this
                .uninstallComponentInternal(componentName, force, targetName);
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetName
     *            name of the target for this operation
     * @return shared library name string.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String,
     *      boolean, boolean, java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        logDebug("uninstalling shared library " + sharedLibraryName);
        
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling uninstallSharedLibrary on InstallerServiceMBean = "
                + installerServiceObjectName);
        
        Object resultObject = null;
        
        Object[] params = new Object[2];
        params[0] = sharedLibraryName;
        params[1] = Boolean.valueOf(retainInDomain);
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";
        
        resultObject = this.invokeMBeanOperation(installerServiceObjectName,
                "uninstallSharedLibrary", params, signature);
        
        if (resultObject == null || !(resultObject instanceof Boolean)
                || !((Boolean) resultObject).booleanValue()) {
            Exception exception = this.createManagementException(
                    "ui.mbean.uninstall.sns.error", null, null);
            throw new ManagementRemoteException(exception);
        }
        
        return sharedLibraryName;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String,
     *      boolean, java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            boolean forceDelete, String targetName)
            throws ManagementRemoteException {
        if (false == forceDelete) {
            return this.uninstallSharedLibrary(sharedLibraryName, targetName);
        }
        return this.uninstallSharedLibraryInternal(sharedLibraryName,
                forceDelete, targetName);
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param targetName
     * @return shared library name.
     * @throws ManagementRemoteException
     *             on error
     * 
     * @see com.sun.esb.management.api.installation.InstallationService#uninstallSharedLibrary(java.lang.String,
     *      java.lang.String)
     */
    public String uninstallSharedLibrary(String sharedLibraryName,
            String targetName) throws ManagementRemoteException {
        logDebug("uninstalling shared library " + sharedLibraryName);
        
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Calling uninstallSharedLibrary on InstallerServiceMBean = "
                + installerServiceObjectName);
        
        Object resultObject = null;
        
        resultObject = this.invokeMBeanOperation(installerServiceObjectName,
                "uninstallSharedLibrary", sharedLibraryName);
        
        if (resultObject == null || !(resultObject instanceof Boolean)
                || !((Boolean) resultObject).booleanValue()) {
            Exception exception = this.createManagementException(
                    "ui.mbean.uninstall.sns.error", null, null);
            throw new ManagementRemoteException(exception);
        }
        
        return sharedLibraryName;
    }
    
    /**
     * updates component ( service engine, binding component)
     * 
     * @param componentName
     *            Name of the component to update.
     * @param zipFilePath
     *            archive file in a zip format
     * @return The name of the component if successful
     * @throws ManagementRemoteException
     *             on error
     * @see com.sun.esb.management.api.installation.InstallationService#upgradeComponent(java.lang.String,
     *      java.lang.String)
     */
    public String upgradeComponent(String componentName, String zipFilePath)
            throws ManagementRemoteException {
        logDebug("Updating Component zip file " + zipFilePath);
        
        String fileURIString = null;
        
        File file = new File(zipFilePath);
        fileURIString = file.getAbsolutePath();
        
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(JBIAdminCommands.DOMAIN_TARGET_KEY);
        
        this.checkForValidTarget(installerServiceObjectName,
                JBIAdminCommands.DOMAIN_TARGET_KEY);
        
        // update component
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = fileURIString;
        
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "java.lang.String";
        
        this.invokeMBeanOperation(installerServiceObjectName,
                "upgradeComponent", params, signature);
        return componentName;
    }
    
    /////////////////////////////////////////////
    // Start of Cumulative Operation Definitions
    /////////////////////////////////////////////
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @return name of the component as map of [targetName,string].
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> installComponent(String zipFilePath,
            Properties paramProps, String[] targetNames)
            throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.installComponent(zipFilePath, paramProps,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
    }
    
    /**
     * installs component ( service engine, binding component)
     * 
     * @return name of the component as map of [targetName,string].
     * @param paramProps
     *            Properties object contains name/value pair.
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> installComponent(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.installComponent(zipFilePath,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * installs shared library
     * 
     * @param zipFilePath
     *            archive file in a zip format
     * @param targetNames
     * @return shared library name as map of [targetName,string].
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> installSharedLibrary(String zipFilePath,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.installSharedLibrary(zipFilePath,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     * @return name of the component as [targetName, String] map.
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> uninstallComponent(String componentName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.uninstallComponent(componentName,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param targetNames
     * @return shared library name as [targetName, string] map.
     * @throws ManagementRemoteException
     *             on error
     * 
     */
    public Map<String, String> uninstallSharedLibrary(String sharedLibraryName,
            String[] targetNames) throws ManagementRemoteException {
        
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.uninstallSharedLibrary(sharedLibraryName,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * installs component from Domain( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> installComponentFromDomain(
            String componentName, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.installComponentFromDomain(componentName,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
    }
    
    /**
     * installs component from Domain( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param component
     *            configuration properties
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> installComponentFromDomain(
            String componentName, Properties properties, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.installComponentFromDomain(componentName,
                        properties, targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> uninstallComponent(
            String componentName, boolean forceDelete, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.uninstallComponent(componentName, forceDelete,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * installs shared library
     * 
     * @param libraryName
     *            name of the library
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> installSharedLibraryFromDomain(
            String libraryName, String[] targetNames) throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.installSharedLibraryFromDomain(libraryName,
                        targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
        
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> uninstallSharedLibrary(
            String sharedLibraryName, boolean forceDelete, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.uninstallSharedLibrary(sharedLibraryName,
                        forceDelete, targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and component name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    
    public Map<String /* targetName */, String /* targetResult */> uninstallComponent(
            String componentName, boolean forceDelete, boolean retainInDomain,
            String[] targetNames) throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.uninstallComponent(componentName, forceDelete,
                        retainInDomain, targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param retainInDomain
     *            true to not delete it from the domain target, false to also
     *            delete it from the domain target.
     * @param targetNames
     *            array of targets for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    public Map<String /* targetName */, String /* targetResult */> uninstallSharedLibrary(
            String sharedLibraryName, boolean forceDelete,
            boolean retainInDomain, String[] targetNames)
            throws ManagementRemoteException {
        String xmlString = null;
        ConcurrentHashMap<String, String> result = new ConcurrentHashMap<String, String>();
        for (int index = 0; index < targetNames.length; index++) {
            try {
                xmlString = this.uninstallSharedLibrary(sharedLibraryName,
                        forceDelete, retainInDomain, targetNames[index]);
                if (xmlString != null) {
                    result.putIfAbsent(targetNames[index], xmlString);
                }
            } catch (ManagementRemoteException exception) {
                xmlString = this.getStackTrace(exception);
                result.putIfAbsent(targetNames[index], xmlString);
            }
            
        }
        return (Map<String, String>) result;
    }
    
    
    /////////////////////////////////////////////
    // End of Cumulative Operation Definitions
    /////////////////////////////////////////////
    
    
    /**
     * set the installer configuration parameters
     * 
     * @param installerObjectName
     *            objectname
     * @param paramProps
     *            name,value pair list
     * @throws ManagementRemoteException
     *             on error.
     */
    protected void configureComponentInstaller(ObjectName installerObjectName,
            Properties paramProps) throws ManagementRemoteException {
        if (paramProps == null || paramProps.size() == 0) {
            logDebug("No Installer Params for " + installerObjectName);
            return;
        }
        
        ObjectName configObjectName = null;
        configObjectName = (ObjectName) this.getMBeanAttribute(
                installerObjectName, "InstallerConfigurationMBean");
        
        if (configObjectName == null) {
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.install.config.mbean.error.not.exist",
                            null, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        // Test is a Configuration MBean is registered. If it is not
        // registered, decide what needs to be done
        if (this.isMBeanRegistered(configObjectName) == false) {
            // TODO need to decide what to do here.
            // Throw an exception? OR return?
            logDebug("Configuration MBean for " + installerObjectName);
            logDebug("No ConfigurationMBean registered with name: "
                    + configObjectName);
            return;
        }
        
        logDebug("Setting the Installer Params on ConfigMBean "
                + configObjectName);
        
        AttributeList attrList = constructMBeanAttributes(configObjectName,
                paramProps);
        try {
            
            this.setMBeanAttributes(configObjectName, attrList);
        } catch (Exception ex) {
            Exception exception = this.createManagementException(
                    "ui.mbean.install.config.mbean.error.set.attrs.error",
                    null, ex);
            throw new ManagementRemoteException(exception);
            
        }
    }
    
    /**
     * finds the component id property in the object name
     * 
     * @param jmxObjectName
     *            jmx obj name
     * @return componentId of the component if embbeded in object name else
     *         null.
     */
    protected String getComponentNameFromJmxObjectName(ObjectName jmxObjectName) {
        String componentName = null;
        try {
            // TODO change COMPONENT_ID_KEY to Component Name when mgmt api
            // changes.
            componentName = ((ObjectName) jmxObjectName)
                    .getKeyProperty(JBIJMXObjectNames.COMPONENT_ID_KEY);
            
        } catch (NullPointerException nullEx) {
            componentName = null;
        }
        return componentName;
    }
    
    /**
     * unloads the installer from memory and removes the component optionally
     * 
     * @param installerObjectName
     *            installerObjectName.
     * @param isIgnoreExceptions
     *            true or false.
     * @param isRemoveComponent
     *            true if this should cleanup disk or false. true is usded in
     *            uninstall process
     * @param targetName
     * @return true or false
     * @throws ManagementRemoteException
     *             on error
     */
    protected boolean unloadComponentInstaller(ObjectName installerObjectName,
            boolean isRemoveComponent, boolean isIgnoreExceptions,
            String targetName) throws ManagementRemoteException {
        String componentName = null;
        if (installerObjectName == null) {
            Exception exception = this.createManagementException(
                    "ui.mbean.install.installer.mbean.not.exist", null, null);
            throw new ManagementRemoteException(exception);
            
        }
        // get the component name from intaller.
        componentName = this
                .getComponentNameFromJmxObjectName(installerObjectName);
        
        if (componentName == null || componentName.trim().length() <= 0) {
            String[] args = { installerObjectName.toString() };
            Exception exception = this.createManagementException(
                    "ui.mbean.install.installer.mbean.has.no.component.name",
                    args, null);
            throw new ManagementRemoteException(exception);
        }
        
        return this.unloadComponentInstaller(componentName, isRemoveComponent,
                isIgnoreExceptions, targetName);
    }
    
    /**
     * Unloads the installer from memory and removes the component optionally
     * 
     * @param componentName
     *            componentName.
     * @param isIgnoreExceptions
     *            true or false.
     * @param isRemoveComponent
     *            true if this should cleanup disk or false. true is
     * @param targetName
     *            usded in uninstall process
     * @return true or false
     * @throws ManagementRemoteException
     *             on error.
     */
    protected boolean unloadComponentInstaller(String componentName,
            boolean isRemoveComponent, boolean isIgnoreExceptions,
            String targetName) throws ManagementRemoteException {
        boolean retainInDomain = false;
        if (retainInDomain == true) {
            isRemoveComponent = false;
            return this.unloadComponentInstallerInternal(componentName,
                    isRemoveComponent, isIgnoreExceptions, targetName);
        }
        return this.unloadComponentInstallerInternal(componentName,
                isRemoveComponent, isIgnoreExceptions, targetName);
    }
    
    /**
     * Unloads the installer from memory and removes the component optionally
     * 
     * @param componentName
     *            componentName.
     * @param isRemoveComponent
     *            true if this should cleanup disk or false. true is
     * @param retainInDomain
     *            true if this should be kept in domain target, false if not
     * @param isIgnoreExceptions
     *            true or false.
     * @param targetName
     *            used in uninstall process
     * @return true or false
     * @throws ManagementRemoteException
     *             on error.
     */
    protected boolean unloadComponentInstallerInternal(String componentName,
            boolean isRemoveComponent, boolean isIgnoreExceptions,
            String targetName) throws ManagementRemoteException {
        boolean result = false;
        logDebug("unloadComponentInstaller ComponentName: " + componentName
                + " isremoveComponent " + isRemoveComponent
                + " isIgnoreExceptions " + isIgnoreExceptions + " targetName "
                + targetName);
        try {
            ObjectName installerServiceObjectName = this
                    .getInstallationServiceMBeanObjectName(targetName);
            
            // call unload installer to unregister the mbean
            Object[] params = new Object[2];
            params[0] = componentName;
            params[1] = Boolean.valueOf(isRemoveComponent);
            
            String[] signature = new String[2];
            signature[0] = "java.lang.String";
            signature[1] = "boolean";
            
            logDebug("Calling unloadInstaller with params ( " + params[0]
                    + " , " + params[1] + " ) on InstallerServiceMBean : "
                    + installerServiceObjectName.toString());
            
            Object resultObject = invokeMBeanOperation(
                    installerServiceObjectName, "unloadInstaller", params,
                    signature);
            result = ((Boolean) resultObject).booleanValue();
        } catch (Exception ex) {
            
            if (isIgnoreExceptions) {
                logError(ex);
            } else {
                throw ManagementRemoteException.filterJmxExceptions(ex);
            }
        }
        return result;
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param targetName
     * @return name of the component.
     * @throws ManagementRemoteException
     *             on error
     */
    protected String uninstallComponentInternal(String componentName,
            boolean force, String targetName) throws ManagementRemoteException {
        boolean retainInDomain = false;
        return this.uninstallComponentInternal(componentName, force,
                retainInDomain, targetName);
    }
    
    /**
     * uninstalls component ( service engine, binding component)
     * 
     * @param componentName
     *            name of the component
     * @param force
     * @param retainInDomain
     * @param targetName
     * @return name of the component.
     * @throws ManagementRemoteException
     *             on error
     */
    protected String uninstallComponentInternal(String componentName,
            boolean force, boolean retainInDomain, String targetName)
            throws ManagementRemoteException {
        logDebug("Uninstalling Component " + componentName + " with force = " + force);
        
        // //////////////////////////////////////////////////////////////////
        // TODO
        // NOTE:
        // Commenting this code out for now based on discussions within the team
        // between Nikita, Keith, Chikkala, and team during the team meeting
        // on Sept 20'06 between 2:00pm to 3:00pm.
        // Will revisit if this causes a problem.
        // //////////////////////////////////////////////////////////////////
        // boolean isExistingComponent = isExistingComponent(componentName,
        // targetName);
        // if (isExistingComponent == false) {
        // throw new
        // ManagementRemoteException(AbstractUIMBeanImpl.getI18NBundle()
        // .getMessage("ui.mbean.component.id.does.not.exist",
        // componentName));
        // }
        // //////////////////////////////////////////////////////////////////
        
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        logDebug("Uninstall Step 1 : Calling loadInstaller on InstallerServiceMBean : "
                + installerServiceObjectName);
               
        // // load installer
        ObjectName installerObjectName = null;
        Object[] params = new Object[2];
        params[0] = componentName;
        params[1] = Boolean.valueOf(force);
            
        String[] signature = new String[2];
        signature[0] = "java.lang.String";
        signature[1] = "boolean";

        installerObjectName = (ObjectName) this.invokeMBeanOperation(
            installerServiceObjectName, "loadInstaller", params, signature);
        if (installerObjectName == null) {
            Exception exception = this.createManagementException(
                    "ui.mbean.uninstall.comp.error.installerMBean", null, null);
            throw new ManagementRemoteException(exception);
       }
        
        boolean uninstalled = false;
        try {
            logDebug("Uninstall Step 2 : Calling Unisntall on InstallerMbean : "
                    + installerObjectName);
            
            params = new Object[1];
            params[0] = Boolean.valueOf(force);
            
            signature = new String[1];
            signature[0] = "boolean";
            
            // invoke uninstall on the component installer
            this.invokeMBeanOperation(installerObjectName, "uninstall", params,
                    signature);
            uninstalled = true;
        } finally {
            // execute always after uninstall
            // call unload installer to unregister the mbean and remove the
            // component jars
            boolean isRemoveComponent = true;
            boolean ignoreExceptions = false;
            //if (!uninstalled) {
            if (!uninstalled) {
                // uninstall not successful,ignore unload exceptions. so that
                // the uninstall exception can be propagated to client.
                ignoreExceptions = true;
                if ( !force ) {
                    isRemoveComponent = false;
                }
            }
            logDebug("Uninstall Step 2 : unloading component installer "
                    + installerObjectName);
            if (retainInDomain == true) {
                isRemoveComponent = false;
            }
            this.unloadComponentInstaller(installerObjectName,
                    isRemoveComponent, ignoreExceptions, targetName);
        }
        return componentName;
        
    }
    
    /**
     * uninstalls shared library
     * 
     * @param sharedLibraryName
     *            name of the shared library
     * @param forceDelete
     *            true to delete, false to not
     * @param targetName
     *            name of the target for this operation
     * @return Map of targetName and shared library name strings.
     * @throws ManagementRemoteException
     *             on error
     */
    protected String uninstallSharedLibraryInternal(String sharedLibraryName,
            boolean forceDelete, String targetName)
            throws ManagementRemoteException {
        return this.uninstallSharedLibrary(sharedLibraryName, targetName);
    }
    
    
}
