/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)AbstractListStateServiceMBeansImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.esb.management.base.services;

import java.io.Serializable;
import java.io.StringReader;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import javax.jbi.management.DeploymentServiceMBean;
import javax.management.Descriptor;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.modelmbean.ModelMBeanAttributeInfo;
import javax.management.ObjectName;

import com.sun.esb.management.common.ManagementRemoteException;
import com.sun.jbi.ComponentInfo;
import com.sun.jbi.ComponentQuery;
import com.sun.jbi.ComponentState;
import com.sun.jbi.ComponentType;
import com.sun.jbi.EnvironmentContext;
import com.sun.jbi.ui.common.JBIComponentInfo;
import com.sun.jbi.ui.common.JBIJMXObjectNames;
import com.sun.jbi.ui.common.ServiceAssemblyInfo;
import com.sun.jbi.ui.common.ServiceUnitInfo;

import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathExpressionException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


/**
 * @author graj
 * 
 */
public class AbstractListStateServiceMBeansImpl extends
        AbstractServiceMBeansImpl implements Serializable {
    
    static final long             serialVersionUID              = -1L;
    
    /**
     * any framework state
     */
    protected static final int    ANY_FRAMEWORK_COMPONENT_STATE = -1;
    
    /**
     * sa started state
     */
    protected static final String FRAMEWORK_SA_STARTED_STATE    = DeploymentServiceMBean.STARTED;
    
    /**
     * sa stopped state
     */
    protected static final String FRAMEWORK_SA_STOPPED_STATE    = DeploymentServiceMBean.STOPPED;
    
    /**
     * sa shutdown state
     */
    protected static final String FRAMEWORK_SA_SHUTDOWN_STATE   = DeploymentServiceMBean.SHUTDOWN;
    
    /**
     * any state
     */
    protected static final String FRAMEWORK_SA_ANY_STATE        = "any";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_STARTED_STATE    = "started";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_STOPPED_STATE    = "stopped";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_SHUTDOWN_STATE   = "shutdown";
    
    /**
     * state
     */
    protected static final String FRAMEWORK_SU_UNKNOWN_STATE    = "unknown";
    
    /**
     * namespace for Componennt version info node
     */
    protected static final String COMPONENT_VERSION_NS =
            "http://www.sun.com/jbi/descriptor/identification";
    
    /**
     * namespace for Componennt version info node
     */
    protected static final String COMPONENT_VERSION_NS_NEW =
            "http://www.sun.com/jbi/descriptor/identification/V1.0";
    
    /**
     * XPath query for new component version
     */
    protected static final String COMPONENT_VERSION_XPATH_QUERY =
            "/jbi:jbi/jbi:component/jbi:identification/identification:VersionInfo";
    
    /**
     * XPath query for new component version
     */
    protected static final String SHARED_LIBRARY_VERSION_XPATH_QUERY =
            "/jbi:jbi/jbi:shared-library/jbi:identification/identification:VersionInfo";
    
    /**
     * @param anEnvContext
     */
    public AbstractListStateServiceMBeansImpl(EnvironmentContext anEnvContext) {
        super(anEnvContext);
    }
    
    /**
     * returns the ObjectName for the lifecycle Mbean of this component.
     * 
     * @return the ObjectName for the lifecycle Mbean.
     * @param componentName
     *            of a binding or engine component.
     * @param targetName
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    protected ObjectName getComponentLifeCycleMBeanObjectName(
            String componentName, String targetName)
            throws ManagementRemoteException {
        
        if (!isExistingComponent(componentName, targetName)) {
            String[] args = { componentName, targetName };
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.lifecycle.mbean.not.found.with.query",
                            args, null);
            throw new ManagementRemoteException(exception);
        }
        
        ObjectName lifecycleObjectNamePattern = null;
        try {
            lifecycleObjectNamePattern = JBIJMXObjectNames
                    .getComponentLifeCycleMBeanObjectNamePattern(componentName,
                            targetName);
        } catch (MalformedObjectNameException ex) {
            throw new ManagementRemoteException(ex);
        }
        
        MBeanServer mbeanServer = this.environmentContext.getMBeanServer();
        
        Set objectNames = mbeanServer.queryNames(lifecycleObjectNamePattern,
                null);
        
        if (objectNames.isEmpty()) {
            String[] args = { componentName, targetName };
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.lifecycle.mbean.not.found.with.query",
                            args, null);
            throw new ManagementRemoteException(exception);
            
        }
        
        if (objectNames.size() > 1) {
            String[] args = { componentName, targetName };
            Exception exception = this.createManagementException(
                    "ui.mbean.multiple.lifecycle.mbeans.found.with.query",
                    args, null);
            throw new ManagementRemoteException(exception);
        }
        
        ObjectName lifecyleObjectName = (ObjectName) objectNames.iterator()
                .next();
        logDebug("LifecyleMBean : " + lifecyleObjectName);
        
        return lifecyleObjectName;
        
    }
    
    /**
     * return component info xml text that has service engines or binding
     * components infos or both which satisfies the options passed to the
     * method.
     * 
     * @param type
     *            ComponentInfo.BINDING, ENGINE, BINDING_ENGINE .
     * @param state
     *            return all the components that are in the specified state.
     *            valid states are JBIComponentInfo.STARTED, STOPPED, INSTALLED
     *            or null for ANY state
     * @param sharedLibraryName
     *            return all the components that have a dependency on the
     *            specified shared library. null value to ignore this option.
     * @param serviceAssemblyName
     *            return all the components that have the specified service
     *            assembly deployed on them. null value to ignore this option.
     * @param targetName
     * @return xml text contain the list of component infos
     * @throws ManagementRemoteException
     *             if error or exception occurs.
     */
    protected String listComponents(ComponentType type, String state,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        
        // ////////////////////////////////////////
        // Start Check to make sure target is valid
        // ////////////////////////////////////////
        ObjectName installerServiceObjectName = this
                .getInstallationServiceMBeanObjectName(targetName);
        this.checkForValidTarget(installerServiceObjectName, targetName);
        // ////////////////////////////////////////
        // End Check to make sure target is valid
        // ////////////////////////////////////////
        
        validateUiJBIComponentInfoState(state);
        
        ComponentState frameworkCompState = ComponentState.UNKNOWN;
        
        if (state != null) {
            frameworkCompState = toFrameworkComponentInfoState(state);
        }
        
        String result = "";
        try {
            result = JBIComponentInfo
                    .writeAsXmlText(toUiComponentInfoList(
                            this
                                    .getFrameworkComponentInfoListForEnginesAndBindings(
                                    /* frameworkCompType */type,
                                            frameworkCompState,
                                            sharedLibraryName,
                                            serviceAssemblyName, targetName),
                            targetName));
        } catch (ManagementRemoteException e) {
            throw new ManagementRemoteException(e);
        }
        
        return result;
    }
    
    /**
     * validates the state value component info
     * 
     * @state component state. valid values null, "", "started", "stopped",
     *        "shutdown"
     * @throws ManagementRemoteException
     *             on validation failure
     */
    protected void validateUiJBIComponentInfoState(String state)
            throws ManagementRemoteException {
        if (state == null || state.length() == 0) {
            // all states
            return;
        }
        String stateValue = state.toString();
        
        if (!(JBIComponentInfo.STARTED_STATE.equalsIgnoreCase(stateValue)
                || JBIComponentInfo.STOPPED_STATE.equalsIgnoreCase(stateValue)
                || JBIComponentInfo.SHUTDOWN_STATE.equalsIgnoreCase(stateValue) || JBIComponentInfo.UNKNOWN_STATE
                .equalsIgnoreCase(stateValue))) {
            String[] args = { state };
            Exception exception = this.createManagementException(
                    "ui.mbean.list.error.invalid.state", args, null);
            throw new ManagementRemoteException(exception);
            
        }
        
    }
    
    /**
     * validates the state value for service assembly
     * 
     * @state component state. valid values null, "", "started", "stopped",
     *        "shutdown"
     * @throws ManagementRemoteException
     *             on validation failure
     */
    protected void validateUiServiceAssemblyInfoState(String state)
            throws ManagementRemoteException {
        if (state == null || state.length() == 0) {
            // all sates
            return;
        }
        String stateValue = state;
        
        if (!(ServiceAssemblyInfo.STARTED_STATE.equalsIgnoreCase(stateValue)
                || ServiceAssemblyInfo.STOPPED_STATE
                        .equalsIgnoreCase(stateValue)
                || ServiceAssemblyInfo.SHUTDOWN_STATE
                        .equalsIgnoreCase(stateValue) || ServiceAssemblyInfo.UNKNOWN_STATE
                .equalsIgnoreCase(stateValue))) {
            String[] args = { state };
            Exception exception = this.createManagementException(
                    "ui.mbean.list.error.invalid.state", args, null);
            throw new ManagementRemoteException(exception);
            
        }
    }
    
    // //////////////////////////////////////////////////////
    // -- Adjusted for ComponentType and ComponentState --
    // //////////////////////////////////////////////////////
    /**
     * framework states
     * 
     * @return state
     * @param uiCompState
     *            state
     */
    protected static ComponentState toFrameworkComponentInfoState(
            String uiCompState) {
        if (uiCompState == null) {
            return ComponentState.UNKNOWN;
        }
        
        String compState = uiCompState.trim();
        
        if (compState.length() <= 0) {
            return ComponentState.UNKNOWN;
        }
        
        if (JBIComponentInfo.SHUTDOWN_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.SHUTDOWN;
        } else if (JBIComponentInfo.STARTED_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.STARTED;
        } else if (JBIComponentInfo.STOPPED_STATE.equalsIgnoreCase(compState)) {
            return ComponentState.STOPPED;
        } else { // any other value
            return ComponentState.UNKNOWN;
        }
    }
    
    /**
     * list of componentinfo objects. this methods check for valid inputs and
     * take only a valid inputs. for example slib, saName both can be null
     * 
     * @param frameworkCompType
     *            component type.
     * @param frameworkCompState
     *            component state
     * @param sharedLibraryName
     *            shared library name
     * @param serviceAssemblyName
     *            service assembly name
     * @param targetName
     * @throws com.sun.jbi.ui.common.ManagementRemoteException
     *             on error
     * @return list of component info
     */
    @SuppressWarnings("unchecked")
    protected List getFrameworkComponentInfoListForEnginesAndBindings(
            ComponentType frameworkCompType, ComponentState frameworkCompState,
            String sharedLibraryName, String serviceAssemblyName,
            String targetName) throws ManagementRemoteException {
        String slibName = null;
        String saName = null;
        
        if (sharedLibraryName != null && sharedLibraryName.trim().length() > 0) {
            slibName = sharedLibraryName.trim();
        }
        
        if (serviceAssemblyName != null
                && serviceAssemblyName.trim().length() > 0) {
            saName = serviceAssemblyName.trim();
        }
        
        logDebug("getFrameworkComponentInfoForEnginesAndBindings : Params : "
                + frameworkCompType + ", " + frameworkCompState + ", "
                + slibName + ", " + saName);
        
        Collection componentNames = new HashSet();
        // there are 4 options with snsId,auId (00,01,10,11)
        if (slibName == null && saName == null) {
            // 00
            componentNames = getComponentNamesWithStatus(frameworkCompType,
                    frameworkCompState, targetName);
        } else if (slibName == null && saName != null) {
            // 01
            componentNames = getComponentNamesDependentOnServiceAssembly(
                    frameworkCompType, frameworkCompState, saName, targetName);
            
        } else if (slibName != null && saName == null) {
            // 10
            componentNames = getComponentNamesDependentOnSharedLibrary(
                    frameworkCompType, frameworkCompState, slibName, targetName);
            
        } else if (slibName != null && saName != null) {
            // 11
            componentNames = getComponentNamesDependentOnSharedLibraryAndServiceAssembly(
                    frameworkCompType, frameworkCompState, slibName, saName,
                    targetName);
            
        } else {
            // not possible.
            componentNames = new HashSet();
        }
        
        return this.getFrameworkComponentInfoList(componentNames, targetName);
    }
    
    /**
     * framework component info list
     * 
     * @param compNameList
     *            list of component names
     * @param targetName
     * @return framework component info list
     */
    @SuppressWarnings("unchecked")
    protected List getFrameworkComponentInfoList(Collection compNameList,
            String targetName) {
        ArrayList compInfoList = new ArrayList();
        try {
            ComponentQuery componentQuery = getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                for (Iterator itr = compNameList.iterator(); itr.hasNext();) {
                    ComponentInfo componentInfo = componentQuery
                            .getComponentInfo((String) itr.next());
                    if (componentInfo != null) {
                        compInfoList.add(componentInfo);
                    }
                }
            }
        } catch (Exception ex) {
            // TODO propagate the exception to client.
            logDebug(ex);
        }
        return compInfoList;
    }
    
    /**
     * this methods check for valid inputs and take only a valid inputs. for
     * example slib, saName both can be null
     * 
     * @param componentName
     *            name
     * @param targetName
     * @throws com.sun.jbi.ui.common.ManagementRemoteException
     *             on error
     * @return list of componentInfo objects
     */
    @SuppressWarnings("unchecked")
    protected List getFrameworkComponentInfoListForSharedLibraries(
            String componentName, String targetName)
            throws ManagementRemoteException {
        String compName = null;
        
        if (componentName != null && componentName.trim().length() > 0) {
            compName = componentName.trim();
        }
        
        logDebug("getFrameworkComponentInfoForSharedLibraries: Params : "
                + compName);
        
        List slibNames = new ArrayList();
        List frameworkCompInfoList = new ArrayList();
        ComponentQuery componentQuery = null;
        if (compName == null) {
            logDebug("Listing All Shared Libraries in the system");
            componentQuery = this.getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                slibNames = componentQuery
                        .getComponentIds(ComponentType.SHARED_LIBRARY);
            }
        } else {
            logDebug("Listing Shared Libraries for the component " + compName);
            componentQuery = this.getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                ComponentInfo componentInfo = componentQuery
                        .getComponentInfo(compName);
                if (componentInfo == null) {
                    String[] args = { compName };
                    Exception exception = this.createManagementException(
                            "ui.mbean.component.id.does.not.exist", args, null);
                    throw new ManagementRemoteException(exception);
                    
                }
                slibNames = componentInfo.getSharedLibraryNames();
            }
        }
        
        frameworkCompInfoList = this
                .getFrameworkComponentInfoListForSharedLibraryNames(slibNames,
                        targetName);
        
        return frameworkCompInfoList;
        
    }
    
    /**
     * list of component infos
     * 
     * @param targetName
     * @return list of component infos
     * @param slibNameList
     *            shared library names
     */
    @SuppressWarnings("unchecked")
    protected List getFrameworkComponentInfoListForSharedLibraryNames(
            Collection slibNameList, String targetName) {
        ArrayList compInfoList = new ArrayList();
        try {
            ComponentQuery componentQuery = this
                    .getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                for (Iterator itr = slibNameList.iterator(); itr.hasNext();) {
                    ComponentInfo componentInfo = componentQuery
                            .getSharedLibraryInfo((String) itr.next());
                    if (componentInfo != null) {
                        compInfoList.add(componentInfo);
                    }
                }
            }
        } catch (Exception ex) {
            // TODO propagate the exception to client.
            logDebug(ex);
        }
        return compInfoList;
    }
    
    /**
     * list of ui component infos
     * 
     * @return list of ui component infos
     * @param frameworkCompType
     *            type of the component
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected List getUiComponentInfoList(ComponentType frameworkCompType,
            String targetName) throws ManagementRemoteException {
        Set compNameSet = this.getComponentNamesWithStatus(frameworkCompType,
                ComponentState.UNKNOWN, targetName);
        List frameworkCompList = this.getFrameworkComponentInfoList(
                new ArrayList(compNameSet), targetName);
        List uiCompList = new ArrayList();
        for (Iterator itr = frameworkCompList.iterator(); itr.hasNext();) {
            ComponentInfo frameworkCompInfo = (ComponentInfo) itr.next();
            JBIComponentInfo uiCompInfo = toUiComponentInfo(frameworkCompInfo,
                    targetName);
            uiCompList.add(uiCompInfo);
        }
        return uiCompList;
    }
    
    /**
     * convert to ui component info list
     * 
     * @return ui component info list
     * @param frameworkCompList
     *            framework component info list
     */
    @SuppressWarnings("unchecked")
    protected List toUiComponentInfoList(List frameworkCompList,
            String targetName) throws ManagementRemoteException {
        List uiCompList = new ArrayList();
        for (Iterator itr = frameworkCompList.iterator(); itr.hasNext();) {
            ComponentInfo frameworkCompInfo = (ComponentInfo) itr.next();
            JBIComponentInfo uiCompInfo = toUiComponentInfo(frameworkCompInfo,
                    targetName);
            uiCompList.add(uiCompInfo);
        }
        return uiCompList;
    }
    
    /**
     * return component info object
     * 
     * @return object
     * @param frameworkCompInfo
     *            framework component info
     * @param targetName
     */
    protected JBIComponentInfo toUiComponentInfo(
            ComponentInfo frameworkCompInfo, String targetName)
            throws ManagementRemoteException {
        
        String state = JBIComponentInfo.UNKNOWN_STATE;
        
        String componentName = frameworkCompInfo.getName();
        String componentDescription = frameworkCompInfo.getDescription();
        
        ComponentState componentStatus = ComponentState.UNKNOWN;
        ComponentType componentType = frameworkCompInfo.getComponentType();
        // Figure out the component state
        if ((false == ComponentType.BINDING.equals(componentType))
                && (false == ComponentType.ENGINE.equals(componentType))
                && (false == ComponentType.BINDINGS_AND_ENGINES
                        .equals(componentType))) {
            componentStatus = frameworkCompInfo.getStatus();
            state = toUiComponentInfoState(componentStatus);
        } else {
            // /////////////////////////////////////////////////////////
            // According to the runtime team (as of Nov 09'06):
            // The ComponentQuery.getStatus() or
            // ComponentQuery.getComponentInfo().getStatus()
            // to get the state of the component actually
            // returns the desired state of the component
            // persisted in the registry.
            //
            // The getCurrentState() operation of the
            // ComponentLifeCycleMBean returns the actual runtime
            // state for a component, which can be used to get the
            // actual runtime state of the component.
            // Therefore the component state is retrieved from the
            // facade ComponentLifeCycleMBean instead of
            // using ComponentQuery
            //
            // Use the LifeCycleMBean for the domain target too.
            // The getCurrentState() for the domain target should
            // return the most advanced state of the component on all targets.
            try {
                ObjectName lifeCycleMBeanObjectName = null;
                lifeCycleMBeanObjectName = this
                        .getComponentLifeCycleMBeanObjectName(componentName,
                                targetName);
                state = (String) this.getMBeanAttribute(
                        lifeCycleMBeanObjectName, "CurrentState");
            } catch (ManagementRemoteException exception) {
                // do not rethrow it. The state is already defaulted to
                // JBIComponentInfo.UNKNOWN_STATE
                componentStatus = ComponentState.UNKNOWN;
                state = JBIComponentInfo.UNKNOWN_STATE;
            }
            // /////////////////////////////////////////////////////////
        }
        if (true == RUNNING_STATE.equals(state)) {
            state = JBIComponentInfo.STARTED_STATE;
        }
        
        String type = toUiComponentInfoType(componentType);
        
        // Get descripor string to greb version and build numbers out
        String componentVersion = "";
        String buildNumber = "";
        
        try {
            String descriptorXml = frameworkCompInfo.getInstallationDescriptor();
            InputSource inputSource = new InputSource(new StringReader(descriptorXml));
            String queryString = "";
            if ( ComponentType.BINDING.equals(componentType)
                || ComponentType.ENGINE.equals(componentType)
                || ComponentType.BINDINGS_AND_ENGINES.equals(componentType) ) {
                queryString = COMPONENT_VERSION_XPATH_QUERY;
            } else if ( ComponentType.SHARED_LIBRARY.equals(componentType) ) {
                queryString = SHARED_LIBRARY_VERSION_XPATH_QUERY;
            }

            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();
            xpath.setNamespaceContext(new IdentificationNSContext());

            Node rtnNode = (Node) xpath.evaluate(queryString,
                                                 inputSource,
                                                 XPathConstants.NODE);
            
            if (rtnNode != null) {
                NamedNodeMap attrs = rtnNode.getAttributes();
                Node verAttr = attrs.getNamedItem("specification-version");
                if (verAttr != null) {
                    componentVersion = verAttr.getNodeValue();
                }
                
                Node buildAttr = attrs.getNamedItem("build-number");
                if (buildAttr != null) {
                    buildNumber = buildAttr.getNodeValue();
                }
            } else {
                // Try the new version attr name
                inputSource = new InputSource(new StringReader(descriptorXml));
                xpath.setNamespaceContext(new IdentificationNewNSContext());
                rtnNode = (Node) xpath.evaluate(queryString,
                                                inputSource,
                                                XPathConstants.NODE);
                if (rtnNode != null) {
                    NamedNodeMap attrs = rtnNode.getAttributes();
                    Node verAttr = attrs.getNamedItem("component-version");
                    if (verAttr != null) {
                        componentVersion = verAttr.getNodeValue();
                    }
                    
                    Node buildAttr = attrs.getNamedItem("build-number");
                    if (buildAttr != null) {
                        buildNumber = buildAttr.getNodeValue();
                    }
                } 
            }
        } catch (Exception exp) {
            String[] args = { "" };
            Exception exception = this.createManagementException(
                        "ui.mbean.install.config.mbean.error.get.attrs.error", args, null);
            throw new ManagementRemoteException(exception);
        }

        JBIComponentInfo compInfo = new JBIComponentInfo(type, // Integer.toString(comp.getComponentType()),
                state, // Integer.toString(comp.getStatus()),
                componentName, componentDescription);

        compInfo.setComponentVersion(componentVersion);
        compInfo.setBuildNumber(buildNumber);

        return compInfo;
    }
    
    /**
     * converts to deployment service service assembly state.
     * 
     * @return state
     * @param uiState
     *            state
     */
    protected static String toFrameworkServiceAssemblyState(String uiState) {
        if (uiState == null) {
            return FRAMEWORK_SA_ANY_STATE;
        }
        
        String saState = uiState.trim();
        
        if (saState.length() <= 0) {
            return FRAMEWORK_SA_ANY_STATE;
        }
        
        if (ServiceAssemblyInfo.STARTED_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_STARTED_STATE;
        } else if (ServiceAssemblyInfo.STOPPED_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_STOPPED_STATE;
        } else if (ServiceAssemblyInfo.SHUTDOWN_STATE.equalsIgnoreCase(saState)) {
            return FRAMEWORK_SA_SHUTDOWN_STATE;
        } else { // any other value
            return FRAMEWORK_SA_ANY_STATE;
        }
    }
    
    /**
     * converts to ui service assembly state
     * 
     * @return state
     * @param frameworkState
     *            state
     */
    protected static String toUiServiceAssemblyState(String frameworkState) {
        String uiState = ServiceAssemblyInfo.UNKNOWN_STATE;
        
        if (frameworkState == null) {
            return ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        
        String saState = frameworkState.trim();
        
        if (saState.length() <= 0) {
            return ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        
        if (FRAMEWORK_SA_STARTED_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.STARTED_STATE;
        } else if (FRAMEWORK_SA_STOPPED_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.STOPPED_STATE;
        } else if (FRAMEWORK_SA_SHUTDOWN_STATE.equalsIgnoreCase(saState)) {
            uiState = ServiceAssemblyInfo.SHUTDOWN_STATE;
        } else {
            uiState = ServiceAssemblyInfo.UNKNOWN_STATE;
        }
        return uiState;
    }
    
    /**
     * converts to ui service unit state
     * 
     * @return state
     * @param frameworkState
     *            state
     */
    protected static String toUiServiceUnitState(String frameworkState) {
        String uiState = ServiceUnitInfo.UNKNOWN_STATE;
        
        if (frameworkState == null) {
            return ServiceUnitInfo.UNKNOWN_STATE;
        }
        
        String suState = frameworkState.trim();
        
        if (suState.length() <= 0) {
            return ServiceUnitInfo.UNKNOWN_STATE;
        }
        
        if (FRAMEWORK_SU_STARTED_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.STARTED_STATE;
        } else if (FRAMEWORK_SU_STOPPED_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.STOPPED_STATE;
        } else if (FRAMEWORK_SU_SHUTDOWN_STATE.equalsIgnoreCase(suState)) {
            uiState = ServiceUnitInfo.SHUTDOWN_STATE;
        } else {
            uiState = ServiceUnitInfo.UNKNOWN_STATE;
        }
        return uiState;
    }
    
    /**
     * converts state
     * 
     * @return state
     * @param frameworkCompState
     *            state
     */
    protected static String toUiComponentInfoState(
            ComponentState frameworkCompState) {
        String uiState = JBIComponentInfo.UNKNOWN_STATE;
        switch (frameworkCompState) {
            case SHUTDOWN:
                uiState = JBIComponentInfo.SHUTDOWN_STATE;
                break;
            case STARTED:
                uiState = JBIComponentInfo.STARTED_STATE;
                break;
            case STOPPED:
                uiState = JBIComponentInfo.STOPPED_STATE;
                break;
            default:
                uiState = JBIComponentInfo.UNKNOWN_STATE;
                break;
        }
        return uiState;
    }
    
    /**
     * converts type
     * 
     * @return int type
     * @param uiCompType
     *            type
     */
    protected static ComponentType toFrameworkComponentInfoType(
            String uiCompType) {
        if (JBIComponentInfo.BINDING_TYPE.equalsIgnoreCase(uiCompType)) {
            return ComponentType.BINDING;
        } else if (JBIComponentInfo.ENGINE_TYPE.equalsIgnoreCase(uiCompType)) {
            return ComponentType.BINDING;
        } else if (JBIComponentInfo.SHARED_LIBRARY_TYPE
                .equalsIgnoreCase(uiCompType)) {
            return ComponentType.SHARED_LIBRARY;
        } else {
            return ComponentType.BINDINGS_AND_ENGINES;
        }
    }
    
    /**
     * converts type
     * 
     * @return type
     * @param frameworkCompType
     *            type
     */
    protected static String toUiComponentInfoType(
            ComponentType frameworkCompType) {
        String uiType = JBIComponentInfo.UNKNOWN_TYPE;
        switch (frameworkCompType) {
            case BINDING:
                uiType = JBIComponentInfo.BINDING_TYPE;
                break;
            case ENGINE:
                uiType = JBIComponentInfo.ENGINE_TYPE;
                break;
            case SHARED_LIBRARY:
                uiType = JBIComponentInfo.SHARED_LIBRARY_TYPE;
                break;
            default:
                uiType = JBIComponentInfo.UNKNOWN_TYPE;
                break;
        }
        return uiType;
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected Set getComponentNamesWithStatus(ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String targetName) {
        List componentIdList = new ArrayList();
        ComponentQuery componentQuery = getFrameworkComponentQuery(targetName);
        if (componentQuery != null) {
            if (frameworkCompStatus == ComponentState.UNKNOWN) {
                // ANY STATE
                componentIdList = componentQuery
                        .getComponentIds(frameworkCompType);
            } else {
                componentIdList = componentQuery.getComponentIds(
                        frameworkCompType, frameworkCompStatus);
            }
        }
        
        // now retain only ones that has auID deployed.
        return new HashSet(componentIdList);
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param saName
     *            service assembly name
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected Collection getComponentNamesDependentOnServiceAssembly(
            ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String saName, String targetName) {
        
        Set compNameSet = new HashSet(getComponentNamesWithStatus(
                frameworkCompType, frameworkCompStatus, targetName));
        Set saNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnServiceAssembly(saName,
                        targetName));
        // now retain only ids in the compIdSet that are in compIdSetWithAuId
        compNameSet.retainAll(saNameDepCompNameSet);
        return compNameSet;
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param slibName
     *            shared library name
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected Collection getComponentNamesDependentOnSharedLibrary(
            ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String slibName,
            String targetName) {
        
        Set compNameSet = new HashSet(this.getComponentNamesWithStatus(
                frameworkCompType, frameworkCompStatus, targetName));
        Set slibNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnSharedLibrary(slibName,
                        targetName));
        // now retain only ids in the compIdSet that are in compIdSetWithSnsd
        compNameSet.retainAll(slibNameDepCompNameSet);
        return compNameSet;
    }
    
    /**
     * list of component names. this method requires non null inputs
     * 
     * @return list of component names.
     * @param frameworkCompType
     *            component type
     * @param frameworkCompStatus
     *            component state
     * @param slibName
     *            shared library name
     * @param saName
     *            service assembly name
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected Collection getComponentNamesDependentOnSharedLibraryAndServiceAssembly(
            ComponentType frameworkCompType,
            ComponentState frameworkCompStatus, String slibName, String saName,
            String targetName) {
        
        Set compNameSet = new HashSet(this.getComponentNamesWithStatus(
                frameworkCompType, frameworkCompStatus, targetName));
        Set slibNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnSharedLibrary(slibName,
                        targetName));
        Set saNameDepCompNameSet = new HashSet(
                this.getComponentNamesDependentOnServiceAssembly(saName,
                        targetName));
        // intersection of SLIB and SA
        slibNameDepCompNameSet.retainAll(saNameDepCompNameSet);
        // intersection of type, status, SLIB, SA
        compNameSet.retainAll(slibNameDepCompNameSet);
        return compNameSet;
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param slibName
     *            shared library name.
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected Collection getComponentNamesDependentOnSharedLibrary(
            String slibName, String targetName) {
        List componentNames = new ArrayList();
        try {
            ComponentQuery componentQuery = this
                    .getFrameworkComponentQuery(targetName);
            if (componentQuery != null) {
                componentNames = componentQuery
                        .getDependentComponentIds(slibName);
            }
        } catch (Exception ex) {
            // log exception
            logDebug(ex);
        }
        return componentNames;
    }
    
    /**
     * list of component names
     * 
     * @return list of component names
     * @param saId
     *            service assembly name.
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected Collection getComponentNamesDependentOnServiceAssembly(
            String saId, String targetName) {
        try {
            String[] componentNames = (String[]) this.invokeMBeanOperation(this
                    .getDeploymentServiceMBeanObjectName(targetName),
                    "getComponentsForDeployedServiceAssembly", saId);
            if (componentNames == null) {
                componentNames = new String[0];
            }
            return new HashSet(Arrays.asList(componentNames));
        } catch (Exception ex) {
            // log exception
            logDebug(ex);
            // empty set
            return new HashSet();
        }
    }
    
    /**
     * return framework query
     * 
     * @param targetName
     * 
     * @return framework query interface
     */
    protected ComponentQuery getFrameworkComponentQuery(String targetName) {
        ComponentQuery componentQuery = null;
        /*
         * ==============================================================
         * According to IN=100359 at
         * http://inf.central.sun.com/inf/integrationReport.jsp?id=100359
         * 
         * The ComponentQueryImpl has been updated to support the "domain"
         * targetName as well, but is not plugged into the JBI Framework.
         * ==============================================================
         */
        // if (JBIAdminCommands.DOMAIN_TARGET_KEY.equals(targetName) == false) {
        // componentQuery = this.environmentContext.getComponentQuery();
        // }
        componentQuery = this.environmentContext.getComponentQuery(targetName);
        return componentQuery;
    }
    
    /**
     * checks the component name in the registry
     * 
     * @return true if component exists else false.
     * @param componentName
     *            name of the component
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected boolean isExistingComponent(String componentName,
            String targetName) {
        List list = new ArrayList();
        ComponentQuery componentQuery = this
                .getFrameworkComponentQuery(targetName);
        if (componentQuery != null) {
            list = componentQuery
                    .getComponentIds(ComponentType.BINDINGS_AND_ENGINES);
        }
        return list.contains(componentName);
    }
    
    /**
     * check for shared namespace
     * 
     * @return true if it is namespace id else false.
     * @param sharedLibraryName
     *            id String
     * @param targetName
     */
    @SuppressWarnings("unchecked")
    protected boolean isExistingSharedLibrary(String sharedLibraryName,
            String targetName) {
        List list = new ArrayList();
        ComponentQuery componentQuery = this
                .getFrameworkComponentQuery(targetName);
        if (componentQuery != null) {
            list = componentQuery.getComponentIds(ComponentType.SHARED_LIBRARY);
        }
        return list.contains(sharedLibraryName);
    }
    
    /**
     * list of service assembly infos
     * 
     * @param frameworkState
     *            state
     * @param componentName
     *            name
     * @param targetName
     * @throws com.sun.jbi.ui.common.ManagementRemoteException
     *             on error
     * @return list of service assembly infos
     */
    @SuppressWarnings("unchecked")
    protected List getServiceAssemblyInfoList(String frameworkState,
            String componentName, String targetName)
            throws ManagementRemoteException {
        String compName = null;
        
        if (componentName != null) {
            compName = componentName.trim();
        }
        
        String[] saNames = new String[0];
        
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        // filter them by component name
        if (compName == null || compName.length() <= 0) {
            logDebug("getting all the deployed service assemblies with state "
                    + frameworkState);
            
            saNames = (String[]) getMBeanAttribute(deploymentServiceObjectName,
                    "DeployedServiceAssemblies");
        } else {
            logDebug("getting all the deployed service assemblies for the comp "
                    + compName);
            
            if (false == this.isExistingComponent(compName, targetName)) {
                String[] args = { compName };
                Exception exception = this.createManagementException(
                        "ui.mbean.component.id.does.not.exist", args, null);
                throw new ManagementRemoteException(exception);
                
            }
            
            saNames = (String[]) invokeMBeanOperation(
                    deploymentServiceObjectName,
                    "getDeployedServiceAssembliesForComponent", compName);
        }
        
        // construct service assembly infos from the descriptor
        
        List saInfoByCompNameList = new ArrayList();
        
        for (int i = 0; i < saNames.length; ++i) {
            String saName = saNames[i];
            logDebug("getting deployment descriptor for " + saName);
            String saDDText = (String) invokeMBeanOperation(
                    deploymentServiceObjectName,
                    "getServiceAssemblyDescriptor", saName);
            ServiceAssemblyInfo saInfo = ServiceAssemblyInfo
                    .createFromServiceAssemblyDD(new StringReader(saDDText));
            // update the state of the service assembly from runtime.
            this.updateServiceAssemblyInfoState(saInfo, targetName);
            
            saInfoByCompNameList.add(saInfo);
        }
        
        // && filter them by state.
        
        List saInfoList = new ArrayList();
        
        if (frameworkState.equalsIgnoreCase(FRAMEWORK_SA_ANY_STATE)) {
            saInfoList.addAll(saInfoByCompNameList);
            
        } else {
            // filter by specific state
            String uiState = toUiServiceAssemblyState(frameworkState);
            for (Iterator itr = saInfoByCompNameList.iterator(); itr.hasNext();) {
                ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
                if (uiState.equalsIgnoreCase(saInfo.getState())) {
                    saInfoList.add(saInfo);
                }
            }
        }
        
        // update Each ServiceUnit's State In ServiceAssembly;
        
        for (Iterator itr = saInfoList.iterator(); itr.hasNext();) {
            ServiceAssemblyInfo saInfo = (ServiceAssemblyInfo) itr.next();
            
            updateEachServiceUnitInfoStateInServiceAssemblyInfo(saInfo,
                    targetName);
        }
        
        return saInfoList;
        
    }
    
    /**
     * update state in service assembly info
     * 
     * @param saInfo
     *            service assembly info
     * @param targetName
     * @throws com.sun.jbi.ui.common.ManagementRemoteException
     *             on error
     */
    protected void updateServiceAssemblyInfoState(ServiceAssemblyInfo saInfo,
            String targetName) throws ManagementRemoteException {
        ObjectName deploymentServiceObjectName = this
                .getDeploymentServiceMBeanObjectName(targetName);
        
        String saName = saInfo.getName();
        String frameworkState = FRAMEWORK_SA_ANY_STATE;
        try {
            frameworkState = (String) invokeMBeanOperation(
                    deploymentServiceObjectName, "getState", saName);
        } catch (Exception ex) {
            logDebug("issue 30: state invalid: " + ex.getMessage());
            frameworkState = "Unknown";
        }
        logDebug("Framework State = " + frameworkState
                + " for Service Assembly = " + saName);
        String uiState = toUiServiceAssemblyState(frameworkState);
        saInfo.setState(uiState);
    }
    
    /**
     * update the state in each service unit info in service assembly info
     * 
     * @param saInfo
     *            service assembly info object
     * @param targetName
     * @throws com.sun.jbi.ui.common.ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    protected void updateEachServiceUnitInfoStateInServiceAssemblyInfo(
            ServiceAssemblyInfo saInfo, String targetName)
            throws ManagementRemoteException {
        List list = saInfo.getServiceUnitInfoList();
        if (list == null) {
            return;
        }
        
        // /////////////////////////////////////////////////////
        // As of Nov 09'06 the runtime team says:
        // To get the actual runtime state of a ServiceAssembly
        // invoke the getState() operation on the facade
        // DeploymentServiceMBean(). You should not be using the
        // DeployerMBean for anything, since you do not have
        // access to the remote instance DeployerMBeans and
        // it is not a standard MBean.
        //
        // Note that the DeploymentServiceMBean returns the
        // state as a String (see
        // javax.jbi.management.DeploymentServiceMBean for
        // the string values returned
        // {Started, Stopped, Shutdown}
        // TODO: Need to comment out the following code once the runtime
        // provides a way to get Service Unit State
        ObjectName deploymentServiceMBean = this
                .getDeploymentServiceMBeanObjectName(targetName);
        for (Iterator itr = list.iterator(); itr.hasNext();) {
            ServiceUnitInfo suInfo = (ServiceUnitInfo) itr.next();
            updateServiceUnitInfoState(suInfo, deploymentServiceMBean);
        }
        
        // /////////////////////////////////////////////////////
    }
    
    /**
     * updates the state in the service unit info
     * 
     * @param suInfo
     *            service unit info object
     * @param ObjectName
     *            deploymentServiceMBean
     */
    protected void updateServiceUnitInfoState(ServiceUnitInfo suInfo,
            ObjectName deploymentServiceMBean) {
        suInfo.setState(ServiceUnitInfo.UNKNOWN_STATE);
        
        String compName = suInfo.getDeployedOn();
        String suName = suInfo.getName();
        try {
            // locate the deployer mbean on the component
            if (deploymentServiceMBean == null) {
                logDebug("DeployerMBean not found for component "
                        + compName);
                return;
            }
            
            // get the state of the service unit from the deployer mbean
            // String getServiceUnitState(String componentName, String
            // serviceUnitName)
            Object[] params = new Object[2];
            params[0] = compName;
            params[1] = suName;
            
            String[] signature = new String[2];
            signature[0] = "java.lang.String";
            signature[1] = "java.lang.String";
            
            String frameworkState = (String) invokeMBeanOperation(
                    deploymentServiceMBean, "getServiceUnitState", params,
                    signature);
            
            logDebug("Framework State = " + frameworkState
                    + " for Service UNIT = " + suName);
            
            String uiState = toUiServiceUnitState(frameworkState);
            
            suInfo.setState(uiState);
            
        } catch (Exception ex) {
            // ignore the exception and log it
            log(getI18NBundle().getMessage(
                    "ui.mbean.exception.getting.service.unit.state", suName,
                    compName, ex.getMessage()));
            logDebug(ex);
        }
    }
    
    /**
     * Looks up the cascaded LoggerMBeans for all JBI Framework
     * System Services for a specific instance
     * 
     * @return array of object names for all system service LoggerMBeans.
     */
    @SuppressWarnings("unchecked")
    protected ObjectName[] getSystemLoggerMBeans(String instanceName)
    {
        com.sun.jbi.management.MBeanNames mbn = environmentContext.getMBeanNames();
        String tmp = mbn.getJmxDomainName();

        tmp += ":" + com.sun.jbi.management.MBeanNames.INSTANCE_NAME_KEY  + "=" + instanceName;
        tmp += "," + com.sun.jbi.management.MBeanNames.COMPONENT_TYPE_KEY + "=" + com.sun.jbi.management.MBeanNames.COMPONENT_TYPE_SYSTEM;
        tmp += "," + com.sun.jbi.management.MBeanNames.CONTROL_TYPE_KEY   + "=" + com.sun.jbi.management.MBeanNames.CONTROL_TYPE_LOGGER;
        //wildcard goes at the end:
        tmp += ",*";

        //exec the query:
        ObjectName mbeanPattern = null;
        try
        {
            mbeanPattern = new ObjectName(tmp);
        }
        catch(javax.management.MalformedObjectNameException mex)
        {
            logDebug(mex.getMessage());
            return new ObjectName[0];
        }
        
        java.util.Set resultSet 
                = environmentContext.getMBeanServer().queryNames(mbeanPattern, null);
        ObjectName[] names = new ObjectName[0];
        
        if ( !resultSet.isEmpty() )
        {
            names = (ObjectName[]) resultSet.toArray(names);
        }
        else
        {
            logDebug("Logger MBeans with ObjectName pattern "
                        + tmp + " not found.");
        }

        return names;
    }
    
   /**
     * Get the attributes on the MBean
     * 
     * @param mbeanServer
     * @param objectName
     * @return
     * @throws JBIRemoteException
     */
    protected Map<String, Descriptor> getConfigurationDescriptors(
            ObjectName objectName) throws ManagementRemoteException 
    {
        Map<String, Descriptor> descrMap = new HashMap<String, Descriptor>();
        MBeanServer mbeanServer = environmentContext.getMBeanServer();
        MBeanInfo mbeanInfo = null;
        try {
            mbeanInfo = mbeanServer.getMBeanInfo(objectName);
            MBeanAttributeInfo[] mbeanAttrInfoArray = mbeanInfo.getAttributes();
            
            for (MBeanAttributeInfo attributeInfo : mbeanAttrInfoArray) 
            {
                if ( attributeInfo instanceof ModelMBeanAttributeInfo )
                {
                    ModelMBeanAttributeInfo modelAttrInfo
                            = (ModelMBeanAttributeInfo) attributeInfo;
                    descrMap.put(modelAttrInfo.getName(), modelAttrInfo.getDescriptor());
                }
            }
        } catch (javax.management.InstanceNotFoundException e) {
            throw new ManagementRemoteException(e);
        } catch (javax.management.IntrospectionException e) {
            throw new ManagementRemoteException(e);
        } catch (javax.management.ReflectionException e) {
            throw new ManagementRemoteException(e);
        }
        
        return descrMap;
    }    
    
    /**
     * Gets the component state
     * 
     * @param componentName
     * @param targetName
     * @return the state of the component
     * @throws ManagementRemoteException
     *             on error
     */
    @SuppressWarnings("unchecked")
    protected String getComponentState(String componentName, String targetName)
            throws ManagementRemoteException {
        String result = JBIComponentInfo.UNKNOWN_STATE;
        String xmlText = null;
        List<JBIComponentInfo> infoList = null;
        String state = null, sharedLibraryName = null, serviceAssemblyName = null;
        try {
            xmlText = this.listComponents(ComponentType.BINDING, state,
                    sharedLibraryName, serviceAssemblyName, targetName);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (info.getName().equals(componentName) == true) {
                        return info.getState();
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore exception
        }
        try {
            xmlText = this.listComponents(ComponentType.ENGINE, state,
                    sharedLibraryName, serviceAssemblyName, targetName);
            if ((xmlText != null) && (xmlText.length() > 0)) {
                infoList = JBIComponentInfo.readFromXmlText(xmlText);
                for (JBIComponentInfo info : infoList) {
                    if (info.getName().equals(componentName) == true) {
                        return info.getState();
                    }
                }
            }
        } catch (ManagementRemoteException e) {
            // Ignore exception
        }
        return result;
    }
    /**
     * Check if a target is up. A JBI Management exception is thrown 
     * if the target is down.
     *
     * @param target - target name, can be standalone or clustered instance, cluster
     *                 or "server".
     * @throws ManagementRemoteException if component is not started on target.
     */
    protected void targetUpCheck(String target)
        throws ManagementRemoteException
    {
        String targetName = target;
        
        if ( this.getPlatformContext().isClusteredServer(targetName) )
        {
            // -- The actual Target is the cluster
            targetName = this.getPlatformContext().getTargetName(target);
        }

        
        if ( !isTargetUp(targetName) )
        {
            Exception exception = this
                    .createManagementException(
                            "ui.mbean.target.down",
                            new String[]{targetName}, null);
            throw new ManagementRemoteException(exception);
        }
    }
    
    
    /**
     * Check if the component is in the "Started" state on the target. A 
     * JBI Management exception is thrown if the component is not in the started
     * state.
     *
     * @param componentName - component name
     * @param target - target name, can be standalone or clustered instance, cluster
     *                 or "server".
     * @throws ManagementRemoteException if component is not started on target.
     */
    protected void componentStartedOnTargetCheck(String componentName, String target)
        throws ManagementRemoteException
    {
        String targetName = target;
        
        if ( this.getPlatformContext().isClusteredServer(targetName) )
        {
            // -- The actual Target is the cluster
            targetName = this.getPlatformContext().getTargetName(target);
        }
        
        String state = this.getComponentState(componentName, targetName);
        
        if ( !(com.sun.jbi.ui.common.JBIComponentInfo.STARTED_STATE.equalsIgnoreCase(state) ||
               com.sun.jbi.ui.common.JBIComponentInfo.STOPPED_STATE.equalsIgnoreCase(state)) )
        {
            Exception exception = this
                .createManagementException(
                    "ui.mbean.component.configuration.not.started.not.stopped.on.target.error",
                            new String[]{componentName, targetName}, null);
            throw new ManagementRemoteException(exception);
        }
    } 
    
    /**
     * Check if the component is installed on the target. A 
     * JBI Management exception is thrown if the component is not installed.
     *
     * @param componentName - component name
     * @param target - target name, can be standalone or clustered instance, cluster
     *                 or "server".
     * @throws ManagementRemoteException if component is not started on target.
     */
    protected void componentInstalledOnTargetCheck(String componentName, String target)
        throws ManagementRemoteException
    {
        String targetName = target;
        
        if ( this.getPlatformContext().isClusteredServer(targetName) )
        {
            // -- The actual Target is the cluster
            targetName = this.getPlatformContext().getTargetName(target);
        }
        
        boolean isInstalled = this.isExistingComponent(componentName, targetName);
        
        if ( !isInstalled )
        {
            Exception exception = this
                    .createManagementException(
                            "ui.component.not.installed.on.target.error",
                            new String[]{componentName, targetName}, null);
            throw new ManagementRemoteException(exception);
        }
    }    
    
    /*
     * Inner class defined for component identification namespace
     */
    class IdentificationNSContext implements NamespaceContext
    {

        /**
         * This method maps the prefix jbi and config to the right URIs
         */
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("jbi"))
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
            else if (prefix.equals("identification"))
            {
                return COMPONENT_VERSION_NS;
            }
            else
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }

    /*
     * Inner class defined for component identification namespace
     */
    class IdentificationNewNSContext implements NamespaceContext
    {

        /**
         * This method maps the prefix jbi and config to the right URIs
         */
        public String getNamespaceURI(String prefix)
        {
            if (prefix.equals("jbi"))
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
            else if (prefix.equals("identification"))
            {
                return COMPONENT_VERSION_NS_NEW;
            }
            else
            {
                return "http://java.sun.com/xml/ns/jbi";
            }
        }

        public String getPrefix(String uri) {
            throw new UnsupportedOperationException();
        }

        public Iterator getPrefixes(String uri) {
            throw new UnsupportedOperationException();
        }
    }
}
