/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestRuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package java4ant;

import javax.management.modelmbean.*;
import javax.management.remote.*;
import javax.management.*;

/**
 * TestRuntimeConfiguration gets the MBeanInfo of a runtime configuration MBean.
 * This gives details on the attributes supported by the MBean and it's meta-data.
 */
public class TestRuntimeConfiguration
{
     
    private MBeanServerConnection mbns;
    
    private static final String USER     = "jmx.user";
    private static final String PASSWORD = "jmx.password";
    private static final String PROVIDER = "jmx.provider";
    private static final String TARGET   = "target";
    private static final String CATEGORY = "category";
    
    public void initMBeanServerConnection()
        throws Exception
    {
        java.util.Map<String, String[]> env = new java.util.HashMap();
        String user = System.getProperty(USER);
        String pass = System.getProperty(PASSWORD);
        String[] credentials = new String[] { user, pass};
        env.put("jmx.remote.credentials", credentials);   
        
        String jmxProvider = System.getProperty(PROVIDER);
        
        JMXConnector connector = JMXConnectorFactory.connect(new JMXServiceURL(jmxProvider), env);

        mbns = connector.getMBeanServerConnection();
    }
    
    public ObjectName getRuntimeConfigMBeanName()
        throws Exception
    {
        String target = System.getProperty(TARGET);
        String category = System.getProperty(CATEGORY);
        String rtConfig = "com.sun.jbi:" +
            "Target=" + target + 
            ",ServiceName=ConfigurationService" +
            ",ServiceType=" + category;
        
        return new ObjectName(rtConfig);
    }
    
    
    public void getMBeanInfo()
        throws Exception
    {
        MBeanInfo mbeanInfo = mbns.getMBeanInfo(getRuntimeConfigMBeanName());
        
        if ( mbeanInfo != null )
        {
            System.out.println("Attribute Information for MBean " + getRuntimeConfigMBeanName());
            
            MBeanAttributeInfo[] attribs = mbeanInfo.getAttributes();
            
            for (MBeanAttributeInfo attrib : attribs )
            {
                System.out.println("\nName : " + attrib.getName());
                System.out.println("Description : " + attrib.getDescription());
                
                if ( attrib instanceof ModelMBeanAttributeInfo )
                {
                    Descriptor descr = ( (ModelMBeanAttributeInfo) attrib).getDescriptor();
                    String[] fields = descr.getFieldNames();
                    for ( String field : fields )
                    {
                        System.out.println("    " + field + " = " + descr.getFieldValue(field));
                    }
                }
            }
        }
        
    }

    
    public static void main (String[] params)
        throws Exception 
    {
        TestRuntimeConfiguration test = new TestRuntimeConfiguration();
        
        test.initMBeanServerConnection();
        test.getMBeanInfo();
    }
    
}
