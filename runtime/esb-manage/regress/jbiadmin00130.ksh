#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00130.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

echo "jbiadmin00130 : Test fix for CR 6514733"

. ./regress_defs.ksh

COMPONENT_ARCHIVE=$JBI_KIT_FB
COMPONENT_NAME=SunFileBinding

#
# Setup :
#
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml runtests

#
# Component test 
#

# Install and uninstall from instance
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="instance1" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target="instance1" -Djbi.keep.archive=true uninstall-component
$JBI_ANT_NEG -Djbi.target="domain" list-binding-components


$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target="domain" -Djbi.keep.archive=false uninstall-component
$JBI_ANT_NEG -Djbi.target="domain" list-binding-components


$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  -Djbi.target="instance1" install-component
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME -Djbi.target="instance1" -Djbi.keep.archive=false uninstall-component
$JBI_ANT_NEG -Djbi.target="domain" list-binding-components
