/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventManagementConstants.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

public interface EventManagementConstants {



    public static final String EVENTMANAGEMENT_CHANNEL_MBEAN_NAME = "EventManagement:name=EventManagementChannelSupportMBean";    
    public static final String MBEAN_SERVER_DELEGATE_OBJECT_NAME = "JMImplementation:type=MBeanServerDelegate";
    public static final String EVENT_MANAGEMENT_REMOVAL_POLICY_TIMER_MBEAN_NAME = "EventManagement:type=service,name=RemovalPolicyIntervalTimerService";
    public static final String EVENT_MANAGEMENT_TARGET_CHECK_TIMER_MBEAN_NAME = "EventManagement:type=service,name=InstanceCheckIntervalTimerService";
    public static final String EVENT_FORWARD_OPERATION = "forwardEvent";    
    public static final String EVENT_CREATEFORWARD_OPERATION = "createAndForwardEvent";    
    public static final String JBI_INSTALL_ROOT_ATTRIBUTE_NAME = "JBIInstallRoot";    
    public static final String ACKNOWLEDGE_ALERT_RECEPTION = "acknowledgeAlertReception";
    public static final String EVENT_MANAGEMENT_UNIQUE_TABLE_NAME_ATTRIBUTE_NAME = "EMuniqueTableName";
    public static final String JCAPS_EVENT_TYPE = "JCAPSEvent";    

    public static final int DEFAULT_INTERVAL = 60000; // 1 minute
    public static final int MAX_CACHE = 500; 
    public static final Long DEFAULT_POLICY_EXEC_INTERVAL = 
        new Long(DEFAULT_INTERVAL*10); // EVERY 10 MIN.
    public static final Long DEFAULT_TARGET_CHECK_INTERVAL = 
        new Long(3000); // EVERY 3 Sec.
    public static final String EVENT_TARGETCHECK_FILTER_TYPE = "eventManagement.instanceCheck";
    
    /**
     * Process controller MBean's "invoke" operation name
     */
    public static final String EVENT_MAMAGENEMT_TABLE_PREFIX = "EVENTMANAGEMENT";

    
    
    public static final String EVENT_CONFIG_DIRECTORY = "/config"; 
    public static final String EVENT_CONFIG_PROPERTIES_FILE = "eventmanagement.properties"; 

    public static final String DATABASE_TYPE_PROPERTY = "DatabaseType";
    public static final String DATABASE_JNDI_NAME_PROPERTY = "DBJndiName";
    public static final String PERSISTENCE_ENABLED_PROPERTY = "PersistenceEnabled";
    public static final String JOURNAL_ENABLED_PROPERTY = "journalEnabled";
    public static final String PERSISTENCE_POLICY_EXEC_ENABLED_PROPERTY = "PersistencePolicyExecEnabled";
    public static final String PERSISTENCE_POLICY_EXEC_INTERVAL_PROPERTY = "PersistencePolicyExecInterval";
    public static final String PERSISTENCE_POLICY_ALERT_LEVEL_PROPERTY = "PersistenceAlertLevel";
    public static final String PERSISTENCE_POLICY_ALERT_AGE_PROPERTY = "PersistenceAlertAge";
    public static final String PERSISTENCE_POLICY_ALERT_COUNT_PROPERTY = "PersistenceAlertCount";
    public static final String PERSISTENCE_ENFORCED_POLICY_PROPERTY = "PolicyEnforced";
    public static final String PERSISTENCE_ENFORCED_POLICY_DELIMITER = ",";
    public static final String DATABASE_ALERT_TABLE_NAME_PROPERTY_NAME = "AlertTableName";
    
    
    // the following constant are identical to the constants define in AlertConstants.java
    // in the CAPSManagementAPI alerts package. they must kept in  sync until it will merge
    // into the open source project (at which time they should be removed here and referenced
    // by include the CAPSManagementAPI jars

    public static final String DISABLE_PERSISTENCE_OPERATION_NAME = "disableAlertsPersistence";
    public static final String ENABLE_PERSISTENCE_OPERATION_NAME = "enableAlertsPersistence";
    public static final String UPDATE_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME = "updatePersistedAlertRemovalPolicy"; 
    public static final String GET_LAST_PERSISTED_ALERT_REMOVAL_PROLICY_OPERATION_NAME = "getLastPersistedAlertRemovalPolicy"; 
    public static final String CHECK_LISTENER_UUID = "checkListenerID";    
    public static final String SET_ATTRIBUTE_OP = "setAttribute";    
    public static final String GET_ATTRIBUTE_OP = "getAttribute";    
    public static final String INVOKE_OP = "invoke";    
    public static final String ALERT_REGISTRATION_WITH_TARGET = "RegisterWithTarget";    
    public static final String GET_HTTP_SERVER_INFO = "getHttpServerInfo"; 
    public static final String GET_DAS_ADMINSERVER_INFO = "getDasAdminServerInfo"; 
    public static final String IS_SECURE_ADMIN_PORT = "isSecureAdminPort"; 
    public static final String GET_QUEUED_EVENTS = "getQueuedEvents"; 
    
    
    
    public static final String PERSISTENCE_ENABLED_ATTRIBUTE_NAME = "alertsPersistenceEnabled";
    public static final String JOURNAL_ENABLED_ATTRIBUTE_NAME = "alertsJournalEnabled";
    public static final String ENABLE_PERSISTENCE_ATTRIBUTE_NAME = "enableTargetPersistence";
    public static final String ENABLE_POLICY_EXECUTION_ATTRIBUTE_NAME = "enablePolicyExcution";
    public static final String PERSISTED_ALERT_MAX_AGE_ATTRIBUTE_NAME = "persistedAlertMaxAge";
    public static final String PERSISTED_ALERT_MAX_COUNT_ATTRIBUTE_NAME = "persistedAlertMaxCount";
    public static final String PERSISTED_ALERT_LEVEL_ATTRIBUTE_NAME = "persistedAlertLevel";
    public static final String PERSISTED_ALERT_REMOVAL_PROLICY_EXEC_INTERVAL_ATTRIBUTE_NAME = "persistedAlertsRemovalPolicyExecInterval";
    public static final String MAX_CACHESIZE_ATTRIBUTE_NAME = "maxInMemoryEventCacheSize";
    public static final String DATABASE_ALERT_TABLE_NAME__ATTRIBUTE_NAME = "alertTableName";
    public static final String CURRENT_PERSISTED_EVENT_COUNT_ATTRIBUTE_NAME =
        "CurrentPersistedEventsCount";
    public static final String DATABASE_JNDI_NAME__ATTRIBUTE_NAME = "DBJndiName";
    public static final String DATABASE_TYPE_ATTRIBUTE_NAME = "DBType";
    public static final String TARGET_CHECK_INTERVAL_ATTRIBUTE_NAME ="tergetCheckInterval";
   
}
