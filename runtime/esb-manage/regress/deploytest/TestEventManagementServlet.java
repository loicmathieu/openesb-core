/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventManagementServlet.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package deploytest;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.management.InstanceAlreadyExistsException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanRegistrationException;
import javax.management.MBeanServer;
import javax.management.MBeanServerFactory;
import javax.management.MalformedObjectNameException;
import javax.management.NotCompliantMBeanException;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public final class TestEventManagementServlet extends HttpServlet {

	private static final String EVENTMANAGEMENT_CHANNEL_MBEAN_NAME = "EventManagement:name=EventManagementChannelSupportMBean";    
    private static final Logger mLogger;
    private static boolean isDebugEnabled;
     static {
        mLogger = Logger.getLogger(TestEventManagementServlet.class.getName());
         isDebugEnabled = mLogger.isLoggable(Level.FINEST);
     
     }
  private TestEventManagementSupport mEventManagementSupportMBean;
  private MBeanServer mMBeanServer;
  public TestEventManagementServlet()  {
  }
    
  public void init() {
      // init the Support mbean here
      
      try {
          mEventManagementSupportMBean = new TestEventManagementSupport();
          ObjectName eventManagementSupportObjectName = new ObjectName(EVENTMANAGEMENT_CHANNEL_MBEAN_NAME);  
          // get default MBeanserver
          List<MBeanServer> mbeanServers = MBeanServerFactory.findMBeanServer(null);
          // assuming that target application server alway has @ least 1 mbeanserver
          MBeanServer lMBeanServer = mbeanServers.get(0);
          if(lMBeanServer.isRegistered(eventManagementSupportObjectName)) {
              lMBeanServer.unregisterMBean(eventManagementSupportObjectName);
          }
          lMBeanServer.registerMBean(mEventManagementSupportMBean,eventManagementSupportObjectName);
    } catch (MalformedObjectNameException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (InstanceAlreadyExistsException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (MBeanRegistrationException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (NotCompliantMBeanException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (NullPointerException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } 
  }
  
  public void destroy() {
      try {
        ObjectName eventManagementSupportObjectName = new ObjectName(EVENTMANAGEMENT_CHANNEL_MBEAN_NAME);  
          List<MBeanServer> mbeanServers = MBeanServerFactory.findMBeanServer(null);
          // assuming that target application server alway has @ least 1 mbeanserver
          MBeanServer lMBeanServer = mbeanServers.get(0);
          if(lMBeanServer.isRegistered(eventManagementSupportObjectName)) {
              lMBeanServer.unregisterMBean(eventManagementSupportObjectName);
          }
    } catch (MalformedObjectNameException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (InstanceNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (MBeanRegistrationException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    } catch (NullPointerException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }

     
  }
  
  public void doGet(HttpServletRequest request, HttpServletResponse response) 
    throws IOException {
      
   }
 

}
