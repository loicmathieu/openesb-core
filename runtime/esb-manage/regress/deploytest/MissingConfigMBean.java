/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)CustomConfigMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

import com.sun.jbi.util.ComponentConfigurationHelper;

import javax.management.MBeanException;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.CompositeType;
import javax.management.openmbean.TabularData;
import javax.management.openmbean.TabularType;


public interface MissingConfigMBean
{
    /**
     * Test method.
     */
    public String getComponentCustomInfo();
    
    /**
     * HostName gettter/setter
     */
    public void setHostName(String hostName);
    public String getHostName();
    
    /**
     * Port get/set
     */
    public void setPort(int port);
    public int getPort();
    
    /**
     * ProxyPassword - secure field
     */
    public void setProxyPassword(String proxyPassword);
    public String getProxyPassword();
    
     /**
      * Get the CompositeType definition for the components application configuration 
      *
      * @return the CompositeType for the components application configuration.
      */
     public CompositeType queryApplicationConfigurationType();
    
     /**
      * Add an application configuration. The configuration name is a part of the CompositeData.
      * The itemName for the configuration name is "configurationName" and the type is SimpleType.STRING
      *
      * @param name - configuration name, must match the value of the field "name" in the namedConfig
      * @param appConfig - application configuration composite 
      * @throws MBeanException if the application configuration cannot be added.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public void addApplicationConfiguration(String name, CompositeData appConfig) throws MBeanException;
    
     /**
      * Delete an application configuration. 
      *
      * @param name - identification of the application configuration to be deleted
      * @throws MBeanException if the configuration cannot be deleted.
      * @return management message string which gives the status of the operation. For 
      *         target=cluster, instance specific details are included. 
      */
     public void deleteApplicationConfiguration(String name) throws MBeanException;
     
    
    /**
     * Get a Map of all application configurations for the component.
     *
     * @return a TabularData of all the application configurations for a 
     *         component keyed by the configuration name. 
     */
    public TabularData getApplicationConfigurations(); 
    
     
    /**
     * This operation sets an application variable. If a variable does not exist with 
     * the same name, its an error.
     * 
     * @param name - name of the application variable
     * @param appVar - this is the application variable compoiste to be updated.
     * @throws MBeanException if one or more application variables cannot be deleted
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public void setApplicationVariable(String name, CompositeData appVar) 
        throws MBeanException;
     
    /**
     * This operation deletes an application variable, if a variable with the specified name does
     * not exist, it's an error.
     *
     * @param name - name of the application variable
     * @throws MBeanException on errors.
     * @return management message string which gives the status of the operation. For 
     *         target=cluster, instance specific details are included. 
     */
    public void deleteApplicationVariable(String name) throws MBeanException;
     
     /**
      * Get the Application Variable set for a component.
      *
      * @return  a TabularData which has all the applicationvariables set on the component. 
      */
     public TabularData getApplicationVariables();
     
         
    /** 
     * Retrieves the component specific configuration schema.
     *
     * @return a String containing the configuration schema.
     */
    public String retrieveConfigurationDisplaySchema();
    
    /** 
     * Retrieves the component configuration metadata.
     * The XML data conforms to the component 
     * configuration schema.
     *
     * @return a String containing the configuration metadata.
     */
    public String retrieveConfigurationDisplayData();
}
