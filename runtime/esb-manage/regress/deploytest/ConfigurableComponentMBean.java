/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConfigurableComponentMBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package deploytest;

/**
 * Simple configuration MBean used for component installer tests.
 *
 * @author Sun Microsystems, Inc.
 */
public interface ConfigurableComponentMBean 
{
    /** Specifies the name of the file to create on install.
     *  @fileName file name
     */
    void setFileName(String fileName);
    
    /** Sets the work directory for the config MBean.
     *  @path absolute path to the work directory
     */
    void setWorkDirectory(String path);
    
    /** Returns the absolute path to the installer work directory.
     *  @return absolute path to the work directory, or null if it has not been set.
     */
    String getWorkDirectory();
    
    /** Returns the name of the file to create on install.
     *  @return file name
     */
    String getFileName();
}
