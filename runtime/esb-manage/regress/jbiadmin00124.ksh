#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00124.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

#############################################################################################
#                              Tests forceful component uninstall                           #
#                                                                                           #             
# This tests the forced component uninstall operation.                                      #
#                                                                                           #
# The test component used in this test throws an exception on uninstall.                    #
#                                                                                           #
# The test :                                                                                #
# (a) Uninstall the component normally - that should fail                                   #
# (b) Uninstall the component with force=true, the JBI Runtime should ignore                #
#     the component exceptions and go ahead with the uninstall which is a success.          #
#                                                                                           #
# The second phase of the test repeats the process after corrupting the component's         #
# install root by removing the component classes. This will cause the bootstrap class load  #
# failure which will prevent a normal uninstall from working, but the forced uninstall      #
# should complete successfully. This tests the fix for open-esb issue #284.                  #
#                                                                                           #
#############################################################################################

echo "jbiadmin00124 : Install a component, uninstall normally, then uninstall forcefully"

. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin00124.xml pkg.test.component

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/bad-uninstall-binding.jar
COMPONENT_NAME=bad-uninstall-binding

# Install the component
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  install-component

# Uninstall the component normally. This will fail.
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME uninstall-component

# Uninstall the component forcefully. This will succeed.
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DCOMPONENT_NAME=$COMPONENT_NAME -f jbiadmin00124.xml uninstall.component.with.force

# This next test forces a failure loading the component's bootstrap class to
# make sure that the forced uninstall still succeeds.

# Install the component again
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE install-component

# Now corrupt the component's archive
rm $JBI_DOMAIN_ROOT/jbi/components/bad-uninstall-binding/install_root/classes/deploytest/*

# Uninstall the component normally. This will fail.
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME uninstall-component

# Uninstall the component forcefully. This will succeed.
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -DCOMPONENT_NAME=$COMPONENT_NAME -f jbiadmin00124.xml uninstall.component.with.force
