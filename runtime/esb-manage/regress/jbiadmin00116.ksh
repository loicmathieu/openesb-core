#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00116.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#####
# jbiadmin00116 - Shared library upgrade test cases for the cluster target
#                 This tests the same cases as jbiadmin1001 but for the cluster target
#####

echo jbiadmin00116 - Shared library upgrade test cases for the cluster target
. ./regress_defs.ksh
. ./cluster-setup.ksh cluster1
. ./clustered-instance-setup.ksh instance2
. ./clustered-instance-setup.ksh instance3

. ./jbiadmin01001 cluster1

. ./clustered-instance-teardown.ksh instance3
. ./clustered-instance-teardown.ksh instance2
. ./cluster-teardown.ksh cluster1
