#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin02001.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
#jbiadmin02001 - Synchronization test cluster component configuration

echo testname is jbiadmin02001
. ./regress_defs.ksh

my_test_domain=CAS
. $SRCROOT/antbld/regress/common_defs.ksh

ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f jbiadmin01600.xml pkg.test.component

# Cluster and Member Instance Setup
asadmin create-cluster  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster
createClusterDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster sccfg-cluster --nodeagent agent1 sccfg-cluster-i1
createInstanceDelay
asadmin create-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS --cluster sccfg-cluster --nodeagent agent1 sccfg-cluster-i2
createInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i1
startInstanceDelay

# Install a component
$JBI_ANT_NEG -Djbi.install.file=$JV_SVC_TEST_CLASSES/dist/component-with-custom-mbean.jar  -Djbi.target="sccfg-cluster" install-component
installComponentDelay

# Start the component
$JBI_ANT_NEG -Djbi.component.name=manage-binding-1 -Djbi.target="sccfg-cluster" start-component
startComponentDelay
$JBI_ANT_NEG -Djbi.component.name=sun-http-binding -Djbi.target="sccfg-cluster" start-component
startComponentDelay

#
# Application Configuration of instances versus cluster
#

# Add application configuration to i1
asadmin create-jbi-application-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster-i1 --component manage-binding-1 --configname i1Config $JV_SVC_REGRESS/deploytest/new-app-config.properties

# Add application varaibles to i1
asadmin create-jbi-application-variable  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster-i1 --component manage-binding-1 $JV_SVC_REGRESS/deploytest/new-app-var.properties

# Switch to instance 2
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster-i1
stopInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i2
startInstanceDelay

#echo "---------------CAS registry"
#cat $AS8BASE/domains/CAS/jbi/config/jbi-registry.xml
#echo "---------------I1 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml
#echo "---------------I2 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml
echo "---------------registry diff"
diff $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

# Add application configuration to cluster
asadmin create-jbi-application-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster --component manage-binding-1 --configname i1Config $JV_SVC_REGRESS/deploytest/updated-app-config.properties

# Add application varaibles to cluster
asadmin create-jbi-application-variable  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster --component manage-binding-1 $JV_SVC_REGRESS/deploytest/update-app-var.properties

# Switch to instance 1 to see if sync picks up the changes.
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster-i2
stopInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i1
startInstanceDelay

#echo "---------------CAS registry"
#cat $AS8BASE/domains/CAS/jbi/config/jbi-registry.xml
#echo "---------------I1 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml
#echo "---------------I2 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

echo "---------------registry diff"
diff $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

# Make sure we get error.
asadmin delete-jbi-application-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster --component manage-binding-1 i1Config
asadmin delete-jbi-application-variable  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster --component manage-binding-1 "securityCredentials,securityPrincipal,port"

# restart instance 2
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i2
startInstanceDelay

asadmin delete-jbi-application-configuration  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster --component manage-binding-1 i1Config
asadmin delete-jbi-application-variable  --port $ASADMIN_PORT $ASADMIN_PW_OPTS --target sccfg-cluster --component manage-binding-1 "securityCredentials,securityPrincipal,port"

#echo "---------------CAS registry"
#cat $AS8BASE/domains/CAS/jbi/config/jbi-registry.xml
#echo "---------------I1 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml
#echo "---------------I2 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

echo "---------------registry diff"
diff $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster-i2
stopInstanceDelay

#
# Application Configuration of clusters.
#

# Add an application configuration
$JBI_ANT_NEG -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$JV_SVC_REGRESS/deploytest/new-app-config.properties -Djbi.component.name=manage-binding-1 -Djbi.target="sccfg-cluster" create-application-configuration

$JBI_ANT_NEG -Djbi.app.variables.file=$JV_SVC_REGRESS/deploytest/new-app-var.properties -Djbi.component.name=manage-binding-1  -Djbi.target="sccfg-cluster" create-application-variable

# Add a configuration
$JBI_ANT -Djbi.component.name="sun-http-binding" -Djbi.config.params.file=$JV_SVC_REGRESS/deploytest/httpbc-config-params.properties -Djbi.target=sccfg-cluster set-component-configuration

# Switch to instance 2 to see if sync picks up the changes.
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster-i1
stopInstanceDelay
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i2
startInstanceDelay

#echo "---------------CAS registry"
#cat $AS8BASE/domains/CAS/jbi/config/jbi-registry.xml
#echo "---------------I1 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml
#echo "---------------I2 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml
echo "---------------registry diff"
diff $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

#Update application configuration
$JBI_ANT_NEG -Djbi.app.config.name=testConfig -Djbi.app.config.params.file=$JV_SVC_REGRESS/deploytest/updated-app-config.properties -Djbi.component.name=manage-binding-1 -Djbi.target="sccfg-cluster" update-application-configuration

#Update application variables
$JBI_ANT_NEG -Djbi.app.variables.file=$JV_SVC_REGRESS/deploytest/update-app-var.properties -Djbi.component.name=manage-binding-1  -Djbi.target="sccfg-cluster" update-application-variable

#Set default component configuration
$JBI_ANT -Djbi.config.params.file=$JV_SVC_REGRESS/deploytest/jbi-default-httpbc-config-params.properties -Djbi.component.name=sun-http-binding -Djbi.target=sccfg-cluster set-component-configuration

# Switch to instance 1 to see if sync picks up the changes.
asadmin start-instance  --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i1
startInstanceDelay

#echo "---------------CAS registry"
#cat $AS8BASE/domains/CAS/jbi/config/jbi-registry.xml
#echo "---------------I1 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml
#echo "---------------I2 registry"
#cat $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml
echo "---------------registry diff"
diff $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

$JBI_ANT_NEG -Djbi.app.config.name=testConfig -Djbi.target="sccfg-cluster" -Djbi.component.name=manage-binding-1 delete-application-configuration

# Stop instances
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster-i1
stopInstanceDelay
asadmin stop-instance $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster-i2
stopInstanceDelay

echo "---------------registry diff"
diff $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/jbi/config/jbi-registry.xml $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/jbi/config/jbi-registry.xml

# Cleanup
$JBI_ANT_NEG -Djbi.component.name=manage-binding-1 -Djbi.target="sccfg-cluster" stop-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=manage-binding-1  -Djbi.target="sccfg-cluster" shut-down-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=manage-binding-1  -Djbi.target="sccfg-cluster" uninstall-component
uninstallComponentDelay
$JBI_ANT_NEG -Djbi.component.name=sun-http-binding -Djbi.target="sccfg-cluster" stop-component
stopComponentDelay
$JBI_ANT_NEG -Djbi.component.name=sun-http-binding -Djbi.target="sccfg-cluster" shut-down-component
stopComponentDelay

# Cluster and Member Instance Cleanup
cp $AS8BASE/nodeagents/agent1/sccfg-cluster-i1/logs/server.log $JV_SVC_REGRESS/../bld/2001-sccfg-cluster-i1.server.log
cp $AS8BASE/nodeagents/agent1/sccfg-cluster-i2/logs/server.log $JV_SVC_REGRESS/../bld/2001-sccfg-cluster-i2.server.log

asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i1
deleteInstanceDelay
asadmin delete-instance --port $ASADMIN_PORT $ASADMIN_PW_OPTS sccfg-cluster-i2
deleteInstanceDelay
asadmin delete-cluster $ASADMIN_PW_OPTS --port $ASADMIN_PORT sccfg-cluster
deleteClusterDelay
