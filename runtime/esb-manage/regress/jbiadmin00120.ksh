#!/bin/sh
#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jbiadmin00120.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

##############################################################################################
#                                Tests fix for Bug 6500177                                   #
#                                                                                            #             
# A component installed to a non-domain target should be automatically installed             #
# to the domain target and the facade component life cycle MBean for target=domain           #
# should return the correct component state.                                                 #
#                                                                                            #
##############################################################################################

echo "jbiadmin00120 : Install a component to a non-domain target, the component should be installed automatically to the domain target"
. ./regress_defs.ksh
ant -q -emacs -DESBMEMBER_ADMIN_PORT="$JBI_ADMIN_PORT" -lib "$REGRESS_CLASSPATH" -f Package.xml 

COMPONENT_ARCHIVE=$JV_SVC_TEST_CLASSES/dist/component-binding1.jar
COMPONENT_NAME=manage-binding-1

# Install a Component to target=server
$JBI_ANT_NEG -Djbi.install.file=$COMPONENT_ARCHIVE  install-component

# List the binding components, the component should be listed both for
# target=server and target=domain with state=Shutdon
$JBI_ANT_NEG  list-binding-components
$JBI_ANT_NEG -Djbi.target=domain list-binding-components

# Test Life Cycle Operations
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME start-component
$JBI_ANT_NEG  list-binding-components
$JBI_ANT_NEG -Djbi.target=domain list-binding-components

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME stop-component
$JBI_ANT_NEG  list-binding-components
$JBI_ANT_NEG -Djbi.target=domain list-binding-components

$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME shut-down-component
$JBI_ANT_NEG  list-binding-components
$JBI_ANT_NEG -Djbi.target=domain list-binding-components

# Clean up
$JBI_ANT_NEG -Djbi.component.name=$COMPONENT_NAME uninstall-component
