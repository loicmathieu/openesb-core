/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)RuntimeConfiguration.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.management.facade;

import com.sun.jbi.platform.PlatformContext;
import com.sun.jbi.management.ConfigurationCategory;
import com.sun.jbi.management.MBeanNames;

import com.sun.jbi.management.config.ConfigurationBuilder;
import com.sun.jbi.management.config.DescriptorSupport;
import com.sun.jbi.management.message.MessageBuilder;
import com.sun.jbi.management.message.MessageHelper;
import com.sun.jbi.management.registry.Updater;
import com.sun.jbi.management.registry.GenericQuery;
import com.sun.jbi.management.registry.xml.RegistryDocument;
import com.sun.jbi.management.util.StringHelper;
import com.sun.jbi.management.util.FacadeMbeanHelper;
import com.sun.jbi.management.system.ManagementException;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import javax.management.Attribute;
import javax.management.AttributeNotFoundException;
import javax.management.InvalidAttributeValueException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import javax.management.MBeanInfo;
import javax.management.MBeanAttributeInfo;
import javax.management.modelmbean.ModelMBeanInfo;
import javax.management.modelmbean.ModelMBeanAttributeInfo;


/**
 * RuntimeConfiguration is a model facade MBean. An instance of this MBean is registered
 * on the DAS for each instance/cluster and each configuration category. 
 *
 * When a set/get attribute operation is invokedon this MBean a corresponding operation 
 * is invoked on the instance or if the target is a cluster, each instance in the cluster. 
 * After which the local DAS registry and the in memory cache is updated.
 * 
 * @author Sun Microsystems, Inc.
 */
public class RuntimeConfiguration 
    extends  com.sun.jbi.management.config.RuntimeConfiguration
        implements com.sun.jbi.util.Constants
{   

    private static final String DOMAIN = "domain";
    
    
    /** Creates a new RuntimeConfiguration MBean.
     *  @param mbeanInfo metadata for the configuration this facade represents
     */
    public RuntimeConfiguration(ModelMBeanInfo mbeanInfo, ConfigurationCategory category,
        String target )
        throws Exception
    {
        super(mbeanInfo, category, target);
    }
    
    
    /*---------------------------------------------------------------------------------*\
     *                   Dynamic MBean Attribute getter/setter                         *
    \*---------------------------------------------------------------------------------*/

    /**
     * Set the value of a specific attribute of the model configuration MBean. 
     *
     * The attribute value is set as follows :
     *
     * (a) The setAttribute() operation is invoked on the corresponding MBean on the
     *     instance.
     * (b) The attribute is set in the registry
     * 
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     * @see #getAttribute
     */
    synchronized public void setAttribute(Attribute attribute) 
        throws AttributeNotFoundException, InvalidAttributeValueException, 
            MBeanException, ReflectionException 
    {
        checkIsValidAttribute(attribute);
        
        /**
         * We get the com.sun.jbi log level from the platform, this is not
         * a very clean solution, but is the only solution. The "com.sun.jbi" 
         * logger is not persisted in the JBI runtime, it is the platforms 
         * responsibility right now.  ~ Nikita
         */
        if ( attribute.getName().equalsIgnoreCase(getPlatformContext().JBI_LOGGER_NAME))
        {
            try
            {
                java.util.logging.Level logLevel = null;
                if ( LOG_LEVEL_DEFAULT.equals(attribute.getValue()) )
                {
                    return;
                }
                else if ( attribute.getValue() != null )
                {
                        logLevel = java.util.logging.Level.parse(attribute.getValue().toString());
                }
                getPlatformContext().setJbiLogLevel(mTarget, logLevel);
                return;
            }
            catch ( IllegalArgumentException iex )
            {
                // Invalid log level passed in 
                javax.jbi.JBIException jbiEx = new javax.jbi.JBIException(
                        getMgtMessage("setAttribute", iex));
                throw new MBeanException(jbiEx);
            }
        }
        
        try
        {
            notifyListenersOfAttributeChange(attribute);
        }
        catch ( Exception ex)
        {
            javax.jbi.JBIException jbiEx = new javax.jbi.JBIException(
                    getMgtMessage("setAttribute", ex));
            throw new MBeanException(jbiEx);
        }
        
        setAttributeOnTarget(attribute);

        // -- If the registry does not have a domain-config, add one
        persistDomainConfig();
        
        /** Persist changes to the registry, this is a attribute override for
         *    non-server targets. Server instance attributes are updated by the
         *    instance configuration MBean, don't need to do this twice
         */
        if ( !mTarget.equals(getPlatformContext().getAdminServerName()))
        {
            try
            {
                /** If the attribute value is the same as the domain config value
                 *    then it is not a override, it is a possible load default, delete the
                 *    override if it exists
                 */
                GenericQuery query = getRegistry().getGenericQuery();
                Updater updater = getRegistry().getUpdater();

                String attrValue = ( attribute.getValue() == null ? "null" : attribute.getValue().toString());
                String domainValue = query.getAttribute(DOMAIN, mCategory, attribute.getName());

                if ( domainValue != null && attrValue.equals(domainValue))
                {
                    
                    updater.deleteAttribute(mTarget, mCategory, attribute.getName());
                }
                else
                {
                    updater.setAttribute(mTarget, mCategory, attribute.getName(),  attrValue);
                }
            }
            catch ( Exception ex)
            {
                javax.jbi.JBIException jbiEx = new javax.jbi.JBIException(
                        getMgtMessage("setAttribute", ex));
                throw new MBeanException(jbiEx);
            }
        }
    }
    
    /**
     * Persist this configuration in the registry only when the target is "domain"
     */
    public void persist()
        throws Exception
    {
        if ( DOMAIN.equals(mTarget) )
        {
            super.persist();
        }
    }
    
    
    /*---------------------------------------------------------------------------------*\
     *                             Private Helpers                                     *
    \*---------------------------------------------------------------------------------*/
    
    /**
     * Set the attribute on the target cluster / instance.
     *
     * @param attribute The identification of the attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     * @exception ReflectionException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown while trying to invoke the MBean's setter.
     */
    private void setAttributeOnTarget(Attribute attribute) 
        throws MBeanException
    {
        try
        {
            if (DOMAIN.equals(mTarget))
            {
                // Update the global configuration.
                java.util.List<String> targets = new java.util.ArrayList();
                try
                {
                    GenericQuery query = getRegistry().getGenericQuery();
                    targets = query.getServers();
                    targets.addAll(query.getClusters());
                    for ( String target : targets )
                    {
                        setGlobalAttributeOnTarget(target, attribute);
                    }
                }
                catch(com.sun.jbi.management.registry.RegistryException rex)
                {
                    javax.jbi.JBIException jbiEx = new javax.jbi.JBIException(
                        sMsgBuilder.buildExceptionMessage("setAttribute", rex));
                    throw new MBeanException(jbiEx);
                }
            }
            else
            {
                if ( getPlatformContext().isStandaloneServer(mTarget) )
                {
                    setAttributeOnInstance(mTarget, attribute);
                }
                else
                {
                    setAttributeOnCluster(attribute);
                }
            }
        }
        catch (ManagementException mex)
        {
            javax.jbi.JBIException jbiEx = new javax.jbi.JBIException(mex.getMessage());
            throw new MBeanException(jbiEx);
        }
    }
    
    
    /**
     * @param instance - target instance name
     * @param attribute - attribute to be set
     * @exception throws a ManagementException with a management message in the
     * exception message.
     */
    private void setAttributeOnInstance(String instance, Attribute attribute) 
        throws ManagementException
    {
        String op = "setAttribute";
        try
        {
            if ( getPlatformContext().isInstanceUp(instance) )
            {
                // Get the object name of the instance config MBean for this category
                ObjectName instConfigMBeanName = getManagementContext()
                    .getMBeanNames(instance).getSystemServiceMBeanName(
                        MBeanNames.SERVICE_NAME_CONFIG_SERVICE, 
                        ConfigurationBuilder.getControlType(
                            MBeanNames.ServiceType.valueOf(mCategory.toString())));

                MBeanServerConnection mbns = getPlatformContext().getMBeanServerConnection(instance);

                if ( mbns.isRegistered(instConfigMBeanName) )
                {
                    mbns.setAttribute(instConfigMBeanName, attribute);
                }
            }
        }
        catch ( javax.management.JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  sMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch ( javax.management.JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  sMsgBuilder.buildExceptionMessage(op, actualEx);
            throw new ManagementException(errMsg);
        }
        catch (Exception ex)
        {
            throw new ManagementException(
                sMsgBuilder.buildExceptionMessage(op, ex));
        }
    } 
    
    /**
     * For each and every instance in the cluster, invoke set attribute on 
     * the corresponding instance configuration MBean for the specific category.
     *
     * @param attribute - the attribute to be set
     * @exception throws a ManagementException with a management message in the
     * exception message. If one or more instances are configured the message is
     * a SUCCESS w/ WARNING message, if the operation fails for all instances 
     * its an ERROR message.
     */
    private void setAttributeOnCluster(Attribute attribute) 
        throws ManagementException
    {
        HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
        HashMap<String, String> responseMap = new HashMap<String, String>();
        
        // For each instance in the cluster
        // invoke the set attribute operation 
        java.util.Set<String> instances = getPlatformContext().getServersInCluster(mTarget);
        for ( String instance : instances )
        {
            try
            {
                setAttributeOnInstance(instance, attribute);
                // need to add a SUCCESS result to the responseMap so
                // in case of exceptions buildCompositeMessage() generates the right task
                // result / message type
                responseMap.put(instance, sMsgBuilder.buildFrameworkMessage("setAttribute", 
                    MessageBuilder.TaskResult.SUCCESS));
            }
            catch ( ManagementException mex )
            {
                /** 
                 * If an attribute cannot be set on one instance in a cluster
                 * then save the exception and continue processing.
                 */
                exceptionMap.put(instance, mex);
            }
        }
        
        /**
         * If the exception map is not empty then one or more instances failed to
         * be configured, user needs to know, throw a exception with a management
         * message that has the instance specific details
         */
        if ( !exceptionMap.isEmpty() )
        {
            MessageBuilder.Message msg = sMsgBuilder.buildCompositeMessage("setAttribute",
                responseMap, exceptionMap, false);
            
             if ( msg.isSuccessWithWarning() )
             {
                 // Log a warning in the server.log
                 String warning = MessageHelper.extractMessageFromXML(msg.getMessage(), 
                    getLogger().getLevel());
                 getLogger().fine(warning);
             }
             else if ( msg.isFailure() ) 
             {
                throw new ManagementException(msg.getMessage());
             }
        }
    } 
    
    
    /**
     * Set the global attribute on the target cluster / instance.
     *
     * @param attribute The identification of the global attribute to
     *        be set and  the value it is to be set to.
     * @exception AttributeNotFoundException
     * @exception InvalidAttributeValueException
     * @exception MBeanException Wraps a <CODE>java.lang.Exception</CODE> 
     *           thrown by the MBean's setter.
     */
    private void setGlobalAttributeOnTarget(String target, Attribute attribute) 
        throws ManagementException
    {
        HashMap<String, Throwable> exceptionMap = new HashMap<String, Throwable>();
        HashMap<String, String> responseMap = new HashMap<String, String>();
        
        if ( getPlatformContext().isStandaloneServer(target) )
        {
            setGlobalAttributeOnInstance(target, attribute, responseMap, exceptionMap);
        }
        else
        {
            setGlobalAttributeOnCluster(target, attribute, responseMap, exceptionMap);
        }
        
        /**
         * If the exception map is not empty then one or more instances failed to
         * be configured, user needs to know, throw a exception with a management
         * message that has the instance specific details
         */
        if ( !exceptionMap.isEmpty() )
        {
            MessageBuilder.Message msg = sMsgBuilder.buildCompositeMessage("setAttribute",
                responseMap, exceptionMap, false);
            
             if ( msg.isSuccessWithWarning() )
             {
                 // Log a warning in the server.log
                 String warning = MessageHelper.extractMessageFromXML(msg.getMessage(), 
                    getLogger().getLevel());
                 getLogger().fine(warning);
             }
             else if ( msg.isFailure() ) 
             {
                throw new ManagementException(msg.getMessage());
             }
        }
    }
    
    /**
     * @param instance - target instance name
     * @param attribute - attribute to be set
     * @param resposneMap - if the set attribute is a success then a SUCCESS message i
     *                      inserted into the responseMap.
     * @param exceptionMap - if the set attribute is a failuere then a FAILURE message i
     *                      inserted into the exceptionMap.
     */
    private void setGlobalAttributeOnInstance(String instance, Attribute attribute,
        Map<String, String> responseMap, Map<String, Throwable> exceptionMap) 
    {
        String op = "setAttribute";
        try
        {
            if ( getPlatformContext().isInstanceUp(instance) )
            {
                // Get the object name of the instance config MBean for this category
                ObjectName instConfigMBeanName = getManagementContext()
                    .getMBeanNames(DOMAIN).getSystemServiceMBeanName(
                        MBeanNames.SERVICE_NAME_CONFIG_SERVICE, 
                        ConfigurationBuilder.getControlType(
                            MBeanNames.ServiceType.valueOf(mCategory.toString())));

                MBeanServerConnection mbns = getPlatformContext().getMBeanServerConnection(instance);
                
                if ( mbns.isRegistered(instConfigMBeanName) )
                {
                    mbns.setAttribute(instConfigMBeanName, attribute);
                    // Add a success message
                    responseMap.put(instance, sMsgBuilder.buildFrameworkMessage("setAttribute", 
                    MessageBuilder.TaskResult.SUCCESS));
                }
            }
        }
        catch ( javax.management.JMException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  getMgtMessage(op, actualEx);
            exceptionMap.put(instance, new ManagementException(errMsg));
        }
        catch ( javax.management.JMRuntimeException ex)
        {
            Throwable actualEx = FacadeMbeanHelper.stripJmException(ex);    
            String errMsg =  getMgtMessage(op, actualEx);
            exceptionMap.put(instance, new ManagementException(errMsg));
        }
        catch (Exception ex)
        {
            exceptionMap.put(instance, new ManagementException(
                getMgtMessage(op, ex)));
        }
    } 
    
    /**
     * Update the domain configuration on each instance in the cluster.
     *
     * @param instance - target cluster name
     * @param attribute - attribute to be set
     * @param resposneMap - for each instance that the "setAttribute" succeeds on 
     *                      a SUCCESS message inserted into the responseMap.
     * @param exceptionMap -  for each instance that the "setAttribute" fails on 
     *                      a FAILURE message inserted into the exceptionMap.
     */
    private void setGlobalAttributeOnCluster(String target, Attribute attribute,
        Map<String, String> responseMap, Map<String, Throwable> exceptionMap) 
    {   
        // For each instance in the cluster
        // invoke the set attribute operation 
        java.util.Set<String> instances = getPlatformContext().getServersInCluster(target);
        for ( String instance : instances )
        {
            setGlobalAttributeOnInstance(instance, attribute, responseMap, exceptionMap);
        }
    }
    
    /**
     * @param op - task id
     * @param extends - exception to get the details from 
     * @return a management exception message constructed from the exception, if 
     *         message builder fails to build the exception return the exception message
     *         as is
     */
    private String getMgtMessage(String op, Throwable ex)
    {
        String msg;
        try
        {
           msg = sMsgBuilder.buildExceptionMessage(op, ex);
        }
        catch(ManagementException mex)
        {
            msg = ex.getMessage();
        }
        return msg;
    }
}
