/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)EventRepositoryException.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */

package com.sun.esb.eventmanagement.impl;

/**
 * This class represents a EventRepositoryException
 *
 * @author bgebbie
 */
public class EventRepositoryException extends Exception {
    /**
     * the RCS id
     */
    static String RCS_ID =
        "$Id: EventRepositoryException.java,v 1.2 2010/07/18 16:18:15 ksorokin Exp $";

    /**
     * The root cause of the exception.
     */
    private Throwable mRootCause;

    /**
     * Construct the exception.
     *
     * @param message the exception message
     */
    public EventRepositoryException(String message) {
        super(message);
    }

    /**
     * Construct the exception.
     *
     * @param cause the original exception
     */
    public EventRepositoryException(Throwable cause) {
        mRootCause = cause;
    }

    /**
     * Construct the exception.
     *
     * @param message the exception message
     * @param cause the original exception
     */
    public EventRepositoryException(String message, Throwable cause) {
        super(message);
        mRootCause = cause;
    }

    /**
     * Obtain the root cause of the exception or <code>null</code> if there
     * if none reported.
     *
     * @return the root cause of the exception
     */
    public Throwable getRootCause() {
        return mRootCause;
    }
}
