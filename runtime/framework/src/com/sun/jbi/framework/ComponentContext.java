/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ComponentContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.framework;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.management.ManagementMessageFactory;
import com.sun.jbi.security.KeyStoreUtil;
import com.sun.jbi.wsdl2.WsdlFactory;
import com.sun.jbi.wsdl2.WsdlException;

import java.io.File;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.jbi.JBIException;
import javax.jbi.component.Bootstrap;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.servicedesc.ServiceEndpoint;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.InitialContext;

import javax.transaction.xa.XAResource;

/**
 * Contains global JBI information for a component to use during its processing.
 * During installation/uninstallation processing, the component can access this
 * context through the <CODE>InstallationContext</CODE>. Only the following
 * methods can be called during this time:
 * <UL>
 * <LI><CODE>getMBeanNames()</CODE></LI>
 * <LI><CODE>getMBeanServer()</CODE></LI>
 * <LI><CODE>getNamingContext()</CODE></LI>
 * <LI><CODE>getTransactionManager()</CODE></LI>
 * </UL>
 *
 * During all other processing, the component can call any of the methods at
 * any time.
 *
 * @author Sun Microsystems, Inc.
 */
public class ComponentContext
    implements com.sun.jbi.component.ComponentContext,
    javax.jbi.management.MBeanNames
{
    /**
     * Component instance representing this component.
     */
    private Component mComponent;

    /**
     * Component name.
     */
    private String mComponentName;

    /**
     * Handle to the JBI EnvironmentContext.
     */
    private EnvironmentContext mContext;

    /**
     * Delivery Channel for communicating with the NMR.
     */
    private com.sun.jbi.messaging.DeliveryChannelImpl mDeliveryChannel;

    /**
     * StringTranslator instance for constructing messages.
     */
    private StringTranslator mTranslator;

    /**
     * Prefix for logger names for components. This is public so that the
     * junit tests have access to it.
     */
    public static final String LOGGER_NAME_PREFIX = "com.sun.jbi.component.";

    /**
     * Period for logger name construction.
     */
    private static final String PERIOD = ".";

    /**
     * Constructor.
     * @param component the Component instance representing the component for
     * which this context is being created.
     * @param jbiContext the JBI EnvironmentContext instance.
     */
    public ComponentContext(Component component,
                            EnvironmentContext jbiContext)
    {
        mComponent = component;
        mComponentName = component.getName();
        mContext = jbiContext;
        mTranslator = (StringTranslator) mContext.getStringTranslatorFor(this);
    }

//
// ---------- Methods defined in javax.jbi.component.ComponentContext ---------
//

    /**
     * Activates the named endpoint with the NMR. This makes the endpoint
     * available to the NMR so it can send requests to the endpoint.
     * @param serviceName the qualified name of the service exposed by the
     * endpoint.
     * @param endpointName the name of the endpoint to be activated.
     * @return a reference to the activated endpoint.
     * @throws javax.jbi.JBIException if the endpoint cannot be activated.
     */
    public ServiceEndpoint activateEndpoint(
        javax.xml.namespace.QName serviceName,
        String endpointName)
        throws javax.jbi.JBIException
    {
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.activateEndpoint(serviceName, endpointName);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }
    
    /**
     * Deactivates the specified endpoint with the NMR. Deactivation indicates
     * to the NMR that this component will no longer process requests sent to
     * the named endpoint.
     * @param endpoint reference to the endpoint to be deactivated.
     * @throws javax.jbi.JBIException if the endpoint cannot be deactivated.
     */
    public void deactivateEndpoint(
        ServiceEndpoint endpoint)
        throws javax.jbi.JBIException
    {
        if ( null != mDeliveryChannel )
        {
            mDeliveryChannel.deactivateEndpoint(endpoint);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

    /**
     * Deregisters the specified external endpoint with the NMR.  This indicates
     * to the NMR that external service consumers can no longer access the
     * internal service by this name.
     * @param externalEndpoint the external endpoint to be deregistered.
     * @throws javax.jbi.JBIException if the endpoint cannot be deregistered.
     */
    public void deregisterExternalEndpoint(
        ServiceEndpoint externalEndpoint)
        throws javax.jbi.JBIException
    {
        if ( null != mDeliveryChannel )
        {
            mDeliveryChannel.deregisterExternalEndpoint(externalEndpoint);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

    /**
     * Get the unique component name for the component for which this context
     * was created.
     * @return the component's unique name.
     */
    public String getComponentName()
    {
        return mComponentName;
    }

    /**
     * Get the DeliveryChannel for the component to use to communicate with
     * the Normalized Message Router. If the component has not yet been
     * initialized, a DeliveryChannel cannot be created.
     * <p>This method is visible to the component, and calls the internal
     * framework-only method to obtain the channel.
     * @return the delivery channel.
     * @throws javax.jbi.messaging.MessagingException if the channel cannot
     * be activated.
     */
    public DeliveryChannel getDeliveryChannel()
        throws javax.jbi.messaging.MessagingException
    {
        return getDeliveryChannel(true);
    }

    /**
     * Get the DeliveryChannel for the component to use to communicate with
     * the Normalized Message Router. If it has not yet been activated, activate
     * it and save it. If it has been closed, replace it with a new one.
     * <p>This method is visible only to the framework.
     * @param activateNewChannel Set to true to allow activation of a new
     * channel if one is not already active; set to false to prevent activation
     * of a new channel.
     * @return the delivery channel or null if there is none.
     * @throws javax.jbi.messaging.MessagingException if the channel cannot be
     * activated.
     */
    DeliveryChannel getDeliveryChannel(boolean activateNewChannel)
        throws javax.jbi.messaging.MessagingException
    {
        if ( activateNewChannel )
        {
            if ( null == mDeliveryChannel || mDeliveryChannel.isClosed() )
            {
                mDeliveryChannel = (com.sun.jbi.messaging.DeliveryChannelImpl)
                    mContext.getNormalizedMessageService().
                        activateChannel(mComponentName, null);
            }
        }
        return (DeliveryChannel) mDeliveryChannel;
    }

    /**
     * Get the service description for the named endpoint, if any exists.
     * @param service the qualified name of the endpoint's service.
     * @param name the name of the endpoint.
     * @return the named endpoint, or <code>null</code> if the named endpoint
     * is not active.
     */
    public ServiceEndpoint getEndpoint(
        javax.xml.namespace.QName service,
        String name)
    {
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.getEndpoint(service, name);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

    /**
     * Retrieve metadata for the specified endpoint.
     * @param endpoint reference to the endpoint.
     * @return the metadata describing the endpoint or <code>null</code> if
     * metadata is unavailable.
     * @throws javax.jbi.JBIException if the endpoint reference is invalid.
     */
    public org.w3c.dom.Document getEndpointDescriptor(
        ServiceEndpoint endpoint)
        throws javax.jbi.JBIException
    {        
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.getEndpointDescriptor(endpoint);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

    /**
     * Queries the NMR for registered endpoints that implement the specified
     * interface. This will return the endpoints for all services and endpoints
     * that implement the named interface.
     * @param serviceName the qualified name of the interface/portType that
     * is implemented by the endpoint.
     * @return array of available endpoints for the specified interface name;
     * can be empty if none exist.
     */
    public ServiceEndpoint[] getEndpointsForService(
        javax.xml.namespace.QName serviceName)
    {
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.getEndpointsForService(serviceName);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

    /**
     * Queries the NMR for external endpoints that implement the specified
     * interface name.
     * @param interfaceName the qualified name of the interface/portType that
     * is implemented by the endpoints.
     * @return array of available external endpoints for the specified interface
     * name; can be empty if none exist.
     */
    public ServiceEndpoint[] getExternalEndpoints(
        javax.xml.namespace.QName interfaceName)
    {
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.getExternalEndpoints(interfaceName);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }  
    }
    
    /**
     * Queries the NMR for active endpoints that implement the given interface.
     * This will return the endpoints for all services and endpoints that 
     * implement the named interface (portType in WSDL 1.1). This method does 
     * NOT include external endpoints (those registered using 
     * registerExternalEndpoint(ServiceEndpoint)).
     * @param interfaceName qualified name of interface/portType that is 
     *  implemented by the endpoint
     * @return ServiceEndpoint[] list of available endpoints for the specified 
     *  interface name; potentially zero-length.
     */
    public ServiceEndpoint[] getEndpoints(
        javax.xml.namespace.QName interfaceName)
    {
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.getEndpoints(interfaceName);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }        
    }

    /**
     * Queries the NMR for external endpoints that are part of the specified
     * service.
     * @param serviceName the qualified name of the service that contains the
     * endpoints.
     * @return array of available external endpoints for the specified service
     * name; can be empty if none exist.
     */
    public ServiceEndpoint[] getExternalEndpointsForService(
        javax.xml.namespace.QName serviceName)
    {
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.getExternalEndpointsForService(serviceName);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }  
    }

    /**
     * Get the installation root directory for this component.
     * @return the installation root directory path.
     */
    public String getInstallRoot()
    {
        if ( File.separator.equals("/") )
        {
            return mComponent.getInstallRoot();
        }
        else
        {
            return mComponent.getInstallRoot().replace('/', File.separatorChar);
        }
    }

    /**
     * Get a logger instance from JBI. Loggers supplied by JBI are guaranteed
     * to have unique names such that they avoid name collisions with loggers
     * from other components created using this method. The suffix parameter
     * allows for the creation of subloggers as needed. The JBI specification
     * says nothing about the exact names to be used, only that they must be
     * unique across components and the JBI implementation itself.
     *
     * In addition to creating the logger instance, this implementation also
     * adds the logger instance to a list used by the ComponentLoggerMBean to
     * display and control of the logger level via the administrative tools.
     *
     * In this implementation, every logger obtained through this method has
     * a prefix added to its name. The prefix can be specified in the logging
     * section of the installation descriptor extension in jbi.xml. If no
     * prefix is specified, it defaults to the component name. Note that any
     * logger with a name that already begins with the prefix is left unchanged.
     * 
     * For every component, a main logger is created, with its logger name
     * set to the prefix (again, either the component name or the prefix
     * provided in jbi.xml). This provides the ability to set the log levels for
     * all of a component's loggers by setting the main logger's level.
     * However, it is important to note that if the log level is explicitly
     * set for a sub-logger, that level stays in effect regardless of the main
     * logger's level, until the sub-logger's level is explicitly changed.
     *
     * If a component does not call this method to create a main logger before
     * calling it to create a sub-logger, the main logger is created
     * automatically before the sub-logger is created.
     *
     * @param suffix logger name suffix for creating subloggers; empty string
     * for base logger.
     * @param resourceBundleName name of <code>ResourceBundle</code> to be used
     * for localizing messages for the logger. May be <code>null</code> if none
     * of the messages require localization. The resource, if non-null, must be
     * loadable using the component's class loader as the initiating loader.
     * @return a standard logger, named uniquely for this component (plus the
     * given suffix, if applicable)
     * @throws java.util.MissingResourceException if the resourceBundleName is
     * non-null and no corresponding resource can be found.
     * @throws javax.jbi.JBIException if any other error occurs.
     */
    public Logger getLogger(String suffix, String resourceBundleName)
        throws java.util.MissingResourceException, javax.jbi.JBIException
    {
        Logger log = null;
        boolean mainLogger = false;
        String logName;
        String prefix; 
        ComponentLogger logMBean = mComponent.getLoggerInstance();

        // Per the JBI specification, the suffix argument cannot be null.

        if ( null == suffix )
        {
            throw new java.lang.IllegalArgumentException(mTranslator.getString(
                LocalStringKeys.CC_LOGGER_NULL_SUFFIX));
        }

        // Construct the logger name using the prefix provided for the component
        // and the caller-provided suffix. If the caller-provided suffix already
        // begins with the prefix, the prefix is not added again.

        if ( null != logMBean )
        {
            prefix = logMBean.getLoggerNamePrefix();
        }
        else
        {
            prefix = mComponentName + ".";
        }

        if ( 0 == suffix.length() ) // request for a main logger
        {
            mainLogger = true;
            logName = prefix.substring(0, prefix.length() - 1);
        }
        else                        // request for a sub-logger
        {
            // If the caller-provided name doesn't already start with the
            // prefix, add it.
            if ( !suffix.startsWith(prefix) )
            {
                if ( suffix.startsWith(PERIOD) )
                {
                    logName = prefix + suffix.substring(1);
                }
                else
                {
                    logName = prefix + suffix;
                }
            }
            else
            {
                logName = suffix;
            }
        }

        // Now obtain the requested logger, using the caller-supplied resource
        // bundle name if it is not null.

        if ( null != resourceBundleName )
        {
            /*
             * The java.util.logging.Logger class no longer does stack walk search 
             * for a logger's resource bundle. The stack walk search was intended 
             * as a temporary measure to allow containers to transition to using 
             * the context class loader and was specified to be removed in a future 
             * release. It will use the thread context class loader (if not set, 
             * use the system class loader) to look up the resource bundle and, 
             * if not found, it will fall back to use the class loader of the 
             * caller class that creates the Logger instance (via the Logger.getLogger() 
             * and Logger.getAnonymousLogger() method with a given resource bundle name).
             */
            
            // Please have a look to release note
            // http://www.oracle.com/technetwork/java/javase/7u25-relnotes-1955741.html
            
            // OpenESB Community JIRA tracker
            // https://openesb.atlassian.net/browse/ESBCORE-20
            ClassLoader fwkClassLoader = Thread.currentThread().getContextClassLoader();
            ClassLoader componentClassloader;
            
            Bootstrap boostrapInstance = mComponent.getBootstrapInstance(false);
            
            if (boostrapInstance == null) {
                // We are in the lifecycle context
                try {
                    componentClassloader = ClassLoaderFactory.getInstance().getComponentClassLoader(mComponentName);
                } catch (JBIException e) {
                    // In this case we are staying in the default classloader
                    // This was added as it seems to be an issue in unit tests
                    componentClassloader = fwkClassLoader;
                }
            } else {
                // We are in the component boostrapping
                componentClassloader = boostrapInstance.getClass().getClassLoader();
            }
            
            try
            {
                Thread.currentThread().setContextClassLoader(componentClassloader);
                log = Logger.getLogger(logName, resourceBundleName);
            }
            catch ( java.util.MissingResourceException mrEx )
            {
                mContext.getLogger().log(Level.WARNING, mTranslator.getString(
                    LocalStringKeys.CC_LOGGER_MISSING_RESOURCE,
                    logName, resourceBundleName), mrEx);
                throw mrEx;
            }
            catch ( IllegalArgumentException iaEx )
            {
                String msg = mTranslator.getString(
                    LocalStringKeys.CC_LOGGER_DIFFERENT_RESOURCE,
                    logName, resourceBundleName,
                    Logger.getLogger(logName).getResourceBundleName());
                mContext.getLogger().log(Level.WARNING, msg, iaEx);
                throw new javax.jbi.JBIException(msg, iaEx);
            } finally {
                Thread.currentThread().setContextClassLoader(fwkClassLoader);
            }
        }
        else
        {
            log = Logger.getLogger(logName);
        }

        // If this is the main logger for the component, set its parent
        // to the global JBI logger.

        if ( mainLogger )
        {
            log.setParent(mContext.getJbiLogger());
        }

        // If this is the first request for this logger, add it to the logger
        // table for the component.

        if ( null != logMBean && !logMBean.isLoggerRegistered(logName) )
        {
            // Create a reasonable display name for the logger. This uses the
            // last level of the name, which for the main logger is the same
            // as the logger prefix.

            String displayName;
            int lastDot = logName.lastIndexOf(".");
            if ( -1 < lastDot )
            {
                displayName = logName.substring(lastDot+1);
            }
            else
            {
                displayName = logName;
            }

            // Add the logger to the table and set its initial level

            logMBean.addLogger(log, displayName);
        }
        return log;
    }

    /**
     * Get the JMX MBean naming service handle. This method returns this
     * instance, as it has the methods defined by the public API. These
     * methods in turn use the IPI implementation to perform the operations.
     * @return the JMX MBean naming service handle.
     */
    public javax.jbi.management.MBeanNames getMBeanNames()
    {
        return this; 
    }

    /**
     * Get the JMX MBean server with which all MBeans are registered.
     * This method delegates to the JBI EnvironmentContext method.
     * @return the main JMX MBean server.
     */
    public MBeanServer getMBeanServer()
    {
        return mContext.getMBeanServer();
    }

    /**
     * Get the JNDI naming context currently in effect.
     * This method delegates to the JBI EnvironmentContext method.
     * @return the JNDI naming context.
     */
    public InitialContext getNamingContext()
    {
        return mContext.getNamingContext();
    }

    /**
     * Get the TransactionManager for the current implementation.
     * This method delegates to the JBI EnvironmentContext method.
     * @return the TransactionManager instance.
     */
    public Object getTransactionManager()
    {
        return (Object) mContext.getTransactionManager();
    }

    /**
     * Get the workspace root directory for this component.
     * @return the component's workspace directory path.
     */
    public String getWorkspaceRoot()
    {
        if ( File.separator.equals("/") )
        {
            return mComponent.getWorkspaceRoot();
        }
        else
        {
            return mComponent.getWorkspaceRoot().replace('/', File.separatorChar);
        }
    }

    /**
     * Registers the specified external endpoint with the NMR.  This indicates
     * to the NMR that the specified endpoint is used as a proxy for external
     * service consumers to access an internal service by the same name.
     * @param externalEndpoint the external endpoint to be registered.
     * @throws javax.jbi.JBIException if the endpoint is already registered.
     */
    public void registerExternalEndpoint(
        ServiceEndpoint externalEndpoint)
        throws javax.jbi.JBIException
    {
        if ( null != mDeliveryChannel )
        {
            mDeliveryChannel.registerExternalEndpoint(externalEndpoint);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

    /**
     * Resolves the specified endpoint reference into a service endpoint. This
     * is called by the component when it has an endpoint reference that it
     * needs to resolve into a service endpoint.
     * @param endpointReference the endpoint reference as an XML fragment.
     * @return the service endpoint corresponding to the specified endpoint
     * reference, or <code>null</code> if the reference cannot be resolved.
     */
    public ServiceEndpoint resolveEndpointReference(
        org.w3c.dom.DocumentFragment endpointReference)
    {        
        if ( null != mDeliveryChannel )
        {
            return mDeliveryChannel.resolveEndpointReference(
                endpointReference);
        }
        else
        {
            throw new java.lang.IllegalStateException(
                mTranslator.getString(LocalStringKeys.CC_NO_DC_AVAILABLE,
                mComponentName));
        }
    }

//
// -------- Methods defined in com.sun.jbi.component.ComponentContext ---------
//

    /**
     * Get the management message factory which enables JBI components
     * to construct status and exception messages.
     * This method delegates to the JBI EnvironmentContext method.
     * @return the management message factory handle.
     */
    public ManagementMessageFactory getManagementMessageFactory()
    {
        return mContext.getManagementMessageFactory();
    }

    /**
     * Get a StringTranslator for use in constructing messages using
     * resource bundles.
     * This method delegates to the JBI EnvironmentContext method.
     * @param packageName the name of the package that contains the
     * resource bundle to be used by the StringTranslator.
     * @return the StringTranslator for the specified package name.
     */
    public StringTranslator getStringTranslator(String packageName)
    {
        return mContext.getStringTranslator(packageName, mComponentName);
    }

    /**
     * Get a StringTranslator for use in constructing messages using
     * resource bundles.
     * This method delegates to the JBI EnvironmentContext method.
     * @param object an object that is in the same package as the
     * resource bundle to be used by the StringTranslator.
     * @return the StringTranslator for the specified package.
     */
    public StringTranslator getStringTranslatorFor(Object object)
    {
        return mContext.getStringTranslator(
            object.getClass().getPackage().getName(), mComponentName);
    }

    /**
     * Get a copy of the WSDL factory. This needs to be done before
     * any reading, writing, or manipulation of WSDL documents can
     * be performed using the WSDL API.
     *
     * @return An instance of the WSDL factory.
     * @exception WsdlException If the factory cannot be instantiated.
     */
    public WsdlFactory getWsdlFactory() throws WsdlException
    {
      return mContext.getWsdlFactory ();
    }

    /**
     * Used by a component to register an internal XAResource with the JBI
     * runtime.
     * @param resource to be registered
     * @exception JBIException if something is wrong with the XAResource.
     */
    public void registerXAResource(XAResource resource) throws JBIException
    {
    	((com.sun.jbi.messaging.DeliveryChannelImpl) getDeliveryChannel(true)).
            registerXAResource(resource);
    }


    /**
     * Returns a KeyStoreUtil object that allows components to create,
     * read, update, and delete keys
     *
     * @return       An instance of a KeyStoreUtil
     */
    public KeyStoreUtil getKeyStoreUtil() {
        return mContext.getPlatformContext().getKeyStoreUtil();
    }

//
// ------------ Methods defined in javax.jbi.management.MBeanNames ------------
//

    /**
     * Formulate and return the MBean ObjectName of a custom control MBean for
     * a JBI component.
     *
     * @param customName the name of the custom control.
     * @return the JMX ObjectName of the MBean, or <code>null</code> if 
     * <code>customName</code> is invalid.
     */
    public ObjectName createCustomComponentMBeanName(String customName)
    {
        if ( mComponent.isBinding() )
        {
            return mContext.getMBeanNames().getCustomBindingMBeanName(
                customName, mComponentName);
        }
        else
        {
            return mContext.getMBeanNames().getCustomEngineMBeanName(
                customName, mComponentName);
        }
    }

    /**
     * Retrieve the default JMX Domain Name for MBeans registered in this
     * instance of the JBI implementation.
     *
     * @return the JMX domain name for this instance of the JBI implementation.
     */
    public String getJmxDomainName()
    {
        return mContext.getMBeanNames().getJmxDomainName();
    }

}
