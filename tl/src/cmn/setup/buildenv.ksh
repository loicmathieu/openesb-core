#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)buildenv.ksh
# Copyright 2004-2008 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#

###################
# INPUT PARAMETERS:
###################

# IIS_TOOLROOT         - override the default TOOLROOT setting.
# IIS_CVSROOT          - override the default CVSROOT setting.
# IIS_BRANCH_NAME      - override the default CVS_BRANCH_NAME setting
# IIS_CODELINE         - override the default CODELINE setting
# IIS_MAVEN_REPO_LOCAL - override the default MAVEN_REPO_LOCAL setting
# IIS_MAVEN_DISTROOT   - override the default MAVEN_DISTROOT setting
# IIS_AS8BASE          - override the default AS8BASE setting
# IIS_NETBEANS_HOME    - override the default NETBEANS_HOME setting
#
# WARNING:  the above variables are PRIVATE and are RESERVED for
#           this definition file.  They are unexported after use.

####
#set the path-separator for this platform:
####
export PS
echo $PATH | grep ';' > /dev/null
if [ $? -eq 0 ]; then
    PS=';'
else
    PS=':'
fi

#### this is precautionary only, to divorce from old forte tools:
unset MAINROOT

export PRODUCT=openesb

#this can be preset to a local tunnel host:
if [ "x$JNET_CVS_HOST" = x ]; then
    JNET_CVS_HOST="luna.sfbay"
fi

if [ "x$JNET_USER" = x ]; then
    export JNET_USER="guest"
    echo "WARNING: your java.net user name (JNET_USER) was defaulted to '$JNET_USER'."
    echo "To remove this warning, please export JNET_USER in the environment."
fi

export CVSIGNORE
if [ "x$CVSIGNORE" = x ]; then
    CVSIGNORE='Makefile bld target'
fi

#########
# CVSROOT:
#########
export CVSROOT
if [ "x$IIS_CVSROOT" = x ]; then
    #use default setting (note - CVSROOT is set later):
    IIS_CVSROOT=":pserver:${JNET_USER}@cvs.dev.java.net:/cvs"
fi

pwd=`pwd`

#cannonicalize $SRCROOT if it is set:
if [ "$SRCROOT" != "" ]; then
    srcroot=`sh -c "cd $SRCROOT; pwd"`
else
    srcroot=""
fi

if [ "$srcroot" = "$pwd" ]; then
    export SRCROOT="$pwd"
elif [ "$PROJECT" != "" -a \( "$srcroot" = "" -o "$srcroot" = "$PROJECT" \) ]; then
    ## ASSUME we are using VSPMS
    export SRCROOT="$PROJECT"
else
   cat << 'EOF'
PLEASE SET $SRCROOT OR USE chpj BEFORE SOURCING buildenv.ksh

NOTE:  if you are not using VSPMS and *have* set $SRCROOT, then
       "cd $SRCROOT"  before sourcing this setup file.  Make sure that
       the pwd command returns the same string as your $SRCROOT setting.

###  EXAMPLE 1:  without VSPMS:

$ mkdir -p /somedisk/cvs/mywork
$ cd /somedisk/cvs/mywork
$ cat >> mysetup
export SRCROOT=/somedisk/cvs/mywork
export JAVABASE=C:
export JAVAVERS j2sdk1.4.0
. $SRCROOT/tools/boot/buildenv.ksh
pathinfo    #optional - display key environment settings.
^D

$ . mysetup
    [do this whenever you want work on your project]

###  EXAMPLE 2:  using VSPMS:

$ cd /somedisk/cvs/mywork
$ addpj mywork
    [name your project whatever you want]

$ cat >> $PROJECTRC
export JAVABASE=C:
export JAVAVERS j2sdk1.4.0
. $PROJECT/tools/boot/buildenv.ksh
pathinfo    #optional - display key environment settings.
^D

% chpj mywork
    [do this whenever you want work on your project]

SETUP ABORTED
EOF

    return

fi

#########
#CODELINE:
#########
export CODELINE

#allow user to override codeline var, which determines cvs branch names
#for main repository, and determines the placement of log and kit directories.

if [ "x$IIS_CODELINE" != x ]; then
    CODELINE=$IIS_CODELINE
else
    CODELINE=main
fi

####
#CVS BRANCH NAMES.  Use IIS_BRANCH_NAME to override "main" when bootstraping a branch.
####
export JBI_BRANCH_NAME CVS_BRANCH_NAME
JBI_BRANCH_NAME=main
if [ "x$IIS_BRANCH_NAME" != x ]; then
    JBI_BRANCH_NAME="$IIS_BRANCH_NAME"
fi

CVS_BRANCH_NAME=$JBI_BRANCH_NAME

#now we can set TOOLS_CVSROOT, which is based on the branch and product name:
if [ "x$TOOLS_CVSROOT" = x ]; then
    export TOOLS_CVSROOT=":pserver:anoncvs@iis:/tooldist/$PRODUCT/$CVS_BRANCH_NAME"
fi

#################
# CVS_BRANCH_NAME is used in the following scripts to denote the
# major codeline revision.  It is meant to be generic for generic tools (devtools).
# toolsBuild    - checkout tools src
# makedrv.pl    - checkout tools src
# buildenv.csh  - setup file
# buildenv.ksh  - setup file
# bldcmn.sh     - assert
# fortepj.rc    - cosmetic (sets $REV VSPMS var)
# fortepj.ksh   - cosmetic (sets $REV VSPMS var)
#################

export DEVELOPER_BUILD=1

alias gettools='(mkdir -p tools.new; cd $SRCROOT/tools.new;  cvs -f -d $TOOLS_CVSROOT co ${FORTE_PORT}tools; echo new tools are in tools.new)'
alias cvstools='cvs -d $TOOLS_CVSROOT'

#### CVS DEFS
export CVSREAD=1
export CVSROOT="$IIS_CVSROOT"

#these variables are needed to update the local project tool sources during toolsBuild
export CVS_SRCROOT_PREFIX="open-esb"
export CVS_CO_ROOT="${SRCROOT}/.."

if [ "x$JBI_MODULES" = x ]; then
    export JBI_MODULES="open-esb/m2.ant open-esb/pom.xml open-esb/antbld open-esb/bb open-esb/build-common open-esb/esb-components open-esb/esb-packages open-esb/esb-test open-esb/esb-util open-esb/installers open-esb/jbi open-esb/platform-config open-esb/repo open-esb/ri-clients open-esb/ri-components open-esb/ri-examples open-esb/ri-packages open-esb/rl open-esb/runtime open-esb/vendor-libs"
fi

####### TOOLS SETUP
export TOOLROOT FORTE_PORT

if [ "x$IIS_TOOLROOT" != x ]; then
    TOOLROOT=$IIS_TOOLROOT
else
    TOOLROOT=$SRCROOT/tools
fi

if [ -x $TOOLROOT/boot/whatport.ksh -o -x $TOOLROOT/boot/whatport ]; then
    FORTE_PORT=`$TOOLROOT/boot/whatport`
else
    echo ERROR:  could not find tools - please bootstrap your tools.
    return
fi

PATH="$TOOLROOT/bin/$FORTE_PORT${PS}$TOOLROOT/bin/cmn${PS}$PATH"

export PERL_LIBPATH
if [ "x$PERL5_HOME" != x ]; then
    if [ -d "$PERL5_HOME" ]; then
        #perl installations on solaris and linux differ - solaris has {bin,lib} subdirs:
        if [ -d "$PERL5_HOME/lib" ]; then
            PERL_LIBPATH=".;$PERL5_HOME/lib;$TOOLROOT/lib/cmn;$TOOLROOT/lib/cmn/perl5"
        else
            PERL_LIBPATH=".;$PERL5_HOME;$TOOLROOT/lib/cmn;$TOOLROOT/lib/cmn/perl5"
        fi
        if [ -d "$PERL5_HOME/bin" ]; then
            PATH="$PERL5_HOME/bin${PS}$PATH"
            #otherwise, we assume perl is already in the path
        fi
    else
        echo "WARNING: not a directory, PERL5_HOME='$PERL5_HOME'. Please fix."
    fi
else
    #use port-specific perl libs in $TOOLROOT; this is for old solaris
    #and mks perl installs:
    PERL_LIBPATH=".;$TOOLROOT/lib/cmn;$TOOLROOT/lib/$FORTE_PORT/perl5;$TOOLROOT/lib/cmn/perl5"
fi

#used by makemf utility:
export MAKEMF_LIB=$TOOLROOT/lib/cmn

#used by codegen utility:
export CG_TEMPLATE_PATH=".;$TOOLROOT/lib/cmn/templates;$TOOLROOT/lib/cmn/templates/java"

alias pull='$TOOLROOT/boot/updateDist'

#### set up env required for release and tools builds:
export PATHNAME=`basename $SRCROOT`
export PATHREF=$SRCROOT
export RELEASE_ROOT=$SRCROOT/release
export RELEASE_DISTROOT=$SRCROOT/release
export HOST_NAME="`uname -n`"

export PRIMARY_PORT=solsparc
export TARGET_OS_LIST="cmn,ntcmn,nt,solsparc,linux,macosx,solx86,cygwin"

if [ "x$FORTE_LINKROOT" = x ]; then
    export FORTE_LINKROOT=$SRCROOT
fi

if [ "x$DISTROOT" = x ]; then
    export DISTROOT=$FORTE_LINKROOT/dist/tools
fi

if [ "x$KITROOT" = x ]; then
    export KITROOT=$FORTE_LINKROOT/kits
fi

if [ "x$KIT_DISTROOT" = x ]; then
    export KIT_DISTROOT=$FORTE_LINKROOT/kits/$PRODUCT
fi

if [ "x$KIT_REV" = x ]; then
    export KIT_REV=$CODELINE
fi


if [ "x$REGRESS_DISPLAY" = x ]; then
    if [ "x$DISPLAY" != x ]; then
        export REGRESS_DISPLAY="$DISPLAY"
    else
        export REGRESS_DISPLAY=NULL
    fi
fi

##### JAVA BASE
#adjust this to where you have java installed:
if [ "x$JAVABASE" = x ]; then
    bldmsg -error You must set JAVABASE and JAVAVERS
    bldmsg EXAMPLE:  'export JAVABASE=c:; export JAVAVERS=jdk1.3_0'
    return 1
fi

export JAVA_HOME=$JAVABASE/$JAVAVERS
PATH="$JAVA_HOME/bin${PS}$PATH"
##########

##### JAVA TOOLS
#ant
export ANT_HOME=$TOOLROOT/java/ant
if [ "x$ANT_OPTS" = x ]; then
    export ANT_OPTS=-Xmx200m
fi

######
#maven setup:
######
export MAVEN_REPO_LOCAL
if [ "$IIS_MAVEN_REPO_LOCAL" != "" ]; then
    MAVEN_REPO_LOCAL="$IIS_MAVEN_REPO_LOCAL"
else
    MAVEN_REPO_LOCAL="$SRCROOT/m2/repository"
fi

export MAVEN_DISTROOT
if [ "$IIS_MAVEN_DISTROOT" != "" ]; then
    MAVEN_DISTROOT="$IIS_MAVEN_DISTROOT"
else
    MAVEN_DISTROOT="$SRCROOT/bld/remote"
fi

export MAVEN_HOME M2_HOME
MAVEN_HOME=$TOOLROOT/java/maven
M2_HOME=$TOOLROOT/java/maven2

export MAVEN_OPTS
if [ x$MAVEN_OPTS = x ]; then
    MAVEN_OPTS="-Xmx400m"
fi

##### JREGRESS
if [ "x$JREGRESS_TIMEOUT" = x ]; then
    export JREGRESS_TIMEOUT=650
fi

##### set up JBI_USER_NAME:
#this can be set in the login env, as it is usually invariant
if [ "x$JBI_USER_NAME" = x ]; then
    if [ "x$LOGNAME" != x ]; then
        export JBI_USER_NAME="$LOGNAME"
    elif [ "x$USER" != x ]; then
        export JBI_USER_NAME="$USER"
    else
        echo 'ERROR cannot set JBI_USER_NAME from $LOGNAME or $USER - PLEASE DEFINE IT MANUALLY'
        export JBI_USER_NAME="NONAME"
    fi
fi

##### Default AS8BASE:
export AS8BASE
if [ "$IIS_AS8BASE" != "" ]; then
    AS8BASE="$IIS_AS8BASE"
else
    AS8BASE="$SRCROOT/install/as8"
fi
PATH="$AS8BASE/bin${PS}$PATH"

##### Default JBISE_BASE:
export JBISE_BASE=$SRCROOT/install/jbise

##### Default JBOSSBASE:
export JBOSSBASE=$SRCROOT/install/jboss
PATH="$JBOSSBASE/bin${PS}$PATH"

#######
#cygwin: set java versions of SRCROOT, TOOLROOT (used in ant scripts):
#######
export JV_SRCROOT JV_TOOLROOT JV_MAVEN_REPO_LOCAL JV_MAVEN_DISTROOT JV_AS8BASE
if [ "$FORTE_PORT" = "cygwin" ]; then
    JV_SRCROOT=`cygpath -wm "$SRCROOT"`
    JV_TOOLROOT=`cygpath -wm "$TOOLROOT"`
    JV_MAVEN_REPO_LOCAL=`cygpath -wm "$MAVEN_REPO_LOCAL"`
    JV_MAVEN_DISTROOT=`cygpath -wm "$MAVEN_DISTROOT"`
    JV_AS8BASE=`cygpath -wm "$AS8BASE"`
else
    JV_SRCROOT="$SRCROOT"
    JV_TOOLROOT="$TOOLROOT"
    JV_MAVEN_REPO_LOCAL="$MAVEN_REPO_LOCAL"
    JV_MAVEN_DISTROOT="$MAVEN_DISTROOT"
    JV_AS8BASE="$AS8BASE"
fi

#########
#NetBeans setup:
#########
export NETBEANS_HOME
if [ "$IIS_NETBEANS_HOME" != "" ]; then
    NETBEANS_HOME="$IIS_NETBEANS_HOME"
fi

#make a random guess, based on platform:
if [ "$NETBEANS_HOME" = "" ]; then
    nbvers=5.5beta2
    if [ "$FORTE_PORT" = "macosx" ]; then
        NETBEANS_HOME=/Applications/NetBeans.app/Contents/Resources/NetBeans
    elif [ "$FORTE_PORT" = "cygwin" ]; then
        NETBEANS_HOME=/netbeans/$nbvers
    else
        NETBEANS_HOME=/opt/netbeans/$nbvers
    fi

    unset nbvers
fi

if [  ! -d "$NETBEANS_HOME"  ]; then
    echo "WARNING: not a directory, NETBEANS_HOME='$NETBEANS_HOME'. Please provide IIS_NETBEANS_HOME parameter."
fi

alias runide='"$NETBEANS_HOME"/bin/netbeans -J-Dmaven.repo.local="$JV_SRCROOT"/m2/repository'

#this is a hack to bootstrap the boms.  RT 9/26/05
export JBI_DOT_VERSION="1.1"

#standard project defs:
unset fortepj
export fortepj="$TOOLROOT/lib/cmn/fortepj.ksh"
if [ -r "$fortepj" ]; then
    . "$fortepj"
else
    echo WARNING - cannot open $fortepj
fi

opt=`optpath` > /dev/null 2>&1
if [ $? -eq 0 ]; then
    export PATH="$opt"
else
    echo PATH not optimized
fi

#these variables are PRIVATE and RESERVED for this definition file:
unset IIS_TOOLROOT
unset IIS_CVSROOT
unset IIS_BRANCH_NAME
unset IIS_CODELINE
unset IIS_MAVEN_REPO_LOCAL
unset IIS_MAVEN_DISTROOT
unset IIS_AS8BASE
unset IIS_NETBEANS_HOME

#finally, set the current release tag:
export REV="CP07"
