/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WorkManager.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.file.framework;

import com.sun.jbi.binding.file.FileBindingResources;
import com.sun.jbi.binding.file.util.StringTranslator;

import com.sun.jbi.binding.file.FileBindingContext;
import java.util.Hashtable;
import java.util.logging.Logger;


/**
 * This class manages the work requests and dispatches the requests to be
 * executed in a free thread.
 *
 * @author Sun Microsystems, Inc.
 */
public final class WorkManager
    implements FileBindingResources
{
    /**
     * Handle to the list of Work Manager instances.
     */
    private static Hashtable sWorkManagerBucket = new Hashtable();

    /**
     * Internal handle to the logger instance.
     */
    private static Logger sLog;

    /**
     * Default value for max threads.
     */
    private static final int DEFAULT_MAX_THREADS = 10;

    /**
     * Default value for min threads.
     */
    private static final int DEFAULT_MIN_THREADS = 2;

    /**
     * Localization translator object.
     */
    private static StringTranslator sTranslator;

    /**
     * Handle to store the Work Manager state. Valid values are "INIT", "READY"
     * and "STOP".
     */
    private String mState;

    /**
     * Handle to the WorkThreadPool instance.
     */
    private WorkThreadPool mThreadPool;

    /**
     * Value for maximum number of threads.
     */
    private int mMaxNumberOfThreads = DEFAULT_MAX_THREADS;

    /**
     * Value for minimum number of threads.
     */
    private int mMinNumberOfThreads = DEFAULT_MIN_THREADS;

    /**
     * Creates a new instance of WorkManager.
     */
    private WorkManager()
    {
        mState = "PARKED";
        init();
    }

   
    /**
     * This method can be used to set the maximum threads in the Pool.
     *
     * @param count number of threads.
     */
    public void setMaxThreads(int count)
    {
        mMaxNumberOfThreads = count;
    }

    /**
     * Sets the minimum number of threads in the Thread pool.
     *
     * @param count number of threads.
     */
    public void setMinThreads(int count)
    {
        mMinNumberOfThreads = count;
    }

    /**
     * Returns a handle to the Work Manager instance for a unique service .
     *
     * @param name Some unique name to identify this work manager.
     *
     * @return a work manager instance
     */
    public static WorkManager getWorkManager(String name)
    {
        WorkManager manager = null;

        String serviceName = name;
        sLog = FileBindingContext.getInstance().getLogger();
        sTranslator = new StringTranslator();
        if (serviceName != null)
        {
            manager = (WorkManager) sWorkManagerBucket.get(serviceName);

            if (manager == null)
            {
                manager = new WorkManager();

                sWorkManagerBucket.put(serviceName, manager);
            }
        }
        else
        {            
            sLog.severe(sTranslator.getString(FBC_THREADS_WM_FAILED));
        }

        return manager;
    }

    /**
     * Returns the number of wokring threads in the pool.
     *
     * @return count of number of busy threads
     */
    public int getBusyThreads()
    {
        return mThreadPool.getBusyThreads();
    }

    /**
     * Cleans up the workmanager. It notifies the workthread pool to shutdown
     * all its threads.
     */
    public void cease()
    {
        // Stop the thread pool.
        if (mState.equals("INIT") || mState.equals("STARTED"))
        {
            try
            {
                mThreadPool.stop();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        setState("STOPPED");
    }

    /**
     * This method is invoked to complete all the work running in the threads
     * that are active.
     */
    public void finishWork()
    {
        mThreadPool.exitWhenBusyThreadsDone();
    }

    /**
     * Initializes the Work Manager.
     */
    public void init()
    {
        if (mState.equals("INIT"))
        {
            return;
        }
        sWorkManagerBucket = new Hashtable();
        mThreadPool = new WorkThreadPool();
        mThreadPool.setMinThreads(mMinNumberOfThreads);
        mThreadPool.setMaxThreads(mMaxNumberOfThreads);
        mThreadPool.init();
        setState("INIT");
    }

    /**
     * Process the Command in a different thread. The method places the command
     * in its internal cache and returns control to the invoking thread.
     *
     * @param command - command to be processed.
     *
     * @return true if a free thread is available, false otherwise
     */
    public boolean processCommand(Command command)
    {
        WorkThread workerThread;
        workerThread = mThreadPool.getFreeThread();

        boolean status = false;

        if (workerThread != null)
        {
            sLog.fine(sTranslator.getString(FBC_THREADS_WM_PROCESS));
            workerThread.setCommand(command);
            status = true;
        }
        else
        {
            sLog.severe(sTranslator.getString(FBC_THREADS_WM_NOFREETHREAD));
        }

        return status;
    }

    /**
     * Starts the Work Manager.
     */
    public void start()
    {
        try
        {
            mThreadPool.start();
        }
        catch (Exception e)
        {
            sLog.severe(sTranslator.getString(
                    FBC_THREADS_WM_THREADPOOL_FAILED, e.getMessage()));
        }

        setState("STARTED");
    }

    /**
     * Sets the state of the work manager.
     *
     * @param state - state of the work manager.
     */
    protected void setState(String state)
    {
        mState = state;
    }
}
