#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)file00004.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "filebinding00004.ksh- DEPLOY file binding test"

. ./regress_defs.ksh

# first create a jar by replacing tokens

# jar and zip 
rm -rf $FILEBINDING_BLD_DIR/test/deploy
cp -R $FILEBINDING_REGRESS_DIR/test/deploy $FILEBINDING_BLD_DIR/test
rm -rf $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/CVS
rm -rf $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/META-INF/CVS
rm -rf $FILEBINDING_BLD_DIR/test/deploy/CVS
rm -rf $FILEBINDING_BLD_DIR/test/deploy/META-INF/CVS
rm -f $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/endpoints_tokens.xml
sed -e "s#FILEBINDING_BLD_DIR#$FILEBINDING_BLD_DIR#g" $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/endpoints_tokens.wsdl > $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/endpoints.wsdl
sed -e "s#ARTIFACT#WSDL20#g" $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/META-INF/jbi_tokens.xml> $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/META-INF/jbi.xml
rm -f $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/endpoints11_tokens.wsdl
rm -f $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/META-INF/jbi_tokens.xml
rm -f $FILEBINDING_BLD_DIR/test/deploy/filebindingASA/endpoints_tokens.wsdl
cd $FILEBINDING_BLD_DIR/test/deploy/filebindingASA 
jar cvf ../filebindingASA.jar . 
cd $FILEBINDING_BLD_DIR/test/deploy
rm -r filebindingASA
ant -emacs -q -DFILEBINDING_BLD_DIR="$FILEBINDING_BLD_DIR" -f $FILEBINDING_REGRESS_DIR/scripts/deploy.ant make-deploy

$JBI_ANT -Djbi.deploy.file=$FILEBINDING_BLD_DIR/test/deploy/filetestdeploy.zip deploy-service-assembly

$JBI_ANT -Djbi.service.assembly.name="9ae51feb-0fc3-4ff6-9104-cfd59d2a7815" start-service-assembly

echo Completed Deploying to file binding
