/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestServiceBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import junit.framework.*;

import java.io.Reader;
import java.io.Serializable;
import java.io.Writer;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestServiceBean extends TestCase
{
    /**
     * Creates a new TestServiceBean object.
     *
     * @param testName
     */
    public TestServiceBean(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestServiceBean.class);

        return suite;
    }

    /**
     * Test of deleteTimeout method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testDeleteTimeout()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDescription method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testGetDescription()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getName method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testGetName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getOperation method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testGetOperation()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getServiceid method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testGetServiceid()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getTimeout method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testGetTimeout()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of hasTimeout method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testHasTimeout()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setDescription method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testSetDescription()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setName method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testSetName()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setOperation method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testSetOperation()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setServiceid method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testSetServiceid()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of setTimeout method, of class
     * com.sun.jbi.engine.sequencing.servicelist.ServiceBean.
     */
    public void testSetTimeout()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
