/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestDeploymentRegistry.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;

import junit.framework.*;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.namespace.QName;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestDeploymentRegistry extends TestCase
{
    /**
     *    
     */
    private DeploymentRegistry mRegistry;

    /**
     *    
     */
    private ServicelistBean mListBean;

    /**
     * Creates a new TestDeploymentRegistry object.
     *
     * @param testName
     */
    public TestDeploymentRegistry(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestDeploymentRegistry.class);

        return suite;
    }

    /**
     * Sets up tests.
     */
    public void setUp()
    {
        mRegistry = DeploymentRegistry.getInstance();
        mListBean = new ServicelistBean();
    }

    /**
     * Test of clearRegistry method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testClearRegistry()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of deregisterService method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testDeregisterService()
    {
        // Add your test code below by replacing the default call to fail.        
    }

    /**
     * Test of findService method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testFindService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDeploymentStatus method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testGetDeploymentStatus()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getInstance method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testGetInstance()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getService method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testGetService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isDeployed method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testIsDeployed()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isStarted method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testIsStarted()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of listAllServices method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testListAllServices()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of registerService method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testRegisterService()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of start method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testStart()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of stop method, of class
     * com.sun.jbi.engine.sequencing.DeploymentRegistry.
     */
    public void testStop()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
