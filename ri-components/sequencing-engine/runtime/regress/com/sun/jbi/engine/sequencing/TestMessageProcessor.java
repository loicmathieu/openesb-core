/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestMessageProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.framework.threads.Command;
import com.sun.jbi.engine.sequencing.util.ConfigData;

import junit.framework.*;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.net.URI;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.ExchangeStatus;
import javax.jbi.messaging.InOnly;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;
import javax.jbi.messaging.RobustInOnly;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestMessageProcessor extends TestCase
{
    /**
     * Creates a new TestMessageProcessor object.
     *
     * @param testName
     */
    public TestMessageProcessor(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestMessageProcessor.class);

        return suite;
    }

    /**
     * Test of process method, of class
     * com.sun.jbi.engine.sequencing.MessageProcessor.
     */
    public void testProcess()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processInOnly method, of class
     * com.sun.jbi.engine.sequencing.MessageProcessor.
     */
    public void testProcessInOnly()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processInOut method, of class
     * com.sun.jbi.engine.sequencing.MessageProcessor.
     */
    public void testProcessInOut()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processOutOnly method, of class
     * com.sun.jbi.engine.sequencing.MessageProcessor.
     */
    public void testProcessOutOnly()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of processRobustInOnly method, of class
     * com.sun.jbi.engine.sequencing.MessageProcessor.
     */
    public void testProcessRobustInOnly()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of valid method, of class
     * com.sun.jbi.engine.sequencing.MessageProcessor.
     */
    public void testValid()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
