/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TestSequencingEngineDeployer.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing;

import com.sun.jbi.engine.sequencing.framework.Servicelist;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.util.ConfigData;
import com.sun.jbi.engine.sequencing.util.FileListing;

import junit.framework.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;

import javax.jbi.management.DeploymentException;


/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class TestSequencingEngineDeployer extends TestCase
{
    /**
     * Creates a new TestSequencingEngineDeployer object.
     *
     * @param testName
     */
    public TestSequencingEngineDeployer(java.lang.String testName)
    {
        super(testName);
    }

    /**
     * DOCUMENT ME!
     *
     * @return NOT YET DOCUMENTED
     */
    public static Test suite()
    {
        TestSuite suite = new TestSuite(TestSequencingEngineDeployer.class);

        return suite;
    }

    /**
     * Test of deploy method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineDeployer.
     */
    public void testDeploy()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDeploymentInfo method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineDeployer.
     */
    public void testGetDeploymentInfo()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of getDeployments method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineDeployer.
     */
    public void testGetDeployments()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of isDeployed method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineDeployer.
     */
    public void testIsDeployed()
    {
        // Add your test code below by replacing the default call to fail.
    }

    /**
     * Test of undeploy method, of class
     * com.sun.jbi.engine.sequencing.SequencingEngineDeployer.
     */
    public void testUndeploy()
    {
        // Add your test code below by replacing the default call to fail.
    }

    // Add test methods here, they have to start with 'test' name.
    // for example:
    // public void testHello() {}
}
