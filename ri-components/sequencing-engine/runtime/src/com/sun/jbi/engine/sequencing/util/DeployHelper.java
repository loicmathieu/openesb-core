/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

import com.sun.jbi.engine.sequencing.DeploymentRegistry;
import com.sun.jbi.engine.sequencing.SequencingEngineContext;
import com.sun.jbi.engine.sequencing.SequencingEngineResources;
import com.sun.jbi.engine.sequencing.servicelist.ServiceBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistBean;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistReader;
import com.sun.jbi.engine.sequencing.servicelist.ServicelistValidator;

import org.w3c.dom.Document;

import java.io.File;

import java.util.logging.Logger;

import javax.xml.namespace.QName;


/**
 * This class ia  aHelper clas to do deployment.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployHelper
    extends UtilBase
    implements SequencingEngineResources
{
    /**
     * Engine Context object.
     */
    private javax.jbi.component.ComponentContext mContext;

    /**
     * DD reader
     */
    private DeployDescriptorReader mDeployDescriptorReader;

    /**
     * Logger object.
     */
    private Logger mLog;

    /**
     * List of endpoints.
     */
    private ServicelistBean mServicelist;

    /**
     * Reader object to read config XML file.
     */
    private ServicelistReader mServicelistReader;

    /**
     * Deployment Descriptor schema
     */
    private String mDDSchema;

    /**
     * DeploymentDescriptor file
     */
    private String mDeploymentDescriptor;

    /**
     * Service unit of deployment.
     */
    private String mSUID;

    /**
     * Name of the schema file.
     */
    private String mSchemaFile;

    /**
     * Name of the endpoint config XMl file.
     */
    private String mServicelistFile;

    /**
     * Translator object for internationalization.
     */
    private StringTranslator mTranslator;

    /**
     * Creates a new DeployHelper object.
     *
     * @param suId service unit id
     * @param suPath path of su.
     */
    public DeployHelper(
        String suId,
        String suPath)
    {
        mSUID = suId;
        mContext = SequencingEngineContext.getInstance().getContext();
        mServicelistFile = FileListing.getXMLFile(suPath);
        mSchemaFile =
            mContext.getInstallRoot() + File.separatorChar
            + ConfigData.SCHEMA_FILE;
        mDeploymentDescriptor =
            suPath + File.separatorChar + "META-INF" + File.separatorChar
            + ConfigData.DEPLOY_DESCRIPTOR;
        mDDSchema =
            SequencingEngineContext.getInstance().getContext().getInstallRoot()
            + File.separatorChar + ConfigData.JBI_SCHEMA_FILE;
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        setValid(true);
    }

    /**
     * Method which does the actual deployment.
     *
     * @param isDeploy true if deployment false if starting
     */
    public void doDeploy(boolean isDeploy)
    {
        mLog.fine(SEQ_FINE_DOING_DEPLOY);

        try
        {
            ServicelistValidator validator =
                new ServicelistValidator(mSchemaFile, mServicelistFile);
            validator.setValidating();
            validator.validate();

            Document d = validator.getDocument();

            if ((!validator.isValid()) || (d == null))
            {
                mLog.severe(mTranslator.getString(SEQ_INVALID_CONFIG_FILE,
                        mServicelistFile));
                mLog.severe(validator.getError());
                setError(getError() + "\n"
                    + mTranslator.getString(SEQ_INVALID_CONFIG_FILE,
                        mServicelistFile) + "\n" + validator.getError());
                setValid(false);

                return;
            }

            mServicelistReader = new ServicelistReader();
            mServicelistReader.init(d);
            mServicelist = mServicelistReader.getServicelistBean();

            if ((!mServicelistReader.isValid()) || (mServicelist == null))
            {
                mLog.severe(mTranslator.getString(SEQ_CONFIGDATA_ERROR));
                setError(mTranslator.getString(SEQ_CONFIGDATA_ERROR) + "\n"
                    + getError());
                setValid(false);

                return;
            }

            mServicelist.setDeploymentId(mSUID);

            if ((mDeploymentDescriptor != null)
                    && (new File(mDeploymentDescriptor)).exists())
            {
                doDDParsing();
            }
                   
            /**
             * Here we have a violation of spec 1.0. The spec says that
             * a jbi.xml must be present in an SU, 
             * But we are making it optional here, just to make a backward
             * compatibility.  
             * This optionality will be made mandatory in future.
             *  
             */
            /*else
            {
                setError(mTranslator.getString(SEQ_NO_DD) + "\n" + getError());
                mLog.info(mTranslator.getString(SEQ_NO_DD));
                setValid(false);

                return;
            }
             */

            if (isValid())
            {
                registerService();
            }

            dump(mServicelist);
        }
        catch (Exception de)
        {
            de.printStackTrace();
            setException(de);
            setValid(false);
        }
    }

    /**
     * Verifies if a bean object is populated properly.
     *
     * @param eb servicelist bean
     *
     * @return true/ false
     */
    public boolean verify(ServicelistBean eb)
    {
        if (eb.getServicename().equals(""))
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_SERVICELIST_NAME));

            return false;
        }

        int count = eb.getServiceCount();

        for (int i = 0; i < count; i++)
        {
            try
            {
                ServiceBean sb = eb.getService(i);

                if (!verifyService(sb))
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                mLog.warning(e.getMessage());

                return false;
            }
        }

        return true;
    }

    /**
     * Verifies the registry for duplicate entries.
     */
    private void checkRegistry()
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();
        String ser = mServicelist.getServicename();

        if (dr.isDeployed(ser))
        {
            mLog.warning(mTranslator.getString(SEQ_DUPLICATE_ENDPOINT, ser));
        }
    }

    /**
     * Parses the deployment descriptor jbi.xml.
     */
    private void doDDParsing()
    {
        ServicelistValidator validator =
            new ServicelistValidator(mDDSchema, mDeploymentDescriptor);
        validator.validate();

        Document d = validator.getDocument();

        if ((!validator.isValid()) || (d == null))
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_DD,
                    mDeploymentDescriptor));
            setError(mTranslator.getString(SEQ_INVALID_DD, mDeploymentDescriptor)
                + " " + validator.getError());
            setValid(false);

            return;
        }

        /**
         * Match service names in artifacts file ( endpoints.xml) and the DD.
         * Every endpoint in the the DD file should be present in the
         * artifacts file. We dont care if the artifacts file has more
         * endpoints. Those will be ignored. The condition is the DD XML file
         * should be a sub-set of artifacts file. The spec does not govern
         * this logic so it is upto the component to decide how to veryify
         * consistence between  DD and its artifacts.
         */
        /**
         * A valid question that arises here is why are we having 2 XML files
         * to decorate an endpoint. The artifacts XML file is the older form
         * which was used when the  spec did not accomodate any DD for an SU.
         * So that is still around.  Any component specific decoration for the
         * endpoints can be done  through the DD jbi.xml also. And that would
         * be the correct way to do it. We dont do it that way because there
         * are other modules that might           depend on the endpoints.xml
         * file, so to maintain their functionality  the older endpoints.xml
         * file is still used to decorate endpoints.
         */
        mDeployDescriptorReader = new DeployDescriptorReader();
        mDeployDescriptorReader.init(d);

        if (!mDeployDescriptorReader.isValid())
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_DD,
                    ConfigData.DEPLOY_DESCRIPTOR));
            setError(mTranslator.getString(SEQ_INVALID_DD,
                    ConfigData.DEPLOY_DESCRIPTOR) + " "
                + mDeployDescriptorReader.getError());
            setValid(false);

            return;
        }

        if (mDeployDescriptorReader.getConsumerCount() != 0)
        {
            setError(mTranslator.getString(SEQ_INCONSISTENT_CONSUMER_DATA));

            return;
        }

        if (mDeployDescriptorReader.getProviderCount() != 1)
        {
            setError(mTranslator.getString(SEQ_INCONSISTENT_PROVIDER_DATA));

            return;
        }

        QName sername =
            new QName(mServicelist.getNamespace(), mServicelist.getServicename());
        QName intername =
            new QName(mServicelist.getInterfaceNamespace(),
                mServicelist.getInterfaceName());
        String epname = mServicelist.getEndpointName();

        if (!mDeployDescriptorReader.isPresent(sername, intername, epname))
        {
            setError(mTranslator.getString(SEQ_INCONSISTENT_DATA));

            return;
        }
    }

    /**
     * Dumps deployment information.
     *
     * @param slb list bean
     */
    private void dump(ServicelistBean slb)
    {
        mLog.fine("Dumping contents of Config file ");
        mLog.fine("Deployment id " + slb.getDeploymentId()
            + " has a list with name " + slb.getServicename()
            + " with namespace " + slb.getNamespace() + " and endpoint name "
            + slb.getEndpointName() + " and operation " + slb.getOperation());
        mLog.fine("With " + slb.getServiceCount() + " services ");

        int count = slb.getServiceCount();

        for (int j = 0; j < count; j++)
        {
            ServiceBean sb = slb.getService(j);
            mLog.fine("Service " + j + " with name " + sb.getName()
                + " and description " + sb.getDescription() + " and serviceid "
                + sb.getServiceid() + " will be invoked with operation "
                + sb.getOperation() + " with a time out of " + sb.getTimeout());
        }
    }

    /**
     * Registers a bean and service name with registry.
     */
    private void registerService()
    {
        DeploymentRegistry dr = DeploymentRegistry.getInstance();
        String ser =
            mServicelist.getNamespace() + mServicelist.getServicename()
            + mServicelist.getEndpointName() + mServicelist.getOperation();

        if (dr.isDeployed(ser))
        {
            mLog.warning(mTranslator.getString(SEQ_DUPLICATE_ENDPOINT, ser));
            setError(getError() + "\n"
                + mTranslator.getString(SEQ_DUPLICATE_ENDPOINT, ser));
            setValid(false);

            return;
        }

        if (!verify(mServicelist))
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_CONFIG_FILE_INFO));
            setError(getError() + "\n"
                + mTranslator.getString(SEQ_INVALID_CONFIG_FILE_INFO));
            setValid(false);
        }
        else
        {
            dr.registerService(ser, mServicelist);
        }
    }

    /**
     * Checks if the service bean info is valid.
     *
     * @param sb service bean
     *
     * @return true if valid
     */
    private boolean verifyService(ServiceBean sb)
    {
        if (sb == null)
        {
            return false;
        }

        if ((sb.getName() == null) || (sb.getName().equals("")))
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_SERVICE));
            setError(getError() + "\n"
                + mTranslator.getString(SEQ_INVALID_SERVICE));

            return false;
        }

        if ((sb.getServiceid() == null) || (sb.getServiceid().equals("")))
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_SERVICEID,
                    sb.getName()));
            setError(getError() + "\n"
                + mTranslator.getString(SEQ_INVALID_SERVICEID, sb.getName()));

            return false;
        }

        if ((sb.getOperation() == null) || (sb.getOperation().equals("")))
        {
            mLog.severe(mTranslator.getString(SEQ_INVALID_OPERATION,
                    sb.getServiceid()));
            setError(getError() + "\n"
                + mTranslator.getString(SEQ_INVALID_OPERATION, sb.getServiceid()));

            return false;
        }

        return true;
    }
}
