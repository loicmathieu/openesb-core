/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DeployDescriptorReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.util;

import com.sun.jbi.engine.sequencing.SequencingEngineContext;
import com.sun.jbi.engine.sequencing.SequencingEngineResources;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.StringTokenizer;
import java.util.logging.Logger;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;


/**
 * This class reads and loads the SU jbi.xml.
 *
 * @author Sun Microsystems, Inc.
 */
public class DeployDescriptorReader
    extends UtilBase
    implements SequencingEngineResources
{
    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Logger Object
     */
    private Logger mLog;

    /**
     * i18n
     */
    private StringTranslator mTranslator;

    /**
     * Endpoint List.
     */
    private EndpointInfo [] mEPList;

    /**
     * Number of consumers.
     */
    private int mNoOfConsumers;

    /**
     * Number of providers.
     */
    private int mNoOfProviders;

    /**
     * The total number of end points in the config file
     */
    private int mTotalEndpoints = 0;

    /**
     * Creates a new ConfigReader object.
     */
    public DeployDescriptorReader()
    {
        mLog = SequencingEngineContext.getInstance().getLogger();
        mTranslator = new StringTranslator();
        setValid(true);
    }

    /**
     * Returns the number of consumer endpoints.
     *
     * @return number of consumers.
     */
    public int getConsumerCount()
    {
        return mNoOfConsumers;
    }

    /**
     * Sets the endpoint.
     *
     * @param node node.
     * @param ep endpoint info.
     */
    public void setEndpoint(
        Node node,
        EndpointInfo ep)
    {
        NamedNodeMap map = node.getAttributes();

        try
        {
            String epname = map.getNamedItem("endpoint-name").getNodeValue();
            String sername = map.getNamedItem("service-name").getNodeValue();
            String intername =
                map.getNamedItem("interface-name").getNodeValue();
            ep.setServiceName(new QName(getNamespace(sername),
                    getLocalName(sername)));
            ep.setInterfaceName(new QName(getNamespace(intername),
                    getLocalName(intername)));
            ep.setEndpointName(epname);
        }
        catch (Exception e)
        {
            mLog.severe("Failed loading DD");
            e.printStackTrace();
            setError(getError() + mTranslator.getString(SEQ_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }

    /**
     * Checks if the endpoint is present in the jbi.xml.
     *
     * @param sername  service name.
     * @param intername  interface name.
     * @param epname  endpoint name.
     *
     * @return  true if endpoint is present.
     */
    public boolean isPresent(
        QName sername,
        QName intername,
        String epname)
    {

        for (int ep = 0; ep < mEPList.length; ep++)
        {

            if ((
                        mEPList[ep].getServiceName().toString().equals(sername
                            .toString())
                    )
                    && (
                        mEPList[ep].getInterfaceName().toString().equals(intername
                            .toString())
                    ) && (mEPList[ep].getEndpointName().equals(epname)))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns number of provider endpoints
     *
     * @return number of providers.
     */
    public int getProviderCount()
    {
        return mNoOfProviders;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param doc Name of the config file.
     */
    public void init(Document doc)
    {
        try
        {
            mDoc = doc;
            mDoc.getDocumentElement().normalize();
            load();
        }
        catch (Exception genException)
        {
            mLog.severe(mTranslator.getString(SEQ_LOAD_DD_FAILED));
            genException.printStackTrace();
            setException(genException);
            setError(getError() + mTranslator.getString(SEQ_LOAD_DD_FAILED)
                + "\n" + genException.getMessage());
            setValid(false);
        }
    }

    /**
     * Loads data.
     */
    public void load()
    {
        try
        {
            NodeList providers = mDoc.getElementsByTagName("provides");
            mNoOfProviders = providers.getLength();

            NodeList consumers = mDoc.getElementsByTagName("consumes");
            mNoOfConsumers = consumers.getLength();
            mEPList = new EndpointInfo[mNoOfConsumers + mNoOfProviders];

            for (int i = 0; i < mNoOfProviders; i++)
            {
                Node node = providers.item(i);
                EndpointInfo sb = new EndpointInfo();
                setEndpoint(node, sb);
                sb.setProvider();

                if (!isValid())
                {
                    setError(mTranslator.getString(SEQ_LOAD_DD_FAILED) + "\n"
                        + getError());

                    return;
                }

                mEPList[i] = sb;
            }

            for (int i = 0; i < mNoOfConsumers; i++)
            {
                Node node = consumers.item(i);
                EndpointInfo sb = new EndpointInfo();
                setEndpoint(node, sb);

                if (!isValid())
                {
                    setError(mTranslator.getString(SEQ_LOAD_DD_FAILED) + "\n"
                        + getError());

                    return;
                }

                mEPList[i + mNoOfProviders] = sb;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setError(getError() + mTranslator.getString(SEQ_LOAD_DD_FAILED)
                + "\n" + e.getMessage());
            setValid(false);
        }
    }

    /**
     * Gets the local name from the quname.
     *
     * @param qname Qualified name of service.
     *
     * @return String local name
     */
    private String getLocalName(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");

        try
        {
            if (tok.countTokens() == 1)
            {
                return qname;
            }

            tok.nextToken();

            return tok.nextToken();
        }
        catch (Exception e)
        {
            return "";
        }
    }

    /**
     * Gets the namespace from the qname.
     *
     * @param qname Qname of service
     *
     * @return namespace namespace of service
     */
    private String getNamespace(String qname)
    {
        StringTokenizer tok = new StringTokenizer(qname, ":");
        String prefix = null;

        try
        {
            if (tok.countTokens() == 1)
            {
                return "";
            }

            prefix = tok.nextToken();

            NamedNodeMap map = mDoc.getDocumentElement().getAttributes();

            for (int j = 0; j < map.getLength(); j++)
            {
                Node n = map.item(j);

                if (n.getLocalName().trim().equals(prefix.trim()))
                {
                    return n.getNodeValue();
                }
            }
        }
        catch (Exception e)
        {
            ;
        }

        return "";
    }

    /**
     * Class which holds the endpoint information.
     *
     * @author Sun Microsystems, Inc.
     */
    public class EndpointInfo
    {
        /**
         * Interface name.
         */
        QName interfacename;

        /**
         * Service name.
         */
        QName servicename;

        /**
         *  Endpoint name.
         */
        String endpointname;

        /**
         * Provider endpoint.
         */
        boolean provider = false;

        /**
         * Sets the endpoint name.
         *
         * @param epname endpoint name.
         */
        public void setEndpointName(String epname)
        {
            endpointname = epname;
        }

        /**
         * Returns the endpoint name.
         *
         * @return endpoint name.
         */
        public String getEndpointName()
        {
            return endpointname;
        }

        /**
         * Sets the interface name.
         *
         * @param intername interface name.
         */
        public void setInterfaceName(QName intername)
        {
            interfacename = intername;
        }

        /**
         * Returns the interface name.
         *
         * @return interface name.
         */
        public QName getInterfaceName()
        {
            return interfacename;
        }

        /**
         * Sets the endpoint as provider.
         */
        public void setProvider()
        {
            provider = true;
        }

        /**
         * Returns true if the endpoint is provider.
         *
         * @return true if provider endpoint.
         */
        public boolean getProvider()
        {
            return provider;
        }

        /**
         * Sets the service name.
         *
         * @param sername service name.
         */
        public void setServiceName(QName sername)
        {
            servicename = sername;
        }

        /**
         * Returns the service name.
         *
         * @return the service name.
         */
        public QName getServiceName()
        {
            return servicename;
        }

    }
}
