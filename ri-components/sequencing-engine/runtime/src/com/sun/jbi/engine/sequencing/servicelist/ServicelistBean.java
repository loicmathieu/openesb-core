/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServicelistBean.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.sequencing.servicelist;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.jbi.servicedesc.ServiceEndpoint;


/**
 * Class ServicelistBean.
 *
 * @author Sun Microsystems, Inc.
 */
public class ServicelistBean
    implements java.io.Serializable
{
    /**
     * Field mServicelist
     */
    private java.util.ArrayList mServicelist;

    /**
     * Stores executing instances and states
     */
    private Hashtable mExecutingLists;

    /**
     * Service reference.
     */
    private ServiceEndpoint mServiceReference;

    /**
     * Field deployment Id
     */
    private java.lang.String mDeploymentId;

    /**
     * Endpoint name
     */
    private String mEndpointName;

    /**
     * Interface local name.
     */
    private String mInterfaceName;

    /**
     * Namespace for interface
     */
    private String mInterfaceNamespace;

    /**
     * Field mListdescription
     */
    private java.lang.String mListdescription;

    /**
     * Mep
     */
    private String mMep;

    /**
     * Field namespace uri
     */
    private String mNamespace;

    /**
     * Field mOperation
     */
    private java.lang.String mOperation = "execute";

    /**
     * Operation namespace
     */
    private String mOperationNamespace;

    /**
     * Field mServicename
     */
    private java.lang.String mServicename;

    /**
     * Denotes if this list has executing instances
     */
    private boolean mExecuting;

    /**
     * Creates a new ServicelistBean object.
     */
    public ServicelistBean()
    {
        super();
        setOperation("execute");
        mServicelist = new ArrayList();
        mExecutingLists = new Hashtable();
    }

    /**
     * Sets the value of field 'deploymntId'.
     *
     * @param asaid the value of field 'deploymentId'.
     */
    public void setDeploymentId(java.lang.String asaid)
    {
        this.mDeploymentId = asaid;
    }

    /**
     * Returns the value of field 'deploymentId'.
     *
     * @return the value of field 'deploymentId'.
     */
    public java.lang.String getDeploymentId()
    {
        return this.mDeploymentId;
    }

    /**
     * Sets the endpoint name.
     *
     * @param endpoint endpoint.
     */
    public void setEndpointName(String endpoint)
    {
        mEndpointName = endpoint;
    }

    /**
     * gets the endpoint name.
     *
     * @return endpoint name.
     */
    public String getEndpointName()
    {
        return mEndpointName;
    }

    /**
     * Retrieves the executing state of instances.
     *
     * @return boolean true if has executing instances
     */
    public synchronized boolean isExecuting()
    {
        try
        {
            Iterator iter = mExecutingLists.keySet().iterator();

            while (iter.hasNext())
            {
                String id = (String) iter.next();
                Integer i = (Integer) mExecutingLists.get(id);

                if (i.intValue() != ServicelistState.COMPLETED)
                {
                    if (i.intValue() != ServicelistState.ERROR)
                    {
                        return true;
                    }
                }
            }
        }
        catch (Throwable e)
        {
            return true;
        }

        return false;
    }

    /**
     * Setter for property mInterfaceName.
     *
     * @param mInterfaceName New value of property mInterfaceName.
     */
    public void setInterfaceName(java.lang.String mInterfaceName)
    {
        this.mInterfaceName = mInterfaceName;
    }

    /**
     * Getter for property mInterfaceName.
     *
     * @return Value of property mInterfaceName.
     */
    public java.lang.String getInterfaceName()
    {
        return mInterfaceName;
    }

    /**
     * Setter for property mInterfaceNamespace.
     *
     * @param mInterfaceNamespace New value of property mInterfaceNamespace.
     */
    public void setInterfaceNamespace(java.lang.String mInterfaceNamespace)
    {
        this.mInterfaceNamespace = mInterfaceNamespace;
    }

    /**
     * Getter for property mInterfaceNamespace.
     *
     * @return Value of property mInterfaceNamespace.
     */
    public java.lang.String getInterfaceNamespace()
    {
        return mInterfaceNamespace;
    }

    /**
     * Sets the value of field 'listdescription'.
     *
     * @param listdescription the value of field 'listdescription'.
     */
    public void setListdescription(java.lang.String listdescription)
    {
        this.mListdescription = listdescription;
    }

    /**
     * Returns the value of field 'listdescription'.
     *
     * @return the value of field 'listdescription'.
     */
    public java.lang.String getListdescription()
    {
        return this.mListdescription;
    }

    /**
     * Sets the MEP.
     *
     * @param mep exchange pattern
     */
    public void setMep(String mep)
    {
        mMep = mep;
    }

    /**
     * Gets the MEP.
     *
     * @return mep
     */
    public String getMep()
    {
        return mMep;
    }

    /**
     * Method set Servicenamespace.
     *
     * @param namespace Namespace URI
     */
    public void setNamespace(String namespace)
    {
        this.mNamespace = namespace;
    }

    /**
     * Method get Servicenamespace.
     *
     * @return namespace Namespace URI
     */
    public String getNamespace()
    {
        return mNamespace;
    }

    /**
     * Sets the value of field 'operation'.
     *
     * @param operation the value of field 'operation'.
     */
    public void setOperation(java.lang.String operation)
    {
        this.mOperation = operation;
    }

    /**
     * Returns the value of field 'operation'.
     *
     * @return the value of field 'operation'.
     */
    public java.lang.String getOperation()
    {
        return this.mOperation;
    }

    /**
     * Sets the value of field 'operation'.
     *
     * @param operationnamespace the value of field 'operation'.
     */
    public void setOperationNamespace(java.lang.String operationnamespace)
    {
        this.mOperationNamespace = operationnamespace;
    }

    /**
     * Returns the value of field 'operation'.
     *
     * @return the value of field 'operation'.
     */
    public java.lang.String getOperationNamespace()
    {
        return this.mOperationNamespace;
    }

    /**
     * Method setService.
     *
     * @param serviceArray array of services
     */
    public void setService(
        com.sun.jbi.engine.sequencing.servicelist.ServiceBean [] serviceArray)
    {
        mServicelist.clear();

        for (int i = 0; i < serviceArray.length; i++)
        {
            mServicelist.add(serviceArray[i]);
        }
    }

    /**
     * Method setService.
     *
     * @param index index for the service
     * @param vService the service bean
     *
     * @throws java.lang.IndexOutOfBoundsException excpetion
     * @throws IndexOutOfBoundsException exception
     */
    public void setService(
        int index,
        com.sun.jbi.engine.sequencing.servicelist.ServiceBean vService)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > mServicelist.size()))
        {
            throw new IndexOutOfBoundsException();
        }

        mServicelist.set(index, vService);
    }

    /**
     * Method getService.
     *
     * @return ServiceBean object.
     */
    public com.sun.jbi.engine.sequencing.servicelist.ServiceBean [] getService()
    {
        int size = mServicelist.size();
        com.sun.jbi.engine.sequencing.servicelist.ServiceBean [] mArray =
            new com.sun.jbi.engine.sequencing.servicelist.ServiceBean[size];

        for (int index = 0; index < size; index++)
        {
            mArray[index] =
                (com.sun.jbi.engine.sequencing.servicelist.ServiceBean) mServicelist
                .get(index);
        }

        return mArray;
    }

    /**
     * Method getService.
     *
     * @param index the index of service in the list
     *
     * @return ServiceBean
     *
     * @throws java.lang.IndexOutOfBoundsException exception
     * @throws IndexOutOfBoundsException exception
     */
    public com.sun.jbi.engine.sequencing.servicelist.ServiceBean getService(
        int index)
        throws java.lang.IndexOutOfBoundsException
    {
        //-- check bounds for index
        if ((index < 0) || (index > mServicelist.size()))
        {
            throw new IndexOutOfBoundsException();
        }

        return (com.sun.jbi.engine.sequencing.servicelist.ServiceBean) mServicelist
        .get(index);
    }

    /**
     * Method getServiceCount.
     *
     * @return service count
     */
    public int getServiceCount()
    {
        return mServicelist.size();
    }

    /**
     * Sets the service reference.
     *
     * @param ref service reference
     */
    public void setServiceReference(ServiceEndpoint ref)
    {
        mServiceReference = ref;
    }

    /**
     * Gets the service reference.
     *
     * @return service reference
     */
    public ServiceEndpoint getServiceReference()
    {
        return mServiceReference;
    }

    /**
     * Sets the value of field 'servicename'.
     *
     * @param servicename the value of field 'servicename'.
     */
    public void setServicename(java.lang.String servicename)
    {
        this.mServicename = servicename;
    }

    /**
     * Returns the value of field 'servicename'.
     *
     * @return the value of field 'servicename'.
     */
    public java.lang.String getServicename()
    {
        return this.mServicename;
    }

    /**
     * Method addService.
     *
     * @param vService adds service
     *
     * @throws java.lang.IndexOutOfBoundsException exception.
     */
    public void addService(
        com.sun.jbi.engine.sequencing.servicelist.ServiceBean vService)
        throws java.lang.IndexOutOfBoundsException
    {
        mServicelist.add(vService);
    }

    /**
     * Method addService.
     *
     * @param index index for service
     * @param vService service
     *
     * @throws java.lang.IndexOutOfBoundsException exception
     */
    public void addService(
        int index,
        com.sun.jbi.engine.sequencing.servicelist.ServiceBean vService)
        throws java.lang.IndexOutOfBoundsException
    {
        mServicelist.add(index, vService);
    }

    /**
     * Method clearService.
     */
    public void clearService()
    {
        mServicelist.clear();
    }

    /**
     * Method removeService.
     *
     * @param vService Service name
     *
     * @return true if removed
     */
    public boolean removeService(
        com.sun.jbi.engine.sequencing.servicelist.ServiceBean vService)
    {
        boolean removed = mServicelist.remove(vService);

        return removed;
    }

    /**
     * Sets the executing state.
     *
     * @param id true denotes atleast one execuitng insatnce
     * @param state state
     */
    public synchronized void updateState(
        String id,
        int state)
    {
        try
        {
            mExecutingLists.put(id, new Integer(state));
        }
        catch (Throwable e)
        {
            ;
        }
    }
}
