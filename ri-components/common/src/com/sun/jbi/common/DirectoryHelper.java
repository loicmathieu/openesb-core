/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)DirectoryHelper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common;

import java.io.File;

import java.util.logging.Logger;

/**
 * DOCUMENT ME!
 *
 * @author Sun Microsystems, Inc.
 */
public class DirectoryHelper
{
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    private static Logger sLog = Logger.getLogger("com.sun.jbi.binding.jms.util");

    /**
     * Deletes a directory recursively.
     *
     * @param directoryOrFile directory or file that has to be deleted.
     */
    public static void deleteRecursively(File directoryOrFile)
    {
        if (directoryOrFile == null)
        {
            return;
        }

        File directory = directoryOrFile;

        try
        {
            if (directory.isFile())
            {
                directory.delete();

                return;
            }

            File [] fileList = directory.listFiles();

            if (fileList == null)
            {
                return;
            }

            if (fileList.length == 0)
            {
                directory.delete();

                return;
            }
            else
            {
                for (int i = 0; i < fileList.length; i++)
                {
                    if (fileList[i].isFile())
                    {
                        fileList[i].delete();
                    }

                    if (fileList[i].isDirectory())
                    {
                        deleteRecursively(fileList[i]);
                    }
                }

                directory.delete();

                return;
            }
        }
        catch (Exception e)
        {
            sLog.info(directory.getAbsolutePath() + " " + e.getMessage());
        }

        return;
    }
}
