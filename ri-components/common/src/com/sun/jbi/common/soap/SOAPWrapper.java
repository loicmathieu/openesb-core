/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SOAPWrapper.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.common.soap;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.soap.SOAPMessage;


/**
 * This object provides a wrapper for SOAP Messages and also contains status information.
 * The wrapper allows clients to attach properties to it.
 *
 * @author Sun Microsystems, Inc.
 */
public class SOAPWrapper
{
    /**
     * A place holder to hold additional name-value pairs.
     */
    private Map mMap;

    /**
     * Contains handle to the soap message.
     */
    private SOAPMessage mMessage;

    /**
     * Request Status.
     */
    private int mStatus;

    /**
     * Internal handle to the service URL.
     */
    private String mServiceURL;

    /**
     * Creates a new instance of SOAPWrapper.
     *
     * @param soapMessage - soap message
     */
    public SOAPWrapper(SOAPMessage soapMessage)
    {
        mMessage = soapMessage;
        mMap = new HashMap();
    }

    /**
     * Sets status.
     *
     * @param status request status
     */
    public void setStatus(int status)
    {
        mStatus = status;
    }

    /**
     * Gets status.
     *
     * @return status information
     */
    public int getStatus()
    {
        return mStatus;
    }

    /**
     * Gets Service URL.
     *
     * @return service URL
     */
    public String getServiceURL()
    {
        return mServiceURL;
    }

    /**
     * Sets Service URL.
     *
     * @param serviceURL service url.
     */
    public void setServiceURL(String serviceURL)
    {
        mServiceURL = serviceURL;
    }

    /**
     * Gets the soap message.
     *
     * @return soap message instance
     */
    public SOAPMessage getMessage()
    {
        return mMessage;
    }

    /**
     * Sets a property to the SOAP Wrapper.
     *
     * @param propertyName property name
     * @param value property value
     */
    public void setValue(String propertyName, Object value)
    {
        mMap.put(propertyName, value);
    }

    /**
     * Sets a property to the SOAP Wrapper.
     *
     * @param propertyName property name
     *
     * @return property value
     */
    public Object getValue(String propertyName)
    {
        return mMap.get(propertyName);
    }

    /**
     * Get the property list.
     *
     * @return a list of property names.
     */
    public Iterator getProperties()
    {
        return mMap.keySet().iterator();
    }
}
