#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)te00005.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "te00005.ksh- undeploy, stop, shutdown and uninstall transformation engine tests"

. ./regress_defs.ksh


# undeploy

$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_Service_Assembly undeploy-service-assembly

$JBI_ANT -Djbi.service.assembly.name=SunTransformationEngine_CSV_Assembly undeploy-service-assembly

$JBI_ANT -Djbi.service.assembly.name=9ae51feb-0fc3-4ff6-wsdl-deployment13 undeploy-service-assembly

$JBI_ANT -Djbi.service.assembly.name=9ae51feb-0fc3-4ff6-wsdl11-deployment undeploy-service-assembly

echo "Undeploy completed"

# Stop Transformation Engine
$JBI_ANT -Djbi.component.name=SunTransformationEngine stop-component

echo "Stopped TE"

# Shutdown
$JBI_ANT -Djbi.component.name=SunTransformationEngine shut-down-component

echo "Shutdown completed"

# Uninstall
$JBI_ANT -Djbi.component.name=SunTransformationEngine uninstall-component

# Uninstall WSDL Shared Library
#comment out uninstall of WSDLSL as other tests may expect this system component
#$JBI_ANT -Djbi.shared.library.name=SunWSDLSharedLibrary uninstall-shared-library

echo "UnInstall completed"

# Stop app server
#$AS8BASE/bin/asadmin stop-domain $JBI_DOMAIN_NAME

echo "All tests completed" 
