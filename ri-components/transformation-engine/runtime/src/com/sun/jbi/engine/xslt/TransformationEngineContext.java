/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)TransformationEngineContext.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.util.Hashtable;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.parsers.DocumentBuilderFactory;

import javax.jbi.component.ComponentContext;
import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.JBIException;

import javax.wsdl.Definition;

/**
 * Context object to store Transformation engine specific informaiton.
 *
 * @author Sun Microsystems, Inc.
 */
public final class TransformationEngineContext
{
    /**
     * This object
     */
    private static TransformationEngineContext sTEContext;

    /**
     * ComponentContext object
     */
    private ComponentContext mContext;

    /**
     *    
     */
    private Logger mLogger;

    /**
     *
     */
    private Hashtable mDescriptorCache;

    /**
     * Hashtable holding WSDL Definitions for deployment
     */
    private Hashtable mDefCache;
    
    /**
     * Creates a new Transformation engine context object.
     */
    private TransformationEngineContext()
    {
	mDescriptorCache = new Hashtable();
        mDefCache = new Hashtable();
    }

    /**
     * Gets the delivery channel.
     *
     * @return delivery channel
     */
    public DeliveryChannel getChannel()
    {
        DeliveryChannel chnl = null;

        if (mContext != null)
        {
            try
            {
                chnl = mContext.getDeliveryChannel();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return chnl;
    }

    /**
     * Sets the Component context.
     *
     * @param ctx component context.
     */
    public void setContext(ComponentContext ctx)
    {
        mContext = ctx;
    }

    /**
     * DOCUMENT ME!
     *
     * @return Transformation engine context
     */
    public ComponentContext getContext()
    {
        return mContext;
    }

    /**
     * Used to grab a reference of this object.
     *
     * @return an initialized TransformationEngineContext reference
     */
    public static synchronized TransformationEngineContext getInstance()
    {
        if (sTEContext == null)
        {
            synchronized (TransformationEngineContext.class)
            {
                if (sTEContext == null)
                {
                    sTEContext = new TransformationEngineContext();
                }
            }
        }

        return sTEContext;
    }

    /**
     * This method returns a Logger object obtained from the ComponentFramework
     * Currenly the suffix passed is ignored and default component name is used
     * used to create a logger.
     *
     * @param suffix Suffix to be used for this logger.
     *
     * @return Logger Logger object.
     */
    public Logger getLogger(String suffix)
    {
        try
        {
            //Passing empty string for suffix will use component name as suffix.
            mLogger = mContext.getLogger("", null);
        }
        catch (JBIException e)
        {
            e.printStackTrace();
        }        
        return mLogger;
    }

    /**
     * Adds an endpoint document.
     *
     * @param epname endpoint name.
     * @param df WSDL document.
     */
    public void addEndpointDoc(
                    String epname,
                    Document df)
    {
        if ((epname != null) && (df != null))
        {
           try
           {
              mDescriptorCache.put(epname, df);
           }
           catch (Exception e)
           {
              e.printStackTrace();
           }
        }
    }
    
    /**
     * Adds an endpoint document.
     *
     * @param epname endpoint name.
     * @param filename wsdl file.
     */
    public void addEndpointDoc(
        String key,
        String filename)
    {
        if ((key != null) && (filename != null))
        {
            try
            {
               org.w3c.dom.Document d =
                    DocumentBuilderFactory.newInstance().
                            newDocumentBuilder().parse(filename);
                mDescriptorCache.put(key, d);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

/**
 *
 */
public void clearCache()
{
    mDescriptorCache = new Hashtable();
}

/**
 * 
 *
 * @param ef ServiceEndpoint object
 *
 * @return Document Document Fragment describing endpoint.
 */
public Document getServiceDescription(ServiceEndpoint ef)
{
    try
    {
        Document df =
            (Document) mDescriptorCache.get(ef.getServiceName()
                                                      .toString()
                + ef.getEndpointName());
        /*org.w3c.dom.Document d =
            DocumentBuilderFactory.newInstance().newDocumentBuilder()
                                  .newDocument();
        Node n = d.importNode(df, true);
         d.appendChild(n);
         */

        return df;
    }
    catch (Exception e)
    {
        return null;
    }
}

    public Definition getEndpointDef(ServiceEndpoint epRef)
    {
        try
        {
            Definition epDef = (Definition) mDefCache.get(epRef.getServiceName().toString()
                                                + epRef.getEndpointName());
            
            return epDef;
        }
        catch(Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }
    public void addEndpointDef(String key, Definition epDef)
    {
        if( (key!=null) &&(epDef!=null))
        {
            try
            {
                mDefCache.put(key, epDef);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
