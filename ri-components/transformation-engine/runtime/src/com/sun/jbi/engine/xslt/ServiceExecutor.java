/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ServiceExecutor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.engine.xslt;

import com.sun.jbi.engine.xslt.framework.Command;
import com.sun.jbi.engine.xslt.util.ConfigData;
import com.sun.jbi.engine.xslt.util.Service;
import com.sun.jbi.engine.xslt.util.ServiceManager;
import com.sun.jbi.engine.xslt.util.StringTranslator;
import com.sun.jbi.engine.xslt.TransformationEngineContext;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import java.net.URI;

import java.util.Random;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.logging.Logger;

import javax.jbi.messaging.DeliveryChannel;
import javax.jbi.messaging.InOut;
import javax.jbi.messaging.MessageExchange;
import javax.jbi.messaging.MessagingException;
import javax.jbi.messaging.NormalizedMessage;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import javax.wsdl.Definition;
import com.sun.jbi.wsdl11wrapper.WrapperParser;
import com.sun.jbi.wsdl11wrapper.HelperFactory;
import com.sun.jbi.wsdl11wrapper.util.WrapperUtil;

import com.sun.jbi.wsdl11wrapper.WrapperBuilder;
import com.sun.jbi.wsdl11wrapper.Wsdl11WrapperHelper;
import com.sun.jbi.wsdl11wrapper.Wsdl11WrapperHelperException;
import org.w3c.dom.Node;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.jbi.servicedesc.ServiceEndpoint;
import java.io.StringWriter;
import javax.wsdl.Port;
import javax.wsdl.Part;
import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.extensions.*;
import javax.wsdl.xml.*;

import javax.xml.namespace.QName;

import java.util.List;
import java.util.Iterator;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
/**
 * This command contains the request response implementation.
 *
 * @author Sun Microsystems Inc.
 */
public class ServiceExecutor
    implements Command, TEResources
{
    /**
     * In Out MEP.
     */
    public static final String IN_OUT = "http://www.w3.org/2004/08/wsdl/in-out";

    /**
     * Out In MEP.
     */
    public static final String OUT_IN = "http://www.w3.org/2004/08/wsdl/out-in";

    /**
     *    
     */
    private static Random sRand = new Random();

    /**
     * Delivery Channel
     */
    private DeliveryChannel mChannel = null;

    /**
     * This is logger instance used for logging information.
     */
    private Logger mLogger = null;

    /**
     * MessageExchange containg the input request
     */
    private MessageExchange mExchange = null;

    /**
     *    
     */
    private ServiceManager mServiceManager;

    /**
     *    
     */
    private StringBuffer mOutSuffix = null;

    /**
     *    
     */
    private StringTranslator mTranslator = null;
    
    /**
     * Construtor for creating RequestResponseCommand Object.
     *
     * @param channel for replying back to sender
     * @param exchange received by the sender.
     */
    public ServiceExecutor(
        DeliveryChannel channel,
        MessageExchange exchange)
    {
        mTranslator =
            new StringTranslator("com.sun.jbi.engine.xslt",
                this.getClass().getClassLoader());

        // Do any command specific init.
        mLogger = TransformationEngineContext.getInstance().getLogger("");
        mChannel = channel;
        mExchange = exchange;
        mServiceManager = ServiceManager.getInstance();
        mOutSuffix = new StringBuffer();
        mOutSuffix.append("</outputxml>");
    }

    /**
     * Converts the non-xml data to xml.
     *
     * @param service Service
     * @param outputfile Output file
     * @param inputFile Input file
     *
     * @return boolean
     */
    public boolean convertToXml(
        Service service,
        String outputfile,
        String inputFile)
    {
        boolean bSUCCESS = true;
        boolean bFAILURE = false;

        try
        {
            String tokens = service.getColumnSeparator();
            File file = new File(outputfile);
            FileOutputStream fos = new FileOutputStream(file);
            PrintWriter pw = new PrintWriter(fos);
            String tag1 =
                "<" + mTranslator.getString(TEResources.INPUTXML) + ">";
            String tag2 = "<" + mTranslator.getString(TEResources.RECORD) + ">";
            String tag3 =
                "<" + "/" + mTranslator.getString(TEResources.RECORD) + ">";

            // This has to change after WSDL support, hard coded INPUTXML for now
            String tag4 =
                "<" + "/" + mTranslator.getString(TEResources.INPUTXML) + ">";
            BufferedReader reader =
                new BufferedReader(new InputStreamReader(
                        new FileInputStream(inputFile)));
            String line = null;
            Vector colnames = new Vector();
            boolean usefirstrow = false;
            boolean colrow = false;
            int index = -1;
            int lastIndex = -1;
            int i = 0;
            boolean starttagfound = false;

            while ((line = reader.readLine()) != null)
            {
                i++;
                index = -1;
                lastIndex = -1;

                if (line.equals(""))
                {
                    continue;
                }

                if (!starttagfound)
                {
                    index = line.indexOf(tag1);

                    if (index >= 0)
                    {
                        index = index + tag3.length();
                        starttagfound = true;
                        i = 0;
                        mLogger.finer("Start tag found");
                        pw.println(tag1);
                    }
                    else
                    {
                        mLogger.finer("looping till first tag is found");

                        continue;
                    }
                }

                lastIndex = line.indexOf(tag4);

                if (lastIndex >= 0)
                {
                    mLogger.finer("Endtag is found" + lastIndex);
                }

                if ((lastIndex >= 0) && (index >= 0))
                {
                    mLogger.finer("Start and End tag found");
                    line = line.substring(index, lastIndex);
                }
                else if (index >= 0)
                {
                    line = line.substring(index, line.length());
                }
                else if (lastIndex >= 0)
                {
                    line = line.substring(0, lastIndex);
                }
                else
                {
                    mLogger.finer(" Read line has no start or end tag");
                }

                StringTokenizer st = new StringTokenizer(line, tokens, false);
                int j = 0;

                if ((i == 0) && service.getFirstRowColHeaders())
                {
                    mLogger.finer("Parsing first row for column names");

                    while (st.hasMoreTokens())
                    {
                        colnames.add(st.nextToken());
                    }

                    usefirstrow = true;
                    i++;

                    continue;
                }
                else if ((i == 0) && (service.getColumnNames() != null))
                {
                    mLogger.finer(
                        "Getting column names read from service.xml");
                    colnames = service.getColumnNames();
                    colrow = true;
                    i++;
                }
                else
                {
                    ;
                }

                pw.println("  ");
                pw.print(tag2);

                if (usefirstrow || colrow)
                {
                    while (st.hasMoreTokens())
                    {
                        try
                        {
                            String colname = (String) colnames.elementAt(j);

                            if (colname != null)
                            {
                                pw.println("    ");
                                pw.print("<");
                                pw.print(colname);
                                pw.print(">");
                                pw.print(st.nextToken());
                                pw.print("</");
                                pw.print(colname);
                                pw.print(">");
                            }
                        }
                        catch (ArrayIndexOutOfBoundsException ex)
                        {
                            mLogger.severe(mTranslator.getString(
                                    TEResources.INVALID_INPUT));

                            break;
                        }

                        j++;
                    }
                }
                else
                {
                    while (st.hasMoreTokens())
                    {
                        j++;

                        String columnTag = "<" + "column_" + j + ">";
                        String columnTagEnd = "</" + "column_" + j + ">";
                        pw.println("    ");
                        pw.print(columnTag);
                        pw.print(st.nextToken());
                        pw.print(columnTagEnd);
                    }
                }

                pw.println("  ");
                pw.print(tag3);
            }

            if (lastIndex >= 0)
            {
                pw.println("");
                pw.print(tag4);
            }

            pw.flush();
            pw.close();
            reader.close();

            return bSUCCESS;
        }
        catch (Exception ex)
        {
            ex.printStackTrace();

            return bFAILURE;
        }
    }

    /**
     * This is called by workmanager in a free thread. It extracts the symbol
     * from the normalized message, processes it and generates response or
     * fault depending upon the input message.
     */
    public void execute()
    {
        mLogger.finer(
            "##### SERVICEEXECUTERCOMMAND:EXECUTE() INVOKED BY WORKMANAGER* ########");

        URI pattern = mExchange.getPattern();
        mLogger.finer(mTranslator.getString(TEResources.PATTERN_EXCG_ID)
            + mExchange.getExchangeId() + mTranslator.getString(TEResources.IS)
            + pattern);

        String temp = (pattern.toString()).trim();

        if (mExchange instanceof InOut)
        {
            if ((mExchange.getStatus() != null)
                    && ((mExchange.getStatus()).toString()).trim()
                            .equalsIgnoreCase(ConfigData.ACTIVE))
            {
                mLogger.finer(mTranslator.getString(TEResources.RCVD_IN_OUT)
                    + mExchange.getExchangeId());
                mLogger.finer(mTranslator.getString(
                        TEResources.RCVD_IN_OUT_MSG_EX) + mExchange.getStatus());
                processInOut((InOut) mExchange);
            }
            else
            {
                // Received response
                mLogger.finer(mTranslator.getString(TEResources.RCVD_IN_OUT)
                    + mExchange.getExchangeId()
                    + mTranslator.getString(TEResources.STATUS_IS)
                    + mExchange.getStatus()
                    + mTranslator.getString(TEResources.IGNORING_STATUS));
            }
        }

        else
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.RECEIVED_INVALID_PATTERN)
                + mExchange.getExchangeId());

            return;
        }

        mLogger.info(mTranslator.getString(
                TEResources.SERVICEEXECUTOR_EXECUTE_END));
    }

    /**
     * DOCUMENT ME!
     *
     * @param inout NOT YET DOCUMENTED
     */
    public void processInOut(InOut inout)
    {
        // This must be a request
        try
        {
            String serviceName = inout.getEndpoint().getServiceName().toString();
            serviceName = serviceName.trim();
            mLogger.finer(mTranslator.getString(TEResources.GETTING_SVC_BEAN)
                + serviceName);

            if (serviceName == null)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.SERVICENAME_NULL));
                mLogger.finer(mTranslator.getString(
                        TEResources.NO_PROCESSING_DONE));
                inout.setError(new Exception(mTranslator.getString(
                            TEResources.SERVICE_PROCESSING_NOT_AVAILABLE)));

                try
                {
                    mLogger.info("DEBUG : Service name " + inout.getEndpoint().getServiceName()
                        + " Endpint " + inout.getEndpoint().getEndpointName()
                        + " Service endpoint "
                        + inout.getEndpoint().getServiceName() + " Operation "
                        + inout.getOperation());
                    mChannel.send(inout);
                }
                catch (MessagingException ex)
                {
                    ex.printStackTrace();
                }

                return;
            }

            // Get the Service from ServicerRegistry 
            Service service = mServiceManager.getService(serviceName);

            if (service == null)
            {
                // Error case should happen
                mLogger.severe(mTranslator.getString(
                        TEResources.INVALID_REGISTRY_STATE));
                mLogger.finer(mTranslator.getString(
                        TEResources.NO_PROCESSING_DONE));
                inout.setError(new Exception(mTranslator.getString(
                            TEResources.SERVICE_NULL_NOT_PROCESSED)));

                try
                {
                    mLogger.info("DEBUG : Service name " + inout.getEndpoint().getServiceName()
                        + " Endpint " + inout.getEndpoint().getEndpointName()
                        + " Service endpoint "
                        + inout.getEndpoint().getServiceName() + " Operation "
                        + inout.getOperation());
                    mChannel.send(inout);
                }
                catch (MessagingException ex)
                {
                    ex.printStackTrace();
                }

                return;
            }

            if (service.getStatus().equalsIgnoreCase(ConfigData.START))
            {
                mLogger.finer(mTranslator.getString(TEResources.GOT_SERVICE)
                    + service);

                try
                {
                    //Should check return value of process (if false, set error
                    // on MessageExchange
                    process(serviceName, inout);
                }
                catch (Exception ex)
                {
                    inout.setError(new Exception(ex.getMessage()));
                }
            }
            else
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.SERVICE_NOT_STARTED));
                inout.setError(new Exception(mTranslator.getString(
                            TEResources.SERVICE_NOT_RUNNING)));
            }
        }
        catch (Exception me)
        {
            mLogger.severe(mTranslator.getString(
                    TEResources.COULD_NOT_SEND_MESSAGE) + inout.getExchangeId()
                + mTranslator.getString(TEResources.BECAUSE) + me.getMessage());
            inout.setError(new Exception(me.getMessage()));
        }
        finally
        {
            mLogger.finer(" ******** processInOut Finally called ********");
            mLogger.finer(mTranslator.getString(
                    TEResources.SENDING_MESSAGE_EXCHANGE));

            try
            {
                mLogger.finer("DEBUG : Service name " + inout.getEndpoint().getServiceName()
                    + " Endpint " + inout.getEndpoint().getEndpointName()
                    + " Service endpoint "
                    + inout.getEndpoint().getServiceName() + " Operation "
                    + inout.getOperation());
                mChannel.send(inout);
            }
            catch (MessagingException ex)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.PROBLEM_SENDING_MESSAGE));
                ex.printStackTrace();
            }
        }
    }

    /**
     * This method processes the Message Exchange
     *
     * @param serviceName Service name for which the transformation request has come
     * @param inout Message Exchange object
     *
     * @return boolean true/false
     *
     * @throws javax.jbi.messaging.MessagingException
     * @throws Exception exception
     */
    private boolean process(
        String serviceName,
        InOut inout)
        throws javax.jbi.messaging.MessagingException, Exception
    {
        mLogger.info(mTranslator.getString(TEResources.SERVICEEXECUTOR_START));

        boolean status = false;
        String mparentdir = "";
        String mfilename = "";
        String minputfile = "";
        String mOtherFile = "";

        Source source = null;
        
        Service service = mServiceManager.getService(serviceName);
        NormalizedMessage inputMsg = inout.getInMessage();
        NormalizedMessage outputMsg = inout.createMessage();

        // determine if the message corresponds to the service deployment type
        // If the service was deployed using WSDL 1.1, reject messages sent
        // in other form and vice versa.
        // In case the service was deployed using XML, TE should be able to handle
        // message in either format (WSDL 1.1 or 2.0)
        if( service.getDeploymentType().equalsIgnoreCase("wsdl_11"))
        {
            // do WSDL 1.1 message exchange stuff
            try
            {
                source = handleWSDL11Message(inout, service);
            }
            catch(Wsdl11WrapperHelperException wEx)
            {
                throw new Exception(wEx.getMessage() + " Incorrect incoming Message.");
            }
        }
        else
        {
            // this is either XML deployment or WSDL 2.0
            source = inputMsg.getContent();
        }
        
        if(source != null)
        {
            mLogger.info("[BEGIN_SOURCE_DUMP]");
            //dump(source);
            mLogger.info("[END_SOURCE_DUMP]");
            mLogger.fine("Recevied source of type: "+ source.getClass().getName());
        }
        else
        {
            mLogger.severe("No Source found for Transformation. ERROR.");
            return false;
        }

        String xsltfile = service.getStyleSheet();
        if (xsltfile == null)
        {
            mLogger.severe(mTranslator.getString(TEResources.STYLESHEET_NOT_FOUND));
            inout.setError(new Exception(mTranslator.getString(
                        TEResources.STYLESHEET_NOT_FOUND)));

            return false;
        }
        else
        {
            mLogger.finer(mTranslator.getString(TEResources.STYLESHEET_FOUND)
                + xsltfile);
        }

        //tmp code
        long mnum = 0;

        synchronized (this)
        {
            mnum = Math.abs(sRand.nextLong());
        }

        try
        {
            mparentdir = new File(xsltfile).getParent();
        }
        catch (Exception ex)
        {
            mLogger.severe(mTranslator.getString(TEResources.PARENT_DIR_FAILED));
            // In case of problem, try using INSTALL_ROOT for the component
            mparentdir = "";
        }

        mLogger.finer(mTranslator.getString(TEResources.PARENT_DIR) + mparentdir
            + "*");

        // Writting orignal request to file 
        mfilename = mparentdir + File.separator + new Long(mnum).toString();
        minputfile = mfilename + mTranslator.getString(TEResources.INPUT_FILE);

        // Code for converting non-xml data to xml
        String type = mTranslator.getString(TEResources.NON_XML);

        if ((type != null) && service.getType().equalsIgnoreCase(type.trim()))
        {
            //Covert non-xml to xml
            //Read from StreamSource and store into minputfile
            storeSourceToFile((StreamSource)source, minputfile);
            mOtherFile = mfilename + mTranslator.getString(TEResources.OTHER);
            convertToXml(service, mOtherFile, minputfile);
            minputfile = mOtherFile;
        }
        else
        {
            // Service deployment is XML
            try
            {
                TransformationImpl cprocessor =
                    (TransformationImpl) TransformationProcessorFactory.getImpl();
                TransformerFactory tFactory = cprocessor.getTransformerFactory();

                //Identical Copy Transformation. No Transformation Done actually. 
                //Transformer writes
                //the input XML as it is to the Result (file here)
                Transformer copier = tFactory.newTransformer();
                copier.transform(source, new StreamResult(minputfile));
                cprocessor.putTransformerFactory(tFactory);
            }
            catch (Exception ex)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.WRITTING_INPUT_REQUEST_FAILED));
                inout.setError(new Exception(mTranslator.getString(
                            TEResources.WRITTING_INPUT_REQUEST_FAILED)));
                ex.printStackTrace();

                return false;
            }
        }

        source = new StreamSource(new FileInputStream(minputfile));
        
        if (source instanceof StreamSource)
        {
            mLogger.finer(mTranslator.getString(TEResources.REQ_STREAMSOURCE));

            String filename = "";
            String inputfile = "";
            String outfile = "";

            long num = 0;

            synchronized (this)
            {
                num = Math.abs(sRand.nextLong());
            }

            // Writting orignal request to file 
            filename = mparentdir + File.separator + new Long(num).toString();
            outfile = filename + mTranslator.getString(TEResources.OUTPUT_FILE);
            mLogger.finer(mTranslator.getString(TEResources.GEN_OUT_FILE)
                + outfile);

            StreamResult result = new StreamResult(new File(outfile));
            try
            {
                TransformationImpl processor =
                    (TransformationImpl) TransformationProcessorFactory.getImpl();
                processor.setTemplateName(serviceName);
                status = processor.doTransform(source, xsltfile, result);
                
                // If the message was received in WSDL 1.1 format, wrap the
                // transformed output in JBI Wrapper format.
                if(service.getDeploymentType().equalsIgnoreCase("wsdl_11"))
                {
                    Document msgDoc = prepareWSDL11Response(inout, service, outfile);
                    outputMsg.setContent(new DOMSource(msgDoc.getDocumentElement()));
                }
                else
                {
                    // this is either XML deployment or WSDL 2.0
                    outputMsg.setContent(new StreamSource(outfile));
                }
                
                mLogger.info(mTranslator.getString(
                        TEResources.TRANSFORMATION_SUCCESS));
            }
            catch (Exception ex)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.TRANSFORMATION_FAILED));
                ex.printStackTrace();
                inout.setError(new Exception(ex.getMessage()));

                return false;
            }
        }
        else
        {

            long num = 0;

            synchronized (this)
            {
                num = Math.abs(sRand.nextLong());
            }

            // Writting orignal request to file 
            String filename =
                mparentdir + File.separator + new Long(num).toString();
            String outfile =
                filename + mTranslator.getString(TEResources.OUTPUT_FILE);
            String outfile2 =
                filename + mTranslator.getString(TEResources.OUT_FILE_2);

            StreamResult result = new StreamResult(outfile);

            try
            {
                TransformationImpl processor =
                    (TransformationImpl) TransformationProcessorFactory.getImpl();
                processor.setTemplateName(serviceName);
                status = processor.doTransform(source, xsltfile, result);

                BufferedReader reader =
                    new BufferedReader(new FileReader(outfile));
                PrintWriter pw =
                    new PrintWriter(new BufferedWriter(new FileWriter(outfile2)));
                String readobj = null;

                int itr = 1;

                while ((readobj = reader.readLine()) != null)
                {
                    if (itr == 1)
                    {
                        String prolog = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
                        int length = prolog.length();
                        int index = readobj.indexOf(prolog);

                        if (index >= 0)
                        {
                            String str =
                                readobj.substring(index + length,
                                    readobj.length());
                            pw.println(str);
                        }
                        else
                        {
                            pw.println(readobj);
                        }
                    }
                    else
                    {
                        pw.println(readobj);
                    }

                    itr++;
                }

                pw.flush();
                pw.close();

                reader.close();
                outputMsg.setContent(new StreamSource(outfile2));
                mLogger.info(mTranslator.getString(
                        TEResources.TRANSFORMATION_SUCCESS_OTHER));
            }
            catch (Exception ex)
            {
                mLogger.severe(mTranslator.getString(
                        TEResources.TRANSFORMATION_FAILED));
                ex.printStackTrace();
                inout.setError(new Exception(ex.getMessage()));

                return false;
            }
        }

        // Set the Out Message on Message Exchange
        inout.setOutMessage(outputMsg);
        mLogger.info(mTranslator.getString(TEResources.SERVICEEXECUTOR_END));

        return status;
    }

    private void storeSourceToFile(StreamSource src, String fileName)
    {
        try
        {
            if(src.getSystemId() != null)
            {
                mLogger.finer("System ID = "+src.getSystemId());
                URI fileUri = new URI(src.getSystemId());
                BufferedReader re = new BufferedReader(new FileReader(new File(fileUri)));
                String line;
                BufferedWriter bw = new BufferedWriter(new FileWriter(new File(fileName)));
                while( (line = re.readLine()) != null)
                {
                   mLogger.finer(line);
                   bw.write(line);
                   bw.newLine();

                }
                bw.flush();
                bw.close();
            }
            if(src.getInputStream() != null)
            {
                mLogger.finer("Inputstream is not null.");
            }

            if(src.getReader() != null)
            {
                mLogger.finer("Reader is not null.");
                BufferedReader reader = new BufferedReader(src.getReader());
                String line;
                while( (line = reader.readLine()) != null )
                {
                  mLogger.finer(line);
                }
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    /**
     * This method prepares the Wrapped response. This is done only if the 
     * deployment(service) is of type "wsdl_11".
     *
     * @param source Source object to print
     */
    private void dump(Source source) throws Exception
    {
         TransformerFactory  tf      = TransformerFactory.newInstance();
         Transformer         t       = tf.newTransformer();
         StringWriter sw = new StringWriter();
         StreamResult        stdOut  = new StreamResult(System.out);

         t.transform(source, new StreamResult(sw));
         mLogger.info(sw.toString());
    }
    
    /**
     * This method handles the Wrapped response. This is done only if the 
     * deployment(service) is of type "wsdl_11".  Using the WSDL 11 Wrapper Helper
     * object, the incoming message is "unwrapped" to get the input data.
     *
     * @param inout MessageExchange Object
     * @param service Service object 
     *
     * @return Source 
     */
    private Source handleWSDL11Message(InOut inout, Service service) 
        throws Wsdl11WrapperHelperException
    {
        DOMSource src = null;
        
        //Creatre the Helper class that will do wrapping/unwrapping
        Wsdl11WrapperHelper mWrapHelper = createWrapperHelper(service);
       
        NormalizedMessage inMsg = inout.getInMessage();
        ServiceEndpoint epref = service.getReference();
        Document unwrappedDoc = null;

        unwrappedDoc = mWrapHelper.unwrapMessage(inMsg.getContent(), 
                                epref.getServiceName(), epref.getEndpointName(), 
                                inout.getOperation().getLocalPart(), true);
        
        if(unwrappedDoc != null)
        {
            src = new DOMSource(unwrappedDoc);
        }

        return src;
    }
    
    /**
     * This method creates WSDL11 wrapper helper object for given Service.
     *
     * @param service Service object 
     *
     * @return Wsdl11WrapperHelper
     */
    private Wsdl11WrapperHelper createWrapperHelper(Service serv)
    {
        // Get the WSDL Definition for this service/endpoint
        ServiceEndpoint epref = serv.getReference();
        Definition servDef = TransformationEngineContext.getInstance().getEndpointDef(epref);
        return new Wsdl11WrapperHelper(servDef);
        
    }

    /**
     * This method prepares the Wrapped response. This is done only if the 
     * deployment(service) is of type "wsdl_11".
     *
     * @param inout MessageExchange Object
     * @param service Service object 
     * @param outfile Complete path to the transformed output
     *
     * @return
     */
    private Document prepareWSDL11Response(InOut inout, Service service, String outfile)
    {
        Document wrappedDoc = null;
        
        try
        {
            DocumentBuilderFactory docFac = DocumentBuilderFactory.newInstance();
            docFac.setNamespaceAware(true);
            DocumentBuilder docBuilder = docFac.newDocumentBuilder();
            Document transDoc = docBuilder.parse(new File(outfile));
        
            Wsdl11WrapperHelper mWrapHelper = createWrapperHelper(service);
            QName servName = service.getReference().getServiceName();
            String epName = service.getReference().getEndpointName();
            String operation = inout.getOperation().getLocalPart();

            wrappedDoc = mWrapHelper.wrapMessage(transDoc, servName, epName, operation, false);
            
            
            mLogger.info("[BEGIN_WRAPPED_MESSAGE]");
            if(wrappedDoc != null)
                dump(new DOMSource(wrappedDoc.getDocumentElement()));
            mLogger.info("[END_WRAPPED_MESSAGE]");
        
        }
        catch(Wsdl11WrapperHelperException wEx)
        {
            wEx.printStackTrace();
            inout.setError(wEx);
            return null;
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
            inout.setError(ex);
            return null;
        }
        return wrappedDoc;
    }
}
