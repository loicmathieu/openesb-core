<?xml version="1.0"?>
<!--
 # BEGIN_HEADER - DO NOT EDIT
 #
 # The contents of this file are subject to the terms
 # of the Common Development and Distribution License
 # (the "License").  You may not use this file except
 # in compliance with the License.
 #
 # You can obtain a copy of the license at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # See the License for the specific language governing
 # permissions and limitations under the License.
 #
 # When distributing Covered Code, include this CDDL
 # HEADER in each file and include the License file at
 # https://open-esb.dev.java.net/public/CDDLv1.0.html.
 # If applicable add the following below this CDDL HEADER,
 # with the fields enclosed by brackets "[]" replaced with
 # your own identifying information: Portions Copyright
 # [year] [name of copyright owner]
-->

<!--
 # @(#)update_jbi_domain.ant
 # Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 #
 # END_HEADER - DO NOT EDIT
-->

<!--
# This ant script is run by the adminService whenever the JBI lifecycle is started up.
#
# Current functions:
# 1. create local environment files for sh, csh, and ant that can be sourced by external
#    scripts.  These files are copied from $JBI_HOME/templates.
# 2. garbage collect any directories that we cannot delete at runtime.  This is used
#    to clean up leftover files when a component is un-installed.  Any directories in
#    $JBI_DOMAIN_ROOT/jbi/* that have a file named .DELETE_ME will be deleted.
#
#  24-Jun-2004 (russt)
#       Initial revision
#
#  05-Oct-2004 (russt)
#       Added start-up garbage collection.
#
-->

<project name="aproject" default="admin_startup_tasks">

<target name="admin_startup_tasks" depends="garbage_collection,update_domain_root,cleanup" >
    <echo level="info" message="###### JBI update_jbi_domain: Start up tasks complete." />
</target>

<target name="garbage_collection" depends="init,delete_marked_directories" >
    <echo level="info" message="###### JBI update_jbi_domain: Garbage collection complete." />
</target>

<target name="init" depends="basic_init,init_filter" >
</target>

<target name="basic_init" >
    <property name="templates" value="${com.sun.jbi.home}/templates" />
    <property name="templates" value="${com.sun.jbi.home}/templates" />

    <property name="jbi_domain_config" value="${com.sun.jbi.domain.root}/jbi/config" />
    <mkdir dir="${jbi_domain_config}" />

    <property name="jbi_domain_bin" value="${com.sun.jbi.domain.root}/jbi/bin" />
    <mkdir dir="${jbi_domain_bin}" />

    <property name="delete_me_fn" value=".DELETE_ME" />

    <tempfile prefix="jbiadmin" suffix=".props" property="tmpA" />
    <tempfile prefix="jbiadmin" suffix=".props" property="tmpB" />
    
    <tempfile prefix="esbadmin" suffix=".props" property="tmpA" />
    <tempfile prefix="esbadmin" suffix=".props" property="tmpB" />

</target>

<target name="update_domain_root" depends="init" >
    <!-- create shell env files, property files -->
    <copy todir="${jbi_domain_config}" overwrite="true" filtering="true" verbose="true" failonerror="false" >
        <fileset dir="${templates}/scripts">
            <include name="jbienv.*"/>
        </fileset>
    </copy>
    <copy todir="${jbi_domain_bin}" overwrite="true" filtering="true" verbose="true" failonerror="false" >
        <fileset dir="${templates}/scripts">
            <include name="jbiadmin"/>
            <include name="jbiadmin.*"/>
        </fileset>
    </copy>
    <copy todir="${jbi_domain_bin}" overwrite="true" filtering="true" verbose="true" failonerror="false" >
        <fileset dir="${templates}/scripts">
            <include name="esbadmin"/>
            <include name="esbadmin.*"/>
        </fileset>
    </copy>
    

    <chmod dir="${jbi_domain_bin}" perm="0555" includes="" />

    <echo level="info" message="###### JBI update_jbi_domain: template substitution complete.." />
</target>

<target name="init_filter" >
    <!-- initialize filter-set: -->
    <!-- dump current properties to a file, and use those to filter -->
    <echoproperties destfile="${tmpA}"/>

    <!-- double-up backslashes so ant and shell will read and display them correctly -->
    <replace file="${tmpA}" token="\" value="\\" />

    <!-- convert '\\:' back to '\:' so we don't screw up DOS and path-separator semantics: -->
    <replace file="${tmpA}" token="\\:" value="\:" />

    <!-- create global filter rules: -->
    <filter filtersfile="${tmpA}"/>
</target>

<target name="dump_properties" >
    <!-- this target is provided for debugging: -->
    <echo message="###### APPSERVER PROPERTIES" />
    <echoproperties prefix="com.sun.aas"/>
    <echoproperties prefix="domain"/>
    <echo message="###### JBI PROPERTIES" />
    <echoproperties prefix="com.sun.jbi"/>
</target>

<target name="delete_marked_directories" depends="get_marked_dirs,set_delete_list,do_deletes" >
</target>
    
<target name="get_marked_dirs" >
    <property name="delete_root" value="${com.sun.jbi.domain.root}/jbi" />

    <fileset dir="${delete_root}" id="delete-me-tmp-fileset">
        <include name="**/${delete_me_fn}" />
    </fileset>
    <property name="delete_me_file_listA" refid="delete-me-tmp-fileset" />

    <!-- set property only if our delete-me set is not empty: -->
    <condition property="delete-me-fileset-not-empty" value="true">
        <not>
            <equals arg1="${delete_me_file_listA}" arg2=""/>
        </not>
    </condition>

</target>

<target name="set_delete_list" if="delete-me-fileset-not-empty" >
    <!-- create temp file to hold include patterns for final delete fileset: -->
    <echo file="${tmpB}" >${delete_me_file_listA}
</echo>

    <!-- replace semicolon with newlines: -->
    <replace file="${tmpB}" token=";" >
        <replacevalue>
</replacevalue>
    </replace>
    <!-- replace DELETE_ME file name with /**, as we are going to delete everything underneath: -->
    <replace file="${tmpB}" token="/${delete_me_fn}" value="/**"/>
    <replace file="${tmpB}" token="\${delete_me_fn}" value="\**"/>

    <fileset dir="${delete_root}" id="delete-me-fileset" includesfile="${tmpB}" />
    <property name="delete_me_file_listB" refid="delete-me-fileset" />
    <echo level="info" message="AFTER:  ${delete_me_file_listB}" />
</target>

<target name="do_deletes" if="delete-me-fileset-not-empty" >
    <delete verbose="yes" includeEmptyDirs="true" >
        <fileset refid="delete-me-fileset"/>
    </delete>
</target>

<target name="cleanup" >
    <delete file="${tmpA}" quiet="true" failonerror="false" />
    <delete file="${tmpB}" quiet="true" failonerror="false" />
</target>

</project>
