#
# BEGIN_HEADER - DO NOT EDIT
#
# The contents of this file are subject to the terms
# of the Common Development and Distribution License
# (the "License").  You may not use this file except
# in compliance with the License.
#
# You can obtain a copy of the license at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# See the License for the specific language governing
# permissions and limitations under the License.
#
# When distributing Covered Code, include this CDDL
# HEADER in each file and include the License file at
# https://open-esb.dev.java.net/public/CDDLv1.0.html.
# If applicable add the following below this CDDL HEADER,
# with the fields enclosed by brackets "[]" replaced with
# your own identifying information: Portions Copyright
# [year] [name of copyright owner]
#

#
# @(#)jms00002.ksh
# Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
#
# END_HEADER - DO NOT EDIT
#
echo "jmsbinding00002.ksh- Installation test"
. ./regress_defs.ksh

# install WSDL shared namespace for jms binding

rm -f $JV_SVC_TEST_CLASSES/config/jms_config.xml

sed -e "s#SRCROOT#$JV_SVC_TEST_CLASSES#g" $JV_SVC_TEST_CLASSES/config/jms_config_tokens.xml > $JV_SVC_TEST_CLASSES/config/jms_config.xml

cp $JBI_KIT_JB $JV_SVC_TEST_CLASSES/config/jmsbinding.jar

cd $JV_SVC_TEST_CLASSES/config
jar -uf jmsbinding.jar ./jms_config.xml

$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.install.file=$JBI_KIT_WSDL install-shared-library

 # install the jms binding

$AS8BASE/bin/asant -emacs -q -f $JBI_ADMIN_XML -Djbi.port=$JBI_ADMIN_PORT -Djbi.install.file=$JV_SVC_TEST_CLASSES/config/jmsbinding.jar install-component

echo Completed 
