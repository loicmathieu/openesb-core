/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)SimpleRegistryImpl.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;
import com.sun.jbi.binding.jms.OperationBean;
import com.sun.jbi.binding.jms.JMSConstants;
import com.sun.jbi.binding.jms.config.ConfigConstants;
import com.sun.jbi.binding.jms.util.UtilBase;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;

import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.xml.namespace.QName;

/**
 * This is a very simple registry implementation.
 *
 * @author Sun Microsystems. Inc.
 */
public class SimpleRegistryImpl
    extends com.sun.jbi.binding.jms.util.UtilBase
    implements RegistryImplementor, JMSBindingResources
{
    /**
     * Seperator used to seperate fields in the serialized registry file.
     */
    private static final String FIELD_SEPERATOR = ", ";

    /**
     * Seperator used to seperate fields in a registry key.
     */
    private static final char KEY_FIELD_SEPERATOR = '#';

    /**
     * Character used to comment lines in the serialized registry file.
     */
    private static final char COMMENT_CHAR = '#';

    /**
     * Default number of tokens present a for given registry fiel entry. The
     * default entry identifies a service with no namespace.
     */
    private static final int DEFAULT_TOKEN_COUNT = 10;

    /**
     * Null string that will be stored in registry if a value is not present.
     */
    private static final String NULL_VALUE = "NULL";

    /**
     * Logger.
     */
    private Logger mLogger;

    /**
     * Internal handle to the inbound registry.
     */
    private Map mRegistry;

    /**
     * Internal handle to the deployment properties.
     */
    private Properties mDeploymentProperties;

    /**
     * Deployment file.
     */
    private String mDeploymentFile;

    /**
     * Translator.
     */
    private StringTranslator mStringTranslator;

    /**
     * Creates a new instance of SimpleRegistryImpl.
     */
    public SimpleRegistryImpl()
    {
        mRegistry = new HashMap();
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator =
            JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Returns all keys.
     *
     * @return iterator to keys.
     */
    public Iterator getAllKeys()
    {
        return mRegistry.keySet().iterator();
    }

    /**
     * Gets all values.
     *
     * @return collection of values.
     */
    public Collection getAllValues()
    {
        Collection allvalues = new ArrayList();

        Object value = null;

        //Get the Inbound registry endpoints and unregister them
        Iterator keyIter = getAllKeys();

        while (keyIter.hasNext())
        {
            //Get the registry key
            String regkey = (String) keyIter.next();

            //Get the endpoint for this key
            value = getValue(regkey);

            if (value == null)
            {
                continue;
            }

            allvalues.add(value);
        }

        //end while
        return allvalues;
    }

    /**
     * Returns the keys.
     *
     * @return iterator to keys.
     */
    public Iterator getKeys()
    {
        Iterator keyIter = null;
        keyIter = mRegistry.keySet().iterator();

        return keyIter;
    }

    /**
     * Get the directory which has the schema definitions for the endpoint.
     *
     * @return the Directory which has the schema directory.
     */
    public File getSchemaDir()
    {
        StringBuffer schemaPath = new StringBuffer();

        // Construct deployment file name
        schemaPath.append(JMSBindingContext.getInstance().getContext()
                                           .getInstallRoot());
        schemaPath.append(File.separator);
        schemaPath.append(mDeploymentProperties.getProperty(
                JMSConstants.REGISTRY_FILE_LOCATION));

        // Read the file contents
        return new java.io.File(schemaPath.toString());
    }

    /**
     * Returns value from registry.
     *
     * @param registryKey key.
     *
     * @return object corresponding to key.
     */
    public Object getValue(String registryKey)
    {
        Object value;
        value = mRegistry.get(registryKey);

        return value;
    }

    /**
     * Clears the registry.
     */
    public void clearRegistry()
    {
        mRegistry.clear();
    }

    /**
     * Checks if registry contains the key.
     *
     * @param registryKey key.
     *
     * @return true if registry contains key.
     */
    public boolean containsKey(String registryKey)
    {
        boolean isDeployed = false;
        isDeployed = mRegistry.containsKey(registryKey);

        return isDeployed;
    }

    /**
     * Initializes the registry.
     *
     * @param props properties.
     *
     * @throws JBIException jbi exception.
     */
    public void init(java.util.Properties props)
        throws JBIException
    {
        mDeploymentProperties = props;

        String deploymentFileName =
            props.getProperty(JMSConstants.REGISTRY_FILE_NAME);
        String deploymentFileLocation =
            props.getProperty(JMSConstants.REGISTRY_FILE_LOCATION);
        StringBuffer deploymentFileBuffer = null;

        try
        {
            deploymentFileBuffer = new StringBuffer();

            // Construct deployment file name
            deploymentFileBuffer.append(JMSBindingContext.getInstance()
                                                         .getContext()
                                                         .getInstallRoot());
            deploymentFileBuffer.append(File.separator);
            deploymentFileBuffer.append(deploymentFileLocation);
            deploymentFileBuffer.append(File.separator);
            deploymentFileBuffer.append(deploymentFileName);
            mLogger.fine(mStringTranslator.getString(JMS_REG_FILE_NAME,
                    deploymentFileBuffer.toString()));
            mDeploymentFile = deploymentFileBuffer.toString();
            BufferedReader reader =
                new BufferedReader(new FileReader(mDeploymentFile.toString()));
            String serializedDeployment = reader.readLine();
            while (serializedDeployment != null)
            {
                if (serializedDeployment.charAt(0) != COMMENT_CHAR)
                {
                    EndpointBean endpoint =
                        deserializeEndpoint(serializedDeployment);
                    registerKey(endpoint.getUniqueName(), endpoint);
                }

                serializedDeployment = reader.readLine();
            }
        }
        catch (FileNotFoundException fe)
        {
            mLogger.severe(mStringTranslator.getString(JMS_REG_FILE_NOT_FOUND,
                    deploymentFileBuffer.toString()));
        }
        catch (Exception exception)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_ERROR_LOADING_DEPLOYMENTS, exception.getMessage()));
            exception.printStackTrace();
            throw new JBIException(exception.getMessage());
        }
    }

    /**
     * Registers the key.
     * @param key key.
     * @param value value.
     *
     * @return key.
     */
    public synchronized String registerKey(
        String key,
        Object value)
    {
        mRegistry.put(key, value);

        return key;
    }

    /**
     * Removes deployment from the deployment registry.
     *
     * @param registryKey key.
     */
    public synchronized void removeKey(String registryKey)
    {
        mRegistry.remove(registryKey);
    }

    /**
     * Following method serializes/persists All Endpoints/Deployments in the
     * registry.dat file (default for now).
     *
     * @throws JBIException serialization exception.
     */
    public void serialize()
        throws JBIException
    {
        try
        {
            BufferedWriter writer =
                new BufferedWriter(new FileWriter(getRegFileName()));
            addRegFileHeader(writer);

            // Loop through all the deployments
            Collection alldepl = getAllValues();

            Iterator iter = alldepl.iterator();
            EndpointBean ep = null;

            while (iter.hasNext())
            {
                //Get the Endpoint Object
                ep = (EndpointBean) iter.next();

                if (ep != null)
                {
                    // tokenize each endpoint
                    String tokEp = tokenizeEndpoint(ep);

                    //Write this to registry file
                    writer.write(tokEp);
                    writer.newLine();
                    writer.flush();
                }
            }
            writer.close();
        }
        catch (IOException ioe)
        {
            mLogger.severe(mStringTranslator.getString(
                    JMS_ERROR_LOADING_DEPLOYMENTS, ioe.getMessage()));
            throw new JBIException(ioe.getMessage());
        }
        catch (Exception ex)
        {
            throw new JBIException(ex.getMessage());
        }
    }

    /**
     * Get the registry file name.
     *
     * @return registry file name.
     */
    private String getRegFileName()
    {
        return mDeploymentFile;
    }

    /**
     * Sets the role.
     *
     * @param eb endpoint bean.
     */
    private void setRole(EndpointBean eb)
    {
        String role = (String) eb.getValue(ConfigConstants.ENDPOINT_TYPE);

        if (role == null)
        {
            eb.setRole(ConfigConstants.CONSUMER);
        }
        else
        {
            if (role.trim().equalsIgnoreCase(ConfigConstants.PROVIDER_STRING))
            {
                eb.setRole(ConfigConstants.PROVIDER);
            }
            else
            {
                eb.setRole(ConfigConstants.CONSUMER);
            }
        }
    }

    /**
     * Following method adds the header lines for the registry file This header
     * explains the registry format.
     *
     * @param writer file to write.
     *
     * @throws IOException exception while writing inot file.
     */
    private void addRegFileHeader(BufferedWriter writer)
        throws IOException
    {
        writer.write("#Format of the registry file");
        writer.newLine();
        writer.write(
            "#deploymentId, service_namespace, service_name, endpoint_name, endpoint_type,"
            + "destination_name, destination_style, durability, connection-factory-name,"
            + "connection-user-name, connection-password, message_selector, "
            + "time_to_live, reply_to, ( operation_namespace, "
            + "operation_name, mepname, input_type, output_type) ");
        writer.newLine();
    }

    /**
     * Decorates.
     *
     * @param s string to decorate.
     *
     * @return decorated string.
     */
    private String decorate(String s)
    {
        if (s == null)
        {
            return NULL_VALUE;
        }
        else if (s.trim().equals(""))
        {
            return NULL_VALUE;
        }

        return s;
    }

    /**
     * This is a specific function which has to be customized according to the
     * object that is serialized and deserialized.
     *
     * @param serializedString string to deserialize.
     *
     * @return endpoint bean.
     */
    private EndpointBean deserializeEndpoint(String serializedString)
    {
        EndpointBean endpoint = null;
        StringTokenizer deploymentTokenizer =
            new StringTokenizer(serializedString, FIELD_SEPERATOR);

        QName serviceQName = null;
        mLogger.fine("Tokens : " + deploymentTokenizer.countTokens());

        String deploymentId = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Deployment id: " + deploymentId);

        String serviceNamespace =
            evaluate(deploymentTokenizer.nextToken().trim());
        mLogger.fine("Service Name Space : " + serviceNamespace);

        String serviceName = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Service name : " + serviceName);

        serviceQName = new QName(serviceNamespace, serviceName);

        String endpointName = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Endpoint : " + endpointName);

        String endpointType = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Endpoint type : " + endpointType);

        String destname = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Destination Name :" + destname);

        String deststyle = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Destination style : " + deststyle);

        String confactory = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Connection factory : " + confactory);

        String conuser = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Connection user name : " + conuser);

        String conpassword = deploymentTokenizer.nextToken().trim();
        mLogger.fine("Connection user password: " + conpassword);

        String durability = evaluate(deploymentTokenizer.nextToken().trim());
        mLogger.fine("Durability = " + durability);

        String msgsel = evaluate(deploymentTokenizer.nextToken().trim());
        mLogger.fine("Selector = " + msgsel);

        String timetolive = evaluate(deploymentTokenizer.nextToken().trim());
        mLogger.fine("time to live = " + timetolive);

        String replyto = evaluate(deploymentTokenizer.nextToken().trim());
        mLogger.fine("Reply to = " + replyto);

        endpoint = new EndpointBean();
        endpoint.setValue(ConfigConstants.SERVICE_NAMESPACE, serviceNamespace);
        endpoint.setValue(ConfigConstants.SERVICENAME, serviceName);
        endpoint.setValue(ConfigConstants.ENDPOINTNAME, endpointName);
        endpoint.setValue(ConfigConstants.ENDPOINT_TYPE, endpointType);
        endpoint.setValue(ConfigConstants.DESTINATION_NAME, destname);
        endpoint.setValue(ConfigConstants.DESTINATION_STYLE, deststyle);
        endpoint.setValue(ConfigConstants.CONNECTION_FACTORY, confactory);
        endpoint.setValue(ConfigConstants.DURABILITY, durability);
        endpoint.setValue(ConfigConstants.MESSAGE_SELECTOR, msgsel);
        endpoint.setValue(ConfigConstants.TIME_TO_LIVE, timetolive);
        endpoint.setValue(ConfigConstants.REPLY_TO, replyto);
        endpoint.setValue(ConfigConstants.CONNECTION_USER_ID, conuser);
        endpoint.setValue(ConfigConstants.CONNECTION_PASSWORD, conpassword);
        endpoint.setDeploymentId(deploymentId);

        setRole(endpoint);

        while (deploymentTokenizer.hasMoreTokens())
        {
            String operationNamespace = deploymentTokenizer.nextToken().trim();
            mLogger.fine("Operation name space = " + operationNamespace);

            String operationName = deploymentTokenizer.nextToken().trim();
            mLogger.fine("Operation name = " + operationName);

            String pattern = deploymentTokenizer.nextToken().trim();
            mLogger.fine("pattern = " + pattern);

            String input = evaluate(deploymentTokenizer.nextToken().trim());
            mLogger.fine("input = " + input);

            String output = evaluate(deploymentTokenizer.nextToken().trim());
            mLogger.fine("output = " + output);

            endpoint.addOperation(operationNamespace, operationName, pattern,
                input, output);
        }

        return endpoint;
    }

    /**
     * Evaluator.
     *
     * @param e string to trim.
     *
     * @return string.
     */
    private String evaluate(String e)
    {
        if (e.equals(NULL_VALUE))
        {
            return "";
        }

        return e;
    }

    /**
     * Following method tokenizes an endpoint (Comma seperated).
     *
     * @param endpt - Endpoint that needs to be tokenzied
     *
     * @return tokenized string.
     */
    private String tokenizeEndpoint(EndpointBean endpt)
    {
        //Construct the string that needs to be persisted. 
        //This string is consists of comma (FIELD_SEPARATOR)
        //seperated attributes for an endpoint (service name, endpoint name etc.)
        StringBuffer epBuff = new StringBuffer();

        epBuff.append(endpt.getDeploymentId());
        epBuff.append(FIELD_SEPERATOR);

        //Service namespace        
        epBuff.append(decorate(endpt.getValue(
                    ConfigConstants.SERVICE_NAMESPACE.toString())));
        epBuff.append(FIELD_SEPERATOR);

        //Service Name
        epBuff.append(endpt.getValue(ConfigConstants.SERVICENAME).toString());
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Name
        epBuff.append(endpt.getValue(ConfigConstants.ENDPOINTNAME));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint role
        epBuff.append(endpt.getValue(ConfigConstants.ENDPOINT_TYPE));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(endpt.getValue(ConfigConstants.DESTINATION_NAME));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(endpt.getValue(ConfigConstants.DESTINATION_STYLE));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(endpt.getValue(ConfigConstants.CONNECTION_FACTORY));
        epBuff.append(FIELD_SEPERATOR);

        epBuff.append(endpt.getValue(ConfigConstants.CONNECTION_USER_ID));
        epBuff.append(FIELD_SEPERATOR);

        epBuff.append(endpt.getValue(ConfigConstants.CONNECTION_PASSWORD));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(decorate(endpt.getValue(ConfigConstants.DURABILITY)));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(decorate(endpt.getValue(ConfigConstants.MESSAGE_SELECTOR)));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(decorate(endpt.getValue(ConfigConstants.TIME_TO_LIVE)));
        epBuff.append(FIELD_SEPERATOR);

        //Endpoint Address
        epBuff.append(decorate(endpt.getValue(ConfigConstants.REPLY_TO)));

        //  epBuff.append(FIELD_SEPERATOR);
        Iterator opsIter = endpt.getAllOperations().iterator();

        while (opsIter.hasNext())
        {
            OperationBean op = (OperationBean) opsIter.next();

            epBuff.append(FIELD_SEPERATOR);

            //Operation Namespace
            epBuff.append(op.getNamespace());
            epBuff.append(FIELD_SEPERATOR);

            //Operation Name
            epBuff.append(op.getName());
            epBuff.append(FIELD_SEPERATOR);

            epBuff.append(op.getMep());
            epBuff.append(FIELD_SEPERATOR);

            epBuff.append(decorate(op.getInputType()));
            epBuff.append(FIELD_SEPERATOR);

            epBuff.append(decorate(op.getOutputType()));
            epBuff.append(FIELD_SEPERATOR);
        }

        //end while
        return epBuff.toString();
    }
}
