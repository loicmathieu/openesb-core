/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)MessageStatus.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.monitoring;

/**
 * Typesafe enumeration containing status values for a message .
 *
 * @author Sun Microsystems, Inc.
 */
public final class MessageStatus
{
    /**
     * Indicates that an ME has not been processed to completion.
     */
    public static final MessageStatus PROCESSING =
        new MessageStatus("PROCESSING");

    /**
     * Indicates that an ME has terminated abnormally within the JBI
     * environment.
     */
    public static final MessageStatus INQUEUE = new MessageStatus("INQUEUE");

    /**
     * Indicates that an ME has been processed to completion.
     */
    public static final MessageStatus ERROR = new MessageStatus("ERROR");

    /**
     *
     */
    /**
     *
     */

    /**
     *    
     */
    public static final MessageStatus COMPLETED =
        new MessageStatus("COMPLETED");

    /**
     * String representation of status.
     */
    private String mStatus;

    /**
     * Private constructor used to create a new MessageStatus type.
     *
     * @param status value
     */
    private MessageStatus(String status)
    {
        mStatus = status;
    }

    /**
     * Equality test.
     *
     * @param status
     *
     * @return boolean result of test.
     */
    public boolean equals(MessageStatus status)
    {
        return (mStatus.equals(status.mStatus));
    }

    /**
     * Returns string value of enumerated type.
     *
     * @return String representation of status value.
     */
    public String toString()
    {
        return mStatus;
    }

    /**
     * Returns instance of MessageStatus that corresponds to given string.
     *
     * @param status
     *
     * @return MessageStatus
     *
     * @throws java.lang.IllegalArgumentException if string can't be translated
     */
    public static MessageStatus valueOf(String status)
    {
        MessageStatus instance;

        //
        //  Convert symbolic name to object reference.
        //
        if (status.equals(COMPLETED.toString()))
        {
            instance = COMPLETED;
        }
        else if (status.equals(ERROR.toString()))
        {
            instance = ERROR;
        }
        else if (status.equals(INQUEUE.toString()))
        {
            instance = INQUEUE;
        }
        else if (status.equals(PROCESSING.toString()))
        {
            instance = PROCESSING;
        }
        else
        {
            //
            //  Someone has a problem.
            //
            throw new java.lang.IllegalArgumentException(status);
        }

        return (instance);
    }
}
