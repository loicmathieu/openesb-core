/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSMessageProcessor.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.mq;

import com.sun.jbi.StringTranslator;

import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.EndpointStatus;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResources;
import com.sun.jbi.binding.jms.MessageRegistry;

import com.sun.jbi.binding.jms.deploy.EndpointRegistry;

import com.sun.jbi.binding.jms.handler.InboundMessageHandler;
import com.sun.jbi.binding.jms.handler.MessageHandler;
import com.sun.jbi.binding.jms.handler.OutboundMessageHandler;

import java.util.logging.Logger;

import javax.jbi.messaging.MessageExchange;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 * Processes messages from the MQ.
 *
 * @author Sun Microsystems Inc.
 */
public class JMSMessageProcessor implements JMSBindingResources
{
    /**
     *    Logger.
     */
    private Logger mLogger;    
    /**
     * Translator.
     */    
    private StringTranslator mStringTranslator;

    /**
     * Creates a new instance of JMSMessageProcessor.
     */
    public JMSMessageProcessor()
    {
        mLogger = JMSBindingContext.getInstance().getLogger();
        mStringTranslator = JMSBindingContext.getInstance().getStringTranslator();
    }

    /**
     * Process.
     *
     * @param name name.
     * @param msg JMS msg.
     *
     * @return message handler.
     */
    public MessageHandler process(
        String name,
        Message msg)
    {
        MessageHandler handler = null;

        if (msg == null)
        {
            return null;
        }
        mLogger.finest("JMS Message processer started prcessing message");

        if (name == null)
        {
            //this should be some out response 
            handler = new OutboundMessageHandler();

            MessageRegistry mreg =
                JMSBindingContext.getInstance().getMessageRegistry();
            String cor = null;

            try
            {
                cor = msg.getJMSCorrelationID();
            }
            catch (JMSException je)
            {
                ;
            }

            if (cor == null)
            {
                mLogger.severe(mStringTranslator.getString(JMS_NO_CORRELATION_ID,   
                        msg.toString()));

                return null;
            }

            MessageExchange me = (MessageExchange) mreg.getObject(cor);
            handler.setNMSMessage(me);
            handler.setJMSMessage(msg);
        }
        else
        {
            handler = new InboundMessageHandler();

            EndpointRegistry ereg =
                JMSBindingContext.getInstance().getRegistry();
            EndpointBean eb = null;

            try
            {
                eb = (EndpointBean) ereg.getEndpoint(name);
            }
            catch (Exception e)
            {
                ;
            }

            if (eb == null)
            {
                mLogger.severe(mStringTranslator.getString(JMS_NO_ENDPOINT));

                return null;
            }
            if (!eb.getStatus().equals(EndpointStatus.STARTED))
            {
                mLogger.severe(mStringTranslator.getString
                                    (JMS_ENDPOINT_NOT_STARTED));

                return null;
            }
            mLogger.info("JMS MSG RCVD " + msg);
            handler.setJMSMessage(msg);
            handler.setBean(eb);
        }

        return handler;
    }
}
