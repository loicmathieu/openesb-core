/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)JMSBindingComponent.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms;

import com.sun.jbi.binding.jms.deploy.JMSBindingSUManager;

import javax.jbi.component.Component;
import javax.jbi.servicedesc.ServiceEndpoint;

import org.w3c.dom.DocumentFragment;

/**
 * This class implements the JBI component contract.
 *
 * @author Sun Microsystems Inc.
 */
public class JMSBindingComponent
    implements Component
{
    /**
     * Lifecycle implementation for JMS binding.
     */
    private JMSBindingLifeCycle mLifeCycle;

    /**
     * Resolver implementation.
     */
    private JMSBindingResolver mResolver;
    /**
     * SU Manager implementation.
     */
    private JMSBindingSUManager mSUManager;

    /**
     * Creates a new instance of JMSBindingComponent.
     */
    public JMSBindingComponent()
    {
        mLifeCycle = new JMSBindingLifeCycle();
        mSUManager = new JMSBindingSUManager();
        mResolver = new JMSBindingResolver();
        mLifeCycle.setSUManager(mSUManager);
        mLifeCycle.setResolver(mResolver);
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * provider of the service indicated by the given exchange, can actually
     * perform the operation desired. 
     *
     * @param endpoint endpoint reference.
     * @param exchange message exchange.
     * @return true if this provider component can perform the the given
     *         exchange with the described consumer
     */
    public boolean isExchangeWithConsumerOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }

    /**
     * This method is called by JBI to check if this component, in the role of
     * consumer of the service indicated by the given exchange, can actually
     * interact with the the provider completely. 
     *
     * @param endpoint endpoint reference.
     * @param exchange message exchange.
     * @return true if this consurer component can interact with the described
     *         provider to perform the given exchange.
     */
    public boolean isExchangeWithProviderOkay(
        javax.jbi.servicedesc.ServiceEndpoint endpoint,
        javax.jbi.messaging.MessageExchange exchange)
    {
        return true;
    }

    /**
     * Component lifecyle implemenation is returned.
     *
     * @return lifecycle implementation.
     */
    public javax.jbi.component.ComponentLifeCycle getLifeCycle()
    {
        return mLifeCycle;
    }

    /**
     * Returns the description for an enpoint.
     *
     * @param serviceendpoint endpoint reference.
     *
     * @return DOM based description.
     */
    public org.w3c.dom.Document getServiceDescription(
        javax.jbi.servicedesc.ServiceEndpoint serviceendpoint)
    {
        org.w3c.dom.Document desc = null;

        try
        {
            desc = mResolver.getServiceDescription(serviceendpoint);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return desc;
    }

    /**
     * Returns the service unit manager.
     *
     * @return service unit manager implementation.
     */
    public javax.jbi.component.ServiceUnitManager getServiceUnitManager()
    {
        return mSUManager;
    }
    
    /** Resolve the endpoint reference using the given capabilities of the 
     * consumer. This is called by JBI when its trying to resove the given EPR
     *  on behalf of the component.
     *
     *  @param epr endpoint description.
     *
     *  @return ServiceEndpoint corresponding to the description.
     */
    public ServiceEndpoint resolveEndpointReference(DocumentFragment epr)
    {
        return null;
    }
}
