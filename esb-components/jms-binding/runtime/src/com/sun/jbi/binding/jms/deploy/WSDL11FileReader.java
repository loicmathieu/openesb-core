/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)WSDL11FileReader.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.deploy;

import org.w3c.dom.Document;
import org.w3c.dom.DocumentFragment;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.OperationType;
import javax.wsdl.Port;
import javax.wsdl.PortType;
import javax.wsdl.extensions.UnknownExtensibilityElement;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.JMSBindingContext;
import com.sun.jbi.binding.jms.JMSBindingResolver;
import com.sun.jbi.binding.jms.JMSBindingResources;

import com.sun.jbi.binding.jms.config.ConfigConstants;


import javax.xml.namespace.QName;


/**
 * Reads the configuration file file.xml and loads the data. into the
 * EndpointBean objects.
 *
 * @author Sun Microsystems Inc.
 */
public class WSDL11FileReader
    extends com.sun.jbi.binding.jms.util.UtilBase
    implements JMSBindingResources
{
    /**
     * Definitions document.
     */
    private Definition mDefinition;

    /**
     * Document object of the XML config file.
     */
    private Document mDoc;

    /**
     * Resolver.
     */

    private JMSBindingResolver mResolver;

    /**
     * Logger Object.
     */
    private Logger mLogger;

    /**
     * Translator.
     */

    private StringTranslator mStringTranslator;

    /**
     * The list of endpoints as configured in the config file. This list
     * contains all the encpoints and their attibutes.
     */
    private EndpointBean [] mEndpointInfoList = null;

    /**
     * The total number of end points in the config file.
     */
    private int mTotalEndpoints = 0;
    
    /**
     * WSDL file name
     */
    private String mWsdlFile;

    /**
     * Creates a new WSDL11FileReader object.
     */
    public WSDL11FileReader( JMSBindingResolver rslv)
    {
          setValid(true);
           mResolver = rslv;
           mLogger = JMSBindingContext.getInstance().getLogger();
           mStringTranslator =
               JMSBindingContext.getInstance().getStringTranslator();        
    }

    /**
     * Returns the Bean object corresponding to the endpoint.
     *
     * @return endpoint Bean object.
     */

    public EndpointBean getBean(String endpoint)
       {
           /*Search for the bean corresponding to the service and endpoint name
           */
       for (int j = 0; j < mEndpointInfoList.length; j++)
       {
           String tmp = mEndpointInfoList[j].getUniqueName();
           if (tmp.trim().equals(endpoint))
           {
               return mEndpointInfoList[j];
           }
       }
       return null;
       }
    
    /**
     * Gets the endpoint list corresponding to a config file.
     *
     * @return End point Bean list
     */

    public EndpointBean [] getEndpoint()
       {
           return mEndpointInfoList;
       }

    /**
     * Returns the total number of endopoints in the config file.
     *
     * @return int number of endpoints.
     */
    public int getEndpointCount()
    {
        return mTotalEndpoints;
    }

    /**
     * Initializes the config file and loads services.
     *
     * @param mDeployFileName Name of the config file.
     */
    public void init(String mDeployFileName)
    {
        try
        {
            mWsdlFile = mDeployFileName;
            javax.wsdl.factory.WSDLFactory mFactory = WSDLFactory.newInstance();
            javax.wsdl.xml.WSDLReader mReader = mFactory.newWSDLReader();

            // parser the deployment file - mDeployFileName (WSDL 1.1) and create 
            // definitions
            mDefinition = mReader.readWSDL(null, mDeployFileName);
        }
        catch (Exception ex)
        {
            mLogger.severe("WSDL 1.1 parse exception");
            ex.printStackTrace();
            return;
        }

        loadServicesList();
    }

    /**
     * Gets the value for an attribute.
     *
     * @param map map object.
     * @param attr attribute.
     *
     * @return value.
     */
    private String getAttrValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Sets the con factory.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setConnectionFactory(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.CONNECTION_FACTORY);
        eb.setValue(ConfigConstants.CONNECTION_FACTORY, val);
    }

    /**
     * Sets the connection password.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setConnectionPassword(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.CONNECTION_PASSWORD);
        eb.setValue(ConfigConstants.CONNECTION_PASSWORD, val);
    }

    /**
     * Sets connection user name.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setConnectionUsername(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.CONNECTION_USER_ID);
        mLogger.fine("User ID " + val);
        eb.setValue(ConfigConstants.CONNECTION_USER_ID, val);
    }

    /**
     * Sets destination name.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setDestinationName(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.DESTINATION_NAME);

        if ((val == null) || (val.trim().equals("")))
        {
            setError(mStringTranslator.getString(JMS_DESTINATION_NAME_NULL));
        }

        eb.setValue(ConfigConstants.DESTINATION_NAME, val);
    }

    /**
     * Sets the destination style.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setDestinationStyle(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.DESTINATION_STYLE);
        eb.setValue(ConfigConstants.DESTINATION_STYLE, val);

        if (val.trim().equalsIgnoreCase(ConfigConstants.QUEUE_STRING))
        {
            eb.setStyle(ConfigConstants.QUEUE);
        }
        else
        {
            eb.setStyle(ConfigConstants.TOPIC);
        }
    }

    /**
     * Sets durability.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setDurability(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.DURABILITY);

        if (val == null)
        {
            val = ConfigConstants.DEFAULT_DURABILITY;
        }

        eb.setValue(ConfigConstants.DURABILITY, val);
    }

    /**
     * Gets the node array.
     *
     * @param nodeList node list.
     * @param name element name.
     *
     * @return node array.
     */
    private Node [] getElementNodeArrayByName(
        NodeList nodeList,
        String name)
    {
        Node [] retNode = null;
        List nodeArr = new LinkedList();

        //nodeList will have one element for binding. Get all child elements for binding
        //loop thorugh them to find operation elements
        NodeList tmpNodeList = nodeList.item(0).getChildNodes();

        for (int g = 0; g < tmpNodeList.getLength(); g++)
        {
            Node n = tmpNodeList.item(g);

            if ((n != null) && (n.getNodeType() == Node.ELEMENT_NODE))
            {
                if ((n.getLocalName().equals(name)))
                {
                    nodeArr.add(n);
                }
            }
        }

        // for loop
        retNode = new Node[nodeArr.size()];

        for (int k = 0; k < nodeArr.size(); k++)
        {
            retNode[k] = (Node) nodeArr.get(k);
        }

        return retNode;
    }

    /**
     * Mehotd to determin if the binding information in the Document fragment
     * is a file binding or not.
     *
     * @param aBinding document fragment.
     *
     * @return true if JMS binding.
     */
    private boolean isJMSBinding(javax.wsdl.Binding aBinding)
    {
        boolean bFlag = false;

        java.util.List extList = aBinding.getExtensibilityElements();
        List boper = aBinding.getBindingOperations();
        if (extList != null)
        {
            for (int i = 0; i < extList.size(); i++)
            {
                if (extList.get(i) instanceof javax.wsdl.extensions.UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement sb =
                        (UnknownExtensibilityElement) extList.get(i);

                    if (sb.getElementType().getNamespaceURI().equals(ConfigConstants.JMS_NAMESPACE)
                            && sb.getElementType().getLocalPart().equals("binding"))
                    {
                        bFlag = true;
                        break;
                    }
                }
            }
        }

        return bFlag;
    }

    /**
     * Sets the message selector.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setMessageSelector(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.MESSAGE_SELECTOR);

        if (val == null)
        {
            val = "";
        }

        eb.setValue(ConfigConstants.MESSAGE_SELECTOR, val);
    }

    private void validateType(String type)
    {
        if ((type.equalsIgnoreCase("TextMessage")) ||
            (type.equalsIgnoreCase("ByteMessage")) ||
            (type.equalsIgnoreCase("StreamMessage")) ||
            (type.equalsIgnoreCase("ObjectMessage")) ||
            (type.equalsIgnoreCase("MapMessage")) ||
            (type.equalsIgnoreCase("Message")))
        {
		return;
        }
        setError("Invalid message type specified in Binding operation - Type : " + type );
	
    }
    
    
    /**
     * Sets the operation.
     *
     * @param eb endpoint bean.
     * @param b binding.
     */
    private void setOperations(
        EndpointBean eb,
        Binding b)
    {
        try
        {
            List bopers = b.getBindingOperations();
            PortType bindingif = b.getPortType();
            
            Iterator boperiter = null;

            if (bopers != null)
            {
                boperiter = bopers.iterator();
            }

            while (boperiter.hasNext())
            {
                javax.wsdl.BindingOperation boper =
                    (javax.wsdl.BindingOperation) boperiter.next();
                String bindingopername = boper.getOperation().getName();
                String input = null;
                String output = null;
                String mep = null;
               List inputext = null;
                
                if (!boper.getOperation().getStyle().equals(OperationType.NOTIFICATION))
                {
                    inputext = boper.getBindingInput().getExtensibilityElements();
                    input = getMessageType(inputext);                    
                    if ((input == null) || (input.trim().equals("")))
                    {
                        input = ConfigConstants.DEFAULT_INPUT_MESSAGE_TYPE;
                    }
                    validateType(input);
                }              

                if (!boper.getOperation().getStyle().equals(OperationType.ONE_WAY))
                {
                    List outputext =
                            boper.getBindingOutput().getExtensibilityElements();
                    output = getMessageType(outputext);
                    if ((output == null) || (output.trim().equals("")))
                    {
                        output = ConfigConstants.DEFAULT_OUTPUT_MESSAGE_TYPE;
                    }
                    validateType(output);
                }
                
                mep = resolveMep(boper.getOperation().getStyle());
                if (isValid())
                {
                    eb.addOperation("", bindingopername, mep, input, output);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            setError("Cannot set operations - " + e.getMessage());
        }
    }

    /**
     *
     *
     * @param opstyle  
     *
     * @return  
     */
    private String resolveMep(OperationType opstyle)
    {
        String mep = null;

        if (opstyle.equals(OperationType.ONE_WAY))
        {
            mep = ConfigConstants.IN_ONLY;
        }
        else if (opstyle.equals(OperationType.REQUEST_RESPONSE))
        {
            mep = ConfigConstants.IN_OUT;
        }
        else if (opstyle.equals(OperationType.NOTIFICATION))
        {
            mep = ConfigConstants.IN_ONLY;
        }
        else if (opstyle.equals(OperationType.SOLICIT_RESPONSE))
        {
            mep = ConfigConstants.IN_OUT;
        }
        else
        {
            setError("Unknown Style of operation " + opstyle);
        }

        return mep;
    }

    /**
     * DOCUMENT ME!
     *
     * @param inputext
     *
     * @return
     */
    private String getMessageType(List inputext)
    {
        String type = null;

        try
        {
            for (int i = 0; i < inputext.size(); i++)
            {
                Object obj = inputext.get(i);

                if (obj instanceof UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement jmselement =
                        (UnknownExtensibilityElement) obj;

                    if (jmselement.getElementType().getNamespaceURI().equals(ConfigConstants.JMS_NAMESPACE)
                            && jmselement.getElementType().getLocalPart()
                                             .equals("message"))
                    {
                        type =
                            getValue(jmselement.getElement().getAttributes(),
                                "type");
                    }
                }
            }
        }
        catch (Exception e)
        {
            return null;
        }

        return type;
    }

    /**
     * Sets the reply to.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setReplyTo(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.REPLY_TO);

        if (val == null)
        {
            val = "";
        }

        eb.setValue(ConfigConstants.REPLY_TO, val);
    }

    /**
     * Sets the role.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setRole(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String role = getValue(map, ConfigConstants.ENDPOINT_TYPE);

        if (role == null)
        {
            eb.setRole(ConfigConstants.CONSUMER);
            role = "consumer";
        }
        else
        {
            if (role.trim().equalsIgnoreCase(ConfigConstants.PROVIDER_STRING))
            {
                eb.setRole(ConfigConstants.PROVIDER);
            }
            else if (role.trim().equalsIgnoreCase(ConfigConstants.CONSUMER_STRING))
            {
                eb.setRole(ConfigConstants.CONSUMER);
            }
            else
            {
                setError("Endpoint should be Provider or Consumer");

                return;
            }
        }

        eb.setValue(ConfigConstants.ENDPOINT_TYPE, role);
    }

    /**
     * Sets the time to live.
     *
     * @param map map object.
     * @param eb endpoint bean.
     */
    private void setTimetoLive(
        NamedNodeMap map,
        EndpointBean eb)
    {
        String val = getValue(map, ConfigConstants.TIME_TO_LIVE);
        long l;

        try
        {
            l = Long.parseLong(val);
        }
        catch (Exception e)
        {
            setError(mStringTranslator.getString(JMS_INVALID_TIMEOUT));
            e.printStackTrace();

            return;
        }

        eb.setValue(ConfigConstants.TIME_TO_LIVE, val);
    }

    /**
     * Gets value of an attribute form the map.
     *
     * @param map map.
     * @param attr attribute.
     *
     * @return attribute value.
     */
    private String getValue(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String value = null;

        try
        {
            value = attrnode.getNodeValue();
        }
        catch (Exception e)
        {
            ;
        }

        return value;
    }

    /**
     * Loads the endpoint bean.
     *
     * @param ep endpoint reference.
     * @param eb endpoint bean.
     */
    private void loadEndpointBean(
        Port ep,
        EndpointBean eb)
    {
        List extList = ep.getExtensibilityElements();

        if (extList != null)
        {
            for (int i = 0; i < extList.size(); i++)
            {
                if (extList.get(i) instanceof javax.wsdl.extensions.UnknownExtensibilityElement)
                {
                    UnknownExtensibilityElement sb =
                        (UnknownExtensibilityElement) extList.get(i);

                    if (sb.getElementType().getNamespaceURI().equals(ConfigConstants.JMS_NAMESPACE)
                            && sb.getElementType().getLocalPart().equals("artifacts"))
                    {
                        Element epnode = sb.getElement();
                        NamedNodeMap epattributes = epnode.getAttributes();                      
                        checkRequired(epattributes, ConfigConstants.DESTINATION_NAME);
                        checkRequired(epattributes, ConfigConstants.DESTINATION_STYLE);
                        checkRequired(epattributes, ConfigConstants.CONNECTION_FACTORY);
                        if (!isValid())
                        {
                            break;
                        }
                        setRole(epattributes, eb);
                        setDestinationName(epattributes, eb);
                        setDestinationStyle(epattributes, eb);
                        setDurability(epattributes, eb);
                        setConnectionFactory(epattributes, eb);
                        setConnectionUsername(epattributes, eb);
                        setConnectionPassword(epattributes, eb);
                        setMessageSelector(epattributes, eb);
                        setTimetoLive(epattributes, eb);
                        setReplyTo(epattributes, eb);                       
                    }
                }
            }
        }

        if (!isValid())
        {
            setError("\n\tEndpoint : " + ep.getName());
        }
    }

    /**
     * Parses the config files and loads them into bean objects.
     */
    private void loadServicesList()
    {
        java.util.Map servicemap = mDefinition.getServices();
        List eplist = new LinkedList();
        Collection services = servicemap.values();

        Iterator iter = services.iterator();

        while (iter.hasNext())
        {
            try
            {
                javax.wsdl.Service service = (javax.wsdl.Service) iter.next();

                Map endpointsmap = service.getPorts();
                Collection endpoints = endpointsmap.values();

                Iterator epiter = endpoints.iterator();

                while (epiter.hasNext())
                {
                    javax.wsdl.Port ep = (javax.wsdl.Port) epiter.next();

                    javax.wsdl.Binding b = ep.getBinding();

                    if (!isJMSBinding(b))
                    {
                        continue;
                    }

                    EndpointBean eb = new EndpointBean();
                    eb.setWsdlDefinition(mDefinition);
                    eb.setDeploymentType("WSDL11");
                    loadEndpointBean(ep, eb);

                    eb.setValue(ConfigConstants.SERVICE_NAMESPACE,
                        service.getQName().getNamespaceURI());
                    eb.setValue(ConfigConstants.SERVICENAME,
                        service.getQName().getLocalPart());
                    eb.setValue(ConfigConstants.ENDPOINTNAME, ep.getName());
                    eb.setValue(ConfigConstants.INTERFACE_LOCALNAME,
                        ep.getBinding().getPortType().getQName().getLocalPart());
                    eb.setValue(ConfigConstants.INTERFACE_NAMESPACE,
                        ep.getBinding().getPortType().getQName()
                          .getNamespaceURI());
                    setOperations(eb, b);

                    if (!isValid())
                    {
                        setError(("\n\t" + "Service : "
                            + service.getQName().toString()));
                        eplist.clear();

                        return;
                    }

                    eplist.add(eb);
                    mResolver.addEndpointDoc(service.getQName().toString()
                        + ep.getName(), mWsdlFile);                     
                }
            }
            catch (Exception e)
            {
                setError(mStringTranslator.getString(
                       JMS_ENDPOINT_VALIDATION_ERROR, e.getMessage()));
                setException(e);
                e.printStackTrace();

                return;
            }
        }

        mEndpointInfoList = new EndpointBean[eplist.size()];

        for (int x = 0; x < eplist.size(); x++)
        {
            mEndpointInfoList[x] = (EndpointBean) eplist.get(x);
        }

        mTotalEndpoints = eplist.size();
    }

    /**
     * Checks if the mep is valid for the style.
     *
     * @param eb endpoint bean.
     * @param mep mep.
     *
     * @return true if valid.
     */
    private boolean operationAllowed(
        EndpointBean eb,
        String mep)
    {
        int style = eb.getStyle();
        boolean allowed = true;

        if (style == ConfigConstants.TOPIC)
        {
            if (mep.trim().equals(ConfigConstants.IN_OUT)
                    || mep.trim().equals(ConfigConstants.IN_OPTIONAL_OUT)
                    || mep.trim().equals(ConfigConstants.OUT_IN)
                    || mep.trim().equals(ConfigConstants.OUT_OPTIONAL_IN)
                    || mep.trim().equals(ConfigConstants.ROBUST_OUT_ONLY))
            {
                allowed = false;
            }
        }

        return allowed;
    }

   /**
     * Checks the madatory information.
     *
     * @param map map object.
     * @param attr attribute to be checked.
     */
    private void checkRequired(
        NamedNodeMap map,
        String attr)
    {
        Node attrnode = map.getNamedItem(attr);
        String curvalue;
        if ((attrnode == null))
        {            
            setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR, attr));                 
            return;
        }

        try
        {
            curvalue = attrnode.getNodeValue().trim();

            if (curvalue.equals(""))
            {
                setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR, attr));
            }
        }
        catch (Exception e)
        {
            setError(mStringTranslator.getString(JMS_REQUIRED_ATTR_ERROR, attr));
            setException(e);
        }
    }

    /**
     * DOCUMENT ME!
     *
     * @param args
     */
    /*
    public static void main(String [] args)
    {
        WSDL11FileReader reader = new WSDL11FileReader();
        reader.init("endpoints.wsdl");
        reader.dump();

        System.out.println("reader.getError() = " + reader.getError());
    }
    */
}
