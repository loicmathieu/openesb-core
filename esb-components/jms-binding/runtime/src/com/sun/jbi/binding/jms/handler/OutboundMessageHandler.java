/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)OutboundMessageHandler.java
 * Copyright 2004-2007 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.jms.handler;

import com.sun.jbi.StringTranslator;
import com.sun.jbi.wsdl11wrapper.Wsdl11WrapperHelper;
import com.sun.jbi.binding.jms.EndpointBean;
import com.sun.jbi.binding.jms.EndpointStatus;
import com.sun.jbi.binding.jms.JMSBindingContext;

import com.sun.jbi.binding.jms.mq.MQDestination;
import com.sun.jbi.binding.jms.mq.MQManager;
import com.sun.jbi.binding.jms.mq.MQSession;

import com.sun.jbi.binding.jms.config.ConfigConstants;
import java.util.logging.Logger;

import javax.jbi.JBIException;

import javax.jbi.messaging.MessageExchange;

import javax.jms.Destination;
import javax.jms.Message;

import javax.xml.namespace.QName;

/**
 * This class handles only In MEPS
 */
public class OutboundMessageHandler
    extends MessageHandlerImpl
{
    /**
     * Creates a new OutboundMessageHandler object.
     */
    public OutboundMessageHandler()
    {
        super();
    }

    /**
     * Checks the message.
     */
    public void check()
    {
        Message msg = getJMSMessage();
        MessageExchange exch = getNMSMessage();
        QName operation = null;

        EndpointBean eb = null;

        if (exch != null)
        {
            operation = exch.getOperation();

            try
            {
                eb = (EndpointBean) (
                        JMSBindingContext.getInstance().getRegistry()
                                         .getEndpoint(exch.getEndpoint())
                    );
            }
            catch (javax.jbi.JBIException je)
            {
                ;
            }

            if ((eb == null) || (!eb.getStatus().equals(EndpointStatus.STARTED)))
            {
                setError(mStringTranslator.getString(JMS_NO_ENDPOINT,
                        exch.getExchangeId(), exch.getEndpoint().getServiceName().toString(),
                        exch.getEndpoint().getEndpointName()));

                return;
            }

            String mep = eb.getMEP(operation.getLocalPart());

            if (mep == null)
            {
                setError(mStringTranslator.getString(
                        JMS_HANDLER_INVALID_OPERATION, exch.getOperation(),
                        exch.getExchangeId(), exch.getEndpoint().getServiceName().toString(),
                        exch.getEndpoint().getEndpointName()));

                return;
            }
        }
        else
        {
            mLogger.severe(mStringTranslator.getString(JMS_IRRECOVERABLE_ERROR));
            setError(mStringTranslator.getString(JMS_IRRECOVERABLE_ERROR));

            return;
        }

        setBean(eb);
        setCurrentOperation(operation);
    }

    /**
     * Exceute the handler.
     */
    public void execute()
    {
        super.execute();

        /* do sanity first
         */
        check();

        if (!isValid())
        {
            mLogger.severe("**INVALID MESSAGE**");
            sendNMSStatus();

            return;
        }

        if (getJMSMessage() != null)
        {
            processJMSMessage();

            if (!isValid())
            {
                mLogger.severe(mStringTranslator.getString(JMS_MESSAGE_FAILURE));
                sendNMSStatus();

                return;
            }
        }
        else if (getNMSMessage() != null)
        {
            processNMSMessage();

            if (!isValid())
            {
                mLogger.severe(mStringTranslator.getString(JMS_MESSAGE_FAILURE));
                sendNMSStatus();

                return;
            }
        }
        else
        {
            mLogger.severe(mStringTranslator.getString(JMS_MESSAGE_FAILURE));
            return;
        }
    }

    /**
     * Process JMS message.
     */
    private void processJMSMessage()
    {
        Message msg = getJMSMessage();
        EndpointBean eb = getBean();
        MessageAdaptor adaptor = MessageAdaptorFactory.getAdaptor(msg, 
                                            eb.getDeploymentType());

        if (adaptor == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));
            setError(
                mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));

            return;
        }

        Wsdl11WrapperHelper wrapperhelper;
        if (eb.getDeploymentType().trim().equals("WSDL11"))
        {            
            wrapperhelper = new Wsdl11WrapperHelper(eb.getWsdlDefinition());
            adaptor.setEpilogueProcessor(wrapperhelper);
        }


        if (eb == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_NO_ENDPOINT));
            setError(mStringTranslator.getString(JMS_NO_ENDPOINT));

            return;
        }

        String inputtype = eb.getOutputType(getCurrentOperation().getLocalPart());

        if (inputtype == null)
        {
            mLogger.severe(
               mStringTranslator.getString(JMS_INCONSISTENT_MESSAGE_TYPE));
            setError(
                mStringTranslator.getString(JMS_INCONSISTENT_MESSAGE_TYPE));

            return;
        }

        if (!adaptor.getName().equals(inputtype.trim()))
        {
            mLogger.severe(
                mStringTranslator.getString(JMS_INCONSISTENT_MESSAGE_TYPE));
            setError(
                mStringTranslator.getString(JMS_INCONSISTENT_MESSAGE_TYPE));

            //send errorr
            return;
        }

        setNMSHeaders();

        MessageExchange ex = getNMSMessage();

        if (ex == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_JMS_NMR_CONVERSION_FAILED, "NULL"));
            setError(mStringTranslator.getString(JMS_JMS_NMR_CONVERSION_FAILED, "NULL"));

            //free up any resources here. set them to null
            return;
        }

        adaptor.convertJMStoNMSMessage(getJMSMessage(), ex);

        if (ex == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_JMS_NMR_CONVERSION_FAILED, 
                adaptor.getError()));
            setError(mStringTranslator.getString(JMS_JMS_NMR_CONVERSION_FAILED, 
                adaptor.getError()));

            //free up any resources here. set them to null
            return;
        }

        if (!send(ex))
        {
            mLogger.severe("**SEND FAILED**");
            setError("**SEND FAILED**" + "\n" + getError());

            return;
        }

        Object replyto =
            MessageExchangeHelper.getDestinationName(adaptor.getJMSReplyTo(), eb);
        mLogger.fine("**REPLY DESTINATION " + replyto);

        registerMessage(ex.getExchangeId(), replyto);

        return;
    }

    /**
     * Process the NMR message.
     */
    private void processNMSMessage()
    {
        MessageExchange exch = getNMSMessage();

        if (canTerminateProvider())
        {
            return;
        }

        String oper = getCurrentOperation().getLocalPart();
        mLogger.fine("**OPERATION** " + oper);

        EndpointBean eb = getBean();
        mLogger.fine("**OUTPUT-TYPE**" + eb.getInputType(oper));

        MessageAdaptor adaptor =
            MessageAdaptorFactory.getAdaptor(eb.getInputType(oper), 
                                                eb.getDeploymentType());
        MQSession session = getSession();

        if (session == null)
        {
            setError(mStringTranslator.getString(JMS_CANNOT_GET_SESSION));
            mLogger.severe(mStringTranslator.getString(JMS_CANNOT_GET_SESSION));

            return;
        }

        Message msg = session.createJMSMessage(eb.getInputType(oper));

        if (adaptor == null)
        {
            setError(
                mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));
            mLogger.severe(
                mStringTranslator.getString(JMS_UNSUPPORTED_MESSAGE));

            return;
        }

        Wsdl11WrapperHelper wrapperhelper;
        if (eb.getDeploymentType().trim().equals("WSDL11"))
        {            
            wrapperhelper = new Wsdl11WrapperHelper(eb.getWsdlDefinition());
            adaptor.setEpilogueProcessor(wrapperhelper);
        }
        MQManager man = JMSBindingContext.getInstance().getMQManager();

        MQDestination dest = man.getTemporaryQueue();

        if (dest == null)
        {
            setError(mStringTranslator.getString(JMS_CANNOT_GET_TEMP_DEST));
            mLogger.severe(mStringTranslator.getString(JMS_CANNOT_GET_TEMP_DEST));

            return;
        }

        adaptor.setJMSReplyTo(dest.getDestination());

        adaptor.convertNMStoJMSMessage(exch, msg);

        if (!adaptor.isValid())
        {
            setError(adaptor.getError());

            return;
        }

        String id =
            (String) exch.getProperty(MessageProperties.JMS_CORRELATION_ID);

        if (id == null)
        {
            id = exch.getExchangeId();
        }

        String destname =
            (String) eb.getValue(ConfigConstants.DESTINATION_NAME);
        mLogger.fine("**SENDING MESSAGE TO " + destname);

        // register upfront to avoid race conditions
        // we can deregister if not appllicable
        registerMessage(id, exch);

        if (!send(msg, destname, session))
        {
            mLogger.severe(mStringTranslator.getString(JMS_CANNOT_SEND));
            setError(mStringTranslator.getString(JMS_CANNOT_SEND));
            man.releaseSession(session);
            deRegisterMessage(id);

            return;
        }

        //send the message with reply to set to temporary queue        
        clear();

        if ((exch.getPattern().toString().trim().equals(ConfigConstants.IN_ONLY)))
        {
            // life of an outonly ends here
            mLogger.fine(mStringTranslator.getString(JMS_SEND_STATUS_NMR));

            // dont register response not expected
            deRegisterMessage(id);
            sendNMSStatus();
        }

        man.releaseSession(session);
    }

    /**
     * Sends the JMS error.
     */
    private void sendJMSError()
    {
        EndpointBean eb = getBean();

        String mep = eb.getMEP(getCurrentOperation().getLocalPart());

        if ((mep == null) || (mep.trim().equals(ConfigConstants.IN_ONLY)))
        {
            return;
        }

        MessageAdaptor adaptor =
            MessageAdaptorFactory.getAdaptor(eb.getOutputType(getCurrentOperation().
                                                                  getLocalPart()), "");

        if (adaptor == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_IRRECOVERABLE_ERROR));

            return;
        }

        MQSession session = getSession();

        if (session == null)
        {
            mLogger.severe(mStringTranslator.getString(JMS_IRRECOVERABLE_ERROR));

            return;
        }

        Message msg =
            session.createJMSMessage(eb.getOutputType(getCurrentOperation().
                                                          getLocalPart()));
        adaptor.updateJMSErrorMessage(msg, getError());

        MessageExchange exch = getNMSMessage();
        Object replyto =
            MessageExchangeHelper.getDestinationName(adaptor.getJMSReplyTo(), eb);

        if (!send(msg, replyto, session))
        {
            ;
        }
    }
}
