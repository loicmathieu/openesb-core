/*
 * BEGIN_HEADER - DO NOT EDIT
 *
 * The contents of this file are subject to the terms
 * of the Common Development and Distribution License
 * (the "License").  You may not use this file except
 * in compliance with the License.
 *
 * You can obtain a copy of the license at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * When distributing Covered Code, include this CDDL
 * HEADER in each file and include the License file at
 * https://open-esb.dev.java.net/public/CDDLv1.0.html.
 * If applicable add the following below this CDDL HEADER,
 * with the fields enclosed by brackets "[]" replaced with
 * your own identifying information: Portions Copyright
 * [year] [name of copyright owner]
 */

/*
 * @(#)ConnectionsInfo.java
 * Copyright 2004-2009 Sun Microsystems, Inc. All Rights Reserved.
 *
 * END_HEADER - DO NOT EDIT
 */
package com.sun.jbi.binding.proxy;

import com.sun.jbi.binding.proxy.connection.Event;
import com.sun.jbi.binding.proxy.connection.EventInfo;

import javax.jbi.servicedesc.ServiceEndpoint;

import javax.xml.namespace.QName;


/**
 * Represents a service connection registration in the distributed registry.
 * @author Sun Microsystems, Inc
 */
public class ConnectionsInfo
        extends EventInfo
        implements java.io.Serializable
{
    private QName                       mServiceLinkName;
    private String                      mEndpointLinkName;
    private QName                       mServiceName;
    private String                      mEndpointName;
    private String                      mLink;
    private String                      mInstanceId;
    private transient String            mAction;
    
    public static final String          EVENTNAME = "ServiceConnection";    
    public static final String          ACTION_ADD  = "Add";
    public static final String          ACTION_REMOVE = "Remove";
        
    /**
     * Constructor.
     */
    public ConnectionsInfo(QName serviceLink, String endpointLink,
        QName service, String endpoint, String link, String action, String instanceId)
    {
        mServiceLinkName = serviceLink;
        mEndpointLinkName = endpointLink;
        mServiceName = service;
        mEndpointName = endpoint;
        mLink = link;
        mAction = action;
        mInstanceId = instanceId;
    }
    
    public String getEventName()
    {
        return (EVENTNAME);
    }
    
    /**
     * Accessor for ServiceName.
     * @return QName containing the ServiceName.
     */
    public QName getServiceName()
    {
        return (mServiceName);
    }
    
    /**
     * Accessor for EndpointName.
     * @return String containing the EndpointName.
     */
    public String getEndpointName()
    {
        return (mEndpointName);
    }
    
    /**
     * Accessor for ServiceLinkName.
     * @return QName containing the ServiceLinkName.
     */
    public QName getServiceLinkName()
    {
        return (mServiceLinkName);
    }
    
    /**
     * Accessor for EndpointLinkName.
     * @return String containing the EndpointLinkName.
     */
    public String getEndpointLinkName()
    {
        return (mEndpointLinkName);
    }

    /**
     * Accessor for EndpointLinkName.
     * @return String containing the EndpointLinkName.
     */
    public String getInstanceId()
    {
        return (mInstanceId);
    }
    
    /**
     * Accessor for Link.
     * @return String containing the EndpointLinkName.
     */
    public String getLink()
    {
        return (mLink);
    }
    
    /**
     * Accessor for Action.
     * @return String containing the Action.
     */
    public String getAction()
    {
        return (mAction);
    }
    
    public ConnectionsInfo(Event event)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        super(event);
        
        String          namespace;
        String          local;
        
        mInstanceId = event.getSender();
        mAction = event.getString();
        mLink = event.getString();
        namespace = event.getString();
        local = event.getString();
        mServiceLinkName = new QName(namespace, local);
        mEndpointLinkName = event.getString();
        namespace = event.getString();
        local = event.getString();
        mServiceName = new QName(namespace, local);
        mEndpointName = event.getString();
    }
    
    public void encodeEvent(Event event)
        throws com.sun.jbi.binding.proxy.connection.EventException
    {
        event.putString(mAction);
        event.putString(mLink);
        event.putString(mServiceLinkName.getNamespaceURI());
        event.putString(mServiceLinkName.getLocalPart());
        event.putString(mEndpointLinkName);
        event.putString(mServiceName.getNamespaceURI());
        event.putString(mServiceName.getLocalPart());
        event.putString(mEndpointName);
    }
    
    public int hashCode()
    {
        int     hash = 0;
        String  value;
        
        if (mServiceName != null)
        {
            if ((value = mServiceName.getNamespaceURI()) != null)
            {
                hash ^= hash + value.hashCode();
            }
            if ((value = mServiceName.getLocalPart()) != null)
            {
                hash ^= hash + value.hashCode();
            }
        }
        if (mEndpointName != null)
        {
            hash ^= hash + mEndpointName.hashCode();
        }
        return (hash);
    }
    
    public boolean equals(Object equalTo)
    {
        ConnectionsInfo        ci;
        
        if (equalTo instanceof ConnectionsInfo)
        {
            ci = (ConnectionsInfo)equalTo;
            if (mServiceLinkName == null)
            {
                if (ci.getServiceLinkName() != null)
                {
                    return (false);
                }
            }
            else
            {
                if (!mServiceLinkName.getNamespaceURI().equals(ci.getServiceLinkName().getNamespaceURI()) ||
                    !mServiceLinkName.getLocalPart().equals(ci.getServiceLinkName().getLocalPart()))
                {
                    return (false);
                }
            }
            if (mEndpointLinkName == null)
            {
                if (ci.getEndpointLinkName() != null)
                {
                    return (false);
                }
            }
            else
            {
                if (!mEndpointLinkName.equals(ci.getEndpointLinkName()))
                {
                    return (false);
                }
            }
                

            return (true);
        }
        return (false);
    }
    
    public String toString()
    {
        StringBuffer        sb = new StringBuffer();
        
        sb.append("        InstanceId (");
        sb.append(mInstanceId);
        sb.append(")\n          ServiceLink  (");
        sb.append(mServiceLinkName.toString());
        sb.append(")\n          EndpointLink (");
        sb.append(mEndpointLinkName);
        sb.append(")\n            Service  (");
        sb.append(mServiceName.toString());
        sb.append(")\n            Endpoint (");
        sb.append(mEndpointName);
        sb.append(")\n");
        return (sb.toString());
    }
}
