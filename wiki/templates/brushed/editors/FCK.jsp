<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki"%>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%@ page import="com.ecyrd.jspwiki.ui.*" %>
<%@ page import="com.ecyrd.jspwiki.tags.*" %>
<%@ page import="org.apache.commons.lang.*" %>

<%--
    This provides the FCK editor for JSPWiki.
--%>
<%  WikiContext context = WikiContext.findContext( pageContext );
    String usertext = EditorManager.getEditedText(pageContext);
    TemplateManager.addResourceRequest( context, "script", "scripts/fckeditor/fckeditor.js" );
 %>   

<wiki:CheckRequestContext context="edit"><%
    if( usertext == null )
    {
        usertext = context.getEngine().getText( context, context.getPage() );
    }%>
</wiki:CheckRequestContext>
<% if( usertext == null ) usertext = "";
   String pageAsHtml = StringEscapeUtils.escapeJavaScript( context.getEngine().textToHTML( context, usertext ) );
%>

<form action="<wiki:CheckRequestContext context='edit'><wiki:EditLink
      format='url'/></wiki:CheckRequestContext><wiki:CheckRequestContext context='comment'><wiki:CommentLink
      format='url'/></wiki:CheckRequestContext>" 
      method="post" accept-charset="<wiki:ContentEncoding/>" 
    onsubmit="return WikiForm.submitOnce( this );"
        name="editForm" enctype="application/x-www-form-urlencoded" >
    <p>
        <%-- Edit.jsp relies on these being found.  So be careful, if you make changes. --%>
        <input type="hidden" name="page" value="<wiki:Variable var="pagename"/>" />
        <input type="hidden" name="action" value="save" />
        <input type="hidden" name="edittime" value="<%=pageContext.getAttribute("lastchange",
                                                       PageContext.REQUEST_SCOPE )%>" />
    </p>
<div>
<script type="text/javascript">
   var oFCKeditor = new FCKeditor( 'htmlPageText' );
   oFCKeditor.BasePath = 'scripts/fckeditor/';
   oFCKeditor.Value = '<%=pageAsHtml%>';
   oFCKeditor.Width  = '100%';
   oFCKeditor.Height = '500';
   oFCKeditor.ToolbarSet = 'JSPWiki';
   oFCKeditor.Config['CustomConfigurationsPath'] = '<wiki:Link format="url" jsp="scripts/fckconfig.js"/>';
   oFCKeditor.Create();
</script>
<noscript>
  <br>
  <div class="error">You need to enable Javascript in your browser to use the WYSIWYG editor</div>
</noscript>

   <wiki:CheckRequestContext context="comment">
   </p>
        <table border="0" class="small">
          <tr>
            <td><label for="authorname" accesskey="n">Your <u>n</u>ame</label></td>
            <td><input type="text" name="author" id="authorname" value="<wiki:UserName/>" /></td>
            <td><label for="rememberme">Remember me?</label>
            <input type="checkbox" name="remember" id="rememberme" /></td>
          </tr>
          <tr>
            <td><label for="link" accesskey="m">Homepage or e<u>m</u>ail</label></td>
            <td colspan="2"><input type="text" name="link" id="link" size="40" value="<%=pageContext.getAttribute("link",PageContext.REQUEST_SCOPE)%>" /></td>
          </tr>
        </table>
    </p>
    </wiki:CheckRequestContext>

    <p>
      <input type="submit" value="Save" name="ok" style="display:none;"/>
      <input type="button" value="Save" name="proxy1" onclick="this.form.ok.click();" 
        accesskey="s"\
            title="Save your changes to the current page [ s ]" />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="submit" value="Preview" name="preview" style="display:none;"/>
      <input type="button" value="Preview" name="proxy2" onclick="this.form.preview.click();" 
        accesskey="v"
            title="Preview [ v ]" />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="submit" value="Cancel" name="cancel" style="display:none;"/>
      <input type="button" value="Cancel" name="proxy3" onclick="this.form.cancel.click();" 
        accesskey="q" 
            title="Cancel editing. Your comment will be lost. [ q ]" />
    </p>
</div>
</form>
