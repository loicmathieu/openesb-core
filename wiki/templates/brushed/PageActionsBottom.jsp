<%@ taglib uri="/WEB-INF/jspwiki.tld" prefix="wiki" %>
<%@ page import="com.ecyrd.jspwiki.*" %>
<%!
  /* replace wiki:PageDate with timezone support */
  private String wiki_PageDate( WikiPage page, String format, String timezone )
  {
    if( page == null ) return "" ;
    java.util.Date date = page.getLastModified();  
    if( date == null ) return "&lt;never&gt;" ;
    java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat( format );
    java.util.TimeZone tz = java.util.TimeZone.getDefault();
    try 
    {
      tz.setRawOffset( Integer.parseInt( timezone ) );
    }
    catch( Exception e) { /* dont care */ } ;
    fmt.setTimeZone( tz );     
    return fmt.format( date ) ;
  }  
%>
<%
  /* see commonheader.jsp */
  String prefDateFormat = (String) session.getAttribute("prefDateFormat");
  String prefTimeZone   = (String) session.getAttribute("prefTimeZone");
%>
<%
  /* should become a wiki:FrontPage jsp tag */
  String homePage = "Main";
  WikiContext wikiContext = WikiContext.findContext(pageContext);
  try 
  { 
    homePage = wikiContext.getEngine().getFrontPage(); 
  } 
  catch( Exception  e )  { /* dont care */ } ;
%>
<%-- similar to PageActionsTop, except for
     ** no accesskey definitions
     ** additional quick2Top, actionPageInfo  
     ** no Login/Logout, quick2Bottom 
  --%>
<div id='actionsBottom' class="pageactions"> 

  <span class='quicklinks quick2Top'>
    <a href="#wikibody" title='Go to Top' >&laquo;</a>
  </span>

  <span class="actionHome">
    <a href="<wiki:LinkTo page='<%= homePage %>' format='url' />"
       title="Go to home page <%= homePage %> ">Home</a>
  </span>
  
  <wiki:CheckRequestContext context='view|info|diff|upload'>
    <wiki:Permission permission="edit">
      <span class="actionEdit">
        <wiki:PageType type="page">
<%--        <wiki:Link title="Edit current page [ e ]"
                     context="edit"
               accessKey="e">Edit</wiki:Link>
--%>                   
          <a href="<wiki:EditLink format='url' />" 
            title="Edit current page [ e ]">Edit</a>
        </wiki:PageType>
        <wiki:PageType type="attachment">
          <a href="<wiki:BaseURL/>Edit.jsp?page=<wiki:ParentPageName />" 
            title="Edit parent page [ e ]">Edit parent</a>
        </wiki:PageType>
      </span>
    </wiki:Permission>

    <wiki:Permission permission="comment">
      <span class="actionComment">
        <wiki:PageType type="page">
          <a href="<wiki:CommentLink format='url' />" 
            title="Add a new comment to the current page" >Comment?</a>
        </wiki:PageType>
        <wiki:PageType type="attachment">
          <a href="<wiki:BaseURL/>Comment.jsp?page=<wiki:ParentPageName />" 
            title="Add a new comment to the parent page" >Comment?</a>
        </wiki:PageType>
      </span>
    </wiki:Permission>
  </wiki:CheckRequestContext>
  
  <span class="actionIndex">
     <a href="<wiki:LinkTo page='PageIndex' format='url' />"
       title="Alphabetically sorted list of pages [ x ]">Index</a>
  </span>
  
  <span class="actionRecentChanges">
     <a href="<wiki:LinkTo page='RecentChanges' format='url' />"
        title="Pages sorted by modification date [ u ]">Changes</a>
  </span>

  <wiki:CheckRequestContext context='!prefs'>
    <wiki:CheckRequestContext context='!preview'>
      <span class="actionPrefs">
        <a href="<wiki:LinkTo page='UserPreferences' format='url' />"
           title="Set User Preferences" >Prefs</a>
      </span>
    </wiki:CheckRequestContext>
  </wiki:CheckRequestContext>

  <wiki:CheckRequestContext context='view|diff|edit|upload|info'>
    <div class="pageinfo">
      <wiki:CheckVersion mode="latest">
        This page (revision-<wiki:PageVersion />) was last changed on 
        <a href="<wiki:DiffLink format='url' version='latest' newVersion='previous' />"
          title="Show changes of last page update" >  
        <%--<wiki:PageDate format='<%= prefDateFormat %>'/>--%>
        <%= wiki_PageDate( wikiContext.getPage(), prefDateFormat, prefTimeZone ) %></a>
      </wiki:CheckVersion>
      <wiki:CheckVersion mode="notlatest">
        This particular version was published on 
        <%--<wiki:PageDate format='<%= prefDateFormat %>'/> --%>
        <%= wiki_PageDate( wikiContext.getPage(), prefDateFormat, prefTimeZone ) %>
      </wiki:CheckVersion>
      by <wiki:Author />

      <wiki:NoSuchPage>Page not created yet.</wiki:NoSuchPage>
    </div>  
  </wiki:CheckRequestContext>

  <div style="clear:both; height:0;"> </div> 
</div>
